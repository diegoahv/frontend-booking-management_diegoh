import React from 'react';
import PropTypes from 'prop-types';
import MenuPrimeOption from '../../PrimeBox';
import { useTranslation } from 'react-i18next';
import { MenuEntry } from './MenuEntry';
import { RightArrowIcon } from 'prisma-design-system';

export const PrimeMenuOption = ({ type, action, isEnabled }) => {
  const { t } = useTranslation();
  return (
    <MenuEntry type={type} isEnabled={isEnabled}>
      <MenuPrimeOption
        title={t('managebooking:menu.prime.title')}
        description={t('managebooking:menu.prime.description')}
        buttonText={t('managebooking:menu.prime.button')}
        buttonIcon={<RightArrowIcon size="small" color="white" />}
        buttonClickHandler={action}
      />
    </MenuEntry>
  );
};

PrimeMenuOption.propTypes = {
  type: PropTypes.string.isRequired,
  action: PropTypes.func.isRequired,
  isEnabled: PropTypes.bool,
};
