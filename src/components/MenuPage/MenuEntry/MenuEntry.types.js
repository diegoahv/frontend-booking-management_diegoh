export const MAIN_MENU_ENTRY_TYPES = {
  SCHEDULE: 'schedule',
  INVOLUNTARY: 'involuntary',
  MODIFY: 'modify',
  REFUNDS: 'refunds',
  DOCUMENTATION: 'documentation',
  INVOICE: 'invoice',
  PASSENGERS: 'passengers',
  BAGS: 'bags',
  SEATS: 'seats',
  CHECKIN: 'checkin',
  ADDSERVICES: 'addservices',
  PRIME: 'prime',
};

export const DOCUMENTATION_ENTRY_TYPES = {
  CONFIRMATION: 'confirmation',
  ETICKETS: 'etickets',
};

export const ADDITIONAL_MENU_ENTRY_TYPES = {
  HOTEL: 'hotel',
  CAR: 'car',
  TRANSFER: 'transfer',
  ACTIVITIES: 'activities',
};
