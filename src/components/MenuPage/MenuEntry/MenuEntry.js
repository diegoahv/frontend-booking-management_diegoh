import React from 'react';
import PropTypes from 'prop-types';
import { Box } from 'prisma-design-system';
import { getTracking } from '../MenuPage/MenuPage.tracking';
import styled from '@emotion/styled';
import css from '@styled-system/css';
import {
  ApplicationContext,
  Tracking,
} from '@frontend-react-component-builder/conchita';

const MenuEntryWrapper = styled(Box)((props) =>
  css({
    mb: props.isMobile ? '2' : '4',
  })
);

const EnabledMenuEntry = ({ type, children }) => {
  const [{ isMobile = false }] = ApplicationContext.useFlow();
  const { label, action } = getTracking(type);
  const [session] = ApplicationContext.useSession();
  const { flowProduct } = session;
  return (
    <MenuEntryWrapper isMobile={isMobile} className={`menu-entry-${type}`}>
      <Tracking.TrackableClick label={label(flowProduct)} action={action}>
        {children}
      </Tracking.TrackableClick>
    </MenuEntryWrapper>
  );
};

export const MenuEntry = ({ type, isEnabled, children }) => {
  return isEnabled ? (
    <EnabledMenuEntry type={type}>{children}</EnabledMenuEntry>
  ) : (
    <React.Fragment />
  );
};

MenuEntry.propTypes = {
  type: PropTypes.string.isRequired,
  isEnabled: PropTypes.bool,
  children: PropTypes.node,
};
