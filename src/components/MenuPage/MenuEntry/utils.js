import i18next from 'i18next';
import { MAIN_MENU_ENTRY_TYPES } from './MenuEntry.types';

export const composeUrl = ({ action, token }) => {
  const mmbUrl = i18next.t('managebooking:old.button.url');
  const entryUrl = `${mmbUrl}?mb=${token}&sc=${action}`;
  return () => window.open(entryUrl);
};

export const isMainMenuOption = (type) =>
  Object.values(MAIN_MENU_ENTRY_TYPES).includes(type);
