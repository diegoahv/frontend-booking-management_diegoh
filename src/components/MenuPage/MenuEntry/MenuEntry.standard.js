import React from 'react';
import PropTypes from 'prop-types';
import MenuOption from '../../MenuOption/MenuOption';
import { useTranslation } from 'react-i18next';
import { MenuEntry } from './MenuEntry';

export const StandardMenuOption = ({ type, action, Icon, isEnabled }) => {
  const { t } = useTranslation();
  return (
    <MenuEntry type={type} isEnabled={isEnabled}>
      <MenuOption
        clickHandler={action}
        title={t(`managebooking:menu.${type}.title`)}
        description={t(`managebooking:menu.${type}.description`)}
        Icon={Icon}
        type={type}
      />
    </MenuEntry>
  );
};

StandardMenuOption.propTypes = {
  type: PropTypes.string.isRequired,
  action: PropTypes.func.isRequired,
  Icon: PropTypes.func,
  isEnabled: PropTypes.bool,
};
