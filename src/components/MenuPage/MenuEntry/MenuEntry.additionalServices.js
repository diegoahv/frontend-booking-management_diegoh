import React from 'react';
import PropTypes from 'prop-types';
import MenuOption from '../../MenuOption/MenuOption';
import { useTranslation } from 'react-i18next';
import { MenuEntry } from './MenuEntry';
import { ADDITIONAL_MENU_ENTRY_TYPES } from '../MenuEntry/MenuEntry.types';
import { useBookingInfo } from '@/src/utils/hooks';

const destinationContentPlaceholder = '[Destination]';
const replaceDestination = (str, destination) =>
  (str + '').replace(destinationContentPlaceholder, destination);

export const AdditionalServicesActivitiesMenuOption = ({ action }) => {
  const type = ADDITIONAL_MENU_ENTRY_TYPES.ACTIVITIES;
  const [{ destinationCity = '' }] = useBookingInfo();
  const { t } = useTranslation();
  const menuTitle = replaceDestination(
    t(`managebooking:menu.${type}.title`),
    destinationCity
  );
  const menuDescription = replaceDestination(
    t(`managebooking:menu.${type}.description`),
    destinationCity
  );
  return (
    <MenuEntry type={type} isEnabled={true}>
      <MenuOption
        clickHandler={action}
        type={type}
        title={menuTitle}
        description={menuDescription}
      />
    </MenuEntry>
  );
};

AdditionalServicesActivitiesMenuOption.propTypes = {
  action: PropTypes.func.isRequired,
};
