import React, { Suspense, lazy } from 'react';
import PropTypes from 'prop-types';
import { MemoryRouter, Switch, Route, Redirect } from 'react-router-dom';
import MainMenuPage from './MainMenuPage';

const AdditionalServicesMenuPage = lazy(() =>
  import('../InnerMenuPages/AdditionalServices/AdditionalServicesMenuPage')
);

const DocumentationMenuPage = lazy(() =>
  import('../InnerMenuPages/Documentation/DocumentationMenuPage')
);

const MainMenuPageRouter = ({
  bookingId = '',
  flags = {},
  addUrls = {},
  token,
}) => {
  return (
    <MemoryRouter>
      <Suspense fallback="">
        <Switch>
          <Route
            exact
            path="/"
            render={() => (
              <MainMenuPage
                bookingId={bookingId}
                flags={flags}
                addUrls={addUrls}
                token={token}
              />
            )}
          />
          <Route
            exact
            path="/additional-services"
            render={() => <AdditionalServicesMenuPage bookingId={bookingId} />}
          />
          <Route
            exact
            path="/booking-documentation"
            render={() => (
              <DocumentationMenuPage
                bookingId={bookingId}
                flags={flags}
                token={token}
              />
            )}
          />
          <Redirect to="/" />
        </Switch>
      </Suspense>
    </MemoryRouter>
  );
};

MainMenuPageRouter.propTypes = {
  bookingId: PropTypes.string.isRequired,
  flags: PropTypes.object.isRequired,
  addUrls: PropTypes.object.isRequired,
  token: PropTypes.string.isRequired,
};

export default MainMenuPageRouter;
