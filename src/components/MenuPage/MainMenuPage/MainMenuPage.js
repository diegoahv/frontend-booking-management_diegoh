import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { APPEARANCES_TRACKING } from '../MenuPage/MenuPage.tracking';
import MenuPage from '../MenuPage';
import {
  StandardMenuOption,
  PrimeMenuOption,
  MAIN_MENU_ENTRY_TYPES,
  composeUrl,
  DOCUMENTATION_ENTRY_TYPES,
} from '../MenuEntry';
import * as Icons from '../../../common/icons';
import {
  ApplicationContext,
  Tracking,
} from '@frontend-react-component-builder/conchita';
import { useHistory } from 'react-router-dom';
import { useBrowserHistory } from '@/src/components/MenuPage/utils';
import { MenuHomeFooter } from '../../Footer';

const MainMenuPage = ({ bookingId = '', flags = {}, addUrls = {}, token }) => {
  const { t } = useTranslation();
  const dispatch = ApplicationContext.useDispatch();
  const history = useHistory();
  const [
    { mmbDocumentation = false, mmbAdditionalServices = false },
  ] = ApplicationContext.useAb();
  const [{ isDesktop = false }] = ApplicationContext.useFlow();
  const goToPrime = () =>
    dispatch({ message: 'MMB:MainMenu:goToPostBookingPrime' });
  const goBack = () => dispatch({ message: 'MMB:MainMenu:goBack' });

  const enterSubMenu = (path = '/') => {
    window.scrollTo(0, 0);
    history.push(path);
  };

  const documentationAction = (mmbDocumentation, token) => {
    return isDesktop || mmbDocumentation
      ? () => enterSubMenu('/booking-documentation')
      : composeUrl({ action: 'confemail', token });
  };

  const documentationEnabled = () =>
    isDesktop || mmbDocumentation
      ? flags[MAIN_MENU_ENTRY_TYPES.DOCUMENTATION]
      : flags[DOCUMENTATION_ENTRY_TYPES.CONFIRMATION];

  useBrowserHistory({ path: '/', exit: true });

  return (
    <Tracking.TrackableAppereances
      label={APPEARANCES_TRACKING.LABEL}
      action={APPEARANCES_TRACKING.ACTION}
    >
      <MenuPage
        bookingId={bookingId}
        pageTitle={t('managebooking:home.header')}
        backAction={isDesktop ? undefined : goBack}
        footer={<MenuHomeFooter />}
        menuTitle={t('managebooking:home.title')}
      >
        <StandardMenuOption
          type={MAIN_MENU_ENTRY_TYPES.SCHEDULE}
          action={composeUrl({ action: 'schedulechanges', token })}
          Icon={Icons.ModifyTrip}
          isEnabled={flags[MAIN_MENU_ENTRY_TYPES.SCHEDULE]}
        />
        <StandardMenuOption
          type={MAIN_MENU_ENTRY_TYPES.INVOLUNTARY}
          action={composeUrl({ action: 'involuntarycanx', token })}
          Icon={Icons.ModifyTrip}
          isEnabled={flags[MAIN_MENU_ENTRY_TYPES.INVOLUNTARY]}
        />
        <StandardMenuOption
          type={MAIN_MENU_ENTRY_TYPES.MODIFY}
          action={composeUrl({ action: 'cancellation', token })}
          Icon={Icons.ModifyTrip}
          isEnabled={flags[MAIN_MENU_ENTRY_TYPES.MODIFY]}
        />
        <StandardMenuOption
          type={MAIN_MENU_ENTRY_TYPES.REFUNDS}
          action={composeUrl({ action: 'refunds', token })}
          Icon={Icons.Refunds}
          isEnabled={flags[MAIN_MENU_ENTRY_TYPES.REFUNDS]}
        />
        <StandardMenuOption
          type={MAIN_MENU_ENTRY_TYPES.DOCUMENTATION}
          action={documentationAction(mmbDocumentation, token)}
          Icon={Icons.Document}
          isEnabled={documentationEnabled()}
        />
        <StandardMenuOption
          type={MAIN_MENU_ENTRY_TYPES.INVOICE}
          action={composeUrl({ action: 'invoice', token })}
          Icon={Icons.Billing}
          isEnabled={flags[MAIN_MENU_ENTRY_TYPES.INVOICE]}
        />
        <PrimeMenuOption
          type={MAIN_MENU_ENTRY_TYPES.PRIME}
          action={() => goToPrime()}
          isEnabled={flags[MAIN_MENU_ENTRY_TYPES.PRIME]}
        />
        <StandardMenuOption
          type={MAIN_MENU_ENTRY_TYPES.PASSENGERS}
          action={composeUrl({ action: 'passengers', token })}
          Icon={Icons.PassengerInfo}
          isEnabled={flags[MAIN_MENU_ENTRY_TYPES.PASSENGERS]}
        />
        <StandardMenuOption
          type={MAIN_MENU_ENTRY_TYPES.BAGS}
          action={() => window.open(addUrls.addBagUrl)}
          Icon={Icons.Bags}
          isEnabled={flags[MAIN_MENU_ENTRY_TYPES.BAGS]}
        />
        <StandardMenuOption
          type={MAIN_MENU_ENTRY_TYPES.SEATS}
          action={() => window.open(addUrls.addSeatUrl)}
          Icon={Icons.Seats}
          isEnabled={flags[MAIN_MENU_ENTRY_TYPES.SEATS]}
        />
        <StandardMenuOption
          type={MAIN_MENU_ENTRY_TYPES.CHECKIN}
          action={composeUrl({ action: 'checkin', token })}
          Icon={Icons.CheckIn}
          isEnabled={flags[MAIN_MENU_ENTRY_TYPES.CHECKIN]}
        />
        <StandardMenuOption
          type={MAIN_MENU_ENTRY_TYPES.ADDSERVICES}
          action={() => enterSubMenu('/additional-services')}
          Icon={Icons.AdditionalServices}
          isEnabled={
            flags[MAIN_MENU_ENTRY_TYPES.ADDSERVICES] &&
            (isDesktop || mmbAdditionalServices)
          }
        />
      </MenuPage>
    </Tracking.TrackableAppereances>
  );
};

MainMenuPage.propTypes = {
  bookingId: PropTypes.string.isRequired,
  flags: PropTypes.object.isRequired,
  addUrls: PropTypes.object.isRequired,
  token: PropTypes.string.isRequired,
};

export default MainMenuPage;
