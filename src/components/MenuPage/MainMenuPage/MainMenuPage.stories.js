import React from 'react';
import { Mocks, DebugAssistant } from '@frontend-react-component-builder/debug';
import MainMenuPage from './MainMenuPage';
import MainMenuPageRouter from './MainMenuPageRouter';
import { MAIN_MENU_ENTRY_TYPES, DOCUMENTATION_ENTRY_TYPES } from '../MenuEntry';

const defaultBackAction = () => console.log('Back button');

const mockedAbs = (abs = {}) =>
  Mocks.CustomApplicationContextMock({
    session: Mocks.Session.DefaultSession(),
    ab: abs,
  });

const defaultMock = mockedAbs();
const documentationEnabledMock = mockedAbs({ mmbDocumentation: true });
const addServicesEnabledMock = mockedAbs({ mmbAdditionalServices: true });

const flags = {
  [MAIN_MENU_ENTRY_TYPES.MODIFY]: true,
  [MAIN_MENU_ENTRY_TYPES.REFUNDS]: true,
  [MAIN_MENU_ENTRY_TYPES.DOCUMENTATION]: true,
  [DOCUMENTATION_ENTRY_TYPES.CONFIRMATION]: true,
  [DOCUMENTATION_ENTRY_TYPES.ETICKETS]: true,
  [MAIN_MENU_ENTRY_TYPES.INVOICE]: true,
  [MAIN_MENU_ENTRY_TYPES.PRIME]: true,
  [MAIN_MENU_ENTRY_TYPES.PASSENGERS]: true,
  [MAIN_MENU_ENTRY_TYPES.BAGS]: true,
  [MAIN_MENU_ENTRY_TYPES.SEATS]: true,
  [MAIN_MENU_ENTRY_TYPES.CHECKIN]: true,
  [MAIN_MENU_ENTRY_TYPES.ADDSERVICES]: true,
};

export default {
  title: 'MMB/Main Menu/Main Menu Page',
  component: MainMenuPage,
};

export const AllEntriesWithoutActiveAbs = () => {
  return (
    <DebugAssistant.AssistantApplicationContextMocker
      applicationContext={defaultMock}
    >
      <MainMenuPageRouter
        backAction={defaultBackAction}
        bookingId="bookingid"
        flags={flags}
        addUrls={{}}
        token=""
      />
    </DebugAssistant.AssistantApplicationContextMocker>
  );
};

export const FewEntries = () => (
  <DebugAssistant.AssistantApplicationContextMocker
    applicationContext={defaultMock}
  >
    <MainMenuPageRouter
      backAction={defaultBackAction}
      bookingId="12345678"
      flags={{
        modify: true,
        refunds: true,
        documentation: true,
      }}
      addUrls={{}}
      token=""
    />
  </DebugAssistant.AssistantApplicationContextMocker>
);

export const DocumentationEntryAction = () => (
  <DebugAssistant.AssistantApplicationContextMocker
    applicationContext={documentationEnabledMock}
  >
    <MainMenuPageRouter
      backAction={defaultBackAction}
      bookingId="12345678"
      flags={{
        [MAIN_MENU_ENTRY_TYPES.DOCUMENTATION]: true,
        [DOCUMENTATION_ENTRY_TYPES.CONFIRMATION]: true,
        [DOCUMENTATION_ENTRY_TYPES.ETICKETS]: true,
      }}
      addUrls={{}}
      token=""
    />
  </DebugAssistant.AssistantApplicationContextMocker>
);
export const AdditionalServicesEntryAbEnabled = () => (
  <DebugAssistant.AssistantApplicationContextMocker
    applicationContext={addServicesEnabledMock}
  >
    <MainMenuPageRouter
      backAction={defaultBackAction}
      bookingId="12345678"
      flags={{ [MAIN_MENU_ENTRY_TYPES.ADDSERVICES]: true }}
      addUrls={{}}
      token=""
    />
  </DebugAssistant.AssistantApplicationContextMocker>
);
