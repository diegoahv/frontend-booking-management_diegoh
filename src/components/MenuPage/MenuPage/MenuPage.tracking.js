import {
  MAIN_MENU_ENTRY_TYPES,
  DOCUMENTATION_ENTRY_TYPES,
  ADDITIONAL_MENU_ENTRY_TYPES,
} from '../MenuEntry/MenuEntry.types';

const LABEL_PREFIX = 'manage_button_click_sel:';

const ACTIONS = {
  trip_details: 'trip_details',
  trip_documentation: 'trip_documentation',
  trip_xsell: 'trip_xsell',
};

const getFlow = (flowProduct) => (flowProduct === 'dp' ? 'mybkdp' : 'mybkfl');
const getSubmenuEntryType = (type) => {
  if (type === 'documentation') {
    return '-bokconf';
  } else if (type === 'additional') {
    return '-addextra';
  } else {
    return '';
  }
};

const productLabelDecorator = (label) => (flowProduct) =>
  `${LABEL_PREFIX}${label}:${getFlow(flowProduct)}`;

const subMenuLabelDecorator = (label, type) => (flowProduct) =>
  `${LABEL_PREFIX}${label}:${getFlow(flowProduct)}${getSubmenuEntryType(type)}`;

const tracking = {
  [MAIN_MENU_ENTRY_TYPES.SCHEDULE]: {
    label: productLabelDecorator('change-trip_pag'),
    action: ACTIONS.trip_details,
  },
  [MAIN_MENU_ENTRY_TYPES.INVOLUNTARY]: {
    label: productLabelDecorator('change-trip_pag'),
    action: ACTIONS.trip_details,
  },
  [MAIN_MENU_ENTRY_TYPES.MODIFY]: {
    label: productLabelDecorator('change-trip_pag'),
    action: ACTIONS.trip_details,
  },
  [MAIN_MENU_ENTRY_TYPES.REFUNDS]: {
    label: productLabelDecorator('ask-refund_pag'),
    action: ACTIONS.trip_details,
  },
  [MAIN_MENU_ENTRY_TYPES.DOCUMENTATION]: {
    label: productLabelDecorator('boar-pass_pag'),
    action: ACTIONS.trip_documentation,
  },
  [MAIN_MENU_ENTRY_TYPES.INVOICE]: {
    label: productLabelDecorator('bill-info_pag'),
    action: ACTIONS.trip_details,
  },
  [MAIN_MENU_ENTRY_TYPES.PRIME]: {
    label: productLabelDecorator('know-prime_pag'),
    action: ACTIONS.trip_xsell,
  },
  [MAIN_MENU_ENTRY_TYPES.PASSENGERS]: {
    label: productLabelDecorator('pax-details_pag'),
    action: ACTIONS.trip_details,
  },
  [MAIN_MENU_ENTRY_TYPES.BAGS]: {
    label: productLabelDecorator('add-bags_pag'),
    action: ACTIONS.trip_xsell,
  },
  [MAIN_MENU_ENTRY_TYPES.SEATS]: {
    label: productLabelDecorator('add-seats_pag'),
    action: ACTIONS.trip_xsell,
  },
  [MAIN_MENU_ENTRY_TYPES.CHECKIN]: {
    label: productLabelDecorator('add-checkin_pag'),
    action: ACTIONS.trip_xsell,
  },
  [MAIN_MENU_ENTRY_TYPES.ADDSERVICES]: {
    label: productLabelDecorator('add-extra_pag'),
    action: ACTIONS.trip_xsell,
  },
  [DOCUMENTATION_ENTRY_TYPES.CONFIRMATION]: {
    label: subMenuLabelDecorator('conf-email_pag', 'documentation'),
    action: ACTIONS.trip_documentation,
  },
  [DOCUMENTATION_ENTRY_TYPES.ETICKETS]: {
    label: subMenuLabelDecorator('e-tick_pag', 'documentation'),
    action: ACTIONS.trip_documentation,
  },
  [ADDITIONAL_MENU_ENTRY_TYPES.HOTEL]: {
    label: subMenuLabelDecorator('xsell-hotel_pag', 'additional'),
    action: ACTIONS.trip_xsell,
  },
  [ADDITIONAL_MENU_ENTRY_TYPES.CAR]: {
    label: subMenuLabelDecorator('xsell-car_pag', 'additional'),
    action: ACTIONS.trip_xsell,
  },
  [ADDITIONAL_MENU_ENTRY_TYPES.TRANSFER]: {
    label: subMenuLabelDecorator('xsell-transfer_pag', 'additional'),
    action: ACTIONS.trip_xsell,
  },
  [ADDITIONAL_MENU_ENTRY_TYPES.ACTIVITIES]: {
    label: subMenuLabelDecorator('xsell-activity_pag', 'additional'),
    action: 'ACTIONS.trip_xsell',
  },
};

export const getTracking = (key) => tracking[key];

export const APPEARANCES_TRACKING = {
  LABEL: 'manage_my_booking_onload_page',
  ACTION: 'on_load_events',
};
