import React from 'react';
import PropTypes from 'prop-types';
import Page from '../../Page/Page';
import PageTopContent from '../../Page/PageTopContent';

const MenuPage = ({
  bookingId,
  pageTitle,
  backAction,
  footer,
  menuTitle,
  children,
}) => {
  return (
    <Page
      footer={footer}
      title={pageTitle}
      backAction={backAction}
      pageClass="menu-page-wrapper"
    >
      <PageTopContent bookingId={bookingId} menuTitle={menuTitle} />
      {children}
    </Page>
  );
};

MenuPage.propTypes = {
  bookingId: PropTypes.string.isRequired,
  pageTitle: PropTypes.string.isRequired,
  backAction: PropTypes.func,
  footer: PropTypes.node,
  menuTitle: PropTypes.string.isRequired,
  children: PropTypes.node,
};

export default MenuPage;
