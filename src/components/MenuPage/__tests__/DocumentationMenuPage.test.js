import React from 'react';
import { render, fireEvent } from 'test-utils';
import DocumentationMenuPage from '../InnerMenuPages/Documentation/DocumentationMenuPage';
import { getTracking } from '../MenuPage/MenuPage.tracking';
import { Standalone } from '@frontend-react-component-builder/conchita';
import { Mocks } from '@frontend-react-component-builder/debug';
import { DOCUMENTATION_ENTRY_TYPES } from '../MenuEntry';

const FLOW_PRODUCT = 'FLOW_PRODUCT';

const trackEvent = jest.fn();

const flagsAllTrue = {};
for (let k of Object.keys(DOCUMENTATION_ENTRY_TYPES)) {
  flagsAllTrue[DOCUMENTATION_ENTRY_TYPES[k]] = true;
}

const renderStandaloneDocumentationMenuPage = ({ flags }) => (
  <Standalone applicationContext={getApplicationContextMock()} events={{}}>
    <DocumentationMenuPage bookingId="1" flags={flags} token="" />
  </Standalone>
);

const renderDocumentationMenuPage = ({ flags = flagsAllTrue } = {}) =>
  render(renderStandaloneDocumentationMenuPage({ flags }));

const getApplicationContextMock = () =>
  Mocks.CustomApplicationContextMock({
    tracking: {
      trackEvent,
    },
    session: Mocks.Session.CustomSession({ flowProduct: FLOW_PRODUCT }),
  });

const getAllEntryTypes = () => Object.values(DOCUMENTATION_ENTRY_TYPES);

describe('MainMenuPage tests', () => {
  describe('Page wrapper tests', () => {
    it('Should render page wrapper', () => {
      const { container } = renderDocumentationMenuPage();

      const $pageWrapper = container.querySelector('.menu-page-wrapper');
      expect($pageWrapper).not.toBeNull();
    });
  });

  describe('TrackableClick', () => {
    it('should match action and label', () => {
      getAllEntryTypes().forEach((type) => {
        const tracking = getTracking(type);

        const { container } = renderDocumentationMenuPage();
        const menuItem = container.querySelector(`.menu-entry-${type}`);
        expect(menuItem).not.toBeNull();
        fireEvent.click(menuItem.firstChild);
        expect(trackEvent).toHaveBeenCalledWith(
          tracking.action,
          tracking.label(FLOW_PRODUCT)
        );
      });
    });
  });
});
