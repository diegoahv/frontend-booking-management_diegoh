import React from 'react';
import { render, fireEvent } from 'test-utils';
import MainMenuPage from '../MainMenuPage/MainMenuPage';
import {
  getTracking,
  APPEARANCES_TRACKING,
} from '../MenuPage/MenuPage.tracking';
import { Standalone } from '@frontend-react-component-builder/conchita';
import { Mocks } from '@frontend-react-component-builder/debug';
import { MAIN_MENU_ENTRY_TYPES, DOCUMENTATION_ENTRY_TYPES } from '../MenuEntry';

const FLOW_PRODUCT = 'FLOW_PRODUCT';

const trackEvent = jest.fn();

const flagsAllTrue = {};
for (let k of Object.keys(MAIN_MENU_ENTRY_TYPES)) {
  flagsAllTrue[MAIN_MENU_ENTRY_TYPES[k]] = true;
}
for (let k of Object.keys(DOCUMENTATION_ENTRY_TYPES)) {
  flagsAllTrue[DOCUMENTATION_ENTRY_TYPES[k]] = true;
}

const renderStandaloneMainMenuPage = ({ flags = {}, ab = {}, flow = '' }) => (
  <Standalone
    applicationContext={getApplicationContextMock({ ab, flow })}
    events={{}}
  >
    <MainMenuPage
      backAction={() => {}}
      bookingId="1"
      flags={flags}
      token=""
      addUrls={{}}
    />
  </Standalone>
);

const renderMainMenuPage = ({ flags = {}, ab = {}, flow = '' } = {}) =>
  render(renderStandaloneMainMenuPage({ flags, ab, flow }));

const getApplicationContextMock = ({ ab = {}, flow = '' }) =>
  Mocks.CustomApplicationContextMock({
    tracking: {
      trackEvent,
    },
    session: Mocks.Session.CustomSession({ flowProduct: FLOW_PRODUCT, flow }),
    ab,
  });

const getAllEntryTypesButAddServices = () =>
  Object.values(MAIN_MENU_ENTRY_TYPES).filter(
    (value) => value !== MAIN_MENU_ENTRY_TYPES.ADDSERVICES
  );

describe('MainMenuPage tests', () => {
  describe('Page wrapper tests', () => {
    it('Should render page wrapper', () => {
      const { container } = renderMainMenuPage();
      const $pageWrapper = container.querySelector('.menu-page-wrapper');
      expect($pageWrapper).not.toBeNull();
    });
  });

  describe('TrackableAppereances', () => {
    it('should match action and label', () => {
      renderMainMenuPage();
      expect(trackEvent).toHaveBeenCalledWith(
        APPEARANCES_TRACKING.ACTION,
        APPEARANCES_TRACKING.LABEL
      );
    });
  });

  describe('TrackableClick', () => {
    it('should match action and label', () => {
      getAllEntryTypesButAddServices().forEach((type) => {
        const tracking = getTracking(type);

        const { container } = renderMainMenuPage({
          flags: flagsAllTrue,
        });
        const menuItem = container.querySelector(`.menu-entry-${type}`);
        expect(menuItem).not.toBeNull();
        fireEvent.click(menuItem.firstChild);
        expect(trackEvent).toHaveBeenCalledWith(
          tracking.action,
          tracking.label(FLOW_PRODUCT)
        );
      });
    });
  });

  describe('/schedule', () => {
    it('should appear always', () => {
      const { container } = renderMainMenuPage({
        flags: {
          [MAIN_MENU_ENTRY_TYPES.SCHEDULE]: true,
        },
      });

      const scheduleItem = container.querySelector(
        `.menu-entry-${MAIN_MENU_ENTRY_TYPES.SCHEDULE}`
      );
      expect(scheduleItem).not.toBeNull();
    });
  });

  describe('/additional-services', () => {
    it('should appear when A/B is true and flow is mobile', () => {
      const { container } = renderMainMenuPage({
        flags: {
          [MAIN_MENU_ENTRY_TYPES.ADDSERVICES]: true,
        },
        ab: {
          mmbAdditionalServices: true,
        },
        flow: 'mobile',
      });

      const scheduleItem = container.querySelector(
        `.menu-entry-${MAIN_MENU_ENTRY_TYPES.ADDSERVICES}`
      );
      expect(scheduleItem).not.toBeNull();
    });

    it('should appear when A/B is false and flow is desktop', () => {
      const { container } = renderMainMenuPage({
        flags: {
          [MAIN_MENU_ENTRY_TYPES.ADDSERVICES]: true,
        },
        ab: {
          mmbAdditionalServices: false,
        },
        flow: 'desktop',
      });

      const scheduleItem = container.querySelector(
        `.menu-entry-${MAIN_MENU_ENTRY_TYPES.ADDSERVICES}`
      );
      expect(scheduleItem).not.toBeNull();
    });

    it('should not appear when A/B is false and flow is mobile', () => {
      const { container } = renderMainMenuPage({
        flags: {
          [MAIN_MENU_ENTRY_TYPES.ADDSERVICES]: true,
        },
        ab: {
          mmbAdditionalServices: false,
        },
        flow: 'mobile',
      });

      const scheduleItem = container.querySelector(
        `.menu-entry-${MAIN_MENU_ENTRY_TYPES.ADDSERVICES}`
      );
      expect(scheduleItem).toBeNull();
    });
  });
});
