import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';

const queue = [];

export const useBrowserHistory = ({ path = '/', exit = true } = {}) => {
  const [exiting, setExiting] = useState(false);
  const history = useHistory();

  const [{ isMobile = false }] = ApplicationContext.useFlow();

  const onGoBack = ({ state = {} } = {}) => {
    const { mmbMenu = false, path = '/', exit = true } = state || {};

    if (mmbMenu) {
      history.push(path);
      queue.pop();
    } else if (isMobile && exit && !exiting) {
      setExiting(true);
      window.history.back();
    }
  };

  useEffect(() => {
    const current = window.history.state || {};

    if (!queue.includes(path)) {
      queue.push(path);
    }

    if (path !== current.path) {
      window.history.pushState({ mmbMenu: true, path, exit }, '', '');
    }

    window.addEventListener('popstate', onGoBack, false);

    return () => {
      window.removeEventListener('popstate', onGoBack);
    };
  });
};
