import React from 'react';
import { render, fireEvent } from 'test-utils';
import AdditionalServicesMenuPage from '../AdditionalServicesMenuPage';

import { Standalone } from '@frontend-react-component-builder/conchita';
import { Mocks } from '@frontend-react-component-builder/debug';
import { getBookingInfo } from 'serverMocks';

const mocks = {
  bookingId: 'mockedBookingId',
  appContext: () =>
    Mocks.CustomApplicationContextMock({
      dispatch: mocks.dispatch,
      data: { bookingInfo: getBookingInfo() },
    }),
};

const entries = ['hotel', 'car', 'transfer', 'activities'];
const selectors = {};
const messages = {};
const capitalize = (str) => str[0].toUpperCase() + str.slice(1);
for (let entry of entries) {
  selectors[entry] = `.menu-entry-${entry} [class*=MenuCard]`;
  messages[entry] = `MMB:InnerMenu:AdditionalServices:goTo${capitalize(entry)}`;
}

const MockedStandalone = ({ children }) => (
  <Standalone applicationContext={mocks.appContext()} events={{}}>
    {children}
  </Standalone>
);

const renderPage = () =>
  render(<AdditionalServicesMenuPage bookingId={mocks.bookingId} />, {
    wrapper: MockedStandalone,
  });

describe('AdditionalServicesMenuPage tests', () => {
  beforeEach(() => {
    mocks.dispatch = jest.fn();
  });
  describe('Should render all the additional options', () => {
    for (let entry of entries) {
      it(`Should render entry: ${entry}`, () => {
        const { container } = renderPage();
        const item = container.querySelector(selectors[entry]);
        expect(item).not.toBeNull();
      });
    }
  });
  describe('Each option should dispatch the appropriate message', () => {
    for (let entry of entries) {
      it(`Should dispatch the correct message for ${entry}`, () => {
        const { container } = renderPage();
        const item = container.querySelector(selectors[entry]);
        expect(mocks.dispatch).not.toHaveBeenCalled();
        fireEvent.click(item);
        expect(mocks.dispatch).toBeCalledWith(
          expect.objectContaining({ message: messages[entry] })
        );
      });
    }
  });
});
