import React from 'react';
import PropTypes from 'prop-types';
import MenuPage from '../../MenuPage';
import { useBrowserHistory } from '@/src/components/MenuPage/utils';
import { useTranslation } from 'react-i18next';
import {
  AdditionalServicesActivitiesMenuOption,
  ADDITIONAL_MENU_ENTRY_TYPES,
  StandardMenuOption,
} from '../../MenuEntry';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import { EmptyFooter } from '../../../Footer';

const eventMessage = (key) => ({
  message: `MMB:InnerMenu:AdditionalServices:${key}`,
});

const AdditionalServicesMenuPage = ({ bookingId = '' }) => {
  const { t } = useTranslation();
  const dispatch = ApplicationContext.useDispatch();
  useBrowserHistory({ path: '/additional-services', exit: false });
  return (
    <MenuPage
      bookingId={bookingId}
      pageTitle={t('managebooking:addservices.header')}
      backAction={() => window.history.back()}
      footer={<EmptyFooter />}
      menuTitle={t('managebooking:addservices.title')}
    >
      <StandardMenuOption
        type={ADDITIONAL_MENU_ENTRY_TYPES.HOTEL}
        action={() => dispatch(eventMessage('goToHotel'))}
        isEnabled={true}
      />
      <StandardMenuOption
        type={ADDITIONAL_MENU_ENTRY_TYPES.CAR}
        action={() => dispatch(eventMessage('goToCar'))}
        isEnabled={true}
      />
      <StandardMenuOption
        type={ADDITIONAL_MENU_ENTRY_TYPES.TRANSFER}
        action={() => dispatch(eventMessage('goToTransfer'))}
        isEnabled={true}
      />
      <AdditionalServicesActivitiesMenuOption
        action={() => dispatch(eventMessage('goToActivities'))}
      />
    </MenuPage>
  );
};

AdditionalServicesMenuPage.propTypes = {
  bookingId: PropTypes.string.isRequired,
};

export default AdditionalServicesMenuPage;
