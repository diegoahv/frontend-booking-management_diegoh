import React from 'react';
import AdditionalServicesMenuPage from './AdditionalServicesMenuPage';

export default {
  title: 'MMB/Menu/Additional Services Menu Page',
  component: AdditionalServicesMenuPage,
};

export const Default = () => {
  return <AdditionalServicesMenuPage bookingId="bookingid" />;
};
