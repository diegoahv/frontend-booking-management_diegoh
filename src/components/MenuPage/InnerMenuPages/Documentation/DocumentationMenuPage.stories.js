import React from 'react';
import DocumentationMenuPage from './DocumentationMenuPage';
import { DOCUMENTATION_ENTRY_TYPES } from '../../MenuEntry';

export default {
  title: 'MMB/Menu/Documentation Menu Page',
  component: DocumentationMenuPage,
};

const flags = {
  [DOCUMENTATION_ENTRY_TYPES.CONFIRMATION]: true,
  [DOCUMENTATION_ENTRY_TYPES.ETICKETS]: true,
};

export const Default = () => {
  return (
    <DocumentationMenuPage bookingId="bookingid" flags={flags} token="token" />
  );
};
