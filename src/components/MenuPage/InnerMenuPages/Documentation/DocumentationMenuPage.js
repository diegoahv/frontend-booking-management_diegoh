import React from 'react';
import PropTypes from 'prop-types';
import MenuPage from '../../MenuPage';
import { useBrowserHistory } from '@/src/components/MenuPage/utils';
import { useTranslation } from 'react-i18next';
import {
  DOCUMENTATION_ENTRY_TYPES,
  StandardMenuOption,
  composeUrl,
} from '../../MenuEntry';
import { EmptyFooter } from '../../../Footer';

const DocumentationMenuPage = ({ bookingId = '', flags = {}, token }) => {
  const { t } = useTranslation();
  const documentationTitle = t('managebooking:menu.documentation.title');
  useBrowserHistory({ path: '/booking-documentation', exit: false });
  return (
    <MenuPage
      bookingId={bookingId}
      pageTitle={documentationTitle}
      backAction={() => window.history.back()}
      footer={<EmptyFooter />}
      menuTitle={documentationTitle}
    >
      <StandardMenuOption
        type={DOCUMENTATION_ENTRY_TYPES.CONFIRMATION}
        action={composeUrl({ action: 'confemail', token })}
        isEnabled={flags[DOCUMENTATION_ENTRY_TYPES.CONFIRMATION]}
      />
      <StandardMenuOption
        type={DOCUMENTATION_ENTRY_TYPES.ETICKETS}
        action={composeUrl({ action: 'passengers', token })}
        isEnabled={flags[DOCUMENTATION_ENTRY_TYPES.ETICKETS]}
      />
    </MenuPage>
  );
};

DocumentationMenuPage.propTypes = {
  bookingId: PropTypes.string.isRequired,
  flags: PropTypes.object.isRequired,
};

export default DocumentationMenuPage;
