import React from 'react';
import { render, fireEvent } from 'test-utils';
import PageHeader from '../PageHeader.mobile';

const defaults = {
  title: 'pageHeaderDefaultTitle',
  clickHandler: () => {},
};

const renderPageHeader = (clickHandler = defaults.clickHandler) =>
  render(
    <PageHeader title={defaults.title} clickHandler={clickHandler}></PageHeader>
  );

describe('PageHeader (Mobile) tests', () => {
  it('Should render the title', () => {
    const { container } = renderPageHeader();
    expect(container).toContainHTML(defaults.title);
  });
  it('Should have a click handler', () => {
    const handlerMock = jest.fn();
    const { container } = renderPageHeader(handlerMock);
    const arrow = container.querySelector('.PageHeader-ClickableArrow');
    expect(arrow).not.toBeNull();
    expect(handlerMock).not.toHaveBeenCalled();
    fireEvent.click(arrow);
    expect(handlerMock).toHaveBeenCalledTimes(1);
  });
});
