import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { Flex } from 'prisma-design-system';
import { Header, HeaderText } from '../../common';
import BackButton from './BackButton';
import { Cross } from '../../common/icons';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';

const CloseButton = () => {
  const dispatch = ApplicationContext.useDispatch();
  const css = {
    position: 'absolute',
    top: '50%',
    right: '24px',
    transform: 'translate(0, -50%)',
    cursor: 'pointer',
  };
  const clickHandler = () => dispatch({ message: 'MMB:MainMenu:Close' });
  return (
    <Flex css={css} onClick={clickHandler}>
      <Cross color="neutrals.2" size="medium" />
    </Flex>
  );
};

export default function PageHeader({ title, clickHandler }) {
  const { t } = useTranslation();
  const css = {
    height: '72px',
    borderTopLeftRadius: '8px',
    borderTopRightRadius: '8px',
  };
  const backButton = clickHandler ? (
    <BackButton
      clickHandler={clickHandler}
      label={t('managebooking:menu.back.label', '')}
    />
  ) : (
    <React.Fragment />
  );
  return (
    <Header css={css} fixedHeader={'fixed'}>
      <Flex alignItems="center">
        {backButton}
        <HeaderText fontWeight="medium" priority={2}>
          {title}
        </HeaderText>
        <CloseButton />
      </Flex>
    </Header>
  );
}

PageHeader.propTypes = {
  title: PropTypes.string.isRequired,
  clickHandler: PropTypes.func,
};
