import React from 'react';
import PropTypes from 'prop-types';
import { Flex } from 'prisma-design-system';
import { Header, HeaderText } from '../../common';
import BackButton from './BackButton';

export default function PageHeader({
  title,
  clickHandler,
  fixedHeader = 'relative',
}) {
  return (
    <Header fixedHeader={fixedHeader}>
      <Flex alignItems="center">
        <BackButton clickHandler={clickHandler} />
        <HeaderText fontWeight="medium" priority={2}>
          {title}
        </HeaderText>
      </Flex>
    </Header>
  );
}

PageHeader.propTypes = {
  title: PropTypes.string.isRequired,
  clickHandler: PropTypes.func,
};

PageHeader.defaultProps = {
  clickHandler: () => {},
};
