import React from 'react';
import { text } from '@storybook/addon-knobs';
import PageHeaderMobile from './PageHeader.mobile';
import PageHeaderDesktop from './PageHeader.desktop';

export default {
  title: 'Common/Header/Page Header',
};

export const Mobile = () => {
  return (
    <PageHeaderMobile
      title={text('Title', 'Header Title Example')}
      clickHandler={() => console.log('test')}
    />
  );
};

export const Desktop = () => {
  return (
    <PageHeaderDesktop
      title={text('Title', 'Header Title Example')}
      clickHandler={() => console.log('test')}
    />
  );
};
