import React from 'react';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import PageHeaderMobile from './PageHeader.mobile';
import PageHeaderDesktop from './PageHeader.desktop';

export default ({ ...props }) => {
  const [{ isDesktop = false }] = ApplicationContext.useFlow();
  return isDesktop ? (
    <PageHeaderDesktop {...props} />
  ) : (
    <PageHeaderMobile {...props} />
  );
};
