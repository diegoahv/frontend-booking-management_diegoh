import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import css from '@styled-system/css';
import { Flex, Text } from 'prisma-design-system';
import { LeftArrow } from '../../common/icons';

const BackButtonText = styled(Text)(
  css({
    color: 'brandPrimary.2',
    fontSize: 'body.2',
  })
);
const BackButton = ({ clickHandler, label }) => {
  const text = label ? (
    <BackButtonText>{label}</BackButtonText>
  ) : (
    <React.Fragment />
  );
  return (
    <Flex
      alignItems="center"
      onClick={clickHandler}
      className="PageHeader-ClickableArrow"
    >
      <LeftArrow color="brandPrimary.2" size="medium" />
      {text}
    </Flex>
  );
};

BackButton.propTypes = {
  clickHandler: PropTypes.func.isRequired,
  label: PropTypes.string,
};
BackButton.defaultProps = {
  label: '',
};

export default BackButton;
