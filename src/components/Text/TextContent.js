import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'prisma-design-system';

export default function TextInformative({ children }) {
  return (
    <Text as="p" fontWeight="normal" priority={2} textAlign="left">
      {children}
    </Text>
  );
}

TextInformative.propTypes = {
  children: PropTypes.string.isRequired,
};
