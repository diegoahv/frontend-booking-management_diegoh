import TextInformative from './TextInformative';
import TextContent from './TextContent';
import H1 from './H1';
import H2 from './H2';

export { TextInformative, H1, H2, TextContent };
