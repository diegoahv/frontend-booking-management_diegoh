import React from 'react';
import PropTypes from 'prop-types';
import { Text, getTheme } from 'prisma-design-system';

export default function H2({ children }) {
  return (
    <Text
      as="div"
      fontWeight="medium"
      priority={1}
      textAlign="left"
      css={{ color: getTheme().colors.brandPrimary[4] }}
    >
      {children}
    </Text>
  );
}

H2.propTypes = {
  children: PropTypes.string.isRequired,
};
