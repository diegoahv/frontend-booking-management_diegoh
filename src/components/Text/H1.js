import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'prisma-design-system';

export default function H1({ children }) {
  return (
    <Text as="div" fontWeight="bold" priority={1} textAlign="left">
      {children}
    </Text>
  );
}

H1.propTypes = {
  children: PropTypes.string.isRequired,
};
