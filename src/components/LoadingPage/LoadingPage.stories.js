import React from 'react';
import LoadingPage from './LoadingPage';
import { Mocks, DebugAssistant } from '@frontend-react-component-builder/debug';

const getMock = (flow) =>
  Mocks.CustomApplicationContextMock({
    session: {
      ...Mocks.Session.DefaultSession(),
      flow,
    },
  });

const Component = ({ flow }) => (
  <DebugAssistant.AssistantApplicationContextMocker
    applicationContext={getMock(flow)}
  >
    <LoadingPage
      headerTitle={'Manage booking'}
      headerBackAction={() => {}}
      loadingText={'Loading...'}
    />
  </DebugAssistant.AssistantApplicationContextMocker>
);

export default {
  title: 'MMB/Loading Page/Home Loading Page',
  component: LoadingPage,
};

export const Mobile = () => <Component flow="mobile" />;

export const Desktop = () => <Component flow="desktop" />;
