import React from 'react';
import PropTypes from 'prop-types';
import { Text, Box, Flex } from 'prisma-design-system';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import LoadingSpinner from '../../common/components/LoadingSpinner';
import Page from '../Page/Page';

export default function LoadingPage({
  headerTitle,
  headerBackAction,
  loadingText,
}) {
  const [{ isMobile = false }] = ApplicationContext.useFlow();
  return (
    <Page
      title={headerTitle}
      backAction={isMobile ? headerBackAction : null}
      pageClass="home-loading-page-wrapper"
    >
      <Flex flexDirection="column" alignItems="center">
        <LoadingSpinner />
        <Box mt={4}>
          <Text fontSize="body.2" fontWeight="medium">
            {loadingText}
          </Text>
        </Box>
      </Flex>
    </Page>
  );
}

LoadingPage.propTypes = {
  headerTitle: PropTypes.string.isRequired,
  headerBackAction: PropTypes.func.isRequired,
  loadingText: PropTypes.string,
};
