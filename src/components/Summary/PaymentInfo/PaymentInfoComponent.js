import React from 'react';
import { css } from '@styled-system/css';
import {
  Accordion,
  AccordionItem,
  Box,
  Checkbox,
  Text,
  Flex,
  Wrapper,
  InformationIcon,
} from 'prisma-design-system';
import { getTheme } from '@frontend-react-component-builder/conchita';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import { getTracking } from '@/src/containers/SummaryPage/tracking';

import ItineraryPrice from '../../Results/ItineraryPrice';
import styled from '@emotion/styled';

const PaymentInfoComponent = ({
  currentPrice,
  currency,
  content,
  TCsUpdater,
  areTCsAccepted,
  userClickedContinue,
  isOpen,
  checkboxRef,
}) => {
  const tracking = ApplicationContext.useTracking();

  return (
    <Box data-testid="paymentInfo">
      <Text
        as={'p'}
        css={css({
          color: 'neutrals.800',
          fontWeight: 'medium',
          fontSize: 'heading.0',
          marginBottom: '3',
        })}
      >
        {content.header}
      </Text>
      <Text
        as={'p'}
        css={css({
          color: 'neutrals.800',
          fontWeight: 'normal',
          fontSize: 'body.1',
          marginBottom: '5',
        })}
      >
        {content.body}
      </Text>
      <Flex
        flexDirection={'row'}
        justifyContent="space-between"
        alignItems="center"
      >
        <Text
          as={'div'}
          css={css({
            color: 'neutrals.800',
            fontWeight: 'medium',
            fontSize: 'body.2',
          })}
        >
          {content.totalPrice}{' '}
        </Text>
        <ItineraryPrice currency={currency} amount={currentPrice} />
      </Flex>
      <Accordion>
        <AccordionItem
          header={content.priceBreakdownAccordionHeader}
          open={isOpen}
          css={css(accordionItemStyle)}
          onClick={() => {
            const { category, action, label } = getTracking('PRICE_BREAKDOWN');
            tracking.trackEventWithCategory(category, action, label);
          }}
        >
          <Box pb={5}>
            <Box pb={1}>
              <Text
                css={css({
                  lineHeight: '2',
                  fontSize: 'body.1',
                  color: 'neutrals.800',
                })}
                as={'div'}
              >
                {content.priceBreakdownHeader}
              </Text>
            </Box>
            <Flex
              css={css({ color: 'neutrals.1' })}
              flexDirection="row"
              justifyContent="space-between"
            >
              <Text as={'div'}>{content.priceBreakdownBaseFare}</Text>
              <ItineraryPrice
                currency={currency}
                amount={currentPrice}
                size="small"
              />
            </Flex>
            <Text
              css={css({ fontSize: 'body.0', color: 'neutrals.4' })}
              as={'div'}
            >
              {content.priceBreakdownNumberPax}
            </Text>
          </Box>
        </AccordionItem>
      </Accordion>
      <Box pb={6}>
        <StyledWrapper>
          <Flex
            flexDirection="row"
            alignItems="center"
            css={css({ color: 'informative.darkest', fontSize: 'body.0' })}
            p={2}
          >
            <Flex flexDirection="column" alignItems="center" flexShrink={0}>
              <InformationIcon />
            </Flex>
            <Box pl={2}>
              <Text>{content.confirmationDetails}</Text>
            </Box>
          </Flex>
        </StyledWrapper>
      </Box>
      <Box ref={checkboxRef}>
        <CheckboxWithState
          TCsUpdater={TCsUpdater}
          areTCsAccepted={areTCsAccepted}
          hasError={!areTCsAccepted && userClickedContinue}
          errorMessage={content.priceBreakdownError}
          userClickedContinue={userClickedContinue}
          checkboxRef={checkboxRef}
          sendTracking={tracking}
        >
          {content.TCs}
        </CheckboxWithState>
      </Box>
    </Box>
  );
};

const accordionItemStyle = {
  borderStyle: 'none',
  color: 'brandPrimary.2',
  fontSize: 'body.1',
  '& > div:last-of-type': {
    paddingX: 0,
  },
  '& > div > span': {
    marginRight: 0,
    fontSize: 'body.1',
    '&:first-of-type': {
      marginLeft: 0,
    },
  },
  '&:last-child': { borderBottom: '0' },
};

const StyledWrapper = styled(Wrapper)(
  css({
    backgroundColor: 'informative.soft',
    borderRadius: '2',
  })
);
const checkBoxStyles = {
  '& input[type=checkbox]:checked + span': {
    backgroundColor: 'brandPrimary.2',
    borderColor: 'brandPrimary.2',
  },
  '& input[type=checkbox]:checked + span > svg': { fill: 'white' },
  '& label': { alignItems: 'start' },
};

const StyledCheckboxWrapper = styled(Box)(css(checkBoxStyles));

const StyledCheckboxErrorWrapper = styled(Box)(
  css({
    ...checkBoxStyles,
    '& input[type=checkbox] + span': {
      border: `1px solid ${getTheme().colors.critical.base}`,
    },
  })
);

const CheckboxWithState = ({ children, ...props }) => {
  const handleChange = (event) => {
    const isChecked = event.target.checked;
    props.TCsUpdater(isChecked);
    if (isChecked) {
      const { category, action, label } = getTracking('CHECKBOX_CHECKED');
      props.sendTracking.trackEventWithCategory(category, action, label);
    }
  };

  if (props.hasError && props.userClickedContinue) {
    return (
      <StyledCheckboxErrorWrapper pb={6}>
        <Checkbox onChange={handleChange} {...props}>
          <Text css={css({ color: 'neutrals.3' })}>{children}</Text>
          <Text css={css({ color: 'critical.dark' })}>
            {props.errorMessage}
          </Text>
        </Checkbox>
      </StyledCheckboxErrorWrapper>
    );
  }

  return (
    <StyledCheckboxWrapper pb={6}>
      <Checkbox
        onChange={handleChange}
        {...props}
        checked={props.areTCsAccepted}
      >
        {children}
      </Checkbox>
    </StyledCheckboxWrapper>
  );
};

export default PaymentInfoComponent;
