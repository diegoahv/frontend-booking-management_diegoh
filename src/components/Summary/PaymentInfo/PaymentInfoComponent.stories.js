import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, number } from '@storybook/addon-knobs';

import PaymentInfoComponent from './index';

export const id = () => number('id', 0);

storiesOf('OpenTicket/Summary/PaymentInfoComponent', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const content = {
      header: 'Summary',
      body: 'blah vlag',
      totalPrice: 'Total price',
      priceBreakdownAccordionHeader: 'Price breakdown',
      priceBreakdownBaseFare: 'Base fare',
      confirmationDetails: 'Confirmation Details',
      errroMessage: 'error',
      TCs: 'Lorem akdngjadnjladnvjlanvla',
      priceBreakdownNumberPax: 'Test Num Pax String',
    };
    const currentPrice = 100;
    const currency = 'EUR';
    return (
      <PaymentInfoComponent
        content={content}
        currentPrice={currentPrice}
        currency={currency}
        isOpen={true}
      ></PaymentInfoComponent>
    );
  });
