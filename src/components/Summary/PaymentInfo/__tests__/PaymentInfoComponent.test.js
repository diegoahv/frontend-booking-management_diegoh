import PaymentInfoComponent from '../';
import React from 'react';
import { render } from 'test-utils';
import { useTranslation } from 'react-i18next';

describe('SummaryPage tests', () => {
  it('Should show the correct price', async () => {
    const currentPrice = 100;
    const currency = 'EUR';

    const { container, getByText } = render(
      <PaymentInfoComponent
        content={{}}
        currentPrice={currentPrice}
        currency={currency}
      ></PaymentInfoComponent>
    );

    expect(container).toContainElement(getByText('€'));
    expect(container).toContainElement(getByText('100.'));
    expect(container).toContainElement(getByText('00'));
  });

  it('Should show the correct number of passengers', async () => {
    const { t } = useTranslation();
    const content = {
      priceBreakdownNumberPax: t(
        'summaryPage:paymentInfo.priceBreakdown.numberPax'
      ),
    };

    const expectedText = 'Test Num Pax String';
    const currentPrice = 100;
    const currency = 'EUR';

    const { container, getByText } = render(
      <PaymentInfoComponent
        content={content}
        currentPrice={currentPrice}
        currency={currency}
        isOpen={true}
      ></PaymentInfoComponent>
    );

    expect(container).toContainElement(getByText(expectedText));
  });
});
