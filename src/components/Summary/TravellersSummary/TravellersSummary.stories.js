import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, number } from '@storybook/addon-knobs';
import TravellerSummary from './index';

export const getTraveller = function () {
  return {
    travellerId: 1,
    travellerType: 'ADULT',
    name: 'Sarah',
    mail: 'sarah.smith@gmail.com',
    firstLastName: 'Smith',
    secondLastName: null,
  };
};

const buyer = { mail: 'sarah.smith@gmail.com' };

export const selectTravellers = function () {
  return Array.from({ length: getNumPax() }, () => getTraveller());
};

export const selectBuyer = function () {
  return buyer;
};

export const getNumPax = function () {
  return number('Num Pax', 2);
};

storiesOf('OpenTicket/Summary/TravellerSummary', module)
  .addDecorator(withKnobs)
  .add('Base', () => {
    return (
      <TravellerSummary travellers={selectTravellers()} buyer={selectBuyer()} />
    );
  });
