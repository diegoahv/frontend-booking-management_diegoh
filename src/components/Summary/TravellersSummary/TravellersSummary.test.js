import React from 'react';
import { render, fireEvent } from 'test-utils';
import TravellersSummary from './TravellersSummary';
import addons, { mockChannel } from '@storybook/addons';
import { getAllByTestId } from '@testing-library/dom';

addons.setChannel(mockChannel());
const travellers = [
  {
    travellerId: 1,
    travellerType: 'ADULT',
    name: 'Sarah',
    mail: 'sarah.smith@gmail.com',
    firstLastName: 'Smith',
    secondLastName: null,
  },
  {
    travellerId: 2,
    travellerType: 'ADULT',
    name: 'John',
    firstLastName: 'Duncan',
    secondLastName: null,
  },
];

const buyer = { mail: 'sarah.smith@gmail.com' };

describe('TravellersSummary component tests', () => {
  it('should render correctly', () => {
    const { container } = renderTravellersSummary({
      travellers,
      buyer,
    });

    const icon = container.getElementsByTagName('svg');
    expect(icon).toHaveLength(1);
    expect(container).toContainElement(icon[0]);
  });

  it('should show the list of passengers', () => {
    const firstTravellerName = travellers[0].name;
    const firstTravellerFirstLastName = travellers[0].firstLastName;
    const secondTravellerName = travellers[1].name;
    const secondTravellerFirstLastName = travellers[1].firstLastName;
    const { container, getByText } = renderTravellersSummary({
      travellers,
      buyer,
    });
    const icon = container.getElementsByTagName('svg')[0];
    fireEvent.click(icon);
    expect(getAllByTestId(container, 'traveller')).toHaveLength(2);
    expect(container).toContainElement(
      getByText(`${firstTravellerName} ${firstTravellerFirstLastName}`)
    );
    expect(container).toContainElement(
      getByText(`${secondTravellerName} ${secondTravellerFirstLastName}`)
    );
  });

  it('should contain the buyers email', () => {
    const mail = buyer.mail;
    const { container, getByText } = renderTravellersSummary({
      travellers,
      buyer,
    });
    const icon = container.getElementsByTagName('svg')[0];
    fireEvent.click(icon);
    expect(getAllByTestId(container, 'mail')).toHaveLength(1);
    expect(container).toContainElement(getByText(mail));
  });
});

function renderTravellersSummary({ travellers = [], buyer = {} } = {}) {
  return render(<TravellersSummary travellers={travellers} buyer={buyer} />);
}
