import React from 'react';
import { Flex, Accordion, AccordionItem } from 'prisma-design-system';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import css from '@styled-system/css';
import { getTracking } from '@/src/containers/SummaryPage/tracking';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';

import Traveller from './Traveller';
import Mail from './Mail';

const StyledAccordionItem = styled(AccordionItem)(
  css({
    borderTop: 'none',
    ':last-child': { borderBottomWidth: [0, 0] },
    '& > div': {
      paddingBottom: 0,
      '&:last-of-type': {
        padding: 0,
        paddingTop: 2,
      },
      '& > span:first-of-type': {
        marginLeft: 0,
      },
      '& > span:last-of-type': {
        marginRight: 0,
      },
    },
  })
);

const TravellersSummary = ({ travellers, buyer = {} }) => {
  const tracking = ApplicationContext.useTracking();
  const { t } = useTranslation();
  const getNumPassengers = function (travellers) {
    const numPax = travellers.length;
    return t(
      'myinfo:tripdetails.openticket.summaryPage.itinerarySummary.numPax',
      { count: numPax }
    );
  };

  const StyledDiv = styled.div(
    css({
      paddingTop: 3,
      ['& div > span > svg']: { color: 'brandPrimary.2' },
    })
  );

  return (
    <Accordion>
      <StyledDiv>
        <StyledAccordionItem
          header={getNumPassengers(travellers)}
          open={false}
          onClick={() => {
            const { category, action, label } = getTracking('PAX_INFO');
            tracking.trackEventWithCategory(category, action, label);
          }}
        >
          {travellers.map((traveller, index) => (
            <Flex key={index}>
              <Traveller traveller={traveller} />
            </Flex>
          ))}
          <Mail value={buyer.mail} />
        </StyledAccordionItem>
      </StyledDiv>
    </Accordion>
  );
};

TravellersSummary.propTypes = {
  travellers: PropTypes.array,
  buyer: PropTypes.object,
};

export default TravellersSummary;
