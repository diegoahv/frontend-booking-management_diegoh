import React from 'react';
import { Flex, Text, MailIcon } from 'prisma-design-system';
import styled from '@emotion/styled';
import css from '@styled-system/css';
const StyledText = styled(Text)(
  css({
    color: 'neutrals.0',
  })
);
const Mail = ({ value = '' }) => (
  <Flex
    flexDirection="row"
    alignItems="center"
    data-testid="mail"
    pt={3}
    justifyContent="flex-start"
  >
    <Flex flexGrow={0}>
      <MailIcon size="small" color="neutrals.5" />
    </Flex>
    <Flex flexGrow={0} pl={1}>
      <StyledText priority={5}>{value}</StyledText>
    </Flex>
  </Flex>
);

export default Mail;
