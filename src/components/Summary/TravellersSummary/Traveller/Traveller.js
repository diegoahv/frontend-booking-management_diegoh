import React from 'react';
import { Flex, Text, PaxIcon } from 'prisma-design-system';
import { lowerCaseAllWordsExceptFirstLetters } from '../../../../utils/StringUtils';
import styled from '@emotion/styled';
import css from '@styled-system/css';

const Traveller = ({ traveller = {} }) => {
  const getTravellerType = function (travellerType) {
    return lowerCaseAllWordsExceptFirstLetters(travellerType);
  };

  const StyledText = styled(Text)(
    css({
      color: 'neutrals.0',
    })
  );

  return (
    <Flex
      flexDirection="row"
      alignItems="center"
      data-testid="traveller"
      pt={3}
      justifyContent="flex-start"
    >
      <Flex flexGrow={0}>
        <PaxIcon size="small" color="neutrals.5" />
      </Flex>
      <Flex flexGrow={0} pl={1}>
        <StyledText
          priority={5}
        >{`${traveller.name} ${traveller.firstLastName}`}</StyledText>
      </Flex>
      <Flex pl={1} flexGrow={0}>
        <Text priority={5}> ({getTravellerType(traveller.travellerType)})</Text>
      </Flex>
    </Flex>
  );
};

export default Traveller;
