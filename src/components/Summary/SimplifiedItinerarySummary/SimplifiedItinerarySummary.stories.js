import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import SimplifiedItinerarySummary from './index';
import { flightsMock } from 'serverMocks';

storiesOf('OpenTicket/Summary/SimplifiedItinerarySummary', module)
  .addDecorator(withKnobs)
  .add('Base', () => {
    return <SimplifiedItinerarySummary requestedFlights={flightsMock} />;
  });
