import React from 'react';
import { Box } from 'prisma-design-system';

import SegmentSummary from '../SegmentSummary';
import PropTypes from 'prop-types';

const SimplifiedItinerarySummary = ({ requestedFlights }) => {
  return (
    <Box>
      {requestedFlights.flights.map((flight, index) => (
        <Box pt={3} key={index}>
          <Box pt={1}>
            <SegmentSummary data={flight} id={index} />
          </Box>
        </Box>
      ))}
    </Box>
  );
};

SimplifiedItinerarySummary.propTypes = {
  requestedFlights: PropTypes.object.isRequired,
};

export default SimplifiedItinerarySummary;
