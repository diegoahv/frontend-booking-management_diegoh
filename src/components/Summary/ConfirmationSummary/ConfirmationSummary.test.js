import React from 'react';
import { render } from 'test-utils';
import ConfirmationSummary from './ConfirmationSummary';
import { flightsMock } from 'serverMocks';

import { ItineraryMocks } from 'serverMocks';
import { ResultsSegmentMocks } from '../../Results/Segment/__mocks__';

const HEADER_MESSAGE = 'Booking change request confirmed!';

describe('ConfirmationSummary component tests', () => {
  it('should show the header message', () => {
    const { SegmentSample } = ResultsSegmentMocks();
    const { ItineraryWith } = ItineraryMocks();
    const itinerary = ItineraryWith({
      segments: [
        SegmentSample({
          duration: 95,
        }),
      ],
    });
    const { container, getByText } = renderConfirmationSummary({ itinerary });
    expect(container).toContainElement(getByText(HEADER_MESSAGE));
  });
});

function renderConfirmationSummary({ itinerary = {} } = {}) {
  return render(
    <ConfirmationSummary
      itinerary={itinerary}
      requestedFlights={flightsMock}
    />,
    {
      applicationContext: { application: { session: { locale: 'en-US' } } },
    }
  );
}
