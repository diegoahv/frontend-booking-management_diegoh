import React from 'react';
import { Card, Box, TickIcon, Flex, Text } from 'prisma-design-system';

import ItinerarySummary from '../ItinerarySummary';
import styled from '@emotion/styled';
import css from '@styled-system/css';
import { useTranslation } from 'react-i18next';
import SimplifiedItinerarySummary from '@/src/components/Summary/SimplifiedItinerarySummary';

const StyleCard = styled(Card)(
  css({
    p: '4',
    borderRadius: '1',
  })
);

const StyledHeader = styled(Card)(
  css({
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    textShadow: '0 1px 0 rgba(0,0,0,.2)',
    backgroundColor: 'success.base',
    fontSize: 'heading.0',
    px: 4,
    py: 3,
  })
);

const ConfirmationSummary = ({ itinerary, requestedFlights }) => {
  const { t } = useTranslation();

  return (
    <>
      <StyledHeader data-testid="confirmationSummary">
        <Flex alignItems="center">
          <TickIcon color={'white'} size={'medium'} />
          <Box pl={2} as={'span'}>
            <Text fontWeight={'medium'} priority={4} color={'white'}>
              {`${t('confirmationPage:summary.header')}`}
            </Text>
          </Box>
        </Flex>
      </StyledHeader>
      <StyleCard>
        <Box>
          {requestedFlights.flights.length > 0 ? (
            <SimplifiedItinerarySummary requestedFlights={requestedFlights} />
          ) : (
            <ItinerarySummary itinerary={itinerary} smallHeader={true} />
          )}
        </Box>
      </StyleCard>
    </>
  );
};

export default ConfirmationSummary;
