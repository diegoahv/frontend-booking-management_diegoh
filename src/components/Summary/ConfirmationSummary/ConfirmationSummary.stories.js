import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean } from '@storybook/addon-knobs';
import ConfirmationSummary from './index';
import { selectItinerary } from '../ItinerarySummary/ItinerarySummary.stories';
import { flightsMock } from 'serverMocks';

storiesOf('OpenTicket/Summary/ConfirmationSummary', module)
  .addDecorator(withKnobs)
  .add('Base', () => {
    const withRedirection = boolean('withRedirection', false);

    return (
      <ConfirmationSummary
        itinerary={selectItinerary()}
        requestedFlights={withRedirection ? flightsMock : { flights: [] }}
      />
    );
  });
