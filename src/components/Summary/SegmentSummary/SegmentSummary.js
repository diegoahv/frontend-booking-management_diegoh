import React from 'react';
import { Flex, Text, Hr, DateTime } from 'prisma-design-system';
import SegmentLocation from '../../Results/SegmentLocation';
import CarrierLogo from '../../Carrier/CarrierLogo';
import styled from '@emotion/styled';
import css from '@styled-system/css';

const SegmentSummary = ({ data }) => {
  const { departureDate, departure, destination, carriers } = data;

  const imageSize = 'medium';

  const StyledText = styled(Text)(
    css({
      color: 'neutrals.1',
    })
  );

  return (
    <Flex
      width="100%"
      flexDirection="row"
      justifyContent="space-between"
      alignItems="center"
      mt={0}
      data-testid="segment"
    >
      <Flex flexGrow={0} alignItems="center">
        {carriers.map((carrier) => (
          <CarrierLogo
            key={carrier.id}
            id={carrier.id}
            name={carrier.name}
            imageSize={imageSize}
          />
        ))}
        <Flex px={1}>
          <SegmentLocation
            customStyle={css({
              color: 'neutrals.0',
              fontSize: 'body.3',
            })}
            location={departure}
          />
        </Flex>
        <Text
          css={{
            color: 'neutrals.0',
            fontSize: 'body.3',
          }}
          as="span"
        >
          -
        </Text>
        <Flex pl={1}>
          <SegmentLocation
            customStyle={css({
              color: 'neutrals.0',
              fontSize: 'body.3',
            })}
            location={destination}
          />
        </Flex>
      </Flex>
      <Flex flexGrow={1} pl={3} pr={2}>
        <Hr width="100%" css={css({ borderColor: 'neutrals.6' })} />
      </Flex>
      <Flex flexGrow={0}>
        <StyledText priority={5}>
          <DateTime value={departureDate} type="date" pattern="medium" />
        </StyledText>
      </Flex>
    </Flex>
  );
};

export default SegmentSummary;
