import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, number } from '@storybook/addon-knobs';
import { selectCarrier } from 'decorators';
import { selectLocation } from '../../Results/SegmentLocation/SegmentLocation.stories';
import ItineraryProvider from '../../Results/Itinerary/Provider';

import SegmentSummary from './index';

export const selectSegmentInfo = () => {
  return {
    carriers: [selectCarrier()],
    departureDate: text('Departure date', '2018-07-22T23:30:00+02:00'),
    departure: selectLocation('Departure location', 'El Prat'),
    destination: selectLocation('Destination location', 'Barajas'),
  };
};

export const id = () => number('id', 0);

storiesOf('OpenTicket/Summary/SegmentSummary', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const segment = selectSegmentInfo();

    return (
      <ItineraryProvider>
        <SegmentSummary data={segment} id={id()} />
      </ItineraryProvider>
    );
  });
