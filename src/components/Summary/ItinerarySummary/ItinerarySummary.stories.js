import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select } from '@storybook/addon-knobs';
import ItinerarySummary from './index';
import { ItineraryMocks } from 'serverMocks';
import { tripTypes } from '@/src/common/utils/enums';
import generateSegments from '@/src/components/Trip/Itinerary/__mocks__';

export const selectItinerary = () => {
  const tripType = select(
    'Trip TYpe',
    Object.keys(tripTypes),
    tripTypes.ROUND_TRIP.type
  );
  const segments = generateSegments(tripTypes[tripType].length);
  const { ItineraryWith } = ItineraryMocks();
  return ItineraryWith({
    segments: segments,
  });
};

storiesOf('OpenTicket/Summary/ItinerarySummary', module)
  .addDecorator(withKnobs)
  .add('Base', () => {
    return <ItinerarySummary itinerary={selectItinerary()} />;
  });
