import React from 'react';
import { Box, Text } from 'prisma-design-system';

import SegmentSummary from '../SegmentSummary';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';

const ItinerarySummary = ({ itinerary, smallHeader = false }) => {
  const { t } = useTranslation();
  const { legs = [] } = itinerary;
  const getSegmentHeader = function (index) {
    const header =
      index === 0
        ? t(
            'myinfo:tripdetails.openticket.summaryPage.itinerarySummary.destination'
          )
        : t(
            'myinfo:tripdetails.openticket.summaryPage.itinerarySummary.return'
          );

    return header.toUpperCase();
  };
  const itineraryHeaderPriority = smallHeader ? 4 : 1;
  const itineraryHeaderPb = smallHeader ? 3 : 4;
  return (
    <div>
      <Box pb={itineraryHeaderPb}>
        <Text fontWeight="medium" priority={itineraryHeaderPriority}>
          {t(
            'myinfo:tripdetails.openticket.summaryPage.itinerarySummary.itinerary'
          )}
        </Text>
      </Box>
      <Box>
        {legs.map((leg, index) => (
          <Box pt={3} key={index}>
            <Text as="div" priority={4}>
              {getSegmentHeader(index)}
            </Text>
            <Box pt={2}>
              <SegmentSummary data={leg.segments[0]} id={index} />
            </Box>
          </Box>
        ))}
      </Box>
    </div>
  );
};

ItinerarySummary.propTypes = {
  itinerary: PropTypes.object.isRequired,
  smallHeader: PropTypes.bool,
};

export default ItinerarySummary;
