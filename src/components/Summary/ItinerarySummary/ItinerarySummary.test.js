import React from 'react';
import { render } from 'test-utils';
import ItinerarySummary from './ItinerarySummary';

import { ItineraryMocks } from 'serverMocks';
import { ResultsSegmentMocks } from '../../Results/Segment/__mocks__';
import { getAllByTestId } from '@testing-library/dom';

describe('ItinerarySummary component tests', () => {
  it('should show outbound segment in one way trip', () => {
    const { SegmentSample } = ResultsSegmentMocks();
    const { ItineraryWith } = ItineraryMocks();
    const itinerary = ItineraryWith({
      segments: [
        SegmentSample({
          duration: 95,
        }),
      ],
    });
    const { container } = renderItinerarySummary({ itinerary });
    expect(getAllByTestId(container, 'segment')).toHaveLength(1);
  });
  it('should show the departure date', () => {
    const expectedDate = 'Feb 27, 2020';
    const { SegmentSample } = ResultsSegmentMocks();
    const { ItineraryWith } = ItineraryMocks();
    const itinerary = ItineraryWith({
      segments: [
        SegmentSample({
          departureDate: '2020-02-27T22:25:40',
        }),
      ],
    });
    const { container, getByText } = renderItinerarySummary({ itinerary });
    expect(container).toContainElement(getByText(expectedDate));
  });

  it('should show outbound and return segments in return trip', () => {
    const { SegmentSample } = ResultsSegmentMocks();
    const { ItineraryWith } = ItineraryMocks();
    const itinerary = ItineraryWith({
      segments: [
        SegmentSample({
          duration: 95,
        }),
        SegmentSample({
          duration: 85,
        }),
      ],
    });
    const { container } = renderItinerarySummary({ itinerary });
    expect(getAllByTestId(container, 'segment')).toHaveLength(2);
  });
});

function renderItinerarySummary({ itinerary = {} } = {}) {
  return render(<ItinerarySummary itinerary={itinerary} />, {
    applicationContext: { application: { session: { locale: 'en-US' } } },
  });
}
