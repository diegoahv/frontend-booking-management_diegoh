import React from 'react';
import { Card, Hr, Box } from 'prisma-design-system';

import ItinerarySummary from '../ItinerarySummary';
import TravellersSummary from '../TravellersSummary';
import styled from '@emotion/styled';
import css from '@styled-system/css';

const StyledCard = styled(Card)(
  css({
    p: '4',
    pb: 5,
    boxShadow: `level.2`,
    borderRadius: 1,
  })
);

const StyledHr = styled(Hr)(
  css({
    borderColor: 'neutrals.6',
  })
);

const FlightSummary = ({ itinerary, travellers, buyer }) => (
  <StyledCard>
    <Box>
      <Box pb={5}>
        <ItinerarySummary itinerary={itinerary} />
      </Box>
      <StyledHr />
      <TravellersSummary travellers={travellers} buyer={buyer} />
    </Box>
  </StyledCard>
);

export default FlightSummary;
