import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import FlightSummary from './index';
import { selectItinerary } from '../ItinerarySummary/ItinerarySummary.stories';
import { selectTravellers } from '../TravellersSummary/TravellersSummary.stories';
import { selectBuyer } from '../TravellersSummary/TravellersSummary.stories';

storiesOf('OpenTicket/Summary/FlightSummary', module)
  .addDecorator(withKnobs)
  .add('Base', () => {
    return (
      <FlightSummary
        itinerary={selectItinerary()}
        travellers={selectTravellers()}
        buyer={selectBuyer()}
      />
    );
  });
