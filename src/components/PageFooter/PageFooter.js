import React from 'react';
import PropTypes from 'prop-types';
import { Footer } from '@/src/common';

const PageFooter = ({ children, isFixed }) => {
  const footerType = isFixed ? 'fixed' : 'relative';
  return <Footer footerType={footerType}>{children}</Footer>;
};

PageFooter.propTypes = {
  isFixed: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

PageFooter.defaultProps = {
  isFixed: false,
};

export default PageFooter;
