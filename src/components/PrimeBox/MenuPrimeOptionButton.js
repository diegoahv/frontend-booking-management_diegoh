import React from 'react';
import PropTypes from 'prop-types';
import { Box, Text, Flex } from 'prisma-design-system';
import styled from '@emotion/styled';
import css from '@styled-system/css';

const StyledButton = styled(Box)((props) =>
  css({
    display: 'inline-block',
    color: 'white',
    cursor: 'pointer',
    backgroundImage: 'linear-gradient(to left, #C7A419, #8F7200)',
    borderRadius: '18px',
    fontSize: 'body.1',
    p: props.isMobile ? 2 : 3,
    mt: props.isMobile ? 4 : 0,
    flex: '1 0 auto',
    maxWidth: props.isMobile ? 'auto' : '40%',
  })
);

export default function MenuPrimeOptionButton({
  icon,
  text,
  clickHandler = () => {},
  isMobile = false,
}) {
  const buttonIcon = icon ? <Box pl={1}>{icon}</Box> : <React.Fragment />;

  return (
    <StyledButton onClick={clickHandler} isMobile={isMobile}>
      <Flex alignItems="center">
        <Text textAlign="center">{text}</Text>
        {buttonIcon}
      </Flex>
    </StyledButton>
  );
}

MenuPrimeOptionButton.propTypes = {
  icon: PropTypes.node,
  text: PropTypes.string.isRequired,
  clickHandler: PropTypes.func.isRequired,
  isMobile: PropTypes.bool,
};
