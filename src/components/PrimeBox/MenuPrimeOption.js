import { MenuCard } from '../../common';
import ImageFixedSize from '../../common/components/ImageFixedSize';
import {
  StyledTitleText,
  StyledDescriptionText,
} from './MenuPrimeStyledComponents';
import { Flex } from 'prisma-design-system';
import MenuPrimeOptionButton from './MenuPrimeOptionButton';
import React from 'react';
import { PRIME_LOGO_URL } from '../../common/utils/url_utils';
import PropTypes from 'prop-types';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';

const getIconUrl = (brand) => PRIME_LOGO_URL[brand] || PRIME_LOGO_URL.ED;

const MenuPrimeOption = ({
  title,
  description,
  buttonText,
  buttonIcon,
  buttonClickHandler = () => {},
}) => {
  const [session] = ApplicationContext.useSession();
  const { brandNewNomenclature } = session;
  const [{ isMobile = false }] = ApplicationContext.useFlow();
  return (
    <MenuCard type="prime">
      <ImageFixedSize height={22} src={getIconUrl(brandNewNomenclature)} />
      <StyledTitleText mt={isMobile ? 2 : 4}>{title}</StyledTitleText>
      <Flex
        flexDirection={isMobile ? 'column' : 'row'}
        alignItems={isMobile ? 'flex-start' : 'flex-end'}
        justifyContent="space-between"
        mt={isMobile ? 0 : 2}
      >
        <StyledDescriptionText
          pr={isMobile ? 0 : 4}
          mt={isMobile ? 3 : 0}
          isMobile={isMobile}
        >
          {description}
        </StyledDescriptionText>
        <MenuPrimeOptionButton
          icon={isMobile ? buttonIcon : <React.Fragment />}
          clickHandler={buttonClickHandler}
          text={buttonText}
          isMobile={isMobile}
        />
      </Flex>
    </MenuCard>
  );
};

MenuPrimeOption.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  buttonText: PropTypes.string.isRequired,
  buttonIcon: PropTypes.node,
  buttonClickHandler: PropTypes.func.isRequired,
};

export default MenuPrimeOption;
