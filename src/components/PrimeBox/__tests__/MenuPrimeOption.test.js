import React from 'react';
import { render } from 'test-utils';
import MenuPrimeOption from '../MenuPrimeOption';

import { withStandalone } from '@frontend-react-component-builder/conchita';
import { Mocks } from '@frontend-react-component-builder/debug';
const {
  EDREAMS,
  OPODO,
  GO_VOYAGES_NEW_NOMENCLATURE,
  TRAVELLINK,
} = Mocks.Session.Brand;

const TEST_TITLE = 'TEST_TITLE';
const TEST_DESCRIPTION = 'TEST_DESCRIPTION';
const TEST_BUTTON_TEXT = 'TEST_BUTTON_TEXT';
const TEST_BUTTON_ICON = 'TEST_BUTTON_ICON';
const TEST_CLICK_HANDLER = () => console.log('CLICK HANDLER');

const getDivPrimeSelector = () => `div[type="prime"]`;

const getDecorativeImg = (brand = EDREAMS) =>
  `https://a1.odistatic.net/images/onefront/bluestone/${brand}/prime_logo/${brand}-logo-prime-negative-short.svg`;

const getGVDecorativeImg = (brand = GO_VOYAGES_NEW_NOMENCLATURE) =>
  `https://a1.odistatic.net/images/onefront/bluestone/${brand}/prime_logo/GO-logo-prime-negative-short.svg`;

const getSrcSelector = (brand = EDREAMS) =>
  brand === GO_VOYAGES_NEW_NOMENCLATURE
    ? `[src="${getGVDecorativeImg(brand)}"]`
    : `[src="${getDecorativeImg(brand)}"]`;

const getApplicationContextMock = (brand) =>
  Mocks.CustomApplicationContextMock({
    session: Mocks.Session.CustomSession({
      brand,
      brandNewNomenclature: brand,
    }),
  });

const StandaloneMenuPrimeOption = withStandalone(
  <MenuPrimeOption
    title={TEST_TITLE}
    description={TEST_DESCRIPTION}
    buttonText={TEST_BUTTON_TEXT}
    buttonIcon={TEST_BUTTON_ICON}
    buttonClickHandler={TEST_CLICK_HANDLER}
  />
);

const renderByBrand = (brand = EDREAMS) =>
  render(
    <StandaloneMenuPrimeOption
      applicationContext={getApplicationContextMock(brand)}
    />
  );

describe('MenuPrimeOption tests', () => {
  describe('Render tests', () => {
    it('Should render the prime banner with the right type', () => {
      const { container } = renderByBrand();

      expect(container).toContainElement(
        container.querySelector(getDivPrimeSelector())
      );
    });

    it('Should render prime banner by (EDREAMS)', () => {
      const { container } = renderByBrand(EDREAMS);

      expect(container).toContainElement(
        container.querySelector(getSrcSelector(EDREAMS))
      );
    });

    it('Should render prime banner by (OPODO)', () => {
      const { container } = renderByBrand(OPODO);

      expect(container).toContainElement(
        container.querySelector(getSrcSelector(OPODO))
      );
    });

    it('Should render prime banner by (GOVOYAGES)', () => {
      const { container } = renderByBrand(GO_VOYAGES_NEW_NOMENCLATURE);

      expect(container).toContainElement(
        container.querySelector(getSrcSelector(GO_VOYAGES_NEW_NOMENCLATURE))
      );
    });

    it('Should render prime banner by (TRAVELLINK), currently using EDREAMS', () => {
      const { container } = renderByBrand(TRAVELLINK);

      expect(container).toContainElement(
        container.querySelector(getSrcSelector(EDREAMS))
      );
    });
  });
});
