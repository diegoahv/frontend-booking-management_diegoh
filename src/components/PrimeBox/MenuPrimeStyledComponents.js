import styled from '@emotion/styled';
import { Box } from 'prisma-design-system';
import css from '@styled-system/css';

export const StyledTitleText = styled(Box)(
  css({
    color: 'brandPrime.2',
    fontSize: 'body.0',
    letterSpacing: 'tracked',
  })
);

export const StyledDescriptionText = styled(Box)((props) =>
  css({
    fontWeight: 'bold',
    fontSize: props.isMobile ? 'body.1' : 'body.3',
  })
);
