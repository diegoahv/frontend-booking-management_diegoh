import React from 'react';
import { withKnobs, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import { RightArrowIcon } from 'prisma-design-system';

import MenuPrimeOption from './MenuPrimeOption';
import { Mocks, DebugAssistant } from '@frontend-react-component-builder/debug';
import { Box } from 'prisma-design-system';
const { EDREAMS, GO_VOYAGES, OPODO, TRAVELLINK } = Mocks.Session.Brand;

const getMock = (brand, flow) =>
  Mocks.CustomApplicationContextMock({
    session: Mocks.Session.CustomSession({
      brand,
      brandNewNomenclature: brand,
      flow,
    }),
  });

export default {
  title: 'MMB/Menu/Menu Prime Option',
  component: MenuPrimeOption,
  decorators: [withKnobs],
};

const Component = ({ brand, flow }) => (
  <Box style={{ maxWidth: '836px' }}>
    <DebugAssistant.AssistantApplicationContextMocker
      applicationContext={getMock(brand, flow)}
    >
      <MenuPrimeOption
        title={text('Title', 'ONLY 1,000,000 PRIME MEMBERSHIPS FOR FREE')}
        description={text(
          'Description',
          'Consigue nuestro número de atención prioritaria las 24 horas y descuentos en el 100% de los vuelos y de hasta el 50% en hoteles.'
        )}
        buttonText={text('Button', 'Obtener Prime gratis')}
        buttonIcon={<RightArrowIcon size="small" color="white" />}
        buttonClickHandler={action('test')}
      />
    </DebugAssistant.AssistantApplicationContextMocker>
  </Box>
);

export const EdreamsDesktop = () => (
  <Component brand={EDREAMS} flow="desktop" />
);

export const EdreamsMobile = () => <Component brand={EDREAMS} flow="mobile" />;

export const GoVoyagesDesktop = () => (
  <Component brand={GO_VOYAGES} flow="desktop" />
);

export const GoVoyagesMobile = () => (
  <Component brand={GO_VOYAGES} flow="mobile" />
);

export const OpodoDesktop = () => <Component brand={OPODO} flow="desktop" />;

export const OpodoMobile = () => <Component brand={OPODO} flow="mobile" />;

export const TravellinkDesktop = () => (
  <Component brand={TRAVELLINK} flow="desktop" />
);

export const TravellinkMobile = () => (
  <Component brand={TRAVELLINK} flow="mobile" />
);
