import React from 'react';
import { withKnobs, text } from '@storybook/addon-knobs';
import MenuOption from './MenuOption';
import { ModifyTrip } from '../../common/icons';

export default {
  title: 'MMB/Menu/Menu Option',
  component: MenuOption,
  decorators: [withKnobs],
};

export const Default = () => {
  return (
    <MenuOption
      title={text('Title', 'Menu Option Title')}
      description={text('Description', 'Menu Option Description')}
      Icon={ModifyTrip}
      clickHandler={() => console.log('test')}
      type="schedule"
    />
  );
};

export const MenuOptionWithoutIcon = () => {
  return (
    <MenuOption
      title={text('Title', 'Menu Option Title')}
      description={text('Description', 'Menu Option Description')}
      clickHandler={() => console.log('test')}
      type="confirmation"
    />
  );
};
