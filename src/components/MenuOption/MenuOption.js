import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Text, Box } from 'prisma-design-system';
import { MenuCard } from '../../common';
import MenuOptionHeader from './MenuOptionHeader';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import { RightArrow } from '../../common/icons';
import css from '@styled-system/css';
import styled from '@emotion/styled';

const StyledMenuCard = styled(MenuCard)(
  css({
    cursor: 'pointer',
  })
);

export default function MenuOption({
  Icon,
  title,
  description,
  clickHandler = () => {},
  type,
}) {
  const [{ isMobile = false }] = ApplicationContext.useFlow();
  const MenuOptionTextStyle = isMobile ? 5 : 3;
  return (
    <StyledMenuCard onClick={clickHandler}>
      <Flex justifyContent="space-between" alignItems="left">
        <Flex
          justifyContent="space-between"
          alignItems="left"
          flexDirection="column"
          css={css({ width: '100%' })}
        >
          <Flex justifyContent="space-between" flexDirection="arrow">
            <Flex justifyContent="flex-start" alignItems="center">
              <MenuOptionHeader type={type} Icon={Icon} title={title} />
            </Flex>
            <Flex
              alignItems="center"
              css={css(isMobile ? { minWidth: '16px' } : { display: 'none' })}
            >
              <RightArrow size="small" />
            </Flex>
          </Flex>
          <Box mt={3}>
            <Text priority={MenuOptionTextStyle}>{description}</Text>
          </Box>
        </Flex>
        <Flex
          alignItems="center"
          css={css(!isMobile ? { minWidth: '16px' } : { display: 'none' })}
        >
          <RightArrow size="small" />
        </Flex>
      </Flex>
    </StyledMenuCard>
  );
}

MenuOption.propTypes = {
  Icon: PropTypes.func,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  clickHandler: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
};
