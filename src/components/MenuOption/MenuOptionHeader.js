import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import css from '@styled-system/css';
import { Flex, Text } from 'prisma-design-system';
import { isMainMenuOption } from '../MenuPage/MenuEntry';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';

const TitleTextStyled = styled(Text)((props) =>
  css({
    color: 'brandPrimary.2',
    fontSize: props.isMobile ? 'body.3' : 'heading.0',
  })
);

const createAdditionalTitleStyle = (type, Icon) => {
  const isMainMenu = isMainMenuOption(type);
  return css({
    ml: Icon ? 3 : 0,
    fontWeight: isMainMenu ? 'normal' : 'medium',
  });
};

export default function MenuOptionHeader({ type, Icon, title }) {
  const iconElement = Icon ? <Icon /> : <React.Fragment />;
  const additionalTitleStyle = createAdditionalTitleStyle(type, Icon);
  const [{ isMobile = false }] = ApplicationContext.useFlow();

  return (
    <Flex justifyContent="space-between" alignItems="center">
      <Flex alignItems="center">
        {iconElement}
        <TitleTextStyled
          isMobile={isMobile}
          css={additionalTitleStyle}
          flexWrap="wrap"
        >
          {title}
        </TitleTextStyled>
      </Flex>
    </Flex>
  );
}

MenuOptionHeader.propTypes = {
  type: PropTypes.string.isRequired,
  Icon: PropTypes.func,
  title: PropTypes.string.isRequired,
};
