import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from 'test-utils';
import { CarrierFlightsummary, Carrier } from '../';
import { AirEuropa, Vueling } from 'serverMocks/Carrier';
const MULTIPLE_AIRLINES = 'Multiple airlines';

describe('Carrier', () => {
  test('Carrier content', () => {
    const { getByText, container } = render(
      <Carrier carriers={[AirEuropa()]} />
    );

    expect(container.textContent).toBe(AirEuropa().name);
    expect(getByText(AirEuropa().name)).toBeVisible();
  });

  test('Multiple carriers', () => {
    const { getByText, getByAltText } = render(
      <Carrier carriers={[AirEuropa(), Vueling()]} />
    );

    expect(getByText(MULTIPLE_AIRLINES)).toBeVisible();
    expect(getByAltText(AirEuropa().name)).toBeVisible();
    expect(getByAltText(Vueling().name)).toBeVisible();
  });

  test('CarrierFlightsummary content', () => {
    const flightCode = 'AABBCC';
    const { getByText, container } = render(
      <CarrierFlightsummary carriers={[AirEuropa()]} flightCode={flightCode} />
    );
    expect(container.textContent).toBe(`${AirEuropa().name} · ${flightCode}`);
    expect(getByText(`${AirEuropa().name} · ${flightCode}`)).toBeVisible();
  });
});
