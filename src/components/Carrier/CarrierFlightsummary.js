import React from 'react';
import { Flex, Text, Box } from 'prisma-design-system';
import CarrierLogo from './CarrierLogo';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';

const SEPARATOR = '·';

const CarrierFlightsummary = ({ carriers, flightCode }) => {
  const { t } = useTranslation();
  const imageSize = 'small';
  const isMulti = carriers.length > 1;

  const displayText = isMulti
    ? t('results:multiple.airlines')
    : carriers[0].name;

  return (
    <Flex alignItems="center">
      <Flex as="span" alignItems="center">
        {carriers.map((carrier) => (
          <CarrierLogo
            key={carrier.id}
            id={carrier.id}
            name={carrier.name}
            imageSize={imageSize}
          />
        ))}
      </Flex>
      <Box pb={1}>
        <Text priority={4}>{`${displayText} ${SEPARATOR} ${flightCode}`}</Text>
      </Box>
    </Flex>
  );
};

CarrierFlightsummary.propTypes = {
  carriers: PropTypes.arrayOf(String).isRequired,
  flightCode: PropTypes.string.isRequired,
};

export default CarrierFlightsummary;
