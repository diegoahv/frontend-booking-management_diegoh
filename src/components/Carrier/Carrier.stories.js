import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';

import { Carrier, CarrierFlightsummary } from './';
import { selectCarrier } from 'decorators';

storiesOf('OpenTicket/Results/Carrier', module)
  .addDecorator(withKnobs)
  .add('Default', () => {
    return <Carrier carriers={[selectCarrier()]} />;
  })
  .add('Multicarrier', () => {
    return (
      <Carrier
        carriers={[
          selectCarrier('Carrier #1', 'Vueling'),
          selectCarrier('Carrier #2', 'Ryanair'),
        ]}
      />
    );
  })
  .add('Carrier Flight summary', () => {
    return <CarrierFlightsummary carriers={[selectCarrier()]} />;
  });
