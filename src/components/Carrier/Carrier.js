import React from 'react';
import { Flex, Text } from 'prisma-design-system';
import { useTranslation } from 'react-i18next';

import CarrierLogo from './CarrierLogo';
import PropTypes from 'prop-types';

const Carrier = ({ carriers }) => {
  const { t } = useTranslation();

  const imageSize = 'small';
  const isMulti = carriers.length > 1;
  const displayText = isMulti
    ? t('results:multiple.airlines')
    : carriers[0].name;

  return (
    <Flex alignItems="center">
      <Flex as="span" alignItems="center">
        {carriers.map((carrier) => (
          <CarrierLogo
            key={carrier.id}
            id={carrier.id}
            name={carrier.name}
            imageSize={imageSize}
          />
        ))}
      </Flex>
      <Text priority={5}>{displayText}</Text>
    </Flex>
  );
};

Carrier.propTypes = {
  carriers: PropTypes.array.isRequired,
};

export default Carrier;
