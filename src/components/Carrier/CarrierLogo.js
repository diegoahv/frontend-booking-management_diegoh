import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Image } from 'prisma-design-system';
import styled from '@emotion/styled';
import { css } from '@styled-system/css';

const RoundedImage = styled(Image)(
  css({
    borderRadius: '50px',
  })
);

const CarrierLogo = ({ id, name, imageSize, bordered }) => {
  const imgSrc =
    id === 'MA'
      ? '/images/mobile/airline_logos/MA.png'
      : `/images/onefront/airlines/sm${id}.gif`;

  return (
    <Flex pr={2} key={id}>
      <RoundedImage
        src={imgSrc}
        alt={name}
        size={imageSize}
        bordered={bordered}
      />
    </Flex>
  );
};

CarrierLogo.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  imageSize: PropTypes.string.isRequired,
};

export default CarrierLogo;
