import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, number, boolean, select } from '@storybook/addon-knobs';

import Stopover from './Stopover';

import selectCarrier from 'decorators/selectCarrier';

storiesOf('OpenTicket/Trip/Stopover', module)
  .addDecorator(withKnobs)
  .add('Default', () => {
    const locationChangeType = select(
      'Location changed',
      ['terminal', 'airport', 'none'],
      'none'
    );
    const carrierChange = boolean('Carrier changed', false);
    const carrier = selectCarrier('Carrier');
    const durationInMinutes = number('Duration in minutes', 999);

    return (
      <Stopover
        duration={durationInMinutes}
        locationChangeType={locationChangeType}
        carrierChange={carrierChange}
        insuranceOffer={{
          policy: 'CARRIER_GUARANTEE',
          carrier,
        }}
      />
    );
  });
