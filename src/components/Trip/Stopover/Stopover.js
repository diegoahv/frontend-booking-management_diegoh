import React, { Fragment } from 'react';
import { DecorationLine, Text, Box } from 'prisma-design-system';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

import CarrierChange from './CarrierChange';
import ConnectionChangeType from '../ConnectionChangeType';
import ConnectionInsurance from '../ConnectionInsurance';
import { minutesToHoursAndMinutes } from 'utils/DateUtils';
import styled from '@emotion/styled';
import css from '@styled-system/css';

const StyledTitle = styled(Text)(
  css({
    fontSize: 'body.2',
    fontWeight: 'medium',
  })
);

const InformativeBox = styled(Box)(
  css({
    backgroundColor: 'informative.lightest',
    color: 'informative.dark',
    '& div[type="circledDot"]': {
      top: '20px',
      borderColor: 'informative.hard',
      '&:after': {
        backgroundColor: 'informative.hard',
      },
    },
  })
);

const Stopover = ({
  duration,
  locationChangeType,
  carrierChange,
  insuranceOffer,
}) => {
  const { t } = useTranslation();
  if (!duration && duration !== 0) return false;

  const { hours, minutes } = minutesToHoursAndMinutes(duration);

  const tripDurationText = `${t(
    'trip:stop.duration'
  )}: ${t('results:segment.duration', { hours, minutes })}`;

  return (
    <Fragment>
      <InformativeBox display="table-row">
        <Box display="table-cell" />
        <DecorationLine decorationType="circledDot" lineType="dashed" />
        <Box display="table-cell" pl={4} py={3} pr={2}>
          <Box pb={2}>
            <StyledTitle priority={4}>{tripDurationText}</StyledTitle>
          </Box>
          <Box>
            {carrierChange && <CarrierChange />}
            <ConnectionChangeType type={locationChangeType} />
            <ConnectionInsurance {...insuranceOffer} />
          </Box>
        </Box>
      </InformativeBox>
      <Box display="table-row">
        <Box display="table-cell" pr={4} pt={6} />
        <DecorationLine decorationType="none" lineType="dashed" />
      </Box>
    </Fragment>
  );
};

Stopover.defaultProps = {
  locationChangeType: 'none',
  carrierChange: false,
  insuranceOffer: {},
};

Stopover.propTypes = {
  locationChangeType: PropTypes.oneOf(['terminal', 'airport', 'none']),
  carrierChange: PropTypes.bool,
  insuranceOffer: PropTypes.object,
  duration: PropTypes.number,
};

export default Stopover;
