import React from 'react';
import { render } from 'utils/test-utils';
import '@testing-library/jest-dom/extend-expect';
import { Vueling } from 'serverMocks/Carrier';

import Stopover from '../';

const CONNECTION_COVERED_TEXT = `Connection covered by ${Vueling().name}`;
const TERMINAL_CHANGE_TEXT = 'Change of terminal';
const CARRIER_CHANGE_TEXT = 'Change of airline';
const STOP_DURATION_TEXT = 'Stop duration';

describe('Stopover', () => {
  test('content passed by prop should be shown 2', () => {
    const carrier = Vueling();
    const { getByText } = render(
      <Stopover
        insuranceOffer={{
          policy: 'CARRIER_GUARANTEE',
          carrier,
        }}
        duration={110}
        locationChangeType={'terminal'}
        carrierChange={true}
      />
    );

    expect(getByText(CONNECTION_COVERED_TEXT)).toBeVisible();
    expect(getByText(`${STOP_DURATION_TEXT}: 1h 50'`)).toBeVisible();
    expect(getByText(TERMINAL_CHANGE_TEXT)).toBeVisible();
    expect(getByText(CARRIER_CHANGE_TEXT)).toBeVisible();
  });
});
