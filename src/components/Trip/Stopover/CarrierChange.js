import React from 'react';
import { useTranslation } from 'react-i18next';
import { Flex, Box, Text, FlightRightIcon } from 'prisma-design-system';

const CarrierChange = () => {
  const { t } = useTranslation();

  return (
    <Flex pb={1} alignItems="center">
      <Box pr={2}>
        <FlightRightIcon size="small" />
      </Box>
      <Flex flexWrap="wrap">
        <Text>{t('trip:stop.airlineChange')}</Text>
      </Flex>
    </Flex>
  );
};

export default CarrierChange;
