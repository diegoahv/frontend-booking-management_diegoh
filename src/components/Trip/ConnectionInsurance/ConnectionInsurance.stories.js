import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select } from '@storybook/addon-knobs';
import selectCarrier from 'decorators/selectCarrier';

import ConnectionInsurance from '.';

const policies = ['CARRIER_GUARANTEE'];

storiesOf('OpenTicket/Trip/ConnectionInsurance', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const carrier = selectCarrier('Carrier');
    const policy = select('policy', policies, policies[0]);

    return <ConnectionInsurance policy={policy} carrier={carrier} />;
  });
