import React, { Fragment } from 'react';
import { useTranslation } from 'react-i18next';
import { Flex, Box, Text, InsuranceBadgeIcon } from 'prisma-design-system';
import PropTypes from 'prop-types';

const ConnectionInsurance = ({ policy, carrier }) => {
  const { t } = useTranslation();
  const carrierName = carrier?.name;

  return (
    <Fragment>
      <Flex pb={1} alignItems="center">
        <Box pr={2}>
          <InsuranceBadgeIcon size="small" />
        </Box>
        <Flex flexWrap="wrap">
          <Text>{t('trip:stop.missedConnection', { carrierName })}</Text>
        </Flex>
      </Flex>
    </Fragment>
  );
};

ConnectionInsurance.propTypes = {
  policy: PropTypes.oneOf(['CARRIER_GUARANTEE']).isRequired,
};

export default ConnectionInsurance;
