import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from 'utils/test-utils';

import { Vueling } from 'serverMocks/Carrier';
import ConnectionInsurance from '..';

const CARRIER_INSURANCE_TEXT = 'Connection covered by Vueling';

describe('ConnectionInsurance', () => {
  test('CARRIER_GUARANTEE policy', () => {
    setTimeout(() => {}, 1000);
    const carrier = Vueling();

    const { getByText } = render(
      <ConnectionInsurance policy="CARRIER_GUARANTEE" carrier={carrier} />
    );
    expect(getByText(CARRIER_INSURANCE_TEXT)).toBeVisible();
  });
});
