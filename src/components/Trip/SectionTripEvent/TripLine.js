import React from 'react';

import { DecorationLine } from 'prisma-design-system';

const TYPES = {
  departure: {
    decorationType: 'circle',
    lineType: 'solid',
  },
  connectionArrival: {
    decorationType: 'disk',
    lineType: 'dashed',
  },
  connectionDeparture: {
    decorationType: 'disk',
    lineType: 'solid',
  },
  arrival: {
    decorationType: 'disk',
    lineType: 'none',
  },
};

const TripLine = ({ type }) => {
  return <DecorationLine {...TYPES[type]} />;
};

export default TripLine;
