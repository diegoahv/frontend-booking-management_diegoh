import React from 'react';
import { render } from 'utils/test-utils';

import SectionTripEvent from '../';

describe('SectionTripEvent', () => {
  test('SectionLocation and SectionDateTime is visible by default', () => {
    const location = {
      name: 'Heathrow',
      terminal: '3',
      cityName: 'London',
      countryName: 'United Kingdom',
      iata: 'LHR',
    };
    const dateTime = '2018-07-22T23:30:00+02:00';
    const type = 'departure';

    const { getByText } = render(
      <SectionTripEvent location={location} dateTime={dateTime} type={type} />
    );

    expect(
      getByText(`${location.cityName}, ${location.countryName}`)
    ).toBeVisible();
    expect(getByText(`23:30`)).toBeVisible();
  });
});
