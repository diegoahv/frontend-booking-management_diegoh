import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select } from '@storybook/addon-knobs';

import SectionTripEvent from './';

storiesOf('OpenTicket/Trip/SectionTripEvent', module)
  .addDecorator(withKnobs)
  .add('Default', () => {
    const type = select(
      'Type',
      ['departure', 'connectionArrival', 'connectionDeparture', 'arrival'],
      'departure'
    );
    const location = {
      name: 'Heathrow',
      terminal: '3',
      cityName: 'London',
      countryName: 'United Kingdom',
      iata: 'LHR',
    };
    const dateTime = '2018-07-22T23:30:00+02:00';

    return (
      <SectionTripEvent type={type} location={location} dateTime={dateTime} />
    );
  });
