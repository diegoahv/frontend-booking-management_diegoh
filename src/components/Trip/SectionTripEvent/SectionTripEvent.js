import React from 'react';
import { Box } from 'prisma-design-system';
import PropTypes from 'prop-types';
import SectionDateTime from '../SectionDateTime';
import SectionLocation from '../SectionLocation';
import TripLine from './TripLine';

const SPACE = 3;

const SectionEvent = ({ location, dateTime, type }) => {
  return (
    <Box display="table-row">
      <Box display="table-cell" whiteSpace="nowrap" pr={SPACE}>
        <SectionDateTime dateTime={dateTime} />
      </Box>
      <TripLine type={type} />
      <Box display="table-cell" pl={SPACE} width="100%">
        <SectionLocation {...location} />
      </Box>
    </Box>
  );
};

SectionEvent.propTypes = {
  location: PropTypes.object.isRequired,
  dateTime: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};

export default SectionEvent;
