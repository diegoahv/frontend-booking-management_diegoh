import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from 'utils/test-utils';

import CabinClass from '../';

const TEXTS = {
  TOURIST: 'Economy',
  PREMIUM_ECONOMY: 'Premium economy',
  FIRST: 'First',
  ECONOMIC_DISCOUNTED: 'Economy',
  BUSINESS: 'Business',
};

describe('CabinClass', () => {
  test('Check type none', () => {
    const type = 'none';

    const { container } = render(<CabinClass type={type} />);
    expect(container).toHaveTextContent('');
  });
  test('Check type BUSINESS', () => {
    const type = 'BUSINESS';

    const { getByText } = render(<CabinClass type={type} />);
    expect(getByText(TEXTS[type])).toBeVisible();
  });
  test('Check type FIRST', () => {
    const type = 'FIRST';

    const { getByText } = render(<CabinClass type={type} />);
    expect(getByText(TEXTS[type])).toBeVisible();
  });
  test('Check type TOURIST', () => {
    const type = 'TOURIST';

    const { getByText } = render(<CabinClass type={type} />);
    expect(getByText(TEXTS[type])).toBeVisible();
  });
  test('Check type PREMIUM_ECONOMY', () => {
    const type = 'PREMIUM_ECONOMY';

    const { getByText } = render(<CabinClass type={type} />);
    expect(getByText(TEXTS[type])).toBeVisible();
  });
  test('Check type ECONOMIC_DISCOUNTED', () => {
    const type = 'ECONOMIC_DISCOUNTED';

    const { getByText } = render(<CabinClass type={type} />);
    expect(getByText(TEXTS[type])).toBeVisible();
  });
});
