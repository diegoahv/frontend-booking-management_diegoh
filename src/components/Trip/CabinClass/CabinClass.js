import React from 'react';
import { Text, SeatIcon, Flex } from 'prisma-design-system';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { toCamelCase } from 'utils/TextUtils';

function CabinClass({ type, showIcon }) {
  const { t } = useTranslation();

  return (
    type !== 'none' && (
      <Flex alignItems="center">
        {showIcon && <SeatIcon color="neutrals.1" size="small" />}
        <Text>{t(`results:cabinClass.${toCamelCase(type, '_')}`)}</Text>
      </Flex>
    )
  );
}

CabinClass.propTypes = {
  showIcon: PropTypes.bool,
  type: PropTypes.oneOf([
    'BUSINESS',
    'ECONOMIC_DISCOUNTED',
    'FIRST',
    'PREMIUM_ECONOMY',
    'TOURIST',
    'none',
  ]),
};
CabinClass.defaultProps = {
  type: 'BUSINESS',
  showIcon: false,
};

export default CabinClass;
