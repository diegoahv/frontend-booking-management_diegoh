import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select } from '@storybook/addon-knobs';

import CabinClass from './CabinClass';

storiesOf('OpenTicket/Trip/CabinClass', module)
  .addDecorator(withKnobs)
  .add('Default', () => {
    const type = select(
      'Type',
      [
        'BUSINESS',
        'ECONOMIC_DISCOUNTED',
        'FIRST',
        'PREMIUM_ECONOMY',
        'TOURIST',
      ],
      'ECONOMIC_DISCOUNTED'
    );

    return <CabinClass type={type} />;
  });
