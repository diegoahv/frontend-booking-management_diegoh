import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from 'utils/test-utils';
import generateSegments from '../../Itinerary/__mocks__';
import { tripTypes } from '@/src/common/utils/enums';
import Segment from '../';

const DEPARTURE_LABEL = 'DEPARTURE';
const DURATION = "4h 10'";
const TECHNICAL_STOP_LABEL = "Stop duration: 0h 25'";
const STOPOVER = 'Change of airline';
const BAGGAGE_LABEL = '2 baggage';
const SEATS_LABEL = '1 seats';

describe('Segment', () => {
  test('Shall show Segment header with correct information', () => {
    const tripType = tripTypes.ROUND_TRIP.type;
    const withStopovers = false;
    const withTechnicalStopovers = false;
    const segment = generateSegments(
      1,
      withStopovers,
      withTechnicalStopovers
    )[0];

    const { getByText } = render(
      <Segment segment={segment} indx={0} tripType={tripType} />
    );

    expect(getByText(DEPARTURE_LABEL)).toBeVisible();
    expect(getByText(DURATION)).toBeVisible();
    //TODO: change key
    expect(getByText(/segment.direct/)).toBeVisible();
  });

  test('Shall show Segment with stopover', () => {
    const tripType = tripTypes.ONE_WAY.type;
    const withStopovers = true;
    const withTechnicalStopovers = false;
    const segment = generateSegments(
      1,
      withStopovers,
      withTechnicalStopovers
    )[0];

    const { getByText } = render(
      <Segment segment={segment} indx={0} tripType={tripType} />
    );

    expect(getByText(STOPOVER)).toBeVisible();
  });

  test('Shall show Segment with technical stopover', () => {
    const tripType = tripTypes.ONE_WAY.type;
    const withStopovers = false;
    const withTechnicalStopovers = true;
    const segment = generateSegments(
      1,
      withStopovers,
      withTechnicalStopovers
    )[0];

    const { getByText } = render(
      <Segment segment={segment} indx={0} tripType={tripType} />
    );

    expect(getByText(TECHNICAL_STOP_LABEL)).toBeVisible();
  });

  test('Shall show Segment with ancillaries info', () => {
    const tripType = tripTypes.ONE_WAY.type;
    const withStopovers = false;
    const withTechnicalStopovers = true;
    const baggageCondition = 2;
    const seatsCondition = 1;
    const segment = generateSegments(
      1,
      withStopovers,
      withTechnicalStopovers,
      baggageCondition,
      seatsCondition
    )[0];

    const { getByText } = render(
      <Segment segment={segment} indx={0} tripType={tripType} />
    );

    expect(getByText(BAGGAGE_LABEL)).toBeVisible();
    expect(getByText(SEATS_LABEL)).toBeVisible();
  });
});
