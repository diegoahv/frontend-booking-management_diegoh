import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select, boolean, number } from '@storybook/addon-knobs';
import generateSegments from '../Itinerary/__mocks__';
import Segment from './Segment';

import { tripTypes } from '@/src/common/utils/enums';

storiesOf('OpenTicket/Trip/Segment', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const tripType = select(
      'Trip type',
      Object.keys(tripTypes),
      tripTypes.ONE_WAY.type
    );
    const withStopovers = boolean('With stops', true);
    const withTechnicalStopovers = boolean('With technical stops', false);
    const baggageCondition = number('Baggage', 1);
    const seatsCondition = number('Seats', 1);

    const segment = generateSegments(
      1,
      withStopovers,
      withTechnicalStopovers,
      baggageCondition,
      seatsCondition
    )[0];
    return <Segment segment={segment} indx={0} tripType={tripType} />;
  });
