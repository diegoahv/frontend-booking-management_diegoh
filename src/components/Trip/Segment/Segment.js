import React, { useMemo, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Box, Flex, FlightRightIcon, Text } from 'prisma-design-system';
import { useTranslation } from 'react-i18next';
import Section from '../Section';
import Stopover from '../Stopover';
import DurationWithIcon from '../DurationWithIcon';
import AncillariesInfo from '../AncillariesInfo';
import styled from '@emotion/styled';
import css from '@styled-system/css';

const StyledTitle = styled(Text)(
  css({
    fontSize: 'body.1',
    color: 'brand.primary.base',
  })
);

const StyledText = styled(Text)(
  css({
    fontSize: 'body.1',
    color: 'brandPrimary.2',
  })
);

const StyledTextContainer = styled(Flex)(
  css({
    whiteSpace: 'pre-wrap',
    maxWidth: '200px',
    flexBasis: '50%',
    flexWrap: 'wrap',
    borderRadius: 1,
  })
);

const RoundedBox = styled(Flex)(
  css({
    width: '2',
    height: '2',
    paddingBottom: '3px',
    borderRadius: '6',
    backgroundColor: 'brand.primary.base',
  })
);

const BrandedBox = styled(Box)(
  css({
    backgroundColor: 'brand.primary.lightest',
  })
);

const ShadowBox = styled(Box)(
  css({
    boxShadow: 'level.2',
    backgroundColor: 'white',
    borderRadius: 2,
    overflow: 'hidden',
  }),
  (props) => {
    return css({
      ...(props.indx > 0 && {
        marginTop: '6',
      }),
    });
  }
);

const SEPARATOR = '·';

const Segment = ({ segment, indx, tripType }) => {
  const { t } = useTranslation();
  const multiTripTypes = ['MULTIPLE_DESTINATIONS', 'MULTI_SEGMENT'];

  const segmentStops =
    segment.stops.length > 0
      ? t('trip:segment.stops', {
          count: segment.stops.length,
        })
      : t('trip:segment.direct');

  const segmentName = useMemo(() => {
    let name = '';
    if (tripType) {
      name = multiTripTypes.includes(tripType)
        ? t('trip:segment.name.multiple', {
            multipleText: t(`trip:segment.name.multiples${indx}`),
          })
        : t(`trip:segment.name.nomultiple${indx}`);
    }
    return name.toUpperCase();
  }, [tripType, t, multiTripTypes, indx]);

  const segmentHasAncillariesIncluded =
    segment.baggageCondition > 0 || segment.seatsCondition > 0;

  return (
    <ShadowBox indx={indx}>
      <BrandedBox px={3} py={3}>
        <Flex
          flexDirection="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <Flex flexDirection="row" alignItems="center">
            <RoundedBox
              pb={0}
              mr={2}
              alignItems="center"
              justifyContent="center"
            >
              <FlightRightIcon color="white" size="medium" />
            </RoundedBox>
            <StyledTitle>{segmentName}</StyledTitle>
          </Flex>
          <StyledTextContainer
            flexDirection="row"
            p={1}
            alignItems="center"
            justifyContent="center"
            color="white"
          >
            <StyledText>
              <DurationWithIcon duration={segment.duration} />
            </StyledText>
            <StyledText>{` ${SEPARATOR} ${segmentStops}`}</StyledText>
          </StyledTextContainer>
        </Flex>
      </BrandedBox>

      <Box pt={7} pb={3} display="table" color="white">
        {segment.sections.map((section, idx) => (
          <Fragment key={section.id}>
            <Section section={section} />
            {idx + 1 !== segment.sections.length && segment.stops[idx] && (
              <Stopover {...segment.stops[idx]} />
            )}
          </Fragment>
        ))}
      </Box>
      {segmentHasAncillariesIncluded && (
        <Box pb={6} ml={15}>
          {segment.baggageCondition > 0 && (
            <AncillariesInfo type={'baggage'} num={segment.baggageCondition} />
          )}
          {segment.seatsCondition > 0 && (
            <AncillariesInfo type={'seats'} num={segment.seatsCondition} />
          )}
        </Box>
      )}
    </ShadowBox>
  );
};

Segment.propTypes = {
  segment: PropTypes.object.isRequired,
  indx: PropTypes.number.isRequired,
  tripType: PropTypes.string.isRequired,
};

export default Segment;
