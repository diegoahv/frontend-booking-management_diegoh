import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from 'utils/test-utils';

import ConnectionChangeType from '../';

const AIRPORT_CHANGE_TEXT = 'Changing airports is your responsibility';
const TERMINAL_CHANGE_TEXT = 'Change of terminal';

describe('ConnectionChangeType', () => {
  test('No change', () => {
    const { container } = render(<ConnectionChangeType type="none" />);
    expect(container).toHaveTextContent('');
  });
  test('Change of airport', () => {
    const { getByText } = render(<ConnectionChangeType type="airport" />);
    expect(getByText(AIRPORT_CHANGE_TEXT)).toBeVisible();
  });

  test('Change of terminal', () => {
    const { getByText } = render(<ConnectionChangeType type="terminal" />);
    expect(getByText(TERMINAL_CHANGE_TEXT)).toBeVisible();
  });
});
