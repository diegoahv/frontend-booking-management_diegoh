import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select } from '@storybook/addon-knobs';

import ConnectionChangeType from '.';

const types = ['airport', 'terminal', 'none'];

storiesOf('OpenTicket/Trip/ConnectionChangeType', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const type = select('Type', types, types[0]);

    return <ConnectionChangeType type={type} />;
  });
