import React from 'react';
import { useTranslation } from 'react-i18next';
import { Flex, Box, Text, ManBaggageWalkingIcon } from 'prisma-design-system';
import PropTypes from 'prop-types';

const ConnectionChangeType = ({ type }) => {
  const { t } = useTranslation();

  return (
    type !== 'none' && (
      <Flex pb={1} alignItems="center">
        <Box pr={2}>
          <ManBaggageWalkingIcon size="small" />
        </Box>
        <Flex flexWrap="wrap">
          <Text>{t(`trip:stop.${type}Change`)}</Text>
        </Flex>
      </Flex>
    )
  );
};

ConnectionChangeType.propTypes = {
  type: PropTypes.oneOf(['airport', 'terminal', 'none']),
};

ConnectionChangeType.defaultProps = {
  type: 'none',
};

export default ConnectionChangeType;
