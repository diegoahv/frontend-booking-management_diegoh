import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Box, Text, DecorationLine, TimeIcon } from 'prisma-design-system';
import { useTranslation } from 'react-i18next';
import { minutesToHoursAndMinutes } from 'utils/DateUtils';
import styled from '@emotion/styled';
import css from '@styled-system/css';

const StyledTitle = styled(Text)(
  css({
    fontSize: 'body.2',
    fontWeight: 'medium',
  })
);

const StyledText = styled(Text)(
  css({
    fontSize: 'body.1',
    color: 'informative.dark',
  })
);

const InformativeBox = styled(Box)(
  css({
    backgroundColor: 'informative.lightest',
    '& div[type="circledDot"]': {
      top: '20px',
      borderColor: 'informative.hard',
      '&:after': {
        backgroundColor: 'informative.hard',
      },
    },
  })
);

const TechnicalStopover = ({ duration }) => {
  const { t } = useTranslation();
  const { hours, minutes } = minutesToHoursAndMinutes(duration);

  const tripDurationText = `${t(
    'trip:stop.duration'
  )}: ${t('results:segment.duration', { hours, minutes })}`;

  return (
    <Fragment>
      <InformativeBox display="table-row">
        <Box display="table-cell" pr={4} />
        <DecorationLine decorationType="circledDot" lineType="dashed" />
        <Box display="table-cell" pl={4} py={3} pr={2}>
          <Box mb={1}>
            <StyledTitle priority={4}>{tripDurationText}</StyledTitle>
          </Box>
          <StyledText>
            <TimeIcon size="small" /> {t('trip:stop.technical')}
          </StyledText>
        </Box>
      </InformativeBox>
      <Box display="table-row">
        <Box display="table-cell" pr={4} pt={6} />
        <DecorationLine decorationType="none" lineType="dashed" />
      </Box>
    </Fragment>
  );
};

TechnicalStopover.propTypes = {
  duration: PropTypes.number.isRequired,
};

export default TechnicalStopover;
