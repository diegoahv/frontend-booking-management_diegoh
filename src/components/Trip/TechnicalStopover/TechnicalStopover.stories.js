import React from 'react';
import { storiesOf } from '@storybook/react';
import { number, withKnobs } from '@storybook/addon-knobs';

import TechnicalStopover from './';

storiesOf('OpenTicket/Trip/TechnicalStopover', module)
  .addDecorator(withKnobs)
  .add('Default', () => {
    const durationInMinutes = number('Duration in minutes', 999);

    return <TechnicalStopover duration={durationInMinutes} />;
  });
