import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from 'utils/test-utils';

import TechnicalStopover from '../.';

describe('SectionTechnicalStop', () => {
  test('Technical stop duration is visible', () => {
    const duration = 180;

    const { getByText } = render(<TechnicalStopover duration={duration} />);

    expect(getByText(`Stop duration: 3h 00'`)).toBeVisible();
  });
});
