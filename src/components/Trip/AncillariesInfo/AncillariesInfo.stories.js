import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select, number } from '@storybook/addon-knobs';

import AncillariesInfo from '.';

const types = ['seats', 'baggage', 'none'];

storiesOf('OpenTicket/Trip/AncillariesInfo', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const type = select('Type', types, types[0]);
    const num = number('Number of ancillaries', 1);

    return <AncillariesInfo type={type} num={num} />;
  });
