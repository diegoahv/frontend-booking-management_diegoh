import React from 'react';
import {
  Text,
  SeatIcon,
  BaggageLargeIcon,
  Flex,
  Box,
} from 'prisma-design-system';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

function AncillariesInfo({ type, num }) {
  const { t } = useTranslation();
  return (
    type !== 'none' &&
    num > 0 && (
      <Flex alignItems="center" pb={1}>
        {type === 'seats' && <SeatIcon color="informative.darkest" />}
        {type === 'baggage' && <BaggageLargeIcon color="informative.darkest" />}
        <Box pl={1}>
          <Text priority={5}>{`${num} ${t(`trip:${type}`)}`}</Text>
        </Box>
      </Flex>
    )
  );
}

AncillariesInfo.propTypes = {
  type: PropTypes.oneOf(['seats', 'baggage', 'none']),
  num: PropTypes.number,
};
AncillariesInfo.defaultProps = {
  type: 'none',
};

export default AncillariesInfo;
