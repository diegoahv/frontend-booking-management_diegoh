import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from 'utils/test-utils';

import AncillariesInfo from '../';

const BAGGAGE_LABEL = '2 baggage';
const SEATS_LABEL = '1 seats';

describe('AncillariesInfo', () => {
  test('Shall show baggage information', () => {
    const type = 'baggage';
    const num = 2;
    const { getByText } = render(<AncillariesInfo type={type} num={num} />);

    expect(getByText(BAGGAGE_LABEL)).toBeVisible();
  });

  test('Shall show seats information', () => {
    const type = 'seats';
    const num = 1;
    const { getByText } = render(<AncillariesInfo type={type} num={num} />);

    expect(getByText(SEATS_LABEL)).toBeVisible();
  });

  test('Shall show ancillaries icon', () => {
    const type = 'seats';
    const num = 1;
    const { container } = render(<AncillariesInfo type={type} num={num} />);

    expect(container.querySelectorAll('svg')).toHaveLength(1);
  });
});
