import React from 'react';
import { storiesOf } from '@storybook/react';
import { number, withKnobs, boolean } from '@storybook/addon-knobs';

import DurationWithIcon from './';

storiesOf('OpenTicket/Trip/DurationWithIcon', module)
  .addDecorator(withKnobs)
  .add('Default', () => {
    const duration = number('Duration in minutes', 999);
    const showIcon = boolean('Show icon', true);

    return <DurationWithIcon duration={duration} showIcon={showIcon} />;
  });
