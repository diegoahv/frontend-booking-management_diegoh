import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from 'utils/test-utils';

import DurationWithIcon from '../';

describe('DurationWithIcon', () => {
  test('DurationWithIcon renders with description text', () => {
    const duration = 60;
    const showIcon = true;

    const { container } = render(
      <DurationWithIcon duration={duration} showIcon={showIcon} />
    );
    expect(container.querySelectorAll('svg')).toHaveLength(1);
  });

  test('DurationWithIcon renders without description text', () => {
    const duration = 60;
    const showIcon = false;

    const { container } = render(
      <DurationWithIcon duration={duration} showIcon={showIcon} />
    );
    expect(container.querySelectorAll('svg')).toHaveLength(0);
  });

  test('DurationWithIcon renders with correct duration time', () => {
    const duration = 90;

    const { container, getByText } = render(
      <DurationWithIcon duration={duration} />
    );
    expect(container).toContainElement(getByText("1h 30'"));
  });
});
