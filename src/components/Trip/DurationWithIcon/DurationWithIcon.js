import React from 'react';
import PropTypes from 'prop-types';
import { Box, Flex, TimeIcon } from 'prisma-design-system';
import { minutesToHoursAndMinutes } from 'utils/DateUtils';
import { useTranslation } from 'react-i18next';

function DurationWithIcon({ duration, showIcon }) {
  const { t } = useTranslation();

  if (!duration && duration !== 0) return false;

  const { hours, minutes } = minutesToHoursAndMinutes(duration);

  return (
    <Flex flexDirection="row" alignItems="center">
      <Box mr={1}>{showIcon && <TimeIcon size="small" />}</Box>

      {t('results:segment.duration', { hours, minutes })}
    </Flex>
  );
}

DurationWithIcon.propTypes = {
  durationInMinutes: PropTypes.number,
  showIcon: PropTypes.bool,
};

DurationWithIcon.defaultProps = {
  type: 'BUSINESS',
  showIcon: false,
};

export default DurationWithIcon;
