import React from 'react';
import '@testing-library/jest-dom/extend-expect';

import SectionLocation from '../SectionLocation';
import { render } from 'utils/test-utils';

describe('SectionLocation', () => {
  test('Check if iata + name + terminal string shows correct when not exist terminal', () => {
    const name = 'Heathrow';
    const terminal = '';
    const cityName = 'Lodon';
    const countryName = 'United Kingdom';
    const iata = 'LHR';

    const { getByText } = render(
      <SectionLocation
        name={name}
        terminal={terminal}
        cityName={cityName}
        countryName={countryName}
        iata={iata}
      />
    );

    expect(getByText(`(${iata}) ${name}`)).toBeVisible();
  });
  test('Check if iata + name + terminal string shows correct when exist terminal', () => {
    const name = 'Heathrow';
    const terminal = '3';
    const cityName = 'Lodon';
    const countryName = 'United Kingdom';
    const iata = 'LHR';

    const { getByText } = render(
      <SectionLocation
        name={name}
        terminal={terminal}
        cityName={cityName}
        countryName={countryName}
        iata={iata}
      />
    );

    expect(getByText(`(${iata}) ${name}, T${terminal}`)).toBeVisible();
  });
  test('Check if cityName string shows correct', () => {
    const name = 'Heathrow';
    const terminal = '3';
    const cityName = 'Lodon';
    const countryName = 'United Kingdom';

    const { getByText } = render(
      <SectionLocation
        name={name}
        terminal={terminal}
        cityName={cityName}
        countryName={countryName}
      />
    );

    expect(getByText(`${cityName}, ${countryName}`)).toBeVisible();
  });
});
