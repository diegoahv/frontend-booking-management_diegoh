import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';

import SectionLocation from './SectionLocation';

storiesOf('OpenTicket/Trip/SectionLocation', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const name = text('Name', 'Heathrow');
    const terminal = text('Terminal', '3');
    const cityName = text('City name', 'London');
    const countryName = text('Country name', 'United Kingdom');
    const iata = text('Iata', 'LHR');

    return (
      <SectionLocation
        name={name}
        terminal={terminal}
        cityName={cityName}
        countryName={countryName}
        iata={iata}
      />
    );
  });
