import React from 'react';
import PropTypes from 'prop-types';

import { Box, Flex, Text } from 'prisma-design-system';
import styled from '@emotion/styled';
import css from '@styled-system/css';

const StyledText = styled(Text)(
  css({
    fontSize: 'body.2',
    fontWeight: 'medium',
  })
);

const SectionLocation = ({ name, terminal, cityName, countryName, iata }) => {
  const nameAndTerminal =
    name + (terminal && terminal.trim() !== '' ? `, T${terminal}` : '');

  return (
    <Flex flexDirection="column" pb={6} pr={2}>
      <Box pb={1}>
        <StyledText priority={4}>
          ({iata}) {nameAndTerminal}
        </StyledText>
      </Box>
      <Box>
        <Text priority={5}>
          {cityName}, {countryName}
        </Text>
      </Box>
    </Flex>
  );
};

SectionLocation.propTypes = {
  name: PropTypes.string,
  cityName: PropTypes.string,
  terminal: PropTypes.string,
  countryName: PropTypes.string,
};

export default SectionLocation;
