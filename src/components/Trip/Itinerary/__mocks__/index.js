import { Barajas, ElPrat, Gatwick, Zurich } from 'serverMocks/Location';
import { Vueling, Iberia } from 'serverMocks/Carrier';

const location = (type) => (location) => ({
  ...location,
  type,
  terminal: '1',
});

const connection = location('connection');
const absoluteLocation = location('absolute');

const trip = (withTechnicalStop, withStops) => {
  const trips = [];
  if (withTechnicalStop) {
    trips.push(
      {
        id: 0,
        carrier: Vueling(),
        operatingCarrier: Vueling(),
        departure: absoluteLocation(Barajas()),
        destination: connection(Zurich()),
        cabinClass: 'ECONOMIC_DISCOUNTED',
        departureDate: '2020-04-29T06:30:00+01:00',
        arrivalDate: '2020-04-29T07:15:33+01:00',
        duration: 75,
        flightNumber: 'BR2654',
      },
      {
        id: 1,
        carrier: Vueling(),
        operatingCarrier: Vueling(),
        departure: connection(Zurich()),
        destination: location(withStops ? 'connection' : 'absolute')(ElPrat()),
        cabinClass: 'ECONOMIC_DISCOUNTED',
        departureDate: '2020-04-29T07:40:00+01:00',
        arrivalDate: '2020-04-29T09:45:33+01:00',
        duration: 125,
        flightNumber: 'BR2654',
      }
    );
  } else {
    trips.push({
      id: 0,
      carrier: Vueling(),
      operatingCarrier: Vueling(),
      departure: connection(Barajas()),
      destination: location(withStops ? 'connection' : 'absolute')(ElPrat()),
      cabinClass: 'ECONOMIC_DISCOUNTED',
      departureDate: '2020-04-29T10:30:00+01:00',
      arrivalDate: '2020-04-29T11:45:33+01:00',
      duration: 999,
      vehicleModel: '320',
      flightNumber: 'AD2654',
    });
  }
  return trips;
};

const section1 = (technicalStops, withStopovers) => {
  return {
    id: 1,
    departureDate: '2020-04-29T06:30:00+01:00',
    arrivalDate: '2020-04-29T09:45:33+01:00',
    departure: Barajas(),
    destination: ElPrat(),
    carrier: Vueling(),
    trips: trip(technicalStops.length > 0, withStopovers),
    technicalStops,
    flightNumber: 'TP123',
    bookingClass: 'A',
    fareBasisId: '6DYY5L',
  };
};

const section2 = () => {
  return {
    id: 2,
    departureDate: '2020-04-29T10:30:00+01:00',
    arrivalDate: '2020-04-29T11:45:33+01:00',
    departure: connection(ElPrat()),
    destination: absoluteLocation(Gatwick()),
    carrier: Vueling(),
    trips: [
      {
        id: 0,
        carrier: Iberia(),
        operatingCarrier: Iberia(),
        departure: connection(ElPrat()),
        destination: absoluteLocation(Gatwick()),
        cabinClass: 'ECONOMIC_DISCOUNTED',
        departureDate: '2020-04-29T10:30:00+01:00',
        arrivalDate: '2020-04-29T11:45:33+01:00',
        duration: 999,
        vehicleModel: '320',
        flightNumber: 'AD2654',
      },
    ],
    flightNumber: 'TP265',
    bookingClass: 'A',
    fareBasisId: '6DYU3O',
    technicalStops: [],
  };
};

const technicalStop = {
  location: Zurich(),
  arrivalDate: '2020-04-29T07:15:00+01:00',
  departureDate: '2020-04-29T07:40:00+01:00',
  duration: 25,
};

const sections = (withTechnicalStopovers, withStopovers) => {
  const sections = [
    section1(withTechnicalStopovers ? [technicalStop] : [], withStopovers),
  ];
  if (withStopovers) sections.push(section2());
  return sections;
};

const Segment = (sections, baggageCondition = 1, seatsCondition = 1) => {
  return {
    sections,
    stops:
      sections.length > 1
        ? [
            {
              duration: 75,
              locationChangeType: 'airport',
              carrierChange: true,
              insuranceOffer: {
                policy: 'CARRIER_GUARANTEE',
                carrier: Vueling(),
              },
            },
          ]
        : [],
    duration: 250,
    baggageCondition,
    seatsCondition,
    carriers: [Vueling()],
    departure: absoluteLocation(ElPrat()),
    destination: absoluteLocation(Gatwick()),
    departureDate: '2020-04-29T07:15:00+01:00',
    arrivalDate: '2020-04-29T11:45:33+01:00',
  };
};

export default (
  length,
  withStopovers,
  withTechnicalStopovers,
  baggageCondition,
  seatsCondition
) => {
  return Array.from({ length }, () =>
    Segment(
      sections(withTechnicalStopovers, withStopovers),
      baggageCondition,
      seatsCondition
    )
  );
};
