import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from 'utils/test-utils';
import generateSegments from '../__mocks__';
import { ItineraryMocks } from 'serverMocks/Itinerary';
import { Barajas, ElPrat, Gatwick, Zurich } from 'serverMocks/Location';
import { Vueling } from 'serverMocks/Carrier';

import Itinerary from '../';
import { tripTypes } from '@/src/common/utils/enums';

const expectsAbsolutesLocationToHaveLength = (queryAllByText, length) => {
  expect(
    queryAllByText(`(${Barajas().iata}) ${Barajas().name}, T1`)
  ).toHaveLength(length);
  expect(
    queryAllByText(`${Barajas().cityName}, ${Barajas().countryName}`)
  ).toHaveLength(length);
  expect(
    queryAllByText(`(${ElPrat().iata}) ${ElPrat().name}, T1`)
  ).toHaveLength(length);
  expect(
    queryAllByText(`${ElPrat().cityName}, ${ElPrat().countryName}`)
  ).toHaveLength(length);
  expect(queryAllByText(Vueling().name, { exact: false })).toHaveLength(length);
};

const DEPARTURE_LABEL = 'DEPARTURE';
const RETURN_LABEL = 'RETURN';
const TECHNICAL_STOP_LABEL = 'Technical stopover';

const getItineraryMock = (tripType, withStopovers, withTechnicalStopovers) => {
  const segments = generateSegments(
    tripType.length,
    withStopovers,
    withTechnicalStopovers
  );
  const { ItineraryWith } = ItineraryMocks();
  return ItineraryWith({
    segments: segments,
  });
};

describe('Itinerary tests', () => {
  describe('Segments display', () => {
    describe('Round Trip Type', () => {
      let itinerary;
      beforeEach(() => {
        itinerary = getItineraryMock(tripTypes.ROUND_TRIP, false, false);
      });
      test('Segment title to be shown', () => {
        const { getByText, queryAllByText } = render(
          <Itinerary
            itinerary={itinerary}
            tripType={tripTypes.ROUND_TRIP.type}
          />
        );

        expect(getByText(DEPARTURE_LABEL)).toBeVisible();
        expect(getByText(RETURN_LABEL)).toBeVisible();
        expectsAbsolutesLocationToHaveLength(queryAllByText, 2);
      });

      test('Duration to be shown in each segment', () => {
        const { queryAllByText } = render(
          <Itinerary
            itinerary={itinerary}
            tripType={tripTypes.ROUND_TRIP.type}
          />
        );

        expect(queryAllByText("16h 39'")).toHaveLength(2);
        expectsAbsolutesLocationToHaveLength(queryAllByText, 2);
      });
    });

    describe('One Way Trip Type', () => {
      test('Section without stops', () => {
        const { queryAllByText } = render(
          <Itinerary
            itinerary={getItineraryMock(tripTypes.ONE_WAY, false, false)}
            tripType={tripTypes.ONE_WAY.type}
          />
        );

        expectsAbsolutesLocationToHaveLength(queryAllByText, 1);
      });

      test('Section with technical stops', () => {
        const { getByText, queryAllByText } = render(
          <Itinerary
            itinerary={getItineraryMock(tripTypes.ONE_WAY, false, true)}
            tripType={tripTypes.ONE_WAY.type}
          />
        );

        expect(getByText("Stop duration: 0h 25'")).toBeVisible();
        expect(getByText(TECHNICAL_STOP_LABEL)).toBeVisible();
        expect(
          getByText(`(${Barajas().iata}) ${Barajas().name}, T1`)
        ).toBeVisible();
        expect(
          getByText(`(${ElPrat().iata}) ${ElPrat().name}, T1`)
        ).toBeVisible();
        expect(
          queryAllByText(`(${Zurich().iata}) ${Zurich().name}, T1`)
        ).toHaveLength(2);
      });

      test('Segment with stops', () => {
        const { getByText, queryAllByText } = render(
          <Itinerary
            itinerary={getItineraryMock(tripTypes.ONE_WAY, true, true)}
            tripType={tripTypes.ONE_WAY.type}
          />
        );

        expect(getByText("1h 15'")).toBeVisible();
        expect(
          getByText(`Connection covered by ${Vueling().name}`)
        ).toBeVisible();
        expect(
          getByText(`(${Barajas().iata}) ${Barajas().name}, T1`)
        ).toBeVisible();
        expect(
          getByText(`(${Gatwick().iata}) ${Gatwick().name}, T1`)
        ).toBeVisible();
        expect(
          queryAllByText(`(${ElPrat().iata}) ${ElPrat().name}, T1`)
        ).toHaveLength(2);
      });
    });
  });
});
