import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from 'prisma-design-system';
import Segment from '../Segment';

const Itinerary = ({ itinerary, tripType }) => {
  const getSelectedSegments = (itinerary) => {
    return itinerary.legs.reduce(
      (selectedSegments, { segments }) => selectedSegments.concat(segments),
      []
    );
  };

  return (
    <Box bgColor="white">
      <Flex flexDirection="column" p={3} mb={7}>
        {getSelectedSegments(itinerary).map((segment, idx) => (
          <Fragment key={idx}>
            <Segment indx={idx} segment={segment} tripType={tripType} />
          </Fragment>
        ))}
      </Flex>
    </Box>
  );
};

Itinerary.propTypes = {
  itinerary: PropTypes.object.isRequired,
};

export default Itinerary;
