import React from 'react';
import { storiesOf } from '@storybook/react';
import {
  withKnobs,
  select,
  boolean,
  text,
  number,
} from '@storybook/addon-knobs';
import generateSegments from './__mocks__';
import { ItineraryMocks } from 'serverMocks/Itinerary';
import { tripTypes } from '@/src/common/utils/enums';

import Itinerary from './Itinerary';

storiesOf('OpenTicket/Trip/Itinerary', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const tripType = select(
      'Trip type',
      Object.keys(tripTypes),
      tripTypes.ONE_WAY.type
    );
    const withStopovers = boolean('With stops', true);
    const withTechnicalStopovers = boolean('With technical stops', true);
    const bookingId = text('BookingId', '12345678');
    const baggageCondition = number('Baggage', 1);
    const seatsCondition = number('Seats', 1);

    const segments = generateSegments(
      tripTypes[tripType].length,
      withStopovers,
      withTechnicalStopovers,
      baggageCondition,
      seatsCondition
    );
    const { ItineraryWith } = ItineraryMocks();
    const itinerary = ItineraryWith({
      segments: segments,
    });
    return (
      <Itinerary
        itinerary={itinerary}
        tripType={tripTypes[tripType].type}
        bookingId={bookingId}
      />
    );
  });
