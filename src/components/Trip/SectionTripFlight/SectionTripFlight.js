import React from 'react';
import { Box, DecorationLine, Text, Flex } from 'prisma-design-system';
import DurationWithIcon from '../DurationWithIcon';
import CabinClass from '../CabinClass';
import CarrierFlightsummary from '../../Carrier/CarrierFlightsummary';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import css from '@styled-system/css';

const CustomBox = styled(Box)(
  css({
    fontSize: 'body.0',
    borderRadius: '4',
  })
);

const InformativeBox = styled(CustomBox)(
  css({
    backgroundColor: 'informative.lightest',
    color: 'informative.darkest',
  })
);

const GreyBox = styled(CustomBox)(
  css({
    backgroundColor: 'neutrals.100',
    color: 'neutrals.500',
  })
);

const CarrierFlightsummaryRow = styled(Flex)(
  css({
    zIndex: 'tooltip',
    marginLeft: '-26px',
    '& img': {
      width: '20px',
    },
  })
);

const SectionTripFlight = ({ carrier, flightCode, duration, cabinClass }) => {
  return (
    <Box display="table-row">
      <Box display="table-cell" pr={4} />
      <DecorationLine decorationType="none" lineType="solid" />
      <Box display="table-cell" pl={4} pb={6}>
        <Text priority={5}>
          <Flex flexDirection="column">
            <CarrierFlightsummaryRow flexDirection="row" alignItems="center">
              <CarrierFlightsummary
                carriers={[carrier]}
                flightCode={flightCode}
              />
            </CarrierFlightsummaryRow>
            <Flex flexDirection="row" alignItems="center" pt={1} ml={-1}>
              <InformativeBox p={1} mr={1}>
                <DurationWithIcon duration={duration} />
              </InformativeBox>
              {cabinClass && (
                <GreyBox p={1}>
                  <CabinClass type={cabinClass} />
                </GreyBox>
              )}
            </Flex>
          </Flex>
        </Text>
      </Box>
    </Box>
  );
};

SectionTripFlight.propTypes = {
  carrier: PropTypes.object.isRequired,
  flightCode: PropTypes.string.isRequired,
  duration: PropTypes.number.isRequired,
  vehicleModel: PropTypes.string,
  cabinClass: PropTypes.string,
};

export default SectionTripFlight;
