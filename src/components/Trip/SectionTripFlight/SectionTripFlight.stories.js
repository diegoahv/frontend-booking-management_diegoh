import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, number, select } from '@storybook/addon-knobs';
import selectCarrier from 'decorators/selectCarrier';

import SectionTripFlight from './SectionTripFlight';

storiesOf('OpenTicket/Trip/SectionTripFlight', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const carrier = selectCarrier('Carrier');
    const flightCode = text('Flight Code', 'BR2654');
    const duration = number('Duration', 99);
    const cabinClass = select(
      'Cabin Class',
      [
        'BUSINESS',
        'ECONOMIC_DISCOUNTED',
        'FIRST',
        'PREMIUM_ECONOMY',
        'TOURIST',
        'none',
      ],
      'none'
    );

    return (
      <SectionTripFlight
        carrier={carrier}
        flightCode={flightCode}
        duration={duration}
        cabinClass={cabinClass}
      />
    );
  });
