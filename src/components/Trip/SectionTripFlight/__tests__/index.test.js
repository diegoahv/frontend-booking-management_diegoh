import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from 'utils/test-utils';
import { Vueling } from 'serverMocks/Carrier';

import SectionTripFlight from '../';

const ECONOMY_LABEL = 'Economy';

describe('SectionTripFlight', () => {
  test('Shall show flight information', () => {
    const flightCode = 'AB123';
    const { getByText } = render(
      <SectionTripFlight
        carrier={Vueling()}
        flightCode={flightCode}
        duration={70}
        cabinClass="ECONOMIC_DISCOUNTED"
      />
    );

    expect(getByText("1h 10'")).toBeVisible();
    expect(getByText(ECONOMY_LABEL)).toBeVisible();
    expect(getByText(`${Vueling().name} · ${flightCode}`)).toBeVisible();
  });
});
