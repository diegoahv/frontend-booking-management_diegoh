import React from 'react';
import { number, select, withKnobs } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import FooterWithPrice from './FooterWithPrice';

const selectPrice = (label = '') => ({
  price: number(`${label} price amount:`, 164.9),
  currency: select('currency', ['USD', 'EUR'], 'USD'),
});

storiesOf('OpenTicket/Trip/FooterWithPrice', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    return <FooterWithPrice price={selectPrice()} />;
  });
