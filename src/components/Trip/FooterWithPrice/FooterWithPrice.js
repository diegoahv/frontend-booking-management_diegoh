import React from 'react';
import { Flex, Button, Text, LockerIcon } from 'prisma-design-system';
import ItineraryPrice from '../../Results/ItineraryPrice';
import { useTranslation } from 'react-i18next';
import styled from '@emotion/styled';
import css from '@styled-system/css';
import { variant } from 'styled-system';
import LoadingSpinner from '@/src/common/components/LoadingSpinner';

const StyledButton = styled(Button)(
  css({
    backgroundColor: 'brandPrimary.2',
    borderColor: 'brandPrimary.2',
    borderRadius: '22px',
  })
);

const StyledFlex = styled(Flex)(
  css({
    position: 'relative',
    zIndex: 'popover',
    marginTop: 'auto',
    boxShadow: 0,
    backgroundColor: 'white',
  }),
  variant({
    variants: {
      fixed: {
        position: 'fixed',
        bg: 'white',
        bottom: 0,
        left: 0,
      },
    },
  })
);

const SecurePaymentWrapper = styled(Flex)(
  variant({
    prop: 'securePaymentText',
    variants: {
      hidden: {
        display: 'none',
      },
    },
  })
);

const FooterWithPrice = ({
  price,
  onClick,
  buttonText,
  securePaymentText = 'hidden',
  variant = 'relative',
  loading,
}) => {
  const { t } = useTranslation();
  const isPrice = Boolean(price);
  return (
    <StyledFlex
      flexDirection="row"
      justifyContent="space-between"
      width="100%"
      p={5}
      variant={variant}
      data-testid="footerWithPrice"
    >
      {isPrice && (
        <Flex flexDirection="column" justifyContent="center">
          <ItineraryPrice
            currency={price.currency}
            amount={price.priceDifference}
            align="flex-start"
            text={t('myinfo:flightitinerary.totalprice')}
          />
        </Flex>
      )}
      <Flex flexDirection="column" flexGrow={isPrice ? 0 : 1}>
        <StyledButton
          type="primary"
          size="medium"
          onClick={onClick}
          fullWidth={!isPrice}
        >
          {loading && <LoadingSpinner color="white" />}
          {!loading &&
            (buttonText ? buttonText : t('myinfo:flightitinerary.continueBtn'))}
        </StyledButton>
        <SecurePaymentWrapper
          flexWrap={'nowrap'}
          pt={4}
          alignItems={'center'}
          justifyContent={'center'}
          securePaymentText={securePaymentText}
        >
          <Text textAlign="center">
            <LockerIcon color={'neutrals.0'} size={'small'} mr={5} />
            &nbsp;{t('myinfo:flightitinerary.thisbookingis')}&nbsp;
            <Text fontWeight={'medium'}>
              {t('myinfo:flightitinerary.secureandecrypted')}
            </Text>
          </Text>
        </SecurePaymentWrapper>
      </Flex>
    </StyledFlex>
  );
};
export default FooterWithPrice;
