import React from 'react';
import { DateTime, Flex, Text, Box } from 'prisma-design-system';
import styled from '@emotion/styled';
import css from '@styled-system/css';

const StyledText = styled(Text)(
  css({
    fontSize: 'body.2',
    fontWeight: 'medium',
  })
);

const SectionDateTime = ({ dateTime }) => (
  <Flex flexDirection="column" alignItems="center" pl={3}>
    <Box pb={1}>
      <StyledText priority={4}>
        <DateTime value={dateTime} type="time" pattern="short" />
      </StyledText>
    </Box>
    <Text priority={5}>
      <DateTime value={dateTime} format="d MMM" />
    </Text>
  </Flex>
);

export default SectionDateTime;
