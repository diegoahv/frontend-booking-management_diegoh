import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import SectionDateTime from '../';
import { render } from 'utils/test-utils';

describe('SectionDateTime', () => {
  test('formats hours and minutes lower than 10 as expected', () => {
    const { getByText } = render(
      <SectionDateTime dateTime="2020-01-19T09:05:00+07:00" />
    );

    expect(getByText('09:05')).toBeVisible();
  });

  test('formats time and date as expected', () => {
    const { getByText } = render(
      <SectionDateTime dateTime="2020-03-05T22:40:00-05:00" />,
      {
        applicationContext: { application: { session: { locale: 'en-US' } } },
      }
    );

    expect(getByText('22:40')).toBeVisible();
    expect(getByText('5 Mar')).toBeVisible();
  });
});
