import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';

import SectionDateTime from './';

storiesOf('OpenTicket/Trip/SectionDateTime', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const dateTime = text('dateTime', '2018-07-22T23:30:00+02:00');

    return <SectionDateTime dateTime={dateTime} />;
  });
