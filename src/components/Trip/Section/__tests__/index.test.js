import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from 'utils/test-utils';
import generateSegments from '../../Itinerary/__mocks__';
import { Barajas, ElPrat } from 'serverMocks/Location';
import Section from '../';

const DEPARTURE_NAME = `(${Barajas().iata}) ${Barajas().name}, T1`;
const DEPARTURE_CITY = `${Barajas().cityName}, ${Barajas().countryName}`;
const DEPARTURE_HOUR = '10:30';
const ARRIVAL_NAME = `(${ElPrat().iata}) ${ElPrat().name}, T1`;
const ARRIVAL_CITY = `${ElPrat().cityName}, ${ElPrat().countryName}`;
const ARRIVAL_HOUR = '11:45';
const CARRIER = 'Vueling · AD2654';

describe('Section tests', () => {
  test('Shall show Section with departure and arrival correct information', () => {
    const withStopovers = false;
    const withTechnicalStopovers = false;
    const segment = generateSegments(
      1,
      withStopovers,
      withTechnicalStopovers
    )[0];

    const { getByText } = render(<Section section={segment.sections[0]} />);

    expect(getByText(DEPARTURE_NAME)).toBeVisible();
    expect(getByText(DEPARTURE_CITY)).toBeVisible();
    expect(getByText(DEPARTURE_HOUR)).toBeVisible();
    expect(getByText(CARRIER)).toBeVisible();
    expect(getByText(ARRIVAL_NAME)).toBeVisible();
    expect(getByText(ARRIVAL_CITY)).toBeVisible();
    expect(getByText(ARRIVAL_HOUR)).toBeVisible();
  });
});
