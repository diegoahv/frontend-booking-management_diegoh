import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import SectionTripEvent from '../SectionTripEvent';
import SectionTripFlight from '../SectionTripFlight';
import TechnicalStopover from '../TechnicalStopover';

const SectionTrip = ({ trip }) => {
  const {
    departure,
    departureDate,
    destination,
    arrivalDate,
    duration,
    flightNumber,
    carrier,
    cabinClass,
  } = trip;
  const departureType =
    trip.departure.type === 'absolute' ? 'departure' : 'connectionDeparture';
  const arrivalType =
    trip.destination.type === 'absolute' ? 'arrival' : 'connectionArrival';
  return (
    <Fragment>
      <SectionTripEvent
        location={departure}
        dateTime={departureDate}
        type={departureType}
      />
      <SectionTripFlight
        duration={duration}
        flightCode={flightNumber}
        carrier={carrier}
        cabinClass={cabinClass}
      />
      <SectionTripEvent
        location={destination}
        dateTime={arrivalDate}
        type={arrivalType}
      />
    </Fragment>
  );
};

const Section = ({ section }) => {
  return (
    <Fragment>
      {section.trips.map((trip, idx) => (
        <Fragment key={trip.id}>
          <SectionTrip trip={trip} />
          {idx + 1 !== section.trips.length && section.technicalStops[idx] && (
            <TechnicalStopover
              duration={section.technicalStops[idx].duration}
            />
          )}
        </Fragment>
      ))}
    </Fragment>
  );
};

Section.propTypes = {
  section: PropTypes.object.isRequired,
};

export default Section;
