import { React, useState, Fragment } from 'react';
import { boolean, text, withKnobs } from '@storybook/addon-knobs';
import { mockEndpoint, autocompleteResponse } from 'serverMocks';
import { FlightIcon } from 'prisma-design-system';

import Autocomplete from './Autocomplete';

export default {
  title: 'OpenTicket/Search/Autocomplete',
  component: Autocomplete,
  decorators: [withKnobs],
};

mockEndpoint({
  method: 'get',
  url:
    'service/geo/autocomplete;searchWord=osa;departureOrArrival=DEPARTURE;addSearchByCountry=true;addSearchByRegion=true;nearestLocations=true;product=FLIGHT',
  data: autocompleteResponse,
});

export const Default = () => {
  const [departureData, setDepartureData] = useState({});
  const showSelection = (data = {}) => {
    return (
      <div>
        <div>Mocked examples: 'Osaka'</div>
        <p>
          Selected data: {data.iata} {data.name} {data.geoNodeId} {data.type}
        </p>
      </div>
    );
  };
  let error = '';
  const hasError = boolean('Has error', true);
  if (hasError) {
    error = text('Error', 'Please select a valid option');
  }
  return (
    <Fragment>
      <pre>{showSelection(departureData)}</pre>
      <Autocomplete
        setLocation={setDepartureData}
        label="Origin"
        icon={FlightIcon}
        errorMeddage="Please select a valid value"
        setError={() => {}}
        error={error}
      />
    </Fragment>
  );
};
