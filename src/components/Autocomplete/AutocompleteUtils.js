import { escapeRegExp } from '../../utils/StringUtils';

export const matchesLocationCriteria = (location, inputValue) => {
  const regexData = new RegExp(escapeRegExp(inputValue), 'i');
  return (
    regexData.test(location.name) ||
    regexData.test(location.city) ||
    regexData.test(location.iata) ||
    regexData.test(location.nearestCity)
  );
};

export const setupLocationsData = (locationsData = []) => {
  if (!Array.isArray(locationsData)) return [];

  return locationsData.reduce((locationsList, location) => {
    if (location.type === 'CITY') {
      return locationsList.concat([{ ...location }]).concat(
        location.relatedLocations.map((city) => ({
          ...city,
          nearestCity: location.city,
          subCategory: true,
        }))
      );
    } else if (location.relatedLocations.length > 0) {
      return locationsList.concat(
        location.relatedLocations.map((city) => ({
          ...city,
          nearestCity: location.city,
          subCategory: false,
        }))
      );
    } else {
      return locationsList;
    }
  }, []);
};

export const getLocationFormatted = (location, t) => {
  return location.type === 'CITY'
    ? getCityFormatted(location, t)
    : getAirportFormatted(location, t);
};

const getCityFormatted = (location, t) => {
  const hasLocations = location.relatedLocations.length;
  const title = `${location.iata} - ${location.city}${
    hasLocations ? ` - ${t('flightsManager:allAirports')}` : ''
  }`;
  const subtitle = location.country;
  return { title, subtitle };
};
const getAirportFormatted = (location, t) => {
  const title = `${location.iata} - ${location.name}`;
  const subtitle = t('airportSelector:relatedDistance', {
    distance: `${location.distance} ${location.distanceUnit}`,
    city: location.nearestCity,
  });
  return {
    title,
    subtitle,
  };
};
