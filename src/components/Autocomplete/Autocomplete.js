import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Downshift from 'downshift';
import Location from './AutocompleteItem/AutocompleteItem';
import LoadingItem from './LoadingItem';
import AutocompleteIcon from './AutocompleteIcon/AutocompleteIcon';
import TextInput from '../../common/components/TextInput';
import { useTranslation } from 'react-i18next';
import {
  matchesLocationCriteria,
  setupLocationsData,
  getLocationFormatted,
} from './AutocompleteUtils';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import css from '@styled-system/css';
import styled from '@emotion/styled';

// todo: change between departure and arrival
const getLocationItemsService = (inputValue) =>
  `/service/geo/autocomplete;searchWord=${inputValue};departureOrArrival=DEPARTURE;addSearchByCountry=true;addSearchByRegion=true;nearestLocations=true;product=FLIGHT`;

const LocationList = styled.ul(
  css({
    width: '100%',
    display: 'inline-block',
    pl: 0,
    position: 'absolute',
    zIndex: 'popup',
    boxShadow: 'elevation.popup',
    margin: 1,
  })
);
const ExportedAutoComplete = React.forwardRef(Autocomplete);
ExportedAutoComplete.displayName = 'Autocomplete';

export default ExportedAutoComplete;

function Autocomplete(
  {
    setLocation,
    label,
    icon,
    placeholder,
    initialValue = '',
    selectedValue = '',
    setError,
    error,
  },
  ref
) {
  const [locationItems, setLocationItems] = useState([]);
  const [value, setValue] = useState(initialValue); // Value is the text that is manually typed.
  const [loading, setLoading] = useState(false);
  const [validated, setValidated] = useState(initialValue.length > 0);
  const fetcher = ApplicationContext.useFetcher();
  const { t } = useTranslation();

  const getLocationItems = function (inputValue = '') {
    setLoading(true);
    fetcher
      .get(getLocationItemsService(inputValue.toLowerCase()))
      .then((locationItemsResponse) => {
        setLocationItems(setupLocationsData(locationItemsResponse));
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const emptyLocationItems = () => setLocationItems([]);

  const clearSelection = function () {
    setLocation({});
  };

  const handleInputValueChange = function (inputValue) {
    if (typeof inputValue === 'object') {
      inputValue = parseSelectedLocationToString(inputValue);
    }
    if (value !== inputValue) {
      clearSelection();
      setValidated(false);
    }
    const minimumCharCount = 3;
    const shouldRequestData = inputValue.length === minimumCharCount;
    const shouldHideLocationOptions = inputValue.length < minimumCharCount;
    if (shouldRequestData) {
      getLocationItems(inputValue);
    } else if (shouldHideLocationOptions) {
      emptyLocationItems();
    }
  };

  const onSelection = (selection) => {
    setLocation({
      iata: selection.iata,
      name: selection.name,
      geoNodeId: selection.geoNodeId,
      type: selection.type,
    });
    setValue(parseSelectedLocationToString(selection));
    setValidated(true);
    setError('');
  };

  const parseSelectedLocationToString = (location) =>
    location ? location.name : '';

  const renderNoResults = (inputValue) => {
    if (inputValue !== '') {
      const title = `${t('citySearch:destination_error_text')} ${inputValue}`;
      return (
        <Location
          locationOptions={{
            css: css({
              color: 'brandPrimary.3',
              pl: '2',
            }),
          }}
          icon={AutocompleteIcon('NOTFOUND')}
          title={title}
        />
      );
    } else {
      return null;
    }
  };
  const filterLocationOptions = (locations, inputValue) => {
    return locations.filter(
      (location) => !inputValue || matchesLocationCriteria(location, inputValue)
    );
  };
  const renderLocationOptions = ({
    locationItems,
    inputValue,
    getItemProps,
    highlightedIndex,
  }) => {
    if (inputValue.length >= 3 && loading) {
      return <LoadingItem />;
    }
    if (inputValue.length >= 3 && !locationItems.length) {
      return renderNoResults(inputValue);
    } else {
      return locationItems.map((location, index) => {
        const { title, subtitle } = getLocationFormatted(location, t);
        return (
          <Location
            locationOptions={getItemProps({
              index,
              item: location,
              css: css({
                backgroundColor:
                  highlightedIndex === index ? 'neutrals.6' : 'white',
                pl: location.subCategory ? '6' : '2',
              }),
            })}
            key={index}
            icon={AutocompleteIcon(location.type)}
            title={title}
            subTitle={subtitle}
          >
            {location.name}
          </Location>
        );
      });
    }
  };
  const onInputClick = ({ inputValue, isOpen, openMenu }) => {
    const charCount = 3;
    if (inputValue.length >= charCount && !isOpen) {
      getLocationItems(inputValue.slice(0, charCount).toLowerCase());
      openMenu();
    }
  };

  const onChangeInputText = (e) => {
    setValue(e.target.value);
  };

  const onCleanClick = ({ closeMenu }) => {
    setValue();
    setValidated(false);
    closeMenu();
    clearSelection();
  };

  return (
    <>
      <Downshift
        onInputValueChange={handleInputValueChange}
        onSelect={onSelection}
        itemToString={parseSelectedLocationToString}
        initialInputValue={initialValue}
      >
        {({
          getInputProps,
          getItemProps,
          getMenuProps,
          isOpen,
          openMenu,
          closeMenu,
          inputValue,
          highlightedIndex,
          getRootProps,
        }) => (
          <div style={{ position: 'relative' }}>
            <div {...getRootProps({}, { suppressRefError: true })}>
              <TextInput
                {...getInputProps({
                  onClick: () => onInputClick({ inputValue, isOpen, openMenu }),
                  onChange: onChangeInputText,
                  onCleanClick: () => onCleanClick({ closeMenu }),
                  name: label,
                  placeholder,
                  type: 'text',
                  error,
                  Icon: icon,
                  label,
                  value,
                  validated,
                  onBlur: (e) => {
                    e.preventDefault();
                  },
                  ref,
                })}
              />
            </div>
            <LocationList {...getMenuProps()}>
              {isOpen
                ? renderLocationOptions({
                    locationItems: filterLocationOptions(
                      locationItems,
                      inputValue
                    ),
                    inputValue,
                    getItemProps,
                    highlightedIndex,
                  })
                : null}
            </LocationList>
          </div>
        )}
      </Downshift>
    </>
  );
}

Autocomplete.propTypes = {
  setLocation: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  icon: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
};

Autocomplete.defaultProps = {
  placeholder: '',
};
