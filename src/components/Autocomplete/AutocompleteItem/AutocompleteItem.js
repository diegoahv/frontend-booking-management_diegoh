import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Box, Text } from 'prisma-design-system';
import styled from '@emotion/styled';
import css from '@styled-system/css';

export const Item = styled.li(
  css({
    listStyleType: 'none',
    pr: '4',
    bg: 'white',
    pt: '2',
    pb: '2',
    borderTop: '1',
    borderColor: 'neutrals.6',
    '&:first-of-type': {
      borderTopLeftRadius: '5px',
      borderTopRightRadius: '5px',
    },
    '&:last-of-type': {
      borderBottomLeftRadius: '5px',
      borderBottomRightRadius: '5px',
    },
  })
);

const SubtitleText = styled(Text)(
  css({
    color: 'neutrals.2',
  })
);

export default function AutocompleteItem({
  locationOptions,
  icon,
  title,
  subTitle,
}) {
  return (
    <Item {...locationOptions}>
      <Flex alignItems="center" flexDirection="row" justifyContent="flex-start">
        <Box>{icon}</Box>
        <Box>
          <Flex
            alignItems="flex-start"
            flexDirection="column"
            justifyContent="flex-start"
          >
            <Text>{title}</Text>
            <SubtitleText>{subTitle}</SubtitleText>
          </Flex>
        </Box>
      </Flex>
    </Item>
  );
}

AutocompleteItem.propTypes = {
  icon: PropTypes.element.isRequired,
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string.isRequired,
};

AutocompleteItem.defaultProps = {
  icon: '',
  title: '',
  subTitle: '',
};
