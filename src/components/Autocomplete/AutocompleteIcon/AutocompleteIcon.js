import {
  Box,
  CityIcon,
  FlightRightIcon,
  InterrogationCircleIcon,
} from 'prisma-design-system';
import React from 'react';

export default function AutocompleteIcon(type) {
  const getIconByType = (type) => {
    switch (type) {
      case 'CITY':
        return <CityIcon color="neutrals.3" size="medium" />;
      case 'NOTFOUND':
        return <InterrogationCircleIcon />;
      default:
        return <FlightRightIcon color="neutrals.3" size="medium" />;
    }
  };
  return (
    <>
      <Box mr={3}>{getIconByType(type)}</Box>
    </>
  );
}
