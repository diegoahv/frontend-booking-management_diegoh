import React from 'react';
import { Item as AutocompleteItem } from './AutocompleteItem/AutocompleteItem';
import LoadingSpinner from '@/src/common/components/LoadingSpinner';

function LoadingItem() {
  return (
    <AutocompleteItem>
      <LoadingSpinner />
    </AutocompleteItem>
  );
}

export default LoadingItem;
