import React from 'react';
import { render, fireEvent } from 'test-utils';
import Autocomplete from './Autocomplete';
import { FlightIcon } from 'prisma-design-system';
import { act } from 'react-dom/test-utils';

const UNRESOLVED_PROMISE = new Promise(() => {});
const RESOLVED_PROMISE = Promise.resolve();

const checkComponentRendering = async ({ fetcher, label }) => {
  let renderedComponent;
  renderedComponent = renderAutocomplete({
    fetcher,
    label,
  });
  const { container, getByText } = renderedComponent;
  const icon = container.getElementsByTagName('svg');
  expect(icon).toHaveLength(1);
  expect(container).toContainElement(icon[0]);
  expect(container).toContainElement(getByText(label));
};

describe('Autocomplete component tests', () => {
  it('should render properly', () => {
    checkComponentRendering({
      fetcher: {
        get: () => UNRESOLVED_PROMISE,
      },
      label: 'test',
    });
  });

  it('should request data when number of characters in input equals 3', async () => {
    const fetcherMock = jest.fn();
    fetcherMock.mockReturnValue(RESOLVED_PROMISE);
    const inputValue = 'tes';
    let renderedComponent;
    await act(async () => {
      renderedComponent = renderAutocomplete({
        fetcher: {
          get: fetcherMock,
        },
      });
      const { container } = renderedComponent;
      const input = container.getElementsByTagName('input')[0];

      const event = { target: { value: inputValue } };

      fireEvent.change(input, event);
    });

    expect(fetcherMock).toHaveBeenCalledWith(
      '/service/geo/autocomplete;searchWord=' +
        inputValue +
        ';departureOrArrival=DEPARTURE;addSearchByCountry=true;addSearchByRegion=true;nearestLocations=true;product=FLIGHT'
    );
  });

  it('should clear the input', async () => {
    const inputValue = 'test';
    let renderedComponent;
    let input, icon;
    await (async () => {
      renderedComponent = renderAutocomplete();
      const { container } = renderedComponent;
      input = container.getElementsByTagName('input')[0];
      const event = { target: { value: inputValue } };
      fireEvent.change(input, event);
      fireEvent.click(input);
      icon = container.getElementsByTagName('svg')[0];
      fireEvent.mouseDown(icon.parentNode);
      expect(input.value).toBe('');
    });
  });
});

function renderAutocomplete({
  fetcher,
  setLocation = () => {},
  label = '',
  icon = FlightIcon,
} = {}) {
  return render(
    <Autocomplete setLocation={setLocation} label={label} icon={icon} />,
    { applicationContext: { fetcher } }
  );
}
