import React from 'react';
import { Box } from 'prisma-design-system';
import styled from '@emotion/styled';
import { css } from '@styled-system/css';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';

const EmptyFooterContent = styled(Box)(
  css({
    backgroundColor: 'white',
  }),
  {
    position: 'fixed',
    bottom: '0',
    left: '0',
    right: '0',
    width: '100%',
    boxShadow: '0 1px 8px 2px rgba(0, 13, 41, 0.24)',
    color: 'white',
  }
);

const EmptyFooterContainer = styled(Box)(
  css({
    minHeight: '48px',
  })
);

const EmptyFooter = () => {
  const [{ isMobile = false }] = ApplicationContext.useFlow();
  return isMobile ? (
    <React.Fragment />
  ) : (
    <EmptyFooterContent>
      <EmptyFooterContainer />
    </EmptyFooterContent>
  );
};

export default EmptyFooter;
