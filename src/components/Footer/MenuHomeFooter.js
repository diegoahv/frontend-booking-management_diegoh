import React from 'react';
import AppsDownloadBlock from '../AppsDownloadBlock/AppsDownloadBlock';
import EmptyFooter from './EmptyFooter';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';

const MenuHomeFooter = () => {
  const [{ isMobile = false }] = ApplicationContext.useFlow();
  return isMobile ? <AppsDownloadBlock /> : <EmptyFooter />;
};

export default MenuHomeFooter;
