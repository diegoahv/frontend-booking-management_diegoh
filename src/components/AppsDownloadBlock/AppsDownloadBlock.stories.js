import React from 'react';
import AppsDownloadBlock from './AppsDownloadBlock';
import { Mocks, DebugAssistant } from '@frontend-react-component-builder/debug';

const { EDREAMS, GO_VOYAGES, OPODO, TRAVELLINK } = Mocks.Session.Brand;

const getMock = (brand) =>
  Mocks.CustomApplicationContextMock({
    session: Mocks.Session.CustomSession({
      brand,
      brandNewNomenclature: brand,
    }),
  });

export default {
  title: 'MMB/Apps download block',
  component: AppsDownloadBlock,
};

const Component = ({ brand }) => (
  <DebugAssistant.AssistantApplicationContextMocker
    applicationContext={getMock(brand)}
  >
    <AppsDownloadBlock />
  </DebugAssistant.AssistantApplicationContextMocker>
);

export const eDreams = () => <Component brand={EDREAMS} />;

export const GoVoyages = () => <Component brand={GO_VOYAGES} />;

export const Opodo = () => <Component brand={OPODO} />;

export const Travellink = () => <Component brand={TRAVELLINK} />;

export const TravellinkBadName = () => <Component brand="TR" />;
