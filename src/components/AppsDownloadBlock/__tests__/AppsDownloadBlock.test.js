import React from 'react';
import { render } from 'test-utils';
import AppsDownloadBlock from '../AppsDownloadBlock';

import { withStandalone } from '@frontend-react-component-builder/conchita';
import { Mocks } from '@frontend-react-component-builder/debug';

const {
  EDREAMS,
  OPODO,
  GO_VOYAGES_NEW_NOMENCLATURE,
  TRAVELLINK,
} = Mocks.Session.Brand;

const getDecorativeImg = (brand = EDREAMS) =>
  `https://ak4.odistatic.net/images/onefront/bluestone/${brand}/app-promo.png`;

const getSrcSelector = (brand = EDREAMS) =>
  `[src="${getDecorativeImg(brand)}"]`;

const getApplicationContextMock = (brand) =>
  Mocks.CustomApplicationContextMock({
    session: Mocks.Session.CustomSession({
      brand,
      brandNewNomenclature: brand,
    }),
  });

const StandaloneAppsDownloadBlock = withStandalone(<AppsDownloadBlock />);

const renderByBrand = (brand = EDREAMS) =>
  render(
    <StandaloneAppsDownloadBlock
      applicationContext={getApplicationContextMock(brand)}
    />
  );

describe('AppsDownloadBlock tests', () => {
  describe('Render tests', () => {
    it('Should render image by brand (EDREAMS)', () => {
      const { container } = renderByBrand(EDREAMS);

      expect(container).toContainElement(
        container.querySelector(getSrcSelector(EDREAMS))
      );
    });

    it('Should render image by brand (GO_VOYAGES)', () => {
      const { container } = renderByBrand(GO_VOYAGES_NEW_NOMENCLATURE);

      expect(container).toContainElement(
        container.querySelector(getSrcSelector(GO_VOYAGES_NEW_NOMENCLATURE))
      );
    });

    it('Should render image by brand (OPODO)', () => {
      const { container } = renderByBrand(OPODO);

      expect(container).toContainElement(
        container.querySelector(getSrcSelector(OPODO))
      );
    });

    it('Should render image by brand (TRAVELLINK)', () => {
      const { container } = renderByBrand(TRAVELLINK);

      expect(container).toContainElement(
        container.querySelector(getSrcSelector(TRAVELLINK))
      );
    });
  });
});
