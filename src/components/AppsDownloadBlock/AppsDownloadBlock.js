import React from 'react';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import { Text, Box, Flex } from 'prisma-design-system';
import styled from '@emotion/styled';
import { css } from '@styled-system/css';
import {
  ApplicationContext,
  Tracking,
} from '@frontend-react-component-builder/conchita';

import ImageFixedSize from '../../common/components/ImageFixedSize';
import {
  APP_DOWNLOAD_URL_IOS as appDownloadUrlIos,
  APP_DOWNLOAD_URL_ANDROID as appDownloadUrlAndroid,
  getIOSButtonImage,
  getAndroidButtonImage,
  getDecorativeImg,
} from '../../common/utils/url_utils';
import { getTracking } from '@/src/containers/ConfirmationPage/tracking';

const DownloadLink = styled('a')(
  css({
    marginRight: 3,
    display: 'inline-block',
    position: 'relative',
  })
);

const DownloadText = styled(Text)(
  css({
    fontSize: '8px',
    position: 'absolute',
    top: '4px',
    color: 'white',
  })
);

const AppDownloadButton = ({ href, src, text, alt, offsetX, upperCase }) => (
  <DownloadLink href={href} target="_blank">
    <DownloadText
      css={{
        left: `${offsetX}px`,
        textTransform: upperCase ? 'uppercase' : 'none',
      }}
    >
      {text}
    </DownloadText>
    <ImageFixedSize width={126} height={38} src={src} alt={alt} />
  </DownloadLink>
);

AppDownloadButton.propTypes = {
  href: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  alt: PropTypes.string,
  offsetX: PropTypes.number.isRequired,
  upperCase: PropTypes.bool,
};
AppDownloadButton.propDefaults = {
  alt: '',
  upperCase: false,
};

const AppDownloadButtonsContainer = styled(Box)(
  css({
    textAlign: 'center',
    marginTop: 5,
  })
);

const TitleTextStyled = styled(Text)(
  css({
    color: 'neutrals.0',
    fontSize: 'body.3',
    fontWeight: 'bold',
    textAlign: 'center',
  })
);

const MainTextStyled = styled(Text)(
  css({
    fontSize: 'body.1',
    color: 'neutrals.3',
    textAlign: 'center',
    marginTop: 1,
  })
);

const brandNomenclatureMap = {
  GO: 'GV',
  TR: 'TL',
};
const mappedBrandName = (brand) => brandNomenclatureMap[brand] || brand;

const AppsDownloadBlock = () => {
  const { t } = useTranslation();
  const [session] = ApplicationContext.useSession();
  const brand = mappedBrandName(session.brandNewNomenclature);
  const { category, action, label } = getTracking('DOWNLOAD_APP');
  return (
    <Box px={4} color="white" data-testid="appsDownloadBlock">
      <TitleTextStyled as="div">
        {t('myinfo:mytrips.app.title')}
      </TitleTextStyled>
      <MainTextStyled as="div">{t('myinfo:mytrips.app.text')}</MainTextStyled>
      <AppDownloadButtonsContainer>
        <Tracking.TrackableClick
          category={category}
          label={label}
          action={action}
        >
          <Tracking.InlineTrackableClick
            label={'apps_download'}
            action={'apps_open_market_apple'}
          >
            <AppDownloadButton
              href={appDownloadUrlIos[brand]}
              src={getIOSButtonImage(brand)}
              offsetX={36}
              text={t('footer:social.applestore')}
              alt="App Store"
            />
          </Tracking.InlineTrackableClick>
        </Tracking.TrackableClick>
        <Tracking.TrackableClick
          category={category}
          label={label}
          action={action}
        >
          <Tracking.InlineTrackableClick
            label={'apps_download'}
            action={'apps_open_market_android'}
          >
            <AppDownloadButton
              href={appDownloadUrlAndroid[brand]}
              src={getAndroidButtonImage(brand)}
              upperCase={true}
              offsetX={26}
              text={t('footer:social.googleplay')}
              alt="Google Play"
            />
          </Tracking.InlineTrackableClick>
        </Tracking.TrackableClick>
      </AppDownloadButtonsContainer>
      <Flex flexDirection="row" justifyContent="center" mt="6">
        <ImageFixedSize
          width={220}
          height={115}
          src={getDecorativeImg(brand)}
        />
      </Flex>
    </Box>
  );
};

export default AppsDownloadBlock;
