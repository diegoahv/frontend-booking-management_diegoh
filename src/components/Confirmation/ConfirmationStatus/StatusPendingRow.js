import React from 'react';
import styled from '@emotion/styled';
import { Box, DecorationLine, Text } from 'prisma-design-system';
import { css } from '@styled-system/css';

const CircleBox = styled(Box)(
  css({
    '& div[type="circle"]': {
      width: '1',
      height: '1',
      margin: '0 0 0 -12px',
      borderColor: 'neutrals.4',
      top: 0,
    },
  })
);

function StatusPendingRow({ content }) {
  return (
    <CircleBox display="table-row">
      <DecorationLine decorationType={'circle'} lineType={'none'} />
      <Box mb={'7'} mt={'2px'} pl={'6'}>
        <Text
          fontWeight="medium"
          textAlign="left"
          priority={2}
          css={css({ color: 'neutrals.3' })}
        >
          {content.step3title}
        </Text>
      </Box>
    </CircleBox>
  );
}

export default StatusPendingRow;
