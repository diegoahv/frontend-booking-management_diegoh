import React from 'react';
import styled from '@emotion/styled';
import {
  Box,
  DecorationLine,
  Flex,
  Text,
  TimeIcon,
  DateTime,
} from 'prisma-design-system';
import { css } from '@styled-system/css';
const CircledDotBox = styled(Box)(
  css({
    '& div[type="circledDot"]': {
      width: '1',
      height: '1',
      borderColor: `success.base`,
      margin: '0 0 0 -12px',
      top: 0,
      '&:after': {
        width: '12px',
        height: '12px',
        backgroundColor: `success.base`,
      },
      '& div:first-of-type:before': { borderColor: 'neutrals.4' },
    },
  })
);
const StyledText = styled(Text)(
  css({
    fontSize: 'body.1',
    my: 2,
  })
);
const StyledInfoBox = styled(Box)(
  css({
    fontSize: 'body.1',
    backgroundColor: 'neutrals.7',
  })
);
const TextWithIconFlex = styled(Flex)(
  css({
    fontSize: 'body.0',
    ml: 3,
    color: 'informative.base',
    '& svg': {
      paddingBottom: '3px',
      marginRight: '3px',
    },
  })
);

function StatusProcessingRow({ content, todDate }) {
  const todDateIso = new Date(todDate).toISOString();
  return (
    <CircledDotBox display="table-row">
      <DecorationLine decorationType={'circledDot'} lineType={'dashed'} />
      <Box mb={'7'} mt={'2px'} pl={'6'}>
        <Text fontWeight="medium" textAlign="left" priority={2}>
          {content.step1Title}
        </Text>
        <StyledText as={'p'} textAlign="left" priority={4} mt={3}>
          <DateTime format={'iiii, MMM dd'} value={todDateIso} />
        </StyledText>

        <StyledInfoBox p={4}>
          <Flex mb={2}>
            <Text fontWeight="bold">{content.step2estimatedTime}</Text>
            <TextWithIconFlex alignItems="center">
              <TimeIcon size="small" />
              <Text> {content.step2estimatedTimeMessage} </Text>
            </TextWithIconFlex>
          </Flex>
          <Text>{content.step2StatusMessage}</Text>
        </StyledInfoBox>
      </Box>
    </CircledDotBox>
  );
}

export default StatusProcessingRow;
