import React from 'react';
import styled from '@emotion/styled';
import { Box, Text } from 'prisma-design-system';
import { css } from '@styled-system/css';
import StatusDoneRow from './StatusDoneRow';
import StatusProcessingRow from './StatusProcessingRow';
import StatusPendingRow from './StatusPendingRow';

const StyledTitle = styled(Text)(
  css({
    fontSize: 'body.3',
    color: 'neutrals.0',
  })
);

function ConfirmationStatus({ content, todDate }) {
  return (
    <Box pt={7} data-testid="confirmationStatus">
      <StyledTitle fontWeight="medium" priority={1}>
        {content.statusHeader}
      </StyledTitle>
      <Box px={3} pt={6}>
        <StatusDoneRow content={content} />
        <StatusProcessingRow content={content} todDate={todDate} />
        <StatusPendingRow content={content} />
      </Box>
    </Box>
  );
}

export default ConfirmationStatus;
