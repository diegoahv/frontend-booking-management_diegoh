import React from 'react';
import styled from '@emotion/styled';
import { Box, DecorationLine, Text, TickIcon } from 'prisma-design-system';
import { css } from '@styled-system/css';

const CircledIconBox = styled(Box)(
  css({
    '& svg': {
      width: '1',
      height: '1',
      color: `success.base`,
      borderColor: `success.base`,
      margin: '0 0 0 -12px',
      position: 'absolute',
      padding: '2px',
      border: '2px solid',
      borderRadius: '6',
      backgroundColor: 'white',
      boxSizing: 'border-box',
    },
    '& div:first-of-type:before': {
      borderColor: 'success.base',
    },
  })
);

function StatusDoneRow({ content }) {
  return (
    <CircledIconBox display="table-row">
      <DecorationLine decorationType={'none'} lineType={'dashed'} />
      <TickIcon size={'medium'} />
      <Box mb={'7'} mt={'2px'} pl={'6'}>
        <Text fontWeight="medium" textAlign="left" priority={2}>
          {content.step1Title}
        </Text>
      </Box>
    </CircledIconBox>
  );
}

export default StatusDoneRow;
