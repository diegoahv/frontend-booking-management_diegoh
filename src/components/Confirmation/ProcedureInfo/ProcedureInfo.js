import React from 'react';
import { Box, Link, Text } from 'prisma-design-system';
import css from '@styled-system/css';
import styled from '@emotion/styled';

import { useTranslation } from 'react-i18next';

const ProcedureInfo = ({ onClickFunction }) => {
  const { t } = useTranslation();
  const mytripsText = t('confirmationPage:nextSteps.myTripsLink');
  const step2ContentMytrips = t('confirmationPage:nextSteps.myTrips', {
    mytrips: '[mytrips]',
  });
  const chunks = step2ContentMytrips.split('[mytrips]');

  return (
    <Box pt={7} data-testid="procedureInfo">
      <Box pb={2}>
        <Text
          fontWeight="medium"
          css={css({ fontSize: 'body.3', color: 'neutrals.0' })}
        >
          {t('confirmationPage:nextSteps.header')}
        </Text>
      </Box>
      <Box pb={2}>
        <Text priority={4}>{t('confirmationPage:nextSteps.step1')}</Text>
      </Box>
      <Box>
        <Text priority={4}>{chunks[0]}</Text>
        <StyledLink onClick={onClickFunction} data-testid="nexStepsLink">
          {mytripsText}
        </StyledLink>
        <Text priority={4}>{chunks[1]}</Text>
      </Box>
    </Box>
  );
};
const StyledLink = styled(Link)(
  css({
    fontSize: 'body.1',
  })
);
export default ProcedureInfo;
