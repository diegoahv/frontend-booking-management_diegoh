import React from 'react';
import { ButtonGroup } from 'prisma-design-system';
import Button from '@/src/prisma-altered/components/Button';
import styled from '@emotion/styled';
import css from '@styled-system/css';

const ItineraryTypeSelector = ({
  content,
  fullWidth,
  onClick = () => {},
  initialSelection = 1,
}) => {
  const StyledButton = styled(Button)(() =>
    css({
      [`${Button}:not(:first-of-type)`]: {
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
      },
      [`${Button}:not(:last-child)`]: {
        borderRight: 0,
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0,
      },
    })
  );

  return (
    <ButtonGroup
      groupName="sortingTabs"
      selectedIndex={initialSelection}
      stretch
    >
      <StyledButton onClick={onClick} fullWidth={fullWidth} variant="base">
        {content.roundTrip}
      </StyledButton>
      <StyledButton onClick={onClick} fullWidth={fullWidth} variant="base">
        {content.oneWay}
      </StyledButton>
    </ButtonGroup>
  );
};

export default ItineraryTypeSelector;
