import React from 'react';
import ItineraryTypeSelector from './ItineraryTypeSelector';
import { withKnobs, select } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';

storiesOf('OpenTicket/Search/Itinerary type selector', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const fullWidth = select('Fullwidth', ['true', 'false'], 'true');
    return (
      <ItineraryTypeSelector
        fullWidth={fullWidth}
        onClick={(...args) => console.log(...args)}
      />
    );
  });
