import React from 'react';
import { useTranslation } from 'react-i18next';
import styled from '@emotion/styled';
import { css } from '@styled-system/css';
import { Box } from 'prisma-design-system';
import PropTypes from 'prop-types';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';

const BookingNumber = styled(Box)((props) =>
  css({
    fontFamily: 'body',
    color: 'neutrals.2',
    fontSize: props.isMobile ? 'body.1' : 'body.2',
    pb: props.isMobile ? '1' : '2',
  })
);

const HowcanWehelp = styled(Box)((props) =>
  css({
    fontFamily: 'body',
    color: 'neutrals.0',
    fontSize: props.isMobile ? 'heading.1' : 'heading.2',
    pb: '6',
    fontWeight: 'medium',
  })
);

const PageTopContent = ({ bookingId, menuTitle }) => {
  const { t } = useTranslation();
  const [{ isMobile = false }] = ApplicationContext.useFlow();
  return (
    <Box>
      <BookingNumber isMobile={isMobile}>
        {t('managebooking:bookingNumber')}: {bookingId}
      </BookingNumber>
      <HowcanWehelp isMobile={isMobile}>{menuTitle}</HowcanWehelp>
    </Box>
  );
};

PageTopContent.propTypes = {
  bookingId: PropTypes.string.isRequired,
  menuTitle: PropTypes.string.isRequired,
};

export default PageTopContent;
