import React from 'react';
import { render, fireEvent } from 'test-utils';
import Page from '../Page';
import { MenuHomeFooter } from '../../Footer';

const mockClasses = {
  children: 'pageChildren',
  footer: 'pageFooter',
  header: 'pageHeader',
};

jest.mock('../../Footer/EmptyFooter.js', () => ({
  __esModule: true,
  default: () => <div className={mockClasses.footer} />,
}));
jest.mock('../../PageHeader', () => ({
  __esModule: true,
  default: ({ title, clickHandler }) => (
    <div className={mockClasses.header} onClick={clickHandler}>
      {title}
    </div>
  ),
}));

jest.mock('../../AppsDownloadBlock/AppsDownloadBlock', () => ({
  __esModule: true,
  default: () => <div className={mockClasses.footer} />,
}));
jest.mock('../../PageHeader', () => ({
  __esModule: true,
  default: ({ title, clickHandler }) => (
    <div className={mockClasses.header} onClick={clickHandler}>
      {title}
    </div>
  ),
}));

const defaults = {
  title: 'pageDefaultTitle',
  clickHandler: () => {},
  children: <div className={mockClasses.children}></div>,
};

const selectors = {};
for (let cls of Object.keys(mockClasses)) {
  selectors[cls] = `.${mockClasses[cls]}`;
}

const renderPageWithFooter = () =>
  render(
    <Page
      footer={<MenuHomeFooter />}
      title={defaults.title}
      backAction={defaults.clickHandler}
    >
      {defaults.children}
    </Page>
  );
const renderPageNoFooter = (backAction = defaults.clickHandler) =>
  render(
    <Page title={defaults.title} backAction={backAction}>
      {defaults.children}
    </Page>
  );

describe('Page tests', () => {
  describe('Footer inclusion', () => {
    it('Should not include footer by default', () => {
      const { container } = renderPageNoFooter();
      const footer = container.querySelector(selectors.footer);
      expect(footer).toBeNull();
    });
    it('Should include the footer when requested', () => {
      const { container } = renderPageWithFooter();
      const footer = container.querySelector(selectors.footer);
      expect(footer).not.toBeNull();
    });
  });
  describe('Render contents', () => {
    it('Should render the title', () => {
      const { container } = renderPageNoFooter();
      expect(container).toContainHTML(defaults.title);
    });
    it('Should render the children', () => {
      const { container } = renderPageNoFooter();
      const children = container.querySelector(selectors.children);
      expect(children).not.toBeNull();
    });
  });
  describe('Back action trigger', () => {
    it('Should call the handler when the event is triggered', () => {
      const handlerMock = jest.fn();
      const { container } = renderPageNoFooter(handlerMock);
      const header = container.querySelector(selectors.header);
      expect(handlerMock).not.toHaveBeenCalled();
      fireEvent.click(header);
      expect(handlerMock).toHaveBeenCalledTimes(1);
    });
  });
});
