import React from 'react';
import Page from './Page';
import MenuOption from '../MenuOption/MenuOption.js';
import { PlaneGoingIcon, Box } from 'prisma-design-system';
import styled from '@emotion/styled-base';
import { css } from '@styled-system/css';
import { MenuHomeFooter } from '../Footer';

export default {
  title: 'Common/Page',
  component: Page,
};
const MenuOptionWrapper = styled(Box)(
  css({
    mb: 2,
  })
);
export const Default = () => {
  return (
    <Page
      footer={<MenuHomeFooter />}
      title="Example page"
      backAction={() => console.log('back button clicked')}
      fixedHeader={'relative'}
    >
      <MenuOptionWrapper>
        <MenuOption
          clickHandler={() => {}}
          title="Item 01"
          description="Lorem ipsum dolor sit amet"
          icon={<PlaneGoingIcon color="brandPrimary.2" size="large" />}
          type="schedule"
        />
      </MenuOptionWrapper>
      <MenuOptionWrapper>
        <MenuOption
          clickHandler={() => {}}
          title="Item iconless"
          description="Lorem ipsum dolor sit amet"
          type="confirmation"
        />
      </MenuOptionWrapper>
    </Page>
  );
};
