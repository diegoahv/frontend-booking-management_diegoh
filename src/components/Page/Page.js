import React from 'react';
import PropTypes from 'prop-types';
import { Box } from 'prisma-design-system';
import Header from '../PageHeader';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import styled from '@emotion/styled';
import css from '@styled-system/css';

const StyledPage = styled(Box)((props) =>
  css({
    pt: props.isMobile ? '7' : '92px',
    pb: props.isMobile ? '48px' : '50px',
    px: props.isMobile ? '4' : '6',
  })
);

const Page = ({
  children,
  footer = <React.Fragment />,
  title,
  backAction,
  className = '',
  pageClass = '',
  fixedHeader = 'relative',
}) => {
  const [{ isMobile = false }] = ApplicationContext.useFlow();
  return (
    <Box className={pageClass} pt={fixedHeader === 'fixed' ? 64 : 0}>
      <Header
        title={title}
        clickHandler={backAction}
        fixedHeader={fixedHeader}
      />
      <StyledPage isMobile={isMobile} color="white" className={className}>
        {children}
      </StyledPage>
      {footer}
    </Box>
  );
};

Page.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string.isRequired,
  backAction: PropTypes.func,
  pageClass: PropTypes.string,
};

export default Page;
