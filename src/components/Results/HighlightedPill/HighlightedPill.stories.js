import React from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, withKnobs } from '@storybook/addon-knobs';

import ItineraryProvider from '../Itinerary/Provider';
import { selectHighlightedType } from '../Itinerary/Itinerary.stories';

import HighlightedPill from './HighlightedPill';

export const selectItineraryInfo = (label = '') => ({
  highlightedType: selectHighlightedType(),
});

storiesOf('OpenTicket/Results/HighlightedPill', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const roundedTop = boolean('roundedTop', false);
    return (
      <ItineraryProvider value={selectItineraryInfo()}>
        <HighlightedPill roundedTop={roundedTop} />
      </ItineraryProvider>
    );
  });
