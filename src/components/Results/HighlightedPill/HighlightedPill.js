import React from 'react';
import { useTranslation } from 'react-i18next';
import { Pill } from 'prisma-design-system';
import styled from '@emotion/styled';
import { css } from '@styled-system/css';
import { useItinerary } from '../Itinerary/Provider';

const StyledPill = styled(Pill)(
  css({
    color: 'success.base',
  })
);

const HighlightedPill = ({ roundedTop }) => {
  const { t } = useTranslation();
  const { highlightedType } = useItinerary();
  return highlightedType !== 'default' ? (
    <StyledPill
      type="positive"
      highlighted={highlightedType === 'recommended'}
      roundedTop={roundedTop}
    >
      {t(`results:highlighted.itinerary.${highlightedType}`)}
    </StyledPill>
  ) : null;
};

export default HighlightedPill;
