import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Money, Text } from 'prisma-design-system';
import styled from '@emotion/styled';
import { css } from '@styled-system/css';

const ALIGN = {
  END: 'flex-end',
  START: 'flex-start',
  CENTER: 'center',
  STRETCH: 'strech',
  BASELINE: 'baseline',
};

const StyledFlex = styled(Flex)(
  css({
    '& > span > span': {
      fontWeight: 'medium',
      color: 'neutrals.1',
    },
  })
);

const ItineraryPrice = ({ amount, currency, align, text, size }) => {
  return (
    <StyledFlex flexDirection="column" alignItems={align}>
      <Money currency={currency} size={size}>
        {amount}
      </Money>
      <Text
        as="div"
        priority={7}
        textAlign={align === ALIGN.START ? 'left' : 'right'}
      >
        {text}
      </Text>
    </StyledFlex>
  );
};

ItineraryPrice.propTypes = {
  amount: PropTypes.number.isRequired,
  currency: PropTypes.string.isRequired,
  align: PropTypes.oneOf([
    ALIGN.END,
    ALIGN.START,
    ALIGN.CENTER,
    ALIGN.STRETCH,
    ALIGN.BASELINE,
  ]),
  size: PropTypes.oneOf(['tiny', 'small', 'medium']),
};

ItineraryPrice.defaultProps = {
  align: ALIGN.END,
  size: 'medium',
};

export default ItineraryPrice;
