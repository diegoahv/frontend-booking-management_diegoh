import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, number, select, text } from '@storybook/addon-knobs';

import ItineraryPrice from './';

storiesOf('OpenTicket/Results/ItineraryPrice', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const amount = number('Amount', 104.99);
    const currency = select('Currency', ['EUR', 'USD', 'XCD', 'COP'], 'EUR');
    const align = select(
      `Price align`,
      ['flex-start', 'flex-end', 'center'],
      'flex-start'
    );
    const label = text('text', 'Total price');

    return (
      <ItineraryPrice
        amount={amount}
        currency={currency}
        align={align}
        text={label}
      />
    );
  });
