import { SegmentMocks } from 'serverMocks';

export const ResultsSegmentMocks = () => {
  const { SegmentSample: ServerSegmentSample } = SegmentMocks();
  const SegmentSample = ({ ...props } = {}) => {
    const segment = ServerSegmentSample(props);
    const { sections, carrier } = segment;
    const firstSection = sections[0];
    const lastSection = sections[sections.length - 1];
    segment.carriers = [carrier];
    segment.departure = firstSection.departure;
    segment.destination = lastSection.destination;
    segment.departureDate = firstSection.departureDate;
    segment.arrivalDate = lastSection.arrivalDate;
    return segment;
  };

  return {
    SegmentSample,
  };
};
