import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from 'test-utils';
import { AirEuropa, Ryanair } from 'serverMocks/Carrier';
import {
  Barajas,
  CharlesDeGaulle,
  ElPrat,
  PalmaDeMallorca,
  Schoenefeld,
  Stansted,
} from 'serverMocks/Location';
import { Euros } from 'serverMocks/Money';
import { segmentAssertions } from './assertions';

import Segment from '../';

const dateWithTime = (time) => `2018-07-22T${time}:00+01:00`;
const TICKETS_LEFT_NUMBER = 5;
const AIREUROPA_FLIGHTCODE = 'UX4321';
const RYANAIR_FLIGHTCODE_FIRST = 'FR4321';
const RYANAIR_FLIGHTCODE_SECOND = 'FR5555';

const segmentMock = () => {
  return {
    id: 1,
    carriers: [AirEuropa()],
    departureDate: dateWithTime('10:30'),
    arrivalDate: dateWithTime('14:35'),
    departure: Barajas(),
    destination: PalmaDeMallorca(),
    duration: 245,
    sections: [
      {
        id: 1,
      },
      {
        id: 2,
      },
    ],
    baggageCondition: 'CABIN_INCLUDED',
  };
};

export const ItineraryMock = () => {
  const outboundSegment = {
    id: 1,
    carriers: [AirEuropa()],
    departureDate: dateWithTime('10:30'),
    arrivalDate: dateWithTime('12:15'),
    duration: 105,
    departure: Barajas(),
    destination: ElPrat(),
    baggageCondition: 'CABIN_INCLUDED',
    stopovers: [],
    sections: [
      {
        id: 1,
        flightCode: AIREUROPA_FLIGHTCODE,
        trips: [
          {
            id: 0,
            carrier: AirEuropa(),
            operatingCarrier: AirEuropa(),
            departureDate: dateWithTime('10:30'),
            arrivalDate: dateWithTime('12:15'),
            departure: {
              ...Barajas(),
              type: 'absolute',
            },
            destination: {
              ...ElPrat(),
              type: 'connection',
            },
            cabinClass: 'ECONOMIC_DISCOUNTED',
            flightCode: 'BR2654',
            duration: 105,
          },
        ],
        technicalStops: [],
      },
    ],
  };
  const inboundSegment = {
    id: 2,
    carriers: [Ryanair()],
    departureDate: dateWithTime('19:40'),
    arrivalDate: dateWithTime('20:45'),
    duration: 65,
    departure: CharlesDeGaulle(),
    destination: Stansted(),
    baggageCondition: 'CABIN_INCLUDED',
    stopovers: [],
    sections: [
      {
        id: 2,
        flightCode: RYANAIR_FLIGHTCODE_FIRST,
        trips: [
          {
            id: 0,
            carrier: AirEuropa(),
            departureDate: dateWithTime('19:40'),
            arrivalDate: dateWithTime('20:15'),
            operatingCarrier: AirEuropa(),
            departure: CharlesDeGaulle(),
            destination: Schoenefeld(),
            cabinClass: 'ECONOMIC_DISCOUNTED',
            duration: 35,
            flightCode: 'BR2654',
          },
        ],
        technicalStops: [],
      },
      {
        id: 3,
        flightCode: RYANAIR_FLIGHTCODE_SECOND,
        trips: [
          {
            id: 1,
            carrier: AirEuropa(),
            operatingCarrier: AirEuropa(),
            departure: Schoenefeld(),
            departureDate: dateWithTime('20:25'),
            arrivalDate: dateWithTime('20:45'),
            destination: Stansted(),
            cabinClass: 'ECONOMIC_DISCOUNTED',
            duration: 60,
            flightCode: 'FR2654',
          },
        ],
        technicalStops: [],
      },
    ],
  };
  return {
    price: Euros(12.5),
    ticketsLeft: TICKETS_LEFT_NUMBER,
    priceSortingPosition: 1,
    legs: [
      {
        segmentKeys: ['0'],
        segments: [outboundSegment],
      },
      {
        segmentKeys: ['0'],
        segments: [inboundSegment],
      },
    ],
  };
};

describe('Segment', () => {
  test('Segment content in Mobile without transport types', (done) => {
    const data = segmentMock();

    const { getByText } = render(<Segment data={data} />);

    setImmediate(() => {
      const assertSegment = segmentAssertions({ getByText });

      assertSegment({
        airline: AirEuropa().name,
        departureTime: '10:30',
        departureIata: Barajas().iata,
        destinationTime: '14:35',
        destinationIata: PalmaDeMallorca().iata,
        duration: '4h 05m',
        stops: '1 stop',
      });
      done();
    });
  });
});
