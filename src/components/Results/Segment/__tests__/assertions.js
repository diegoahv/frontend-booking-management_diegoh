export const segmentAssertions = ({ getByText }) => ({
  airline,
  departureTime,
  departureIata,
  destinationTime,
  destinationIata,
  duration,
  stops,
}) => {
  expect(getByText(airline)).toBeVisible();

  expect(getByText(departureTime)).toBeVisible();
  expect(getByText(departureIata)).toBeVisible();

  expect(getByText(destinationTime)).toBeVisible();
  expect(getByText(destinationIata)).toBeVisible();

  // TODO: Uncomment once i18n not initializing before test error is solved.
  // expect(getByText(duration)).toBeVisible();
  // expect(getByText(stops)).toBeVisible();
};
