import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, number, select } from '@storybook/addon-knobs';
import { selectCarrier } from 'decorators';
import { selectLocation } from '../SegmentLocation/SegmentLocation.stories';
import ItineraryProvider from '../Itinerary/Provider';
import { selectHighlightedType } from '../Itinerary/Itinerary.stories';

import Segment from './index';

const baggageCondition = select(
  'Baggage condition',
  ['CHECKIN_INCLUDED', 'CABIN_INCLUDED', 'BASED_ON_AIRLANE'],
  'CHECKIN_INCLUDED'
);

export const selectSegmentInfo = (numberOfSections) => {
  return {
    carriers: [selectCarrier()],
    departureDate: text('Departure date', '2018-07-22T23:30:00+02:00'),
    arrivalDate: text('Destination date', '2018-07-23T01:15:00+01:00'),
    departure: selectLocation('Departure location', 'El Prat'),
    destination: selectLocation('Destination location', 'Barajas'),
    duration: number('Duration', 45),
    sections: Array.from({ length: numberOfSections }, () => ({})),
    trips: {
      baggageCondition,
    },
  };
};

export const id = () => number('id', 0);

storiesOf('OpenTicket/Results/Segment', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const options = {
      range: true,
      min: 1,
      max: 4,
      step: 1,
    };
    const sections = number('Number of Itineraries', 1, options);
    const segment = selectSegmentInfo(sections);

    return (
      <ItineraryProvider value={selectHighlightedType()}>
        <Segment data={segment} id={id()} />
      </ItineraryProvider>
    );
  });
