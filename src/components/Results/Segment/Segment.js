import React from 'react';
import { Flex, Box } from 'prisma-design-system';
import SegmentDuration from '../SegmentDuration';
import { Carrier } from '../../Carrier';
import SegmentLocation from '../SegmentLocation';
import StopoverQuantity from '../StopoverQuantity';
import DepartureTime from '../DepartureTime';
import DestinationTime from '../DestinationTime';
import HighlightedPill from '../HighlightedPill';

const Segment = ({ data, id }) => {
  const {
    sections,
    departureDate,
    arrivalDate,
    departure,
    destination,
    duration,
    carriers,
  } = data;
  const numberOfStops = sections.length - 1;

  return (
    <Box>
      <Flex
        width="100%"
        flexDirection="row"
        justifyContent="space-between"
        alignItems="center"
      >
        <Carrier carriers={carriers} />
        {id === 0 && <HighlightedPill roundedTop={false} />}
      </Flex>
      <Flex
        width="100%"
        flexDirection="row"
        justifyContent="space-between"
        alignItems="center"
        mt={2}
      >
        <Flex flexGrow={1} flexDirection="column" alignItems="flex-start">
          <Box mb={1}>
            <DepartureTime departureDate={departureDate} />
          </Box>
          <SegmentLocation location={departure} />
        </Flex>
        <Flex flexGrow={6} alignItems="stretch" flexDirection="column">
          <SegmentDuration duration={duration} />
          <StopoverQuantity quantity={numberOfStops} />
        </Flex>
        <Flex flexGrow={1} flexDirection="column" alignItems="flex-end">
          <Box mb={1}>
            <DestinationTime
              arrivalDate={arrivalDate}
              departureDate={departureDate}
            />
          </Box>
          <SegmentLocation location={destination} />
        </Flex>
      </Flex>
    </Box>
  );
};

export default Segment;
