import React from 'react';
import PropTypes from 'prop-types';
import { Wrapper, Flex } from 'prisma-design-system';
import ItineraryPrice from '../ItineraryPrice';
import { useTranslation } from 'react-i18next';

import LegList from '../LegList';
import { css } from '@styled-system/css';
import styled from '@emotion/styled';

const StyledWrapper = styled(Wrapper)(
  css({
    boxShadow: '2px 2px 10px 0 rgba(0, 0, 0, 0.2)', // todo: review with UX
    borderRadius: 2,
  }),
  (props) => {
    return css({
      ...(props.borders === 'emphasis' && {
        borderColor: 'positive.hard',
      }),
    });
  }
);
const ItineraryMobile = ({ itinerary, onClick }) => {
  const { t } = useTranslation();
  const { legs, price, highlightedType } = itinerary;
  const recommended =
    highlightedType === 'recommended' ? 'fullBorder' : undefined;

  return (
    <StyledWrapper
      onClick={onClick}
      data-testid="itinerary"
      borders={recommended && 'emphasis'}
      p={3}
      pt={!recommended && 4}
    >
      <LegList
        legs={legs}
        highlightedType={itinerary.highlightedType}
        itinerary={itinerary}
      />
      <Flex flexDirection="row" justifyContent="flex-end" mt={5}>
        <Flex flexDirection="column">
          <ItineraryPrice
            currency={price.currency}
            amount={price.priceDifference}
            text={t('results:price.difference')}
          />
        </Flex>
      </Flex>
    </StyledWrapper>
  );
};

ItineraryMobile.propTypes = {
  itinerary: PropTypes.object.isRequired,
  onClick: PropTypes.func,
};

export default ItineraryMobile;
