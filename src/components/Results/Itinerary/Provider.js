import { createContext, useContext } from 'react';

const ItineraryContext = createContext();

export const useItinerary = () => useContext(ItineraryContext);

const ItineraryProvider = ItineraryContext.Provider;

export default ItineraryProvider;
