import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, number, select } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import Itinerary from '.';
import { selectLegs } from '../LegList/LegList.stories';

const selectPrice = (label = '') => ({
  price: number(`${label} price amount:`, 164.9),
  currency: select('currency', ['USD', 'EUR'], 'USD'),
});

export const selectHighlightedType = () => {
  return select('highlightedType', ['default', 'recommended'], 'default');
};

const selectItineraryInfo = (label = '') => ({
  ticketsLeft: number('tickets left', 9),
  price: selectPrice(),
  legs: selectLegs(),
  highlightedType: selectHighlightedType(),
});

storiesOf('OpenTicket/Results/Itinerary', module)
  .addDecorator(withKnobs)
  .add('Base', () => {
    return (
      <Itinerary
        itinerary={selectItineraryInfo()}
        selectFlight={action('Select Flight')}
      />
    );
  });
