import React, { useCallback } from 'react';
import PropTypes from 'prop-types';

import ItineraryMobile from './ItineraryMobile';
import ItineraryProvider from './Provider';
import { getTracking } from '@/src/containers/SearchPage/tracking';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';

const Itinerary = ({ itinerary, selectFlight }) => {
  const tracking = ApplicationContext.useTracking();

  const onClick = useCallback(() => {
    selectFlight(itinerary);
    const { category, action, label } = getTracking('RESULT_CLICKED', {
      id: itinerary.id,
      priceDifference: itinerary.price.priceDifference,
    });
    tracking.trackEventWithCategory(category, action, label);
  }, [itinerary, selectFlight, tracking]);

  return (
    <ItineraryProvider value={itinerary}>
      <ItineraryMobile itinerary={itinerary} onClick={onClick} />
    </ItineraryProvider>
  );
};

Itinerary.propTypes = {
  itinerary: PropTypes.object.isRequired,
  selectFlight: PropTypes.func,
};

Itinerary.defaultProps = {
  selectFlight: () => {},
};

export default Itinerary;
