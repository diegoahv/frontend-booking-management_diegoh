import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, number } from '@storybook/addon-knobs';

import StopoverQuantity from './index';

storiesOf('OpenTicket/Results/StopoverQuantity', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const quantity = number('Quantity', 0);

    return <StopoverQuantity quantity={quantity} />;
  });
