import React, { Fragment } from 'react';
import { useTranslation } from 'react-i18next';
import { Flex, Hr, Text } from 'prisma-design-system';

const StopoverQuantity = ({ quantity }) => {
  const { t } = useTranslation();
  const isDirect = quantity === 0;

  return (
    <Fragment>
      <Hr decoration={isDirect ? 'none' : 'disk'} />
      <Flex justifyContent="center" py={1}>
        <Text priority={8}>
          {t('results:segment.stops', {
            count: quantity,
            context: isDirect ? 'none' : 'some',
          })}
        </Text>
      </Flex>
    </Fragment>
  );
};

export default StopoverQuantity;
