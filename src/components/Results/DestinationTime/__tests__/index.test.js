import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import DestinationTime from '../';
import { render } from 'test-utils';

describe('DestinationTime', () => {
  describe('from east to west', () => {
    describe('destination local day exceeds departure local day for one', () => {
      test('destination time should show is from the next day', () => {
        const departureMadridDate = '2020-04-27T22:30:00+01:00';
        const destinationNewYorkDate = '2020-04-28T00:30:00-05:00';

        const { container } = render(
          <DestinationTime
            arrivalDate={destinationNewYorkDate}
            departureDate={departureMadridDate}
          />
        );

        expect(container.textContent).toBe('00:30+1');
      });
    });

    describe('destination date and departure date are the same local date', () => {
      test('just appears the destination time', () => {
        const departureMadridDate = '2020-04-27T09:30:00+01:00';
        const destinationNewYorkDate = '2020-04-27T11:30:00-05:00';

        const { container } = render(
          <DestinationTime
            arrivalDate={destinationNewYorkDate}
            departureDate={departureMadridDate}
          />
        );

        expect(container.textContent).toBe('11:30');
      });
    });
  });

  describe('from west to east', () => {
    describe('destination local day is one day before than departure local day', () => {
      test('just appears the destination time', () => {
        const departureTokyoDate = '2020-04-27T02:00:00+09:00';
        const destinationVancouverDate = '2020-04-26T23:20:00-08:00';

        const { container } = render(
          <DestinationTime
            departureDate={departureTokyoDate}
            arrivalDate={destinationVancouverDate}
          />
        );

        expect(container.textContent).toBe('23:20-1');
      });
    });

    describe('destination local day and departure local day are the same', () => {
      test('just appears the destination time', () => {
        const departureTokyoDate = '2020-04-27T14:00:00+09:00';
        const destinationVancouverDate = '2020-04-27T11:20:00-08:00';

        const { container } = render(
          <DestinationTime
            departureDate={departureTokyoDate}
            arrivalDate={destinationVancouverDate}
          />
        );

        expect(container.textContent).toBe('11:20');
      });
    });
  });
});
