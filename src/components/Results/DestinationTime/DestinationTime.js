import parseISO from 'date-fns/parseISO';
import differenceInDays from 'date-fns/differenceInDays';
import { Box, Flex, Text } from 'prisma-design-system';
import React from 'react';
import SegmentTime from '../DepartureTime/SegmentTime';
import PropTypes from 'prop-types';

const parseLocalDay = (ISODateString) => {
  const [localDay] = ISODateString.match(/^\d+-\d{2}-\d{2}/) || [];
  return parseISO(localDay);
};

const OtherDay = ({ arrivalDate, departureDate }) => {
  const localDepartureDay = parseLocalDay(departureDate);
  const localDestinationDay = parseLocalDay(arrivalDate);
  if (!localDepartureDay || !localDestinationDay) {
    return null;
  }
  const days = differenceInDays(localDestinationDay, localDepartureDay);
  const otherDay = days !== 0;
  const daysSign = Math.sign(days) < 0 ? '-' : '+';

  return (
    <Box mt={-1}>
      <Text priority={6}>{otherDay && `${daysSign}${Math.abs(days)}`}</Text>
    </Box>
  );
};

const DestinationTime = ({ arrivalDate, departureDate, priority = 1 }) => {
  return (
    <Flex justifyContent={'flex-end'}>
      <SegmentTime date={arrivalDate} priority={priority} />
      <OtherDay departureDate={departureDate} arrivalDate={arrivalDate} />
    </Flex>
  );
};

DestinationTime.propTypes = {
  arrivalDate: PropTypes.string.isRequired,
  departureDate: PropTypes.string.isRequired,
};

export default DestinationTime;
