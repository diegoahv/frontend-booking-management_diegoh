import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';

import DestinationTime from './';

storiesOf('OpenTicket/Results/DestinationTime', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const departureDate = text(`Departure date`, '2018-07-22T23:30:00+02:00');
    const arrivalDate = text(`Destination date`, '2018-07-23T01:15:00+01:00');

    return (
      <DestinationTime
        departureDate={departureDate}
        arrivalDate={arrivalDate}
      />
    );
  });
