import React from 'react';
import PropTypes from 'prop-types';

import Segment from '../Segment';

const Leg = ({ leg, id }) => {
  const [segment] = leg.segments;
  return <Segment data={segment} id={id} />;
};

Leg.propTypes = {
  leg: PropTypes.object.isRequired,
};

export default Leg;
