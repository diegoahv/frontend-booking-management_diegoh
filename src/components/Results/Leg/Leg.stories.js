import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';

import ItineraryProvider from '../Itinerary/Provider';
import { selectHighlightedType } from '../Itinerary/Itinerary.stories';

import Leg from './';
import { selectSegmentInfo } from '../Segment/Segment.stories';

const selectLegInfo = ({ numberOfSections }) => ({
  segments: [selectSegmentInfo(numberOfSections)],
});

const selectItineraryInfo = (label = '') => ({
  highlightedType: selectHighlightedType(),
});

storiesOf('OpenTicket/Results/Leg', module)
  .addDecorator(withKnobs)
  .add('Without stops', () => {
    const leg = selectLegInfo({
      numberOfSections: 1,
    });
    return (
      <ItineraryProvider value={selectItineraryInfo()}>
        <Leg leg={leg} id={0} />
      </ItineraryProvider>
    );
  })
  .add('With stops', () => {
    const leg = selectLegInfo({
      numberOfSections: 2,
    });

    return (
      <ItineraryProvider value={selectItineraryInfo()}>
        <Leg leg={leg} id={0} />
      </ItineraryProvider>
    );
  });
