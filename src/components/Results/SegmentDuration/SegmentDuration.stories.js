import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, number } from '@storybook/addon-knobs';

import SegmentDuration from './index';

storiesOf('OpenTicket/Results/SegmentDuration', module)
  .addDecorator(withKnobs)
  .add('Default', () => {
    const duration = number('Duration in minutes', 105);

    return <SegmentDuration duration={duration} />;
  });
