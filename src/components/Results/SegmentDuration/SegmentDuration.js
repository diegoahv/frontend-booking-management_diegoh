import React from 'react';
import PropTypes from 'prop-types';
import { Flex, Text } from 'prisma-design-system';
import { useTranslation } from 'react-i18next';
import { minutesToHoursAndMinutes } from 'utils/DateUtils';

const SegmentDuration = ({ duration, priority = 8 }) => {
  const { t } = useTranslation();
  if (!duration && duration !== 0) return false;

  const { hours, minutes } = minutesToHoursAndMinutes(duration);

  return (
    <Flex justifyContent="center" py={1}>
      <Text priority={priority}>
        {t('results:segment.duration', { hours, minutes })}
      </Text>
    </Flex>
  );
};

SegmentDuration.propTypes = {
  duration: PropTypes.number,
  priority: PropTypes.number,
};

export default SegmentDuration;
