import React from 'react';
import SegmentDuration from '../';
import { render } from 'test-utils';

describe('SegmentDuration', () => {
  test('If duration prop does not exist component is not shown.', () => {
    const { container } = render(<SegmentDuration />);
    expect(container.textContent).toBe('');
  });

  test('If duration prop exists, it shows the component', () => {
    const { container } = render(<SegmentDuration duration={105} />);

    expect(container.textContent).toBe("1h 45'");
  });

  test('Check in minutes appear 2 decimals', () => {
    const durationInMinutes = 120;

    const { getByText } = render(
      <SegmentDuration duration={durationInMinutes} />
    );

    expect(getByText("2h 00'")).toBeVisible();
  });

  test('Large duration', () => {
    const durationInMinutes = 1450;
    const { container } = render(
      <SegmentDuration duration={durationInMinutes} />
    );
    expect(container.textContent).toBe("24h 10'");
  });
});
