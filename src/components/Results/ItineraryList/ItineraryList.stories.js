import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import ItineraryList from '.';

import { number } from '@storybook/addon-knobs';
import { ItineraryMocks } from 'serverMocks';
import { ResultsSegmentMocks } from '../Segment/__mocks__';

storiesOf('OpenTicket/Results/ItineraryList', module)
  .addDecorator(withKnobs)
  .add('Default', () => {
    const { SegmentSample } = ResultsSegmentMocks();
    const { ItineraryWith } = ItineraryMocks();
    const options = {
      range: true,
      min: 1,
      max: 20,
      step: 1,
    };
    const length = number('Number of Itineraries', 2, options);
    const itineraries = Array.from({ length }, (item, index) =>
      ItineraryWith({
        highlightedType: index === 0 ? 'recommended' : 'default',
        price: {
          price: 12.5,
          currency: 'USD',
        },
        segments: [
          SegmentSample({
            duration: 95,
          }),
          SegmentSample({
            duration: 65,
          }),
        ],
      })
    );

    return (
      <ItineraryList
        itineraries={itineraries}
        selectFlight={action('SelectedFlight')}
      />
    );
  });
