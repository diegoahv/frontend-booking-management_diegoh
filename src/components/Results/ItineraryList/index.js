import Itinerary from '../Itinerary';
import React, { Fragment } from 'react';

import { Box } from 'prisma-design-system';

const ItineraryList = ({ itineraries, selectFlight }) => {
  return (
    <Fragment>
      {itineraries.map((itinerary) => (
        <Box key={itinerary.id} mb={4}>
          <Itinerary itinerary={itinerary} selectFlight={selectFlight} />
        </Box>
      ))}
    </Fragment>
  );
};

export default ItineraryList;
