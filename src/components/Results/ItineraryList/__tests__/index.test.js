import React from 'react';
import { render } from 'test-utils';
import { getAllByTestId } from '@testing-library/dom';

import ItineraryList from '../';

import { ItineraryMocks } from 'serverMocks';
import { ResultsSegmentMocks } from '../../Segment/__mocks__';

describe('Itinerary Summary List', () => {
  test('Check length', () => {
    const { SegmentSample } = ResultsSegmentMocks();
    const { ItineraryWith } = ItineraryMocks();
    const itineraries = [
      ItineraryWith({
        price: {
          price: 20.1,
          currency: 'EUR',
        },
        segments: [
          SegmentSample({
            duration: 95,
          }),
        ],
      }),
      ItineraryWith({
        price: {
          price: 26.8,
          currency: 'EUR',
        },
        segments: [
          SegmentSample({
            duration: 45,
          }),
        ],
      }),
    ];

    const { container } = render(
      <ItineraryList itineraries={itineraries} selectFlight={jest.fn()} />
    );

    expect(getAllByTestId(container, 'itinerary')).toHaveLength(2);
  });
});
