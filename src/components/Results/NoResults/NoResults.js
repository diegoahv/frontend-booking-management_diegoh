import React from 'react';
import { Flex, Text, Button } from 'prisma-design-system';
import styled from '@emotion/styled';
import css from '@styled-system/css';

const StyledButton = styled(Button)(
  css({
    backgroundColor: 'brandPrimary.2',
    borderColor: 'brandPrimary.2',
    borderRadius: '22px',
  })
);

const StyledText = styled(Text)(
  css({
    fontSize: 'body.3',
  })
);

const NoResults = ({ onCallToAction, content, icon }) => {
  return (
    <>
      <Flex width="100%" flexDirection="column" alignItems="center" mb={7}>
        {icon}
        <Flex mt={3} mb={2}>
          <StyledText fontWeight="medium" textAlign="center" fontSize="18px">
            {content.title}
          </StyledText>
        </Flex>
        <Flex mb={6}>
          <Text textAlign="center" priority={5}>
            {content.text}
          </Text>
        </Flex>
        <StyledButton
          type="primary"
          size="medium"
          onClick={onCallToAction}
          fullWidth
        >
          <Text align="center">{content.cta}</Text>
        </StyledButton>
      </Flex>
    </>
  );
};

export default NoResults;
