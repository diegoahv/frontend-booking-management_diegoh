import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import HotelPlaneErrorIcon from '@/src/common/icons/HotelPlaneErrorIcon';

import NoResults from './NoResults';

const content = {
  title: "Sorry, we can't find any flights for that search",
  text: 'Why not try flying to a nearby airport or adjusting your dates?',
  cta: 'New Search',
};

storiesOf('OpenTicket/Results/NoResults', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    return (
      <NoResults
        content={content}
        icon={<HotelPlaneErrorIcon />}
        onCallToAction={() => alert('new search invoked')}
      ></NoResults>
    );
  });
