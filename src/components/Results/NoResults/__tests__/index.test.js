import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import NoResults from '../.';

const content = {
  title: 'title',
  text: 'text',
  cta: 'cta',
};

describe('NoResults', () => {
  describe('Actions', () => {
    test('CTA content to be present', () => {
      const onCTA = jest.fn();

      const { getByText } = render(
        <NoResults onCallToAction={onCTA} content={content}></NoResults>
      );
      expect(getByText(content.cta)).toBeVisible();
    });
    test('onCallToAction function is called when button is clicked', () => {
      const onCTA = jest.fn();

      const { getByText } = render(
        <NoResults onCallToAction={onCTA} content={content}></NoResults>
      );
      fireEvent.click(getByText(content.cta));
      expect(onCTA).toHaveBeenCalled();
    });
  });
});
