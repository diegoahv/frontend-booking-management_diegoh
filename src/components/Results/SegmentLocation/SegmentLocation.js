import React from 'react';
import { Text } from 'prisma-design-system';

const SegmentLocation = ({ location, desktop, customStyle }) =>
  desktop ? (
    <Text as="span" css={customStyle} priority={8}>
      {`${location.cityName} (${location.iata})`}
    </Text>
  ) : (
    <Text as="div" css={customStyle} priority={3}>
      {location.iata}
    </Text>
  );

export default SegmentLocation;
