import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, select } from '@storybook/addon-knobs';

import { Locations } from 'serverMocks/Location';
import SegmentLocation from './index';

const selectableLocations = Locations().reduce((result, location) => {
  result[location.name] = location;
  return result;
}, {});

export const selectLocation = (label = 'location', defaultLocation) => {
  const options = Object.keys(selectableLocations);
  const location =
    options.find((name) => name.includes(defaultLocation)) || options[0];
  const locationName = select(label, options, location);
  return selectableLocations[locationName];
};

storiesOf('OpenTicket/Results/SegmentLocation', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const location = selectLocation();

    return <SegmentLocation location={location} />;
  });
