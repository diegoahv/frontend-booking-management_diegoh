import React, { Fragment } from 'react';
import { Box, Hr } from 'prisma-design-system';

import Leg from '../Leg';

const LegList = ({ legs }) => {
  const arrLength = legs.length;

  return (
    <div>
      {legs.map((leg, index) => (
        <Fragment key={index}>
          <Box mb={4}>
            <Leg leg={leg} id={index} />
          </Box>
          {index < arrLength - 1 && (
            <Box mb={4}>
              <Hr type="dashed" />
            </Box>
          )}
        </Fragment>
      ))}
    </div>
  );
};

export default LegList;
