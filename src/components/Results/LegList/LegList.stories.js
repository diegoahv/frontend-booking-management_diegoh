import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, number } from '@storybook/addon-knobs';
import { selectSegmentInfo } from '../Segment/Segment.stories';

import LegList from './LegList';
import { selectHighlightedType } from '../Itinerary/Itinerary.stories';
import ItineraryProvider from '../Itinerary/Provider';

export const selectLeg = (label = '(Leg)') => ({
  segments: [selectSegmentInfo(1)],
});

export const selectLegs = (label = '') => {
  const numberOfLegs = number(`Number of legs ${label}:`, 2, {
    min: 1,
  });
  const leg = selectLeg('For each leg');

  return Array.from({ length: numberOfLegs }, () => leg);
};

const selectItineraryInfo = (label = '') => ({
  highlightedType: selectHighlightedType(),
});

storiesOf('OpenTicket/Results/LegList', module)
  .addDecorator(withKnobs)
  .add('Base', () => {
    return (
      <ItineraryProvider value={selectItineraryInfo()}>
        <LegList legs={selectLegs()} />
      </ItineraryProvider>
    );
  });
