import { DateTime, Text } from 'prisma-design-system';
import React from 'react';

const SegmentTime = ({ date, priority = 1 }) => (
  <Text priority={priority}>
    <DateTime value={date} type="time" pattern="short" />
  </Text>
);

export default SegmentTime;
