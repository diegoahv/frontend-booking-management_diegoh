import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from 'utils/test-utils';
import DepartureTime from '../';

describe('DepartureTime', () => {
  test('content', () => {
    const departureDate = '2020-04-27T22:25:40+08:00';
    const { getByText } = render(
      <DepartureTime departureDate={departureDate} />
    );
    expect(getByText('22:25')).toBeVisible();
  });
});
