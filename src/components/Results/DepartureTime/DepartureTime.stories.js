import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';

import DepartureTime from './';

storiesOf('OpenTicket/Results/DepartureTime', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const departureDate = text(`Departure date`, '2018-07-22T23:30:00+02:00');

    return <DepartureTime departureDate={departureDate} />;
  });
