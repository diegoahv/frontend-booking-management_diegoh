import React from 'react';
import SegmentTime from './SegmentTime';
import PropTypes from 'prop-types';
import DestinationTime from '../DestinationTime';

const DepartureTime = ({ departureDate, priority = 1 }) => (
  <SegmentTime date={departureDate} priority={priority} />
);

DestinationTime.propTypes = {
  departureDate: PropTypes.string.isRequired,
};

export default DepartureTime;
