import React from 'react';
import { useTranslation } from 'react-i18next';

const Component = () => {
  const { t } = useTranslation();

  return (
    <React.Fragment>
      content for "results:baggage.included":
      {t('results:baggage.included')}
    </React.Fragment>
  );
};

export default {
  title: 'Playground/Content/1',
  component: Component,
};

export const Default = () => <Component />;
