import React from 'react';
import styled from '@emotion/styled';
import { Text } from 'prisma-design-system';

/*
    More info
    https://styled-system.com/guides/build-a-box/#extending
*/

const ExtendedWithStyledText = styled(Text)({
  color: 'red',
  fontWeight: '800',
});

const ExtendedWithStyledComponent = () => (
  <ExtendedWithStyledText priority={1}>
    extended with styled (color: red, fontWeight: 800)
  </ExtendedWithStyledText>
);

const ExtendedWithCssComponent = () => (
  <Text
    priority={1}
    css={{
      color: 'blue',
      fontWeight: '300',
    }}
  >
    extended with css (color: blue, fontWeight: 300)
  </Text>
);

const Component = () => (
  <React.Fragment>
    <ul>
      <li>
        <ExtendedWithStyledComponent />
      </li>
      <li>
        <ExtendedWithCssComponent />
      </li>
    </ul>
  </React.Fragment>
);

export default {
  title: 'Playground/Prisma/1 - Extending',
  component: Component,
};

export const Deafult = () => <Component />;
