import React, { useState } from 'react';

export default function useControlledInput({
  placeholder,
  inputType,
  initialValue,
}) {
  const [state, setState] = useState(initialValue);
  const el = function () {
    return (
      <input
        type={inputType}
        placeholder={placeholder}
        value={state}
        onChange={(event) => setState(event.target.value)}
      />
    );
  };

  return [el, state];
}
