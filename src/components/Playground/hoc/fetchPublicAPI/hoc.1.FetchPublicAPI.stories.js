import React, { useEffect, useReducer } from 'react';
import { Row, Col } from 'prisma-design-system';
import reducer from './reducers/reducer';
import {
  loadingSuccess,
  loadingError,
  startLoading,
} from './actions/action-creators';
import MoviesContainer from './components/Movies/MoviesContainer';
import NasaPicturesList from './components/NasaPicturesList';

const initialState = {
  loading: false,
  error: false,
  data: [],
};

const Hoc = function Hoc({
  InputComponent,
  url,
  onSuccess = (input) => input,
}) {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    dispatch(startLoading());
    fetch(url)
      .then((res) => res.json())
      .then((data) => {
        const mappedData = onSuccess(data);
        return dispatch(loadingSuccess(mappedData));
      })
      .catch((error) => dispatch(loadingError()));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (state.loading) {
    return <p>Loading...</p>;
  } else if (state.error) {
    return <p>Error...</p>;
  }
  return <InputComponent data={state.data} dispatch={dispatch} />;
};

const url1 = 'https://ghibliapi.herokuapp.com/films';
const url2 =
  'https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=DEMO_KEY';

function onSuccess(data) {
  return data.photos.map((photo) => photo.img_src);
}

export default {
  title: 'Playground/HOC/1 - Fetch Public API',
  component: Hoc,
};

export const FetchMoviesAndPictures = () => {
  return (
    <>
      <Row>
        <Col col={[12, 6, 6]}>
          <Hoc InputComponent={MoviesContainer} url={url1}></Hoc>
        </Col>
        <Col col={[12, 6, 6]}>
          <Hoc
            InputComponent={NasaPicturesList}
            url={url2}
            onSuccess={onSuccess}
          ></Hoc>
        </Col>
      </Row>
    </>
  );
};
