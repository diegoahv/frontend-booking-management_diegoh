import ActionTypes from '../actions/action-types';

export default function reducer(status, action) {
  switch (action.type) {
    case ActionTypes.START_LOADING:
      return {
        loading: true,
        error: false,
        data: [],
      };
    case ActionTypes.LOADING_SUCCESS:
      return {
        loading: false,
        error: false,
        data: [...action.payload],
      };
    case ActionTypes.LOADING_ERROR:
      return {
        loading: false,
        error: true,
        data: [],
      };
    case ActionTypes.ADD_ITEM:
      return {
        loading: false,
        error: false,
        data: [action.payload, ...status.data],
      };
    default:
      return status;
  }
}
