import ActionTypes from './action-types';

export function startLoading() {
  return {
    type: ActionTypes.START_LOADING,
  };
}

export function loadingError() {
  return {
    type: ActionTypes.LOADING_ERROR,
  };
}

export function loadingSuccess(payload) {
  return {
    type: ActionTypes.LOADING_SUCCESS,
    payload,
  };
}

export function addItem(payload) {
  return {
    type: ActionTypes.ADD_ITEM,
    payload,
  };
}
