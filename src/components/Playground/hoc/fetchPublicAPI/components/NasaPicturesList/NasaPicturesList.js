import React from 'react';
import PropTypes from 'prop-types';

export default function NasaPicturesList({ data = [] }) {
  const maxPictures = 100;
  return (
    <div>
      {data.slice(0, maxPictures).map((picture, index) => {
        return (
          <img key={index} width="100" height="100" src={picture} alt="" />
        );
      })}
    </div>
  );
}

NasaPicturesList.propTypes = {
  data: PropTypes.arrayOf(String).isRequired,
};
