import React from 'react';
import Movie from '../Movie';
import PropTypes from 'prop-types';

export default function MoviesList({ movies }) {
  return (
    <div>
      {movies.map((movie) => {
        return (
          <Movie
            title={movie.title}
            description={movie.description}
            key={movie.id}
          ></Movie>
        );
      })}
    </div>
  );
}

MoviesList.propTypes = {
  movies: PropTypes.arrayOf(Object).isRequired,
};
