import React from 'react';
import { Card } from 'prisma-design-system';
import useControlledInput from '../../../hooks/useControlledInput';
export default function AddMovieForm({ submit }) {
  const [TitleInputEl, titleInputState] = useControlledInput({
    placeholder: 'title',
    inputType: 'text',
    initialValue: '',
  });
  const [DescriptionInputEl, descriptionInputState] = useControlledInput({
    placeholder: 'description',
    inputType: 'text',
    initialValue: '',
  });

  return (
    <Card>
      <TitleInputEl />
      <DescriptionInputEl />
      <button
        onClick={() => {
          submit(titleInputState, descriptionInputState);
        }}
      >
        Add a movie
      </button>
    </Card>
  );
}
