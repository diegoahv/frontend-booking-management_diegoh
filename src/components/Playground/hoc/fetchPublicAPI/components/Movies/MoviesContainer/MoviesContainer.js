import React from 'react';
import MoviesList from '../MoviesList';
import AddMovieForm from '../AddMovieForm';
import { addItem } from '../../../actions/action-creators';

export default function MoviesContainer({ data, dispatch }) {
  return (
    <div>
      <AddMovieForm submit={submit}></AddMovieForm>
      <MoviesList movies={data}></MoviesList>
    </div>
  );

  function submit(title, description) {
    dispatch(addItem({ title: title, description: description }));
  }
}
