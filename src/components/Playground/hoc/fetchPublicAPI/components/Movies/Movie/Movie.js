import React from 'react';
import PropTypes from 'prop-types';
import { Card } from 'prisma-design-system';

export default function Movie({ title, description }) {
  return (
    <Card>
      <div>{title}</div>
      <div>{description}</div>
    </Card>
  );
}

Movie.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};
