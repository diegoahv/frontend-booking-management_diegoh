import React from 'react';

import { useTranslation } from 'react-i18next';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import { DebugAssistant, Mocks } from '@frontend-react-component-builder/debug';

const contextMock = Mocks.CustomApplicationContextMock({
  session: {
    ...Mocks.Session.DefaultSession(),
    brandNewNomenclature: 'OP',
    flow: 'desktop',
    locale: Mocks.Session.Locale.ES_ES,
    website: 'ES',
  },
  ab: Mocks.Ab.DefaultAb(),
});

const KEY = 'results:baggage.included';

const ExampleComponent = () => {
  const { t } = useTranslation();

  const [session] = ApplicationContext.useSession();
  const { brandNewNomenclature, flow, locale, website } = session;
  return (
    <DebugAssistant.AssistantApplicationContextMocker
      applicationContext={contextMock}
    >
      <p>
        content for "{KEY}":{` ${t(KEY)}`}
      </p>
      <ul>
        <li>brand: {brandNewNomenclature}</li>
        <li>flow: {flow}</li>
        <li>locale: {locale}</li>
        <li>website: {website}</li>
      </ul>
    </DebugAssistant.AssistantApplicationContextMocker>
  );
};

export default {
  title: 'Playground/Context/2 - ApplicationContext Mock',
  component: ExampleComponent,
};

export const Default = () => <ExampleComponent />;
