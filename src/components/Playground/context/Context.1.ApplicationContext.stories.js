import React from 'react';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';

const Component = () => {
  const [session] = ApplicationContext.useSession();
  const {
    brandNewNomenclature,
    flow,
    locale,
    website,
    isMeta,
    marketingPortalIsMeta,
  } = session;

  return (
    <ul>
      <li>brand: {brandNewNomenclature}</li>
      <li>flow: {flow}</li>
      <li>locale: {locale}</li>
      <li>website: {website}</li>
      <li>isMeta: {isMeta}</li>
      <li>marketingPortalIsMeta: {marketingPortalIsMeta}</li>
    </ul>
  );
};

export default {
  title: 'Playground/Context/1 - ApplicationContext',
  component: Component,
};

export const Default = () => <Component />;
