import React from 'react';
import styled from '@emotion/styled';
import PropTypes from 'prop-types';

const StyledDiv = styled.div`
  background: red;
  color: ${(props) => props.color};
  border: 4px solid ${(props) => (props.withBorder ? 'blue' : 'transparent')};

  &.dotted {
    border-style: dotted;
  }
`;

const Component = ({ color, withBorder, dotted }) => (
  <StyledDiv
    className={dotted ? 'dotted' : ''}
    color={color}
    withBorder={withBorder}
  >
    My Component
  </StyledDiv>
);

Component.propTypes = {
  color: PropTypes.string.isRequired,
  withBorder: PropTypes.bool,
  dotted: PropTypes.bool,
};

export default {
  title: 'Playground/Basics/4 - Emotion',
  component: Component,
};

export const White = () => <Component color="white" withBorder />;

export const Dotted = () => <Component color="green" withBorder dotted />;

export const Yellow = () => <Component color="yellow" />;
