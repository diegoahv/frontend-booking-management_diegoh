import React, { useState } from 'react';

const Component = () => {
  const [counter, setCounter] = useState(0);

  const onClick = (e) => setCounter(counter + 1);

  return (
    <React.Fragment>
      <button onClick={onClick}>Click me!</button>
      <p>Clicked {counter} times</p>
    </React.Fragment>
  );
};

export default {
  title: 'Playground/Basics/5 - useState',
  component: Component,
};

export const Deafult = () => <Component />;
