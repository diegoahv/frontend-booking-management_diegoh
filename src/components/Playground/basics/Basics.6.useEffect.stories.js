import React, { useState, useEffect } from 'react';

const Component = () => {
  const [counter, setCounter] = useState(0);

  const onClick = (e) => setCounter(counter + 1);

  console.log("I've been rendered");

  useEffect(() => {
    console.log('counter has changed');
  }, [counter]);

  useEffect(() => {
    console.log('Component has been mounted');
  }, []);

  return (
    <React.Fragment>
      <button onClick={onClick}>Click me!</button>
      <p>Clicked {counter} times</p>
    </React.Fragment>
  );
};

export default {
  title: 'Playground/Basics/6 - useEffect',
  component: Component,
};

export const Deafult = () => <Component />;
