import React, { useReducer } from 'react';
import PropTypes from 'prop-types';
import { getTheme, Row, Col } from 'prisma-design-system';
import reducer from './basics-07-useReducer/reducers/counter';
import {
  increment,
  decrement,
  reset,
} from './basics-07-useReducer/actions/action-creators';

const buttonStyles = {
  fontSize: '20px',
  backgroundColor: 'white',
  color: getTheme().colors.brandPrimary[4],
  display: 'inline-block',
  textAlign: 'center',
};

const Counter = function ({ initialValue, step, max, min }) {
  const [state, dispatch] = useReducer(reducer, {
    count: initialValue,
    initialValue,
    step,
    max,
    min,
  });

  function incrementCounter() {
    dispatch(increment());
  }

  function decrementCounter() {
    dispatch(decrement());
  }

  function resetCounter() {
    dispatch(reset());
  }

  return (
    <>
      <Row colGap={2} rowGap={2}>
        <Col>
          <span css={buttonStyles}>{state.count}</span>
        </Col>
        <Col>
          <button onClick={decrementCounter}>Decrement</button>
        </Col>
        <Col>
          <button onClick={incrementCounter}>Increment</button>
        </Col>
        <Col>
          <button onClick={resetCounter}>Reset</button>
        </Col>
      </Row>
    </>
  );
};

Counter.propTypes = {
  initialValue: PropTypes.number,
  step: PropTypes.number,
  max: PropTypes.number,
  min: PropTypes.number,
};

Counter.defaultProps = {
  initialValue: 0,
  step: 1,
  max: 10,
  min: 0,
};

export default {
  title: 'Playground/Basics/7 - useReducer',
  component: Counter,
};

export const DefaultValues = () => <Counter />;
export const ModifyDefaultValues = () => (
  <Counter initialValue={50} step={10} max={100} min={0} />
);
