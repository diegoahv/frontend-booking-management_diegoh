import React from 'react';
import PropTypes from 'prop-types';

const Component = ({ text1, text2 = 'default', number, obj }) => (
  <div>
    <ul>
      <li>{text1}</li>
      <li>{text2}</li>
      <li>{number}</li>
      <li>{JSON.stringify(obj)}</li>
    </ul>
  </div>
);

Component.propTypes = {
  text1: PropTypes.string.isRequired,
  text2: PropTypes.string,
  number: PropTypes.number,
  obj: PropTypes.object,
};

export default {
  title: 'Playground/Basics/2 - Props',
  component: Component,
};

export const Default = () => (
  <Component text1="var" text2={'foo'} number={3} obj={{ key: 'value' }} />
);
