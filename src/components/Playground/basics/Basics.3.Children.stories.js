import React from 'react';
import PropTypes from 'prop-types';

const Child1 = ({ text1 }) => (
  <div style={{ backgroundColor: 'magenta', padding: '10px' }}>{text1}</div>
);

Child1.propTypes = {
  text1: PropTypes.string.isRequired,
};

const Child2 = () => <span>Child 2</span>;

const Parent = ({ children }) => (
  <div style={{ backgroundColor: 'cyan', padding: '10px' }}>{children}</div>
);

Parent.propTypes = {
  children: PropTypes.node,
};

export default {
  title: 'Playground/Basics/3 - Children',
  component: Parent,
};

export const WithChild1 = () => (
  <Parent>
    <Child1 text1="hello" />
  </Parent>
);

export const WithChild2 = () => (
  <Parent>
    <Child2 />
  </Parent>
);

export const WithOther = () => (
  <Parent>
    <strong>1</strong>
    <br />
    <b>2</b>
  </Parent>
);
