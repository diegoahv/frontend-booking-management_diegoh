/* React components */

import React from 'react';

const Component = ({ text }) => <React.Fragment>{text}</React.Fragment>;

const Example = () => {
  return <span>hola</span>;
};

function Example2() {
  return (
    <div className="div-class">
      <div>1</div>
      <div>2</div>
    </div>
  );
}

function Example3() {
  return (
    <React.Fragment>
      <div>3</div>
      <div>4</div>
    </React.Fragment>
  );
}

/* Playground setup and variants */

export default {
  title: 'Playground/Basics/1 - Hello World!',
  component: Component,
};

export const Default1 = () => <Component text="Hello World!" />;

export const Default2 = () => <Component text="Hello Flavour!" />;

export const ExampleOne = () => <Example />;

export const ExampleTwo = () => <Example2 />;

export const ExampleThree = () => <Example3 />;
