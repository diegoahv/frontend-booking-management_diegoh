import actionTypes from '../actions/action-types';

export default function reducer(status, action) {
  switch (action.type) {
    case actionTypes.INCREMENT:
      return {
        ...status,
        count: Math.min(status.count + status.step, status.max),
      };
    case actionTypes.DECREMENT:
      return {
        ...status,
        count: Math.max(status.count - status.step, status.min),
      };
    case actionTypes.RESET:
      return {
        ...status,
        count: status.initialValue,
      };
    default:
      return status;
  }
}
