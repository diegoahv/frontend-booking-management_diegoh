import React from 'react';

import { Tracking } from '@frontend-react-component-builder/conchita';

const Component = () => {
  return (
    <Tracking.TrackableAppereances action="app_action" label="app_label">
      <Tracking.TrackableClick action="click_action" label="click_label">
        <p>
          See the console to see the tracking mock
          <button onClick={() => console.log('click')}>Click</button>
        </p>
      </Tracking.TrackableClick>
    </Tracking.TrackableAppereances>
  );
};

export default {
  title: 'Playground/Tracking/2 - trackable',
  component: Component,
};

export const Default = () => <Component />;
