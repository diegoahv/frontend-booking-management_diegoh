import React, { useEffect } from 'react';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';

const ACTION = 'MyAction';
const LABEL = 'MyLabel';

const Component = () => {
  const tracking = ApplicationContext.useTracking();

  useEffect(() => {
    tracking.trackEvent(ACTION, LABEL);
  });

  return <p>See the console to see the tracking mock</p>;
};

export default {
  title: 'Playground/Tracking/1 - trackEvent',
  component: Component,
};

export const Default = () => <Component />;
