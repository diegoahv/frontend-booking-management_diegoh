import { Mocks } from '@frontend-react-component-builder/debug';

import { standaloneMainMenu } from '@/src/export';
import { DefaultBookingData } from 'serverMocks/bookingData';
import { getBookingInfo } from 'serverMocks';

const translations = require('@/assets/translations/ED/es-ES.json');

const mockContext = {
  ...Mocks.CustomApplicationContextMock(),
  application: {
    session: {
      ...Mocks.Session.DefaultSession(),
      brandNewNomenclature: 'ED',
      flow: 'mobile',
      locale: 'es_ES',
      website: 'ES',
    },
    ab: { mmbDocumentation: true, mmbAdditionalServices: true },
    data: { bookingInfo: getBookingInfo() },
  },
};

Mocks.mockTranslations(translations);

Mocks.Fetcher.mockGet(
  '/service/bookingchangerequest/mybooking/conchita.the.best@odigeo.com/7082689696',
  DefaultBookingData()
);

Mocks.Fetcher.mockGet(
  '/service/users/managebookingtoken?bookingid=7082689696&email=conchita.the.best@odigeo.com',
  {
    messageError: null,
    responseOK: true,
    token: 'token',
  }
);

if (typeof window !== 'undefined') {
  standaloneMainMenu({
    translationPath: '/translations',
    applicationContext: mockContext,
    container: document.querySelector('#one-front-root'),
  }).then((ref) => console.log('ref', ref));
}
