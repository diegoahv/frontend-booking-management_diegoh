import React from 'react';
import MainMenu from './MainMenu';
import { Mocks, DebugAssistant } from '@frontend-react-component-builder/debug';
import { getBookingInfo } from 'serverMocks';

import { DefaultBookingData } from 'serverMocks/bookingData';

const contextMockByFlow = (flow, newFlags = false) =>
  Mocks.CustomApplicationContextMock({
    session: {
      ...Mocks.Session.DefaultSession(),
      flow,
    },
    ab: {
      mmbDocumentation: true,
      mmbAdditionalServices: true,
      mmbNewFlagsPlus5846Desktop: newFlags,
    },
    data: { bookingInfo: getBookingInfo() },
  });

Mocks.Fetcher.mockGet(
  '/service/bookingchangerequest/mybooking/conchita.the.best@odigeo.com/7082689696',
  DefaultBookingData()
);

Mocks.Fetcher.mockGet(
  '/service/users/managebookingtoken?bookingid=7082689696&email=conchita.the.best@odigeo.com',
  {
    messageError: null,
    responseOK: true,
    token: 'token',
  }
);

export default {
  title: 'MMB/Main Menu/Main Menu Container',
  component: MainMenu,
};

export const Mobile = () => (
  <DebugAssistant.AssistantApplicationContextMocker
    applicationContext={contextMockByFlow('mobile')}
  >
    <MainMenu />
  </DebugAssistant.AssistantApplicationContextMocker>
);

export const Desktop = () => (
  <DebugAssistant.AssistantApplicationContextMocker
    applicationContext={contextMockByFlow('desktop')}
  >
    <MainMenu />
  </DebugAssistant.AssistantApplicationContextMocker>
);

export const DesktopNewFlags = () => (
  <DebugAssistant.AssistantApplicationContextMocker
    applicationContext={contextMockByFlow('desktop', true)}
  >
    <MainMenu />
  </DebugAssistant.AssistantApplicationContextMocker>
);
