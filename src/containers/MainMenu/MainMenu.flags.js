import {
  MAIN_MENU_ENTRY_TYPES,
  DOCUMENTATION_ENTRY_TYPES,
} from '../../components/MenuPage/MenuEntry';

const isScheduleBooking = ({ flags = {} } = {}) => {
  const { canxFlags = {} } = flags;
  return flags.contracted === true && canxFlags.schChanges === true;
};

const isInvoluntaryBooking = ({ flags = {} } = {}) => {
  const { canxFlags = {} } = flags;
  return (
    flags.contracted === true &&
    canxFlags.schChanges === false &&
    canxFlags.involuntaryCanx === true
  );
};

const isCancellableBooking = ({ flags = {} } = {}) => {
  const { canxFlags = {} } = flags;
  return (
    flags.contracted === true &&
    canxFlags.schChanges === false &&
    canxFlags.involuntaryCanx === false &&
    canxFlags.contractedInCovid === true
  );
};

const isPostBooking = ({ flags = {} } = {}) => {
  return (
    flags.flown === false &&
    flags.lowcostBooking === false &&
    flags.contracted === true
  );
};

export default ({
  bookingData = {},
  shouldShowPrime = false,
  mmbNewFlagsPlus5846Desktop = false,
} = {}) => {
  const { flags = {} } = bookingData;
  const { bookingFlags = {} } = flags;

  return {
    [MAIN_MENU_ENTRY_TYPES.SCHEDULE]: isScheduleBooking(bookingData),
    [MAIN_MENU_ENTRY_TYPES.INVOLUNTARY]: isInvoluntaryBooking(bookingData),
    [MAIN_MENU_ENTRY_TYPES.MODIFY]: isCancellableBooking(bookingData),
    [MAIN_MENU_ENTRY_TYPES.REFUNDS]: bookingFlags.refunds === true,
    [MAIN_MENU_ENTRY_TYPES.DOCUMENTATION]: true,
    [DOCUMENTATION_ENTRY_TYPES.CONFIRMATION]: true,
    [DOCUMENTATION_ENTRY_TYPES.ETICKETS]: flags.eticket === true,
    [MAIN_MENU_ENTRY_TYPES.INVOICE]: true,
    [MAIN_MENU_ENTRY_TYPES.PRIME]: shouldShowPrime && flags.prime === false,
    [MAIN_MENU_ENTRY_TYPES.PASSENGERS]: true,
    [MAIN_MENU_ENTRY_TYPES.BAGS]: mmbNewFlagsPlus5846Desktop
      ? bookingFlags.bags
      : isPostBooking(bookingData),
    [MAIN_MENU_ENTRY_TYPES.SEATS]: mmbNewFlagsPlus5846Desktop
      ? bookingFlags.seats
      : isPostBooking(bookingData),
    [MAIN_MENU_ENTRY_TYPES.CHECKIN]: mmbNewFlagsPlus5846Desktop
      ? bookingFlags.checkin
      : true,
    [MAIN_MENU_ENTRY_TYPES.ADDSERVICES]: true,
  };
};
