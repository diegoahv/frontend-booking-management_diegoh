import React from 'react';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import MainMenu from './MainMenu';

export default ({ ...props }) => {
  const [{ isDesktop = false }] = ApplicationContext.useFlow();
  return isDesktop ? <MainMenu {...props} /> : <MainMenu {...props} />;
};
