import React, { useEffect, useState } from 'react';

import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import { useBookingInfo } from '@/src/utils/hooks';
import MainMenuPage from '../../components/MenuPage/MainMenuPage';
import getFlags from './MainMenu.flags';
import { getMyBookingUrl } from '../../common/utils/url_utils';
import { getTokenUrl } from '../../common/utils/url_utils';
import { useTranslation } from 'react-i18next';
import LoadingPage from '../../components/LoadingPage';

const MainMenu = () => {
  const { t } = useTranslation();
  const dispatch = ApplicationContext.useDispatch();
  const goBack = () => dispatch({ message: 'MMB:MainMenu:goBack' });
  const [flags, setFlags] = useState(null);
  const [token, setToken] = useState(null);
  const [addUrls, setAddUrls] = useState(null);
  const fetcher = ApplicationContext.useFetcher();
  const [
    { bookingId = '', buyer = {}, isPrimeEnabled = false },
  ] = useBookingInfo();
  const { mail, isPrimeUser } = buyer;
  const [{ mmbNewFlagsPlus5846Desktop = false }] = ApplicationContext.useAb();

  useEffect(() => {
    Promise.all([
      fetcher.get(getMyBookingUrl({ mail, bookingId })),
      fetcher.get(getTokenUrl({ mail, bookingId })),
    ]).then(([bookingData = {}, tokenData = {}]) => {
      const { addUrls = {} } = bookingData;
      const { token = '' } = tokenData;
      setToken(token);
      setAddUrls(addUrls);
      setFlags(
        getFlags({
          bookingData,
          shouldShowPrime: isPrimeEnabled && !isPrimeUser,
          mmbNewFlagsPlus5846Desktop,
        })
      );
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return flags ? (
    <div className="main-menu-page-component">
      <MainMenuPage
        bookingId={bookingId}
        flags={flags}
        addUrls={addUrls}
        token={token}
      />
    </div>
  ) : (
    <LoadingPage
      headerTitle={t('managebooking:home.header')}
      headerBackAction={goBack}
      loadingText={t('managebooking:home.loading.icon.label', '')}
    />
  );
};

export default MainMenu;
