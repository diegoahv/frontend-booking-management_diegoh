import getFlags from '../MainMenu.flags';
import {
  MAIN_MENU_ENTRY_TYPES,
  DOCUMENTATION_ENTRY_TYPES,
} from '../../../components/MenuPage/MenuEntry';

const checkObtainedEntries = ({ entries = [], control = [] } = {}) => {
  const enabledEntries = Object.entries(entries)
    .filter(([key, value]) => value)
    .reduce((result, [key]) => [...result, key], []);
  return expect(
    enabledEntries.length === control.length &&
      enabledEntries.every((entry) => control.includes(entry))
  ).toBe(true);
};

const checkIsNotObtainedEntries = ({ entries = [], control = [] } = {}) => {
  const enabledEntries = Object.entries(entries)
    .filter(([key, value]) => value)
    .reduce((result, [key]) => [...result, key], []);

  return expect(
    enabledEntries.length === control.length &&
      enabledEntries.every((entry) => control.includes(entry))
  ).toBe(false);
};

const getCancellationFlagsMock = (
  covid = false,
  schChanges = false,
  involuntary = false
) => {
  return {
    bookingData: {
      flags: {
        contracted: true,
        canxFlags: {
          contractedInCovid: covid,
          schChanges: schChanges,
          involuntaryCanx: involuntary,
        },
      },
    },
  };
};

const PERMANENT_ENTRIES = [
  MAIN_MENU_ENTRY_TYPES.DOCUMENTATION,
  DOCUMENTATION_ENTRY_TYPES.CONFIRMATION,
  MAIN_MENU_ENTRY_TYPES.INVOICE,
  MAIN_MENU_ENTRY_TYPES.PASSENGERS,
  MAIN_MENU_ENTRY_TYPES.CHECKIN,
  MAIN_MENU_ENTRY_TYPES.ADDSERVICES,
];

describe('MenuEntries tests', () => {
  describe('getFlags tests', () => {
    it('empty params', () => {
      checkObtainedEntries({
        entries: getFlags(),
        control: PERMANENT_ENTRIES,
      });
    });

    it('schedule', () => {
      checkObtainedEntries({
        entries: getFlags(getCancellationFlagsMock(true, true, true)),
        control: [...PERMANENT_ENTRIES, MAIN_MENU_ENTRY_TYPES.SCHEDULE],
      });
    });

    describe('involuntary', () => {
      it('show involuntary', () => {
        checkObtainedEntries({
          entries: getFlags(getCancellationFlagsMock(true, false, true)),
          control: [...PERMANENT_ENTRIES, MAIN_MENU_ENTRY_TYPES.INVOLUNTARY],
        });
      });

      it('not show involuntary', () => {
        checkIsNotObtainedEntries({
          entries: getFlags(getCancellationFlagsMock(true, true, true)),
          control: [...PERMANENT_ENTRIES, MAIN_MENU_ENTRY_TYPES.INVOLUNTARY],
        });
      });
    });

    describe('cancellation', () => {
      it('show cancellation', () => {
        checkObtainedEntries({
          entries: getFlags(getCancellationFlagsMock(true, false, false)),
          control: [...PERMANENT_ENTRIES, MAIN_MENU_ENTRY_TYPES.MODIFY],
        });
      });

      describe('not show cancellation', () => {
        it('schedule is true', () => {
          checkIsNotObtainedEntries({
            entries: getFlags(getCancellationFlagsMock(true, true, false)),
            control: [...PERMANENT_ENTRIES, MAIN_MENU_ENTRY_TYPES.MODIFY],
          });
        });

        it('involuntary is true', () => {
          checkIsNotObtainedEntries({
            entries: getFlags(getCancellationFlagsMock(true, false, true)),
            control: [...PERMANENT_ENTRIES, MAIN_MENU_ENTRY_TYPES.MODIFY],
          });
        });

        it('is not in covid range', () => {
          checkIsNotObtainedEntries({
            entries: getFlags(getCancellationFlagsMock()),
            control: [...PERMANENT_ENTRIES, MAIN_MENU_ENTRY_TYPES.MODIFY],
          });
        });
      });
    });

    it('refunds', () => {
      checkObtainedEntries({
        entries: getFlags({
          bookingData: {
            flags: {
              bookingFlags: {
                refunds: true,
              },
            },
          },
        }),
        control: [...PERMANENT_ENTRIES, MAIN_MENU_ENTRY_TYPES.REFUNDS],
      });
    });

    it('e-ticket', () => {
      checkObtainedEntries({
        entries: getFlags({
          bookingData: {
            flags: {
              eticket: true,
            },
          },
        }),
        control: [...PERMANENT_ENTRIES, DOCUMENTATION_ENTRY_TYPES.ETICKETS],
      });
    });

    it('prime', () => {
      checkObtainedEntries({
        entries: getFlags({
          bookingData: {
            flags: {
              prime: false,
            },
          },
          shouldShowPrime: true,
        }),
        control: [...PERMANENT_ENTRIES, MAIN_MENU_ENTRY_TYPES.PRIME],
      });
    });

    it('bags and seats', () => {
      checkObtainedEntries({
        entries: getFlags({
          bookingData: {
            flags: {
              flown: false,
              lowcostBooking: false,
              contracted: true,
            },
          },
        }),
        control: [
          ...PERMANENT_ENTRIES,
          MAIN_MENU_ENTRY_TYPES.BAGS,
          MAIN_MENU_ENTRY_TYPES.SEATS,
        ],
      });
    });

    it('bags, seats and checkin if mmbNewFlagsPlus5846Desktop', () => {
      checkObtainedEntries({
        entries: getFlags({
          mmbNewFlagsPlus5846Desktop: true,
          bookingData: {
            flags: {
              bookingFlags: {
                checkin: true,
                bags: true,
                seats: true,
              },
            },
          },
        }),
        control: [
          ...PERMANENT_ENTRIES,
          MAIN_MENU_ENTRY_TYPES.BAGS,
          MAIN_MENU_ENTRY_TYPES.SEATS,
        ],
      });
    });
  });
});
