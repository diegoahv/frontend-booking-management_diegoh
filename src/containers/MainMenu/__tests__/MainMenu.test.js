import React from 'react';
import { render } from 'test-utils';
import { act } from 'react-dom/test-utils';
import MainMenu from '../MainMenu';
import { Standalone } from '@frontend-react-component-builder/conchita';
import { Mocks } from '@frontend-react-component-builder/debug';
import { getBookingInfo } from 'serverMocks';

const UNRESOLVED_PROMISE = new Promise(() => {});

const RESOLVED_PROMISE = Promise.resolve();

const renderMainMenu = (data = {}) =>
  render(
    <Standalone
      applicationContext={Mocks.CustomApplicationContextMock(data)}
      events={{}}
    >
      <MainMenu />
    </Standalone>
  );

const checkComponentRendering = async ({
  fetcher,
  shouldBeRendered = false,
  done,
}) => {
  let renderedComponent;
  await act(async () => {
    renderedComponent = renderMainMenu({
      fetcher,
      data: { bookingInfo: getBookingInfo() },
    });
  });
  const { container } = renderedComponent;
  const component = container.querySelector('.main-menu-page-component');
  const emptyComponent = container.querySelector('.home-loading-page-wrapper');

  setImmediate(() => {
    expect(emptyComponent === null).toBe(shouldBeRendered);
    expect(component === null).toBe(!shouldBeRendered);
    done();
  });
};

describe('MainMenu tests', () => {
  describe('render inner component', () => {
    it('before resolving any fetch should show empty state', async (done) => {
      checkComponentRendering({
        fetcher: {
          get: () => UNRESOLVED_PROMISE,
        },
        shouldBeRendered: false,
        done,
      });
    });

    it('before resolving all fetchs should show empty state', async (done) => {
      let fetchCount = 0;

      checkComponentRendering({
        fetcher: {
          get: () => {
            const result =
              fetchCount === 0 ? RESOLVED_PROMISE : UNRESOLVED_PROMISE;
            fetchCount++;
            return result;
          },
        },
        shouldBeRendered: false,
        done,
      });
    });

    it('after resolving all fetchs should show component', async (done) => {
      checkComponentRendering({
        fetcher: {
          get: () => RESOLVED_PROMISE,
        },
        shouldBeRendered: true,
        done,
      });
    });
  });

  describe('fetch requests', () => {
    it('should call proper endpoints', async () => {
      const BOOKING_ID = 'BOOKING_ID';
      const MAIL = 'MAIL';
      const fetcherMock = jest.fn();

      await act(async () => {
        renderMainMenu({
          fetcher: {
            get: fetcherMock,
          },
          data: {
            bookingInfo: {
              ...getBookingInfo(),
              bookingId: BOOKING_ID,
              buyer: { mail: MAIL },
            },
          },
        });
      });
      expect(fetcherMock).toHaveBeenCalledWith(
        '/service/bookingchangerequest/mybooking/MAIL/BOOKING_ID'
      );
      expect(fetcherMock).toHaveBeenCalledWith(
        '/service/users/managebookingtoken?bookingid=BOOKING_ID&email=MAIL'
      );
    });
  });
});
