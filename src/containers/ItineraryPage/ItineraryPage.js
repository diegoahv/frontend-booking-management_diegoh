import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { Flex, Wrapper } from 'prisma-design-system';
import { useTranslation } from 'react-i18next';
import FooterWithPrice from '@/src/components/Trip/FooterWithPrice';
import Itinerary from '@/src/components/Trip/Itinerary';
import Page from '@/src/components/Page/Page';
import PageTopContent from '@/src/components/Page/PageTopContent';
import useAppRouter from '@/src/export/OpenTicket/useAppRouter';
import { Tracking } from '@frontend-react-component-builder/conchita';
import { getTracking } from '@/src/containers/ItineraryPage/tracking';
import { ApplicationContext } from 'frontend-react-component-builder/dist/conchita';

const ItineraryPage = ({ itinerary, bookingId, tripType }) => {
  const { t } = useTranslation();
  const appRouter = useAppRouter();
  const tracking = ApplicationContext.useTracking();

  const onClick = useCallback(() => {
    const { category, action, label } = getTracking('CONTINUE_BUTTON');
    tracking.trackEventWithCategory(category, action, label);
    appRouter.next();
  }, [tracking, appRouter]);

  const onBackClick = useCallback(() => {
    const { category, action, label } = getTracking('BACK_BUTTON');
    tracking.trackEventWithCategory(category, action, label);
    appRouter.previous();
  }, [tracking, appRouter]);

  const footer = (
    <FooterWithPrice
      price={itinerary.price}
      onClick={onClick}
      variant="fixed"
    />
  );

  return (
    <Page
      title={t('searchPage:header')}
      backAction={onBackClick}
      fixedHeader="fixed"
      footer={footer}
      isFixedFooter={true}
    >
      <Tracking.TrackablePage page={'open-ticket/itinerary-details'}>
        <Wrapper px={1}>
          <Flex flexDirection="column" px={3}>
            <PageTopContent bookingId={bookingId} menuTitle={t('trip:title')} />
          </Flex>
          <Itinerary itinerary={itinerary} tripType={tripType} />
        </Wrapper>
      </Tracking.TrackablePage>
    </Page>
  );
};

ItineraryPage.propTypes = {
  itinerary: PropTypes.object.isRequired,
  bookingId: PropTypes.string.isRequired,
  tripType: PropTypes.string.isRequired,
};

export default ItineraryPage;
