const LABEL_PREFIX = 'pb_flexible_';
const LABEL_SUFFIX = '_pag:flit';

const ACTIONS = {
  searcher_flights_flexible_ticket: 'searcher_flights_flexible_ticket',
  main_button_flexible_ticket: 'main_button_flexible_ticket',
  info_box_flexible_ticket: 'info_box_flexible_ticket',
};

const labelDecorator = (label) => `${LABEL_PREFIX}${label}${LABEL_SUFFIX}`;

const tracking = {
  CONTINUE_BUTTON: {
    category: ' pb_flexible_itinerary_page',
    label: labelDecorator('go_next'),
    action: ACTIONS.searcher_flights_flexible_ticket,
  },
  BACK_BUTTON: {
    category: 'pb_flexible_itinerary_page',
    label: labelDecorator('go_back'),
    action: ACTIONS.main_button_flexible_ticket,
  },
};

export const getTracking = (key, data) => {
  return data ? tracking[key](data) : tracking[key];
};
