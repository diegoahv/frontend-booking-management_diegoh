import React from 'react';
import { storiesOf } from '@storybook/react';
import {
  boolean,
  number,
  select,
  text,
  withKnobs,
} from '@storybook/addon-knobs';
import { ItineraryMocks } from 'serverMocks/Itinerary';
import generateSegments from '@/src/components/Trip/Itinerary/__mocks__';
import { tripTypes } from '@/src/common/utils/enums';
import ItineraryPage from './';
import StoryRouter from 'storybook-react-router';

storiesOf('OpenTicket/Pages/Itinerary Page', module)
  .addDecorator(StoryRouter())
  .addDecorator(withKnobs)
  .add('Playground', () => {
    const selectPrice = (label = '') => ({
      price: number(`${label} price amount:`, 164.9),
      currency: select('currency', ['USD', 'EUR'], 'USD'),
    });
    const bookingId = text('BookingId', '12345678');
    const tripType = select(
      'Trip Type',
      Object.keys(tripTypes),
      tripTypes.ONE_WAY.type
    );
    const withStopovers = boolean('With stops', true);
    const withTechnicalStopovers = boolean('With technical stops', true);

    const segments = generateSegments(
      tripTypes[tripType].length,
      withStopovers,
      withTechnicalStopovers
    );
    const { ItineraryWith } = ItineraryMocks();
    const itinerary = ItineraryWith({
      price: selectPrice(),
      segments: segments,
    });
    return (
      <ItineraryPage
        itinerary={itinerary}
        bookingId={bookingId}
        tripType={tripType}
      />
    );
  });
