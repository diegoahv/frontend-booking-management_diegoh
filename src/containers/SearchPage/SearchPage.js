import React from 'react';
import { css } from '@styled-system/css';
import { Text, Box } from 'prisma-design-system';

import PageTopContent from '@/src/components/Page/PageTopContent';
import FlexibleTicketCard from '../../common/components/FlexTicketCard';
import Search from './Search';
import styled from '@emotion/styled';

const FlexibleTicketCardBlockTitle = styled(Text)(
  css({
    color: 'neutrals.800',
    fontWeight: 'medium',
    fontSize: 'content.1',
    marginBottom: '4',
  })
);

function SearchPage({ bookingInfo = {}, carrier, content }) {
  const { page, search, flexibleTicketCard } = content;

  const scrollToRef = (ref) => {
    ref.current.scrollIntoView({ behavior: 'smooth' });
  };
  const bookingId = bookingInfo.bookingId;

  return (
    <Box>
      <PageTopContent bookingId={bookingId} menuTitle={page.subtitle} />
      <FlexibleTicketCardBlockTitle as="div">
        {flexibleTicketCard.header}
      </FlexibleTicketCardBlockTitle>
      <FlexibleTicketCard carrier={carrier} content={flexibleTicketCard} />
      <Search
        bookingInfo={bookingInfo}
        content={search}
        scrollToFlightResults={scrollToRef}
      />
    </Box>
  );
}

export default SearchPage;
