const LABEL_SEARCH_PREFIX = 'pb_flexible_';
const LABEL_SEARCH_SUFFIX = '_pag:flho';
const LABEL_RESULTS_PREFIX = 'pb_flexible_';
const LABEL_RESULTS_SUFFIX = '_pag:flres';

const ACTIONS = {
  searcher_flights_flexible_ticket: 'searcher_flights_flexible_ticket',
  main_button_flexible_ticket: 'main_button_flexible_ticket',
  info_box_flexible_ticket: 'info_box_flexible_ticket',
};

const TRAVELLER_TYPES = {
  ADULT: 'ADULT',
  CHILDREN: 'CHILDREN',
  INFANT: 'INFANT',
};

const labelGoBackDecorator = (label) =>
  `${LABEL_RESULTS_PREFIX}${label}${LABEL_RESULTS_SUFFIX}`;

const labelResultClickedDecorator = (label, data) => {
  const { id, priceDifference } = data;
  const M = 'price';
  const P = id;
  return `${LABEL_RESULTS_PREFIX}${label}${id}_tab:${M}_pos:${P}_prcdif:${Math.floor(
    priceDifference
  )}${LABEL_RESULTS_SUFFIX}`;
};

const labelDecorator = (label) =>
  `${LABEL_SEARCH_PREFIX}${label}${LABEL_SEARCH_SUFFIX}`;

const labelFlightTypeDecorator = (data) => {
  const { flightType = '' } = data;
  return `${LABEL_SEARCH_PREFIX}${flightType.toLowerCase()}${LABEL_SEARCH_SUFFIX}`;
};
const labelCabinClassDecorator = (label, data) => {
  const { cabinClass = '' } = data;
  return `${LABEL_SEARCH_PREFIX}${label}${cabinClass.toLowerCase()}${LABEL_SEARCH_SUFFIX}`;
};
const labelNumPaxDecorator = (label, data) => {
  const { travellers } = data;
  const { adults, children, infant } = getNumPaxByType(travellers);
  return `${LABEL_SEARCH_PREFIX}${label}${adults}_${children}_${infant}${LABEL_SEARCH_SUFFIX}`;
};
const labelPaxShortcutDecorator = (label, data) => {
  const { travellers } = data;
  const numPax = travellers.length;
  return `${LABEL_SEARCH_PREFIX}${label}${numPax}${LABEL_SEARCH_SUFFIX}`;
};

const getNumPaxByType = (travellers) => {
  return travellers.reduce(
    (accum, curr) => {
      if (curr.travellerType === TRAVELLER_TYPES.ADULT) {
        accum.adults += 1;
      } else if (curr.travellerType === TRAVELLER_TYPES.CHILDREN) {
        accum.children += 1;
      } else if (curr.travellerType === TRAVELLER_TYPES.INFANT) {
        accum.infant += 1;
      }
      return accum;
    },
    { adults: 0, children: 0, infant: 0 }
  );
};

const tracking = {
  TERMS_AND_CONDITIONS: {
    category: 'pb_flexible_home_page',
    label: labelDecorator('terms_conditions'),
    action: ACTIONS.searcher_flights_flexible_ticket,
  },
  ONE_WAY: {
    category: 'pb_flexible_home_page',
    label: labelDecorator('select_one_way'),
    action: ACTIONS.searcher_flights_flexible_ticket,
  },
  ROUND_TRIP: {
    category: 'pb_flexible_home_page',
    label: labelDecorator('select_return'),
    action: ACTIONS.searcher_flights_flexible_ticket,
  },
  BACK_SEARCH_BUTTON: {
    category: 'pb_flexible_home_page',
    label: labelDecorator('go_back'),
    action: ACTIONS.main_button_flexible_ticket,
  },
  TERMS_CLOSE: {
    category: 'pb_flexible_home_page',
    label: labelDecorator('close_conditions'),
    action: ACTIONS.info_box_flexible_ticket,
  },
  TERMS_MORE_INFO: {
    category: 'pb_flexible_home_page',
    label: labelDecorator('more_conditions'),
    action: ACTIONS.info_box_flexible_ticket,
  },
  SEARCH_SUCCESS: {
    category: 'pb_flexible_home_page',
    label: labelDecorator('search_flights_1'),
    action: ACTIONS.searcher_flights_flexible_ticket,
  },
  SEARCH_FLIGHT_TYPE_SUCCESS: function (data) {
    return {
      category: 'pb_flexible_home_page',
      label: labelFlightTypeDecorator(data),
      action: ACTIONS.searcher_flights_flexible_ticket,
    };
  },
  SEARCH_CABIN_CLASS_SUCCESS: function (data) {
    return {
      category: 'pb_flexible_home_page',
      label: labelCabinClassDecorator('cabin_class_', data),
      action: ACTIONS.searcher_flights_flexible_ticket,
    };
  },
  SEARCH_NUM_PAX_SUCCESS: function (data) {
    return {
      category: 'pb_flexible_home_page',
      label: labelNumPaxDecorator('number_pax_', data),
      action: ACTIONS.searcher_flights_flexible_ticket,
    };
  },
  SEARCH_ERROR: {
    category: 'pb_flexible_home_page',
    label: labelDecorator('search_flights_FE_error'),
    action: ACTIONS.searcher_flights_flexible_ticket,
  },
  LOCATION_DEPARTURE_ERROR: {
    category: 'pb_flexible_home_page',
    label: labelDecorator('location_departure_leg_1_error'),
    action: ACTIONS.searcher_flights_flexible_ticket,
  },
  LOCATION_ARRIVAL_ERROR: {
    category: 'pb_flexible_home_page',
    label: labelDecorator('location_arrival_leg_1_error'),
    action: ACTIONS.searcher_flights_flexible_ticket,
  },
  DATE_DEPARTURE_ERROR: {
    category: 'pb_flexible_home_page',
    label: labelDecorator('date_departure_leg_1_error'),
    action: ACTIONS.searcher_flights_flexible_ticket,
  },
  DATE_ARRIVAL_ERROR: {
    category: 'pb_flexible_home_page',
    label: labelDecorator('date_arrival_leg_1_error'),
    action: ACTIONS.searcher_flights_flexible_ticket,
  },
  RESULT_CLICKED: function (data) {
    return {
      category: 'pb_flexible_results_page',
      label: labelResultClickedDecorator('sel_p:flight_res:', data),
      action: ACTIONS.info_box_flexible_ticket,
    };
  },
  BACK_RESULTS_BUTTON: {
    category: 'pb_flexible_results_page',
    label: labelGoBackDecorator('go_back'),
    action: ACTIONS.main_button_flexible_ticket,
  },
  PAX_SHORTCUT_ERROR: (data) => {
    return {
      category: 'pb_flexible_home_page',
      label: labelPaxShortcutDecorator('passenger_shortcut_', data),
      action: ACTIONS.searcher_flights_flexible_ticket,
    };
  },
};

export const getTracking = (key, data) => {
  return data ? tracking[key](data) : tracking[key];
};
