import React from 'react';
import SearchPage from './index';
import {
  mockEndpoint,
  autocompleteResponse,
  searchResults,
  getBookingInfo,
  getPNRInfo,
} from 'serverMocks';
import { boolean, withKnobs } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import { Mocks, DebugAssistant } from '@frontend-react-component-builder/debug';
import StoryRouter from 'storybook-react-router';
import { OpenTicketContextDecorator } from 'decorators/OpenTicketContextDecorator';

const getMock = () =>
  Mocks.CustomApplicationContextMock({
    data: {
      bookingInfo: getBookingInfo(),
      pnrInfo: getPNRInfo(),
      services: {
        bookingChangeRequest: (input) => '/' + input,
      },
    },
  });

storiesOf('OpenTicket/Pages/Search Page', module)
  .addDecorator(withKnobs)
  .addDecorator(StoryRouter())
  .addDecorator(OpenTicketContextDecorator)
  .add('Base', () => {
    const hasResults = boolean('Has results', true);

    mockEndpoint({
      method: 'get',
      url:
        '/service/geo/autocomplete;searchWord=osa;departureOrArrival=DEPARTURE;addSearchByCountry=true;addSearchByRegion=true;nearestLocations=true;product=FLIGHT',
      data: autocompleteResponse,
    });

    mockEndpoint({
      method: 'post',
      url: '/searchAlternatives',
      data: hasResults ? searchResults : null,
    });

    return (
      <DebugAssistant.AssistantApplicationContextMocker
        applicationContext={getMock()}
      >
        <SearchPage />
      </DebugAssistant.AssistantApplicationContextMocker>
    );
  });
