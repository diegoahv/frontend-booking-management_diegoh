import { tripTypes } from '@/src/common/utils/enums';

function searchRepository({ fetcher, urlFormatter }) {
  const module = {};

  module.getResults = function getResults(params) {
    return fetcher
      .post(urlFormatter('searchAlternatives'), {
        originalBookingId: params.bookingId,
        pnr: params.pnr,
        departureLocation: { iataCode: params.departureIata },
        destinationLocation: { iataCode: params.destinationIata },
        departureDate: formatDate(params.departureDate),
        searchType: getTripType(params.searchType),
        ...(params.searchType === tripTypes.ROUND_TRIP.type && {
          returnDate: formatDate(params.returnDate),
        }),
      })
      .then((results) => {
        return typeof results === 'object' && Object.keys(results).length > 0
          ? results
          : Promise.reject('no results');
      });
  };

  return module;
}

function getTripType(type) {
  switch (type) {
    case tripTypes.ONE_WAY.type:
      return 'ONE_WAY';
    case tripTypes.ROUND_TRIP.type:
      return 'RETURN_TRIP';
    default:
      return '';
  }
}

function formatDate(date) {
  if (date) {
    const month = date.getMonth() + 1;
    const day = date.getDate();
    return `${date.getFullYear()}-${formatNumber(month)}-${formatNumber(day)}`;
  }

  return '';
}

function formatNumber(number) {
  return (number < 10 ? '0' : '') + number;
}

export default searchRepository;
