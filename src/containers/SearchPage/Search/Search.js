import React, { useRef } from 'react';
import SearchForm from './SearchForm/SearchForm';
import * as actions from './actions';
import searchRepository from './search_repository';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import NoResults from '@/src/components/Results/NoResults';
import BottomDrawer from '@/src/common/components/BottomDrawer';
import ItineraryList from '@/src/components/Results/ItineraryList';
import HotelPlaneErrorIcon from '@/src/common/icons/HotelPlaneErrorIcon';
import { Tracking } from '@frontend-react-component-builder/conchita';
import { css } from '@styled-system/css';
import { Hr, Text, Box } from 'prisma-design-system';
import itineraryResolver from './SearchForm/SelectedItineraryResolver';
import { useOpenTicketContext } from '@/src/export/OpenTicket/OpenTicketContext';
import useAppRouter from '@/src/export/OpenTicket/useAppRouter';
import { formatDate } from 'utils/DateUtils';
import { tripTypes } from '@/src/common/utils/enums';
import { getTracking } from '@/src/containers/SearchPage/tracking';

function Search({ content, bookingInfo = {}, scrollToFlightResults }) {
  const [context] = ApplicationContext.useApplicationData();
  const { pnrInfo, services } = context;

  const fetcher = ApplicationContext.useFetcher();
  const flightResultsRef = useRef();
  const appRouter = useAppRouter();
  const [openTicketState, dispatch] = useOpenTicketContext();
  const state = openTicketState.results;
  const tracking = ApplicationContext.useTracking();
  const searchService = React.useMemo(
    () =>
      searchRepository({
        urlFormatter: services.bookingChangeRequest,
        fetcher,
      }),
    [services, fetcher]
  );

  const onLaunchSearch = (formState) => {
    dispatch(actions.startSearch());
    searchService
      .getResults({
        bookingId: bookingInfo.bookingId,
        pnr: pnrInfo.pnr,
        departureIata: formState.outbound.location.iata,
        destinationIata: formState.inbound.location.iata,
        departureDate: formState.outbound.date,
        returnDate: formState.inbound.date,
        searchType: formState.flightType,
      })
      .then((results) => {
        dispatch(actions.onResults(results));
        const { cabinClass } = results;
        const { flightType } = formState;
        const { travellers } = bookingInfo;

        sendSearchSuccessTracking({ flightType, cabinClass, travellers });
        scrollToFlightResults(flightResultsRef);
        tracking.trackPage(
          'open-ticket/results',
          getResultsCustomDimensions(formState)
        );
      })
      .catch((error) => {
        dispatch(actions.onError(error));
      });
  };

  const onSubmitError = (formState) => {
    sendSearchErrorTracking(formState);
  };

  const onCleanSearch = () => {
    dispatch(actions.onSearchReset());
  };

  const getHomeCustomDimensions = () => {
    return {
      customDimensions: {
        dimension5: 'FI+IN',
      },
    };
  };

  const getResultsCustomDimensions = (formState) => {
    const returnDate =
      formState.flightType === tripTypes.ROUND_TRIP.type
        ? formatDate(formState.inbound.date, 'dd-MM-yyyy')
        : undefined;

    return {
      customDimensions: {
        dimension2: formatDate(formState.outbound.date, 'dd-MM-yyyy'),
        dimension3: returnDate,
        dimension6: `${formState.outbound.location.iata}-${formState.inbound.location.iata}`,
      },
    };
  };

  const sendSearchSuccessTracking = (data) => {
    trackEvent(getTracking('SEARCH_SUCCESS'));
    trackEvent(getTracking('SEARCH_FLIGHT_TYPE_SUCCESS', data));
    trackEvent(getTracking('SEARCH_CABIN_CLASS_SUCCESS', data));
    trackEvent(getTracking('SEARCH_NUM_PAX_SUCCESS', data));
  };

  const sendSearchErrorTracking = (formState) => {
    trackEvent(getTracking('SEARCH_ERROR'));
    if (!formState.outbound.date) {
      trackEvent(getTracking('DATE_DEPARTURE_ERROR'));
    }
    if (
      !formState.inbound.date &&
      formState.flightType === tripTypes.ROUND_TRIP
    ) {
      trackEvent(getTracking('DATE_ARRIVAL_ERROR'));
    }
    if (!formState.outbound.location.name) {
      trackEvent(getTracking('LOCATION_DEPARTURE_ERROR'));
    }
    if (
      !formState.inbound.location.name &&
      formState.flightType === tripTypes.ROUND_TRIP
    ) {
      trackEvent(getTracking('LOCATION_ARRIVAL_ERROR'));
    }
  };

  const trackEvent = ({ category, action, label }) => {
    tracking.trackEventWithCategory(category, action, label);
  };

  return (
    <Tracking.TrackablePage
      page={'open-ticket/home'}
      config={getHomeCustomDimensions()}
    >
      <SearchForm
        content={content}
        onSubmit={onLaunchSearch}
        onSubmitError={onSubmitError}
        loading={state.loading}
        onCleanSearch={onCleanSearch}
        trackEvent={trackEvent}
      />
      {state.results && (
        <Box ref={flightResultsRef} pt={64}>
          <Hr
            css={css({
              borderTopWidth: '2px',
              width: '100%',
            })}
          />
          <Box mt={6} mb={6}>
            <Text
              css={css({
                color: 'neutrals.800',
                fontWeight: 'medium',
                fontSize: 'heading.1',
              })}
            >
              {content.resultsPage.title}
            </Text>
          </Box>
          <ItineraryList
            itineraries={state.results.itineraryResults}
            selectFlight={(itinerary) => {
              itinerary = itineraryResolver(itinerary);
              dispatch(actions.selectItinerary(itinerary));
              appRouter.next();
            }}
          />
        </Box>
      )}
      <BottomDrawer
        open={!!state.error}
        onClose={() => dispatch(actions.onSearchReset())}
      >
        <Tracking.TrackablePage
          page={'open-ticket/error/results_error_no_itineraries_results'}
        >
          <NoResults
            onCallToAction={() => dispatch(actions.onSearchReset())}
            icon={<HotelPlaneErrorIcon />}
            content={content.noResults}
          />
        </Tracking.TrackablePage>
      </BottomDrawer>
    </Tracking.TrackablePage>
  );
}

export default Search;
