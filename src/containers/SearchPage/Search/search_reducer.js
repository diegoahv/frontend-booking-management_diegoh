import * as actionTypes from './actions/search_action_types';

export const getInitialState = () => ({
  loading: false,
  error: '',
  results: null,
  selectedItinerary: {
    price: {
      price: null,
      priceDifference: null,
      originalPriceDifference: null,
      percDiscountApplied: null,
      currency: null,
    },
  },
});

function searchReducer(state, action) {
  switch (action.type) {
    case actionTypes.START_SEARCH: {
      state.loading = true;
      return { ...state };
    }
    case actionTypes.SEARCH_SUCCESS: {
      state.loading = false;
      state.results = action.payload;
      return { ...state };
    }
    case actionTypes.SEARCH_ERROR: {
      state.loading = false;
      state.error = action.payload;
      return { ...state };
    }
    case actionTypes.SEARCH_RESET: {
      return getInitialState();
    }
    case actionTypes.SELECT_ITINERARY: {
      state.selectedItinerary = action.payload;
      return { ...state };
    }
  }

  return state;
}

export default searchReducer;
