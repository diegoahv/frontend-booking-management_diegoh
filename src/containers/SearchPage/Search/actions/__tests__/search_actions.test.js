import * as actionTypes from '../search_action_types';
import * as searchActions from '../search_actions';

describe('Search Page actions tests', () => {
  describe('startSearch action tests', () => {
    runCommonExpectations(
      searchActions.startSearch(),
      actionTypes.START_SEARCH
    );
  });

  describe('onResults action tests', () => {
    const payload = [1, 2, 3];
    const action = searchActions.onResults([1, 2, 3]);
    runCommonExpectations(action, actionTypes.SEARCH_SUCCESS, payload);
  });

  describe('onError action tests', () => {
    const payload = 'fatal';
    const action = searchActions.onError('fatal');
    runCommonExpectations(action, actionTypes.SEARCH_ERROR, payload);
  });

  describe('onError action tests', () => {
    const action = searchActions.onSearchReset();
    runCommonExpectations(action, actionTypes.SEARCH_RESET);
  });
});

function runCommonExpectations(action, expectedType, expectedPayload) {
  it('should return an action with properties type and payload', () => {
    const expectedProperties = ['type', 'payload'];

    Object.keys(action).forEach((key) => {
      expect(expectedProperties).toContain(key);
    });
  });

  it('should return an action with the expected payload and type', () => {
    expect(action).toEqual({
      type: expectedType,
      payload: expectedPayload,
    });
  });
}
