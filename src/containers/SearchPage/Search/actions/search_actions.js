import {
  START_SEARCH,
  SEARCH_SUCCESS,
  SEARCH_ERROR,
  SEARCH_RESET,
  SELECT_ITINERARY,
} from './search_action_types';

export function startSearch() {
  return createAction(START_SEARCH);
}

export function onResults(results) {
  return createAction(SEARCH_SUCCESS, results);
}

export function onError(error) {
  return createAction(SEARCH_ERROR, error);
}

export function onSearchReset() {
  return createAction(SEARCH_RESET);
}

export function selectItinerary(itinerary) {
  return createAction(SELECT_ITINERARY, itinerary);
}

function createAction(type, payload) {
  return { type, payload };
}
