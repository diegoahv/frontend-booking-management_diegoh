import searchReducer, { getInitialState } from '../search_reducer';
import * as actions from '../actions';

let initialState = null;
beforeEach(() => {
  initialState = getInitialState();
});

describe('search reducer tests', () => {
  const results = [1, 2, 3];
  const error = 'fatal';

  describeCaseForAction({
    message: 'START_SEARCH reducer test',
    mutationObject: { loading: true },
    action: actions.startSearch,
  });

  describeCaseForAction({
    message: 'SEARCH_SUCCESS reducer test',
    mutationObject: { loading: false, results },
    action: actions.onResults,
    payload: results,
  });

  describeCaseForAction({
    message: 'SEARCH_ERROR reducer test',
    mutationObject: { loading: false, error },
    action: actions.onError,
    payload: error,
  });

  describeCaseForAction({
    message: 'SEARCH_RESET reducer test',
    mutationObject: { loading: false, error: '' },
    action: actions.onSearchReset,
  });
});

function describeCaseForAction({ message, mutationObject, action, payload }) {
  describe(message, () => {
    it('should return a new reference to state', () => {
      const newState = searchReducer(initialState, action(payload));
      expect(initialState).not.toBe(newState);
    });

    it('should set PROPERTY on state', () => {
      const newState = searchReducer(initialState, action(payload));
      expect(newState).toEqual(getExpectedState(mutationObject));
    });
  });
}

function getExpectedState({ loading = false, error = '', results = null }) {
  return {
    loading,
    error,
    results,
    selectedItinerary: {
      price: {
        price: null,
        priceDifference: null,
        originalPriceDifference: null,
        percDiscountApplied: null,
        currency: null,
      },
    },
  };
}
