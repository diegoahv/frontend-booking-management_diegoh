import { searchFormValidator } from './validator';
import { tripTypes } from '@/src/common/utils/enums';

const content = {
  errors: {
    empty_location: 'Location needed',
    empty_date: 'date needed',
    invalid_date: 'selected dates are not valid',
  },
};
const validator = searchFormValidator(content);

describe('Search form validator tests', () => {
  describe('locations tests', () => {
    it('should set an error if outbound location is empty', () => {
      const searchParams = getSearchParams();
      const notification = validator.validateForm(searchParams);

      expect(notification.errors.outboundLocation).toBe(
        content.errors.empty_location
      );
      expect(notification.errors.inboundLocation).toBe(
        content.errors.empty_location
      );
    });

    it('should be ok when location have an iata property', () => {
      const searchParams = getSearchParams({
        outboundLocation: { iata: 'BCN' },
      });
      const notification = validator.validateForm(searchParams);

      expect(notification.errors.outboundLocation).toBeFalsy();
    });
  });

  describe('date tests', () => {
    describe('one way date validations', () => {
      it('should set an error when date is not an instance of date', () => {
        const searchParams = getSearchParams();
        const notification = validator.validateForm(searchParams);

        expect(notification.errors.datePicker).toBe(content.errors.empty_date);
      });

      it('should set an error when date is a past date', () => {
        const searchParams = getSearchParams({
          outboundDate: new Date(2000, 8, 15),
        });
        const notification = validator.validateForm(searchParams);

        expect(notification.errors.datePicker).toBe(
          content.errors.invalid_date
        );
      });

      it('should be ok otherwise', () => {
        const searchParams = getSearchParams({
          outboundDate: new Date(),
        });
        const notification = validator.validateForm(searchParams);

        expect(notification.errors.datePicker).toBeFalsy();
      });
    });

    describe('round trip date validations', () => {
      it('should set an error when dates has an item that is not a date', () => {
        const searchParams = getSearchParams({
          outboundDate: new Date(),
          flightType: tripTypes.ROUND_TRIP.type,
        });
        const notification = validator.validateForm(searchParams);

        expect(notification.errors.datePicker).toBe(content.errors.empty_date);
      });

      it('should set an error when date is a past date', () => {
        const searchParams = getSearchParams({
          outboundDate: new Date(2000, 8, 15),
          inboundDate: new Date(),
          flightType: tripTypes.ROUND_TRIP.type,
        });
        const notification = validator.validateForm(searchParams);

        expect(notification.errors.datePicker).toBe(
          content.errors.invalid_date
        );
      });

      it('should set an error when inbound is before outbound', () => {
        const searchParams = getSearchParams({
          outboundDate: new Date(),
          inboundDate: new Date(2000, 8, 15),
          flightType: tripTypes.ROUND_TRIP.type,
        });
        const notification = validator.validateForm(searchParams);

        expect(notification.errors.datePicker).toBe(
          content.errors.invalid_date
        );
      });

      it('should be ok otherwise', () => {
        const searchParams = getSearchParams({
          outboundDate: new Date(),
          inboundDate: new Date(),
          flightType: tripTypes.ROUND_TRIP.type,
        });
        const notification = validator.validateForm(searchParams);

        expect(notification.errors.datePicker).toBeFalsy();
      });
    });
  });
});

function getSearchParams({
  outboundDate,
  inboundDate,
  flightType = tripTypes.ONE_WAY.type,
  outboundLocation = {},
  inboundLocation = {},
} = {}) {
  return {
    flightType,
    outbound: {
      location: outboundLocation,
      date: outboundDate,
    },
    inbound: {
      location: inboundLocation,
      date: inboundDate,
    },
  };
}
