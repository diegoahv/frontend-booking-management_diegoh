import { differenceInMinutes, parseISO } from 'date-fns';

const calculateMinutesDuration = (ending, starting) =>
  differenceInMinutes(parseISO(ending), parseISO(starting));

const calculateLocationChangeType = (
  destination,
  departure,
  arrivalTerminal,
  departureTerminal
) => {
  if (destination.id !== departure.id) {
    return 'airport';
  } else if (arrivalTerminal !== departureTerminal) {
    return 'terminal';
  }
  return 'none';
};

const createStopover = (previousSection, nextSection) => {
  const {
    destination,
    arrivalDate,
    arrivalTerminal,
    carrier,
    insuranceOffer,
  } = previousSection;
  const { departure, departureTerminal, departureDate } = nextSection;
  return {
    __typename: 'Stopover',
    duration: calculateMinutesDuration(departureDate, arrivalDate),
    locationChangeType: calculateLocationChangeType(
      destination,
      departure,
      arrivalTerminal,
      departureTerminal
    ),
    carrierChange: carrier.id !== nextSection.carrier.id,
    insuranceOffer: insuranceOffer || {
      policy: 'CARRIER_GUARANTEE',
      carrier,
    },
  };
};

const extendLocation = (location, terminal) => ({
  ...location,
  terminal,
  id: location.id + (terminal ? '_' + terminal : ''),
});

const addLocationType = (location, type) => ({
  ...location,
  type,
});

const createTrip = (
  section,
  { departure, destination, departureDate, arrivalDate, id }
) => ({
  ...section,
  __typename: 'Trip',
  id,
  departure,
  destination,
  departureDate,
  arrivalDate,
  duration: calculateMinutesDuration(arrivalDate, departureDate),
});

const sectionResolver = (section, isLastInSegment, isFirstSegment) => {
  const technicalStops = [];
  const trips = [];
  let departureLocation = extendLocation(
    addLocationType(
      section.departure,
      isFirstSegment ? 'absolute' : 'connection'
    ),
    section.departureTerminal
  );
  let tripDepartureDate = section.departureDate;

  section.technicalStops = section.technicalStops
    ? section.technicalStops.forEach((technicalStop, index) => {
        trips.push(
          createTrip(section, {
            id: `${section.id}_${index}`,
            departure: departureLocation,
            departureDate: tripDepartureDate,
            destination: addLocationType(technicalStop.location, 'connection'),
            arrivalDate: technicalStop.arrivalDate,
          })
        );
        technicalStops.push(technicalStopoverResolver(technicalStop));
        departureLocation = addLocationType(
          technicalStop.location,
          'connection'
        );
        tripDepartureDate = technicalStop.departureDate;
      })
    : [];

  trips.push(
    createTrip(section, {
      id: `${section.id}_${technicalStops.length}`,
      departure: departureLocation,
      destination: extendLocation(
        addLocationType(
          section.destination,
          isLastInSegment ? 'absolute' : 'connection'
        ),
        section.arrivalTerminal
      ),
      departureDate: tripDepartureDate,
      arrivalDate: section.arrivalDate,
    })
  );

  return {
    ...section,
    trips,
    technicalStops,
  };
};

const technicalStopoverResolver = (technicalStop) => {
  const duration = calculateMinutesDuration(
    technicalStop.departureDate,
    technicalStop.arrivalDate
  );
  return {
    ...technicalStop,
    duration,
  };
};

const segmentResolver = (segment) => {
  segment.stops = segment.sections
    .slice(0, -1)
    .map((previousSection, index) => {
      const nextSection = segment.sections[index + 1];
      return createStopover(previousSection, nextSection);
    });
  segment.sections = segment.sections.map((section, idx) =>
    sectionResolver(section, idx === segment.sections.length - 1, idx === 0)
  );
  return segment;
};

const legResolver = (leg) => {
  return {
    ...leg,
    segments: leg.segments.map((segment) => segmentResolver(segment)),
  };
};

export default function itineraryResolver(itinerary) {
  itinerary.legs.map((leg) => legResolver(leg));
  return itinerary;
}
