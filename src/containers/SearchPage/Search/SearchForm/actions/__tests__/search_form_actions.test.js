import * as actionTypes from '../action_types';
import * as searchFormActions from '../actions';
import { tripTypes } from '@/src/common/utils/enums';

describe('Search Form actions tests', () => {
  describe('setFlightType action tests', () => {
    const payload = tripTypes.ONE_WAY.type;
    const action = searchFormActions.setFlightType(payload);
    runCommonExpectations(action, actionTypes.SET_FLIGHT_TYPE, payload);
  });
  describe('setOutboundLocation action tests', () => {
    const payload = { name: 'barcelona' };
    const action = searchFormActions.setOutboundLocation(payload);
    runCommonExpectations(action, actionTypes.SET_OUTBOUND_LOCATION, payload);
  });
  describe('setInboundLocation action tests', () => {
    const payload = { name: 'barcelona' };
    const action = searchFormActions.setInboundLocation(payload);
    runCommonExpectations(action, actionTypes.SET_INBOUND_LOCATION, payload);
  });
  describe('setDates action tests', () => {
    const payload = [new Date(), new Date()];
    const action = searchFormActions.setDates(payload);

    runCommonExpectations(action, actionTypes.SET_DATES, payload);
  });

  describe('setInboundLocationError action tests', () => {
    const payload = 'Please introduce a valid location';
    const action = searchFormActions.setInboundLocationError(payload);

    runCommonExpectations(
      action,
      actionTypes.SET_INBOUND_LOCATION_ERROR,
      payload
    );
  });

  describe('setOutboundLocationError action tests', () => {
    const payload = 'Please introduce a valid location';
    const action = searchFormActions.setOutboundLocationError(payload);

    runCommonExpectations(
      action,
      actionTypes.SET_OUTBOUND_LOCATION_ERROR,
      payload
    );
  });

  describe('setValidationErrors action tests', () => {
    const payload = {
      outboundLocation: 'location needed',
    };
    const action = searchFormActions.setValidationErrors(payload);

    runCommonExpectations(action, actionTypes.VALIDATION_ERROR, {
      ...payload,
      datePicker: '',
      inboundLocation: '',
    });

    it('should return an object with expected keys as payload', () => {
      const expectedKeys = [
        'outboundLocation',
        'inboundLocation',
        'datePicker',
      ];
      Object.keys(action.payload).forEach((key) => {
        expect(expectedKeys).toContain(key);
      });
    });
  });
});

function runCommonExpectations(action, expectedType, expectedPayload) {
  it('should return an action with properties type and payload', () => {
    const expectedProperties = ['type', 'payload'];

    Object.keys(action).forEach((key) => {
      expect(expectedProperties).toContain(key);
    });
  });

  it('should return an action with the expected payload and type', () => {
    expect(action).toEqual({
      type: expectedType,
      payload: expectedPayload,
    });
  });
}
