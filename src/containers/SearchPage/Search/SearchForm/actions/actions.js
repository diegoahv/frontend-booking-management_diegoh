import {
  SET_FLIGHT_TYPE,
  SET_OUTBOUND_LOCATION,
  SET_DATES,
  SET_INBOUND_LOCATION,
  VALIDATION_ERROR,
  SET_OUTBOUND_LOCATION_ERROR,
  SET_INBOUND_LOCATION_ERROR,
} from './action_types';

export function setFlightType(type) {
  return createAction(SET_FLIGHT_TYPE, type.toUpperCase());
}

export function setOutboundLocation(location) {
  return createAction(SET_OUTBOUND_LOCATION, location);
}

export function setInboundLocation(location) {
  return createAction(SET_INBOUND_LOCATION, location);
}

export function setOutboundLocationError(error) {
  return createAction(SET_OUTBOUND_LOCATION_ERROR, error);
}

export function setInboundLocationError(error) {
  return createAction(SET_INBOUND_LOCATION_ERROR, error);
}

export function setDates(dates = []) {
  return createAction(SET_DATES, dates);
}

export function setValidationErrors({
  outboundLocation = '',
  inboundLocation = '',
  datePicker = '',
} = {}) {
  return createAction(VALIDATION_ERROR, {
    outboundLocation,
    inboundLocation,
    datePicker,
  });
}

function createAction(type, payload) {
  return { type, payload };
}
