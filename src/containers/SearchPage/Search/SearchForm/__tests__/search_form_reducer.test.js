import searchFormReducer, { getInitialState } from '../search_form_reducer';
import * as actions from '../actions';
import { tripTypes } from '@/src/common/utils/enums';

// todo: no se porque no funciona, creo que es por las variables declaradas en el describe
let initialState = null;
beforeEach(() => {
  initialState = getInitialState();
});

describe('searchForm reducer tests', () => {
  const flightType = tripTypes.ONE_WAY.type;
  const location = { name: 'BARCELONA' };
  const dates = [new Date(), new Date()];
  const datePickerError = "Sorry, we can't change the past";
  const locationError = 'Please, introduce a valid location';

  describeCaseForAction({
    message: 'SET_FLIGHT_TYPE reducer test',
    mutationObject: { flightType },
    action: actions.setFlightType,
    payload: flightType,
  });

  describeCaseForAction({
    message: 'SET_OUTBOUND_LOCATION reducer test',
    mutationObject: {
      outboundLocation: location,
    },
    action: actions.setOutboundLocation,
    payload: location,
  });

  describeCaseForAction({
    message: 'SET_INBOUND_LOCATION reducer test',
    mutationObject: {
      inboundLocation: location,
    },
    action: actions.setInboundLocation,
    payload: location,
  });

  describeCaseForAction({
    message: 'SET_DATES reducer test',
    mutationObject: { dates },
    action: actions.setDates,
    payload: dates,
  });

  describeCaseForAction({
    message: 'VALIDATION_ERROR reducer test',
    mutationObject: { datePickerError },
    action: actions.setValidationErrors,
    payload: { datePicker: datePickerError },
  });

  describeCaseForAction({
    message: 'SET_OUTBOUND_LOCATION_ERROR reducer test',
    mutationObject: { outboundLocationError: locationError },
    action: actions.setOutboundLocationError,
    payload: locationError,
  });

  describeCaseForAction({
    message: 'SET_INBOUND_LOCATION_ERROR reducer test',
    mutationObject: { inboundLocationError: locationError },
    action: actions.setInboundLocationError,
    payload: locationError,
  });
});

function describeCaseForAction({ message, mutationObject, action, payload }) {
  describe(message, () => {
    it('should return a new reference to state', () => {
      const newState = searchFormReducer(initialState, action(payload));
      expect(initialState).not.toBe(newState);
    });

    it('should set PROPERTY on state', () => {
      const newState = searchFormReducer(initialState, action(payload));
      expect(newState).toEqual(getExpectedState(mutationObject));
    });
  });
}

function getExpectedState({
  flightType = tripTypes.ONE_WAY.type,
  outboundLocation = {},
  dates = [],
  inboundLocation = {},
  outboundLocationError = '',
  inboundLocationError = '',
  datePickerError = '',
}) {
  return {
    flightType,
    outbound: {
      location: outboundLocation,
      date: dates[0],
    },
    inbound: {
      location: inboundLocation,
      date: dates[1],
    },
    errors: {
      outboundLocation: outboundLocationError,
      inboundLocation: inboundLocationError,
      datePicker: datePickerError,
    },
  };
}
