import * as actionTypes from './actions/action_types';
import { tripTypes } from '@/src/common/utils/enums';

export const getInitialState = ({
  tripType = tripTypes.ONE_WAY.type,
  outboundLocation = {},
  outboundDate = undefined,
  inboundLocation = {},
  inboundDate = undefined,
} = {}) => ({
  flightType: tripType,
  outbound: {
    location: outboundLocation,
    date: outboundDate,
  },
  inbound: {
    location: inboundLocation,
    date: inboundDate,
  },
  errors: {
    outboundLocation: '',
    inboundLocation: '',
    datePicker: '',
  },
});

const validDates = (selectedDates, state) => {
  switch (state.flightType) {
    case tripTypes.ONE_WAY.type:
      return selectedDates.length === 1;
    case tripTypes.ROUND_TRIP.type:
      return selectedDates.length === 2;
    default:
      return false;
  }
};

function searchFormReducer(state, action) {
  switch (action.type) {
    case actionTypes.SET_FLIGHT_TYPE: {
      state.flightType = action.payload;
      return { ...state };
    }
    case actionTypes.SET_OUTBOUND_LOCATION: {
      state.outbound.location = action.payload;
      return { ...state };
    }
    case actionTypes.SET_INBOUND_LOCATION: {
      state.inbound.location = action.payload;
      return { ...state };
    }
    case actionTypes.SET_OUTBOUND_LOCATION_ERROR: {
      state.errors.outboundLocation = action.payload;
      return { ...state };
    }
    case actionTypes.SET_INBOUND_LOCATION_ERROR: {
      state.errors.inboundLocation = action.payload;
      return { ...state };
    }
    case actionTypes.SET_DATES: {
      state.outbound.date = action.payload[0];
      state.inbound.date = action.payload[1];

      if (validDates(action.payload, state)) {
        state.errors.datePicker = '';
      }
      return { ...state };
    }
    case actionTypes.VALIDATION_ERROR: {
      state.errors = action.payload;
      return { ...state };
    }
  }
  return state;
}

export default searchFormReducer;
