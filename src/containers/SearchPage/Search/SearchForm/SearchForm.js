import React, { useRef } from 'react';
import styled from '@emotion/styled';
import {
  PlaneGoingIcon,
  PlaneLandingIcon,
  Text,
  Button,
} from 'prisma-design-system';
import LoadingSpinner from '@/src/common/components/LoadingSpinner';
import DatePicker from '@/src/common/components/DatePicker';
import Autocomplete from '@/src/components/Autocomplete';
import ItineraryTypeSelector from '@/src/components/ItineraryTypeSelector';
import { css } from '@styled-system/css';
import * as actions from './actions';
import { searchFormValidator } from './validator';
import { tripTypes } from '@/src/common/utils/enums';
import { useOpenTicketContext } from '@/src/export/OpenTicket/OpenTicketContext';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import { getTracking } from '@/src/containers/SearchPage/tracking';

const flightTypesButtonGroup = {
  ROUND_TRIP: 0,
  ONE_WAY: 1,
};

const StyledButton = styled(Button)(
  css({
    backgroundColor: 'brandPrimary.2',
    borderColor: 'brandPrimary.2',
    borderRadius: '22px',
  })
);

const FormTitle = styled(Text)(
  css({
    color: 'neutrals.800',
    fontWeight: 'medium',
    fontSize: 'content.1',
    marginTop: '6',
    marginBottom: '4',
  })
);

function SearchForm({
  content,
  onSubmit,
  onSubmitError,
  onCleanSearch,
  loading = false,
}) {
  const outboundLocationRef = useRef();
  const inboundLocationRef = useRef();
  const datePickerRef = useRef();
  const [openTicketState, dispatch] = useOpenTicketContext();
  const state = openTicketState.form;
  const tracking = ApplicationContext.useTracking();

  const callbacks = {
    setFlightType: (type) => {
      dispatch(actions.setFlightType(type));
      onCleanSearch();
    },
    setOutboundLocation: (location) => {
      dispatch(actions.setOutboundLocation(location));
      onCleanSearch();
    },
    setInboundLocation: (location) => {
      dispatch(actions.setInboundLocation(location));
      onCleanSearch();
    },
    setOutboundLocationError: (errorMessage) => {
      dispatch(actions.setOutboundLocationError(errorMessage));
      onCleanSearch();
    },
    setInboundLocationError: (errorMessage) => {
      dispatch(actions.setInboundLocationError(errorMessage));
      onCleanSearch();
    },
    setDates: (selectedDates) => {
      dispatch(actions.setDates(selectedDates));
      onCleanSearch();
    },
  };

  const { title, segment, flightTypeSwitch } = content;
  const onSelection = (action, predicate = () => {}) => {
    return (...args) => {
      const refs = [outboundLocationRef, inboundLocationRef, datePickerRef];
      action(...args);
      if (predicate(...args)) {
        focusNextElement(refs, ...args);
        onCleanSearch();
      }
    };
  };

  const validator = React.useMemo(() => searchFormValidator(content), [
    content,
  ]);

  const validateForm = () => {
    const notification = validator.validateForm(state);
    dispatch(actions.setValidationErrors(notification.errors));
    return notification;
  };

  const processForm = () => {
    if (!validateForm().hasErrors) {
      onSubmit(state);
    } else {
      onSubmitError(state);
    }
  };

  const initialFlightType = flightTypesButtonGroup[state.flightType];

  function onFlightTypeSelection(index) {
    const tripType =
      index === 0 ? tripTypes.ROUND_TRIP.type : tripTypes.ONE_WAY.type;
    callbacks.setFlightType(tripType);
    const { category, action, label } = getTracking(tripType);
    tracking.trackEventWithCategory(category, action, label);
  }

  return (
    <>
      <FormTitle as={'p'} fontWeight="medium" fontSize="content.1">
        {title}
      </FormTitle>
      <form>
        <div css={css({ marginBottom: '5' })}>
          <ItineraryTypeSelector
            fullWidth={true}
            getTracking={getTracking}
            initialSelection={initialFlightType}
            onClick={onFlightTypeSelection}
            content={flightTypeSwitch}
          />
        </div>
        <div css={css({ marginBottom: '2' })}>
          <Autocomplete
            label={segment.origin}
            setLocation={onSelection(
              callbacks.setOutboundLocation,
              validateLocation
            )}
            icon={PlaneGoingIcon}
            initialValue={state.outbound.location.name}
            ref={outboundLocationRef}
            selectedValue={state.outbound.location.name}
            setError={callbacks.setOutboundLocationError}
            error={state.errors.outboundLocation}
          />
        </div>
        <div css={css({ marginBottom: '2' })}>
          <Autocomplete
            label={segment.destination}
            setLocation={onSelection(
              callbacks.setInboundLocation,
              validateLocation
            )}
            icon={PlaneLandingIcon}
            initialValue={state.inbound.location.name}
            ref={inboundLocationRef}
            selectedValue={state.inbound.location.name}
            setError={callbacks.setInboundLocationError}
            error={state.errors.inboundLocation}
          />
        </div>
        <div css={css({ marginBottom: '5' })}>
          <DatePicker
            label={segment.date}
            onSelectDate={onSelection(callbacks.setDates, validateDates)}
            initialDate={state.outbound.date || new Date()}
            ref={datePickerRef}
            selectedDates={getInitialDates(state)}
            numberOfDates={tripTypes[state.flightType].length}
            from={new Date()}
            error={state.errors.datePicker}
          />
        </div>
        <StyledButton
          disabled={loading}
          fullWidth={true}
          nativeType="button"
          size="medium"
          variant="primary"
          onClick={processForm}
        >
          {loading && <LoadingSpinner color="white" />}
          {!loading && content.submitBtn}
        </StyledButton>
      </form>
    </>
  );
}

function getInitialDates(state) {
  const dates = [];
  if (state.outbound.date) {
    dates.push(state.outbound.date);
  }
  if (state.flightType === tripTypes.ROUND_TRIP.type && state.inbound.date) {
    dates.push(state.inbound.date);
  }

  return dates;
}

function focusNextElement(refs) {
  setTimeout(() => {
    const nextFocus = refs.find((item) => !item.current.value);

    if (nextFocus && nextFocus.current) {
      nextFocus.current.focus();
    }
  }, 0);
}

function validateLocation(location) {
  return Object.keys(location).length > 0;
}

function validateDates(dateRange, numberOfDates) {
  return dateRange.length === numberOfDates;
}

export default SearchForm;
