import { isAfterDate, isPastDate } from 'utils/DateUtils';
import { tripTypes } from '@/src/common/utils/enums';

export function searchFormValidator(content) {
  const module = {};

  module.validateLocation = function validateLocation(location) {
    return isInValidLocation(location) ? content.errors.empty_location : '';
  };

  module.validateDates = function validateDates(flightType, dates) {
    if (isEmptyDate(flightType, dates)) {
      return content.errors.empty_date;
    }
    return isInvalidDate(flightType, dates) ? content.errors.invalid_date : '';
  };

  module.validateForm = function validateForm(formState) {
    const { outbound, inbound, flightType } = formState;
    const errors = {
      outboundLocation: module.validateLocation(outbound.location),
      inboundLocation: module.validateLocation(inbound.location),
      datePicker: module.validateDates(flightType, [
        outbound.date,
        inbound.date,
      ]),
    };

    return {
      hasErrors: Object.values(errors).some(Boolean),
      errors,
    };
  };

  return module;
}

function isInValidLocation(location) {
  return !(typeof location.iata === 'string' && location.iata.length === 3);
}

function isEmptyDate(flightType, dateRange) {
  switch (flightType) {
    case tripTypes.ONE_WAY.type:
      return isNotDate(dateRange[0]);
    case tripTypes.ROUND_TRIP.type:
      return dateRange.some(isNotDate);
    default:
      return false;
  }
}

function isInvalidDate(flightType, dateRange) {
  switch (flightType) {
    case tripTypes.ONE_WAY.type:
      return isPastDate(dateRange[0]);
    case tripTypes.ROUND_TRIP.type:
      return (
        isPastDate(dateRange[0]) || !isAfterDate(dateRange[1], dateRange[0])
      );
    default:
      return false;
  }
}

function isNotDate(date) {
  return !(date instanceof Date);
}
