import React from 'react';
import { useTranslation } from 'react-i18next';
import { useBookingInfo } from '@/src/utils/hooks';
import useAppRouter from '@/src/export/OpenTicket/useAppRouter';
import SearchPage from '@/src/containers/SearchPage/SearchPage';
import Page from '@/src/components/Page/Page';
import { useOpenTicketContext } from '@/src/export/OpenTicket/OpenTicketContext';
import { getTracking } from '@/src/containers/SearchPage/tracking';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import { useCallback } from 'react';

function SearchPageContainer() {
  const [bookingInfo] = useBookingInfo();
  const { t } = useTranslation();
  const [state] = useOpenTicketContext();
  const [appData] = ApplicationContext.useApplicationData();
  const { carrier } = appData.pnrInfo;
  const content = React.useMemo(() => getContent(t, { carrier }), [t, carrier]);
  const appRouter = useAppRouter();
  const tracking = ApplicationContext.useTracking();

  const onBackClick = useCallback(() => {
    const { category, action, label } = state.results.results
      ? getTracking('BACK_RESULTS_BUTTON')
      : getTracking('BACK_SEARCH_BUTTON');
    tracking.trackEventWithCategory(category, action, label);
    appRouter.previous();
  }, [state, appRouter, tracking]);

  return (
    <Page
      title={content.page.header}
      backAction={onBackClick}
      fixedHeader="fixed"
    >
      <SearchPage
        content={content}
        bookingInfo={bookingInfo}
        carrier={carrier}
      />
    </Page>
  );
}

function getContent(t, params = {}) {
  const { code: carrierId, name: carrierName } = params.carrier;

  return {
    page: {
      headerOverline: t('searchPage:headerOverline', {
        bookingId: params.bookingId,
      }),
      header: t('searchPage:header'),
      subtitle: t('searchPage:subtitle'),
    },
    flexibleTicketCard: {
      header: t('searchPage:flexibleTicketCard.header'),
      title: t('myinfo:tripdetails.openticket.header.title'),
      subtitle: t('myinfo:tripdetails.openticket.header.description', {
        airline: carrierName,
      }),
      validUntil: t(
        `myinfo:openticket.tripdetails.info.expiration.${carrierId}`
      ),
      footer: t('myinfo:tripdetails.openticket.info.footer'),
      modalTitle: t('myinfo:tripdetails.openticket.tc.title'),
      modalBody: t(
        `myinfo:openticket.tripdetails.info.contenttext.${carrierId}`
      ),
    },
    search: {
      title: t('searchPage:form.title'),
      flightTypeSwitch: {
        roundTrip: t('searchPage:form.flightType.roundTrip'),
        oneWay: t('searchPage:form.flightType.oneWay'),
      },
      segment: {
        origin: t('searchPage:form.segment.origin.label'),
        destination: t('searchPage:form.segment.destination.label'),
        date: t('searchPage:form.segment.date.label'),
      },
      submitBtn: t('searchPage:form.submit.btn'),
      errors: {
        empty_location: t('searchPage:form.errors.empty_location'),
        empty_date: t('searchPage:form.errors.empty_date'),
        invalid_date: t('searchPage:form.errors.invalid_date'),
      },
      resultsPage: {
        title: t('resultsPage:title'),
      },
      noResults: {
        title: t('resultsPage:noResults.modal.title'),
        text: t('resultsPage:noResults.modal.text'),
        cta: t('resultsPage:noResults.modal.cta'),
      },
    },
  };
}

export default SearchPageContainer;
