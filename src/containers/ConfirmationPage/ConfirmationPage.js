import React from 'react';
import { Text, Box, Button } from 'prisma-design-system';
import { css } from '@styled-system/css';
import styled from '@emotion/styled';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import { platforms } from '@/src/common/utils/enums';

import Page from '@/src/components/Page/Page';
import ConfirmationSummary from '../../components/Summary/ConfirmationSummary';
import ProcedureInfo from '../../components/Confirmation/ProcedureInfo';
import ConfirmationStatus from '../../components/Confirmation/ConfirmationStatus';
import AppsDownloadBlock from '../../components/AppsDownloadBlock/AppsDownloadBlock';

function ConfirmationPage({
  itinerary,
  content,
  requestedFlights,
  redemptionRequest,
  backAction,
  CTAaction,
}) {
  const { page, confirmationStatusComponent } = content;
  const [appContext = {}] = ApplicationContext.useApplicationData();
  const { platform = platforms.MOBILE } = appContext;

  return (
    <Page
      title={page.headerTitle}
      pageClass="confirmation-page-wrapper"
      backAction={backAction}
      fixedHeader="fixed"
      css={{ paddingBottom: 0 }}
    >
      <PageHeader page={page} />
      <ConfirmationSummary
        itinerary={itinerary}
        requestedFlights={requestedFlights}
      />
      <ProcedureInfo onClickFunction={CTAaction} />
      <ConfirmationStatus
        content={confirmationStatusComponent}
        todDate={redemptionRequest.creationDate}
      />
      <StyledButton
        type="primary"
        size="medium"
        onClick={CTAaction}
        fullWidth={true}
      >
        {content.footer.cta}
      </StyledButton>
      {platform !== platforms.NATIVE && (
        <Box pt={7}>
          <AppsDownloadBlock />
        </Box>
      )}
    </Page>
  );
}

const StyledButton = styled(Button)(
  css({
    backgroundColor: 'brandPrimary.2',
    borderColor: 'brandPrimary.2',
    borderRadius: '22px',
  })
);

function PageHeader({ page }) {
  return (
    <Box mt={3}>
      <Text
        as={'span'}
        css={css({
          color: 'neutrals.600',
          fontWeight: 'regular',
          fontSize: 'content.2',
        })}
      >
        {page.headerOverline}
      </Text>
      <Text
        as={'p'}
        css={css({
          color: 'neutrals.800',
          fontWeight: 'medium',
          fontSize: 'title.2',
          marginTop: '2',
        })}
      >
        {page.header}
      </Text>
    </Box>
  );
}
export default ConfirmationPage;
