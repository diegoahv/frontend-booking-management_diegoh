import React from 'react';
import { RenderWithOpenTicketContext } from 'test-utils';
import { getAllByTestId, queryByTestId } from '@testing-library/dom';
import { ItineraryMocks } from 'serverMocks';
import { platforms } from '@/src/common/utils/enums';

import ConfirmationPageContainer from '../';
import { ResultsSegmentMocks } from '../../../components/Results/Segment/__mocks__';

const CONTINUE_BUTTON_SELECTOR = `button[type="button"]`;
const CONFIRMATIONSUMMARY_SELECTOR = `confirmationSummary`;
const PROCEDUREINFO_SELECTOR = `procedureInfo`;
const CONFIRMATIONSTATUS_SELECTOR = `confirmationStatus`;
const APPSDOWNLOADBLOCK_SELECTOR = `appsDownloadBlock`;

describe('ConfirmationPage tests', () => {
  const { ItineraryWith } = ItineraryMocks();
  const bookingInfo = {
    bookingId: '123456789',
    price: {
      amount: 156.63,
      currency: 'EUR',
    },
    travellers: [
      {
        name: 'TEST',
      },
      {
        name: 'TEST2',
      },
    ],
  };
  const { SegmentSample } = ResultsSegmentMocks();
  const itinerary = ItineraryWith({
    price: {
      price: 1000,
      currency: 'EUR',
    },
    segments: [
      SegmentSample({
        duration: 95,
      }),
    ],
  });

  describe('It should render the page and its components', function () {
    let renderResult;
    beforeEach(() => {
      renderResult = RenderWithOpenTicketContext(
        <ConfirmationPageContainer
          bookingData={bookingInfo}
          itinerary={itinerary}
        />
      );
    });
    it('Should render page wrapper', () => {
      const component = renderResult.container.querySelector(
        '.confirmation-page-wrapper'
      );
      expect(component !== null).toBe(true);
    });

    it('Should render ConfirmationSummary', () => {
      expect(
        getAllByTestId(renderResult.container, CONFIRMATIONSUMMARY_SELECTOR)
      ).toHaveLength(1);
    });

    it('Should render ProcedureInfo', () => {
      expect(
        getAllByTestId(renderResult.container, PROCEDUREINFO_SELECTOR)
      ).toHaveLength(1);
    });

    it('Should render ConfirmationStatus', () => {
      expect(
        getAllByTestId(renderResult.container, CONFIRMATIONSTATUS_SELECTOR)
      ).toHaveLength(1);
    });

    it('Should render FooterWithPrice', () => {
      const continueButton = renderResult.container.querySelector(
        CONTINUE_BUTTON_SELECTOR
      );
      expect(continueButton).not.toBeNull();
    });
  });

  describe('AppsDownloadBlock render tests', function () {
    const renderWithContext = (
      <ConfirmationPageContainer
        bookingData={bookingInfo}
        itinerary={itinerary}
      />
    );
    it('Should render AppsDownloadBlock if platform is MOBILE', () => {
      const renderResult = RenderWithOpenTicketContext(
        renderWithContext,
        platforms.MOBILE
      );
      expect(
        getAllByTestId(renderResult.container, APPSDOWNLOADBLOCK_SELECTOR)
      ).toHaveLength(1);
    });

    it('Should render AppsDownloadBlock if platform is DESKTOP', () => {
      const renderResult = RenderWithOpenTicketContext(
        renderWithContext,
        platforms.DESKTOP
      );
      expect(
        getAllByTestId(renderResult.container, APPSDOWNLOADBLOCK_SELECTOR)
      ).toHaveLength(1);
    });

    it('Should not render AppsDownloadBlock if platform is NATIVE', () => {
      const renderResult = RenderWithOpenTicketContext(
        renderWithContext,
        platforms.NATIVE
      );
      expect(
        queryByTestId(renderResult.container, APPSDOWNLOADBLOCK_SELECTOR)
      ).toBeNull();
    });
  });
});
