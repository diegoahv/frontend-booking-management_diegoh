import React from 'react';
import ConfirmationPageContainer from './index';
import { number, select, withKnobs } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import { getBookingInfo, ItineraryMocks, flightsMock } from 'serverMocks';
import { tripTypes } from '@/src/common/utils/enums';
import generateSegments from '@/src/components/Trip/Itinerary/__mocks__';
import {
  DebugAssistant,
  Mocks,
} from 'frontend-react-component-builder/dist/debug';
import { OpenTicketContextDecorator } from 'decorators/OpenTicketContextDecorator';
import StoryRouter from 'storybook-react-router';
import OpenTicketContext from '@/src/export/OpenTicket/OpenTicketContext';
import { mockOpenTicketContext } from '@/.storybook/utils';

const contextMock = () =>
  Mocks.CustomApplicationContextMock({
    session: {
      ...Mocks.Session.DefaultSession(),
    },
    data: { bookingInfo: getBookingInfo() },
  });

storiesOf('OpenTicket/Pages/Confirmation Page', module)
  .addDecorator(withKnobs)
  .addDecorator(OpenTicketContextDecorator)
  .addDecorator(StoryRouter())
  .add('Playground', () => {
    const selectPrice = (label = '') => ({
      price: number(`${label} price amount:`, 210),
      currency: select('currency', ['USD', 'EUR'], 'EUR'),
    });
    const tripType = select(
      'Trip Type',
      Object.keys(tripTypes),
      tripTypes.ONE_WAY.type
    );
    const segments = generateSegments(tripTypes[tripType].length);
    const { ItineraryWith } = ItineraryMocks();
    const itinerary = ItineraryWith({
      price: selectPrice(),
      segments: segments,
    });
    return (
      <DebugAssistant.AssistantApplicationContextMocker
        applicationContext={contextMock()}
      >
        <ConfirmationPageContainer itinerary={itinerary} />
      </DebugAssistant.AssistantApplicationContextMocker>
    );
  })
  .add('Redirected Confirmation', () => {
    const initState = mockOpenTicketContext({
      redemption: { requestedFlights: flightsMock },
    });
    return (
      <DebugAssistant.AssistantApplicationContextMocker
        applicationContext={contextMock()}
      >
        <OpenTicketContext.Provider value={[initState, () => {}]}>
          <ConfirmationPageContainer itinerary={null} />
        </OpenTicketContext.Provider>
      </DebugAssistant.AssistantApplicationContextMocker>
    );
  });
