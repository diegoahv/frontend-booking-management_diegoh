const LABEL_PREFIX = 'pb_flexible_';
const LABEL_SUFFIX = '_pag:flconf';

const ACTIONS = {
  searcher_flights_flexible_ticket: 'searcher_flights_flexible_ticket',
  main_button_flexible_ticket: 'main_button_flexible_ticket',
  info_box_flexible_ticket: 'info_box_flexible_ticket',
  trip_details_flexible_ticket: 'trip_details_flexible_ticket',
};

const labelDecorator = (label) => `${LABEL_PREFIX}${label}${LABEL_SUFFIX}`;

const tracking = {
  DOWNLOAD_APP: {
    category: ' pb_flexible_confirmation_page',
    label: labelDecorator('download_app'),
    action: ACTIONS.trip_details_flexible_ticket,
  },
  RETURN_TRIP_DETAILS: {
    category: 'pb_flexible_confirmation_page',
    label: labelDecorator('go_details'),
    action: ACTIONS.main_button_flexible_ticket,
  },
  BACK_BUTTON: {
    category: 'pb_flexible_confirmation_page',
    label: labelDecorator('go_back'),
    action: ACTIONS.main_button_flexible_ticket,
  },
};

export const getTracking = (key, data) => {
  return data ? tracking[key](data) : tracking[key];
};
