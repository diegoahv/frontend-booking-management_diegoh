import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';

import { useBookingInfo } from '@/src/utils/hooks';
import ConfirmationPage from '@/src/containers/ConfirmationPage/ConfirmationPage';
import { useOpenTicketContext } from '@/src/export/OpenTicket/OpenTicketContext';
import useAppRouter from '@/src/export/OpenTicket/useAppRouter';
import {
  ApplicationContext,
  Tracking,
} from '@frontend-react-component-builder/conchita';
import { getTracking } from '@/src/containers/ConfirmationPage/tracking';

function ConfirmationPageContainer({ itinerary }) {
  const [bookingInfo] = useBookingInfo();
  const { t } = useTranslation();
  const [state] = useOpenTicketContext();
  const bookingId = bookingInfo.bookingId;
  const numPax = bookingInfo.travellers.length;
  const appRouter = useAppRouter();
  const content = React.useMemo(() => getContent(t, { bookingId, numPax }), [
    t,
    bookingId,
    numPax,
  ]);
  const tracking = ApplicationContext.useTracking();

  const onBackClick = useCallback(() => {
    const { category, action, label } = getTracking('BACK_BUTTON');
    tracking.trackEventWithCategory(category, action, label);
    appRouter.previous();
  }, [tracking, appRouter]);

  const navigateToMyTrips = useCallback(() => {
    const { category, action, label } = getTracking('RETURN_TRIP_DETAILS');
    tracking.trackEventWithCategory(category, action, label);
    appRouter.next();
  }, [tracking, appRouter]);

  return (
    <Tracking.TrackablePage page={'open-ticket/confirmation'}>
      <ConfirmationPage
        content={content}
        itinerary={itinerary}
        requestedFlights={state.redemption.requestedFlights}
        redemptionRequest={state.redemption.redemptionRequest}
        backAction={onBackClick}
        CTAaction={navigateToMyTrips}
      />
    </Tracking.TrackablePage>
  );
}

function getContent(t, params = {}) {
  return {
    page: {
      headerTitle: t('myinfo:tripdetails.openticket.header.title'),
      headerOverline: t('confirmationPage:bookingNumber', {
        bookingId: params.bookingId,
      }),
      header: t('confirmationPage:header'),
    },
    itinerarySummaryComponent: {
      informativeMessage: t('confirmationPage:informativeMessage'),
    },
    confirmationStatusComponent: {
      statusHeader: t('confirmationPage:status.header'),
      step1Title: t('confirmationPage:status.step1.title'),
      step2Title: t('confirmationPage:status.step2.title'),
      step2estimatedTime: t('confirmationPage:status.step2.estimatedTime.time'),
      step2estimatedTimeMessage: t(
        'confirmationPage:status.step2.estimatedTime.message'
      ),
      step2DelayMessage: t('confirmationPage:status.step2.delayMessage'),
      step2StatusMessage: t('confirmationPage:status.step2.statusMessage'),
      step3title: t('confirmationPage:status.step3.title'),
    },
    footer: {
      cta: t('confirmationPage:CTA'),
    },
  };
}

export default ConfirmationPageContainer;
