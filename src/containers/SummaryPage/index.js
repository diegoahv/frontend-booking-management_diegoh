import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';

import SummaryPage from '@/src/containers/SummaryPage/SummaryPage';
import Page from '@/src/components/Page/Page';
import useAppRouter from '@/src/export/OpenTicket/useAppRouter';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import { getTracking } from '@/src/containers/SummaryPage/tracking';

function SummaryPageContainer({ bookingInfo, itinerary }) {
  const { t } = useTranslation();
  const appRouter = useAppRouter();
  const numPax = bookingInfo.travellers.length;

  const content = React.useMemo(() => getContent(t, { numPax }), [t, numPax]);
  const tracking = ApplicationContext.useTracking();

  const onBackClick = useCallback(() => {
    const { category, action, label } = getTracking('BACK_BUTTON');
    tracking.trackEventWithCategory(category, action, label);
    appRouter.previous();
  }, [tracking, appRouter]);

  return (
    <Page
      title={t('myinfo:tripdetails.openticket.header.title')}
      pageClass="summary-page-wrapper"
      backAction={onBackClick}
      fixedHeader={'fixed'}
    >
      <SummaryPage
        content={content}
        bookingInfo={bookingInfo}
        itinerary={itinerary}
        onContinue={appRouter.next}
      />
    </Page>
  );
}

function getContent(t, params = {}) {
  return {
    page: {
      headerOverline: t('summaryPage:bookingNumber', {
        bookingId: params.bookingId,
      }),
      header: t('summaryPage:header'),
      insurance: t('summaryPage:insurance'),
    },
    paymentInfoComponent: {
      header: t('summaryPage:paymentInfo.header'),
      body: t('summaryPage:paymentInfo.body'),
      priceBreakdownAccordionHeader: t(
        'summaryPage:paymentInfo.priceBreakdown.accordionHeader'
      ),
      priceBreakdownHeader: t('summaryPage:paymentInfo.priceBreakdown.header'),
      priceBreakdownBaseFare: t(
        'summaryPage:paymentInfo.priceBreakdown.baseFare'
      ),
      priceBreakdownNumberPax: t(
        'summaryPage:paymentInfo.priceBreakdown.numberPax',
        { numPax: params.numPax }
      ),
      priceBreakdownError: t('summaryPage:paymentInfo.priceBreakdown.error'),
      priceBreakdownCta: t('summaryPage:paymentInfo.priceBreakdown.cta'),
      totalPrice: t('summaryPage:paymentInfo.totalPrice'),
      confirmationDetails: t('summaryPage:paymentInfo.confirmationDetails'),
      TCs: t('summaryPage:paymentInfo.TCs', {
        brandName: t('variables:brand.name'),
      }),
    },
    footer: {
      cta: t('summaryPage:paymentInfo.cta'),
    },
    error: {
      title: t('summaryPage:error.modal.title'),
      text: t('summaryPage:error.modal.text'),
      cta: t('summaryPage:error.modal.cta'),
    },
  };
}

export default SummaryPageContainer;
