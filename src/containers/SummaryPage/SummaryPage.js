import React, { useRef, useState } from 'react';
import { css } from '@styled-system/css';
import { Text, Hr, Box } from 'prisma-design-system';
import styled from '@emotion/styled';
import {
  ApplicationContext,
  Tracking,
} from '@frontend-react-component-builder/conchita';
import * as actions from './actions';
import confirmRepository from './confirm_repository';
import PageTopContent from '@/src/components/Page/PageTopContent';
import PaymentInfoComponent from '../../components/Summary/PaymentInfo/PaymentInfoComponent';
import FlightSummary from '../../components/Summary/FlightSummary';
import FooterWithPrice from '../../components/Trip/FooterWithPrice';
import BottomDrawer from '@/src/common/components/BottomDrawer';
import NoResults from '@/src/components/Results/NoResults';
import { CrossCircleLight } from '@/src/common/icons';
import { useOpenTicketContext } from '@/src/export/OpenTicket/OpenTicketContext';
import { getFlightsFromItinerary } from '@/src/export/OpenTicket/flightFactory';
import { getTracking } from '@/src/containers/SummaryPage/tracking';

const StyledHr = styled(Hr)(
  css({
    borderTopWidth: '2',
    my: '6',
    marginLeft: `-16px`,
    width: 'calc(100% + 32px)',
    backgroundColor: 'neutrals.6',
  })
);

function SummaryPage({ bookingInfo, itinerary, content, onContinue }) {
  const fetcher = ApplicationContext.useFetcher();
  const [appContext = {}] = ApplicationContext.useApplicationData();
  const { services = {}, pnrInfo } = appContext;
  const { page, paymentInfoComponent } = content;
  const [areTCsAccepted, setTCsAccepted] = useState(false);
  const [userClickedContinue, userClickedContinueUpdater] = useState(false);
  const checkboxRef = useRef();
  const [openTicketState, dispatch] = useOpenTicketContext();
  const state = openTicketState.redemption;
  const bookingId = bookingInfo.bookingId;
  const flightsForTod = getFlightsFromItinerary(itinerary);
  const tracking = ApplicationContext.useTracking();

  const confirmService = React.useMemo(
    () =>
      confirmRepository({
        urlFormatter: services.bookingChangeRequest,
        fetcher,
      }),
    [services, fetcher]
  );

  const onLaunchItineraryConfirm = () => {
    dispatch(actions.startConfirm());
    confirmService
      .createTod({
        pnr: pnrInfo.pnr,
        originalPriceDifference: itinerary.price.originalPriceDifference,
        priceDifference: itinerary.price.priceDifference,
        currency: itinerary.price.currency,
        flights: flightsForTod,
        bookingId: bookingId,
      })
      .then(() => {
        dispatch(
          actions.onConfirm({ creationDate: Date.now(), status: 'PENDING' })
        );
        onContinue();
      })
      .catch((error) => {
        dispatch(actions.onError(error));
      });
  };

  const scrollToRef = (ref) => {
    ref.current.scrollIntoView({ behavior: 'smooth' });
  };

  return (
    <Tracking.TrackablePage page={'open-ticket/summary'}>
      <Box pb={80}>
        <PageTopContent bookingId={bookingId} menuTitle={page.header} />
        <FlightSummary
          itinerary={itinerary}
          travellers={bookingInfo.travellers}
          buyer={bookingInfo.buyer}
        />
        <Box pt={6}>
          <Text
            as={'p'}
            css={css({
              color: 'neutrals.2',
              fontWeight: 'regular',
              fontSize: 'content.2',
            })}
          >
            {page.insurance}
          </Text>
        </Box>
        <StyledHr />
        <PaymentInfoComponent
          currentPrice={itinerary.price.priceDifference}
          currency={itinerary.price.currency}
          content={paymentInfoComponent}
          TCsUpdater={setTCsAccepted}
          areTCsAccepted={areTCsAccepted}
          userClickedContinue={userClickedContinue}
          checkboxRef={checkboxRef}
        />
        <FooterWithPrice
          price={null}
          onClick={() => {
            const { category, action, label } = getTracking('FINISH_REQUEST');
            tracking.trackEventWithCategory(category, action, label);
            userClickedContinueUpdater(true);
            if (areTCsAccepted) {
              onLaunchItineraryConfirm();
            } else {
              scrollToRef(checkboxRef);
            }
          }}
          buttonText={content.footer.cta}
          variant={'fixed'}
          securePaymentText={'visible'}
          loading={state.loading}
        />
      </Box>
      <BottomDrawer
        open={!!state.error}
        onClose={() => dispatch(actions.onConfirmReset())}
      >
        <Tracking.TrackablePage
          page={'open-ticket/error/summary_error_create_open_ticket_error'}
        >
          <NoResults
            onCallToAction={() => dispatch(actions.onConfirmReset())}
            icon={<CrossCircleLight size="xlarge" color="critical.dark" />}
            content={content.error}
          />
        </Tracking.TrackablePage>
      </BottomDrawer>
    </Tracking.TrackablePage>
  );
}

export default SummaryPage;
