function confirmRepository({ fetcher, urlFormatter }) {
  const module = {};

  module.createTod = function createTod({
    bookingId,
    flights,
    priceDifference,
    pnr,
    currency,
    originalPriceDifference,
  }) {
    const payload = {
      bookingId,
      flights,
      originalPriceDifference,
      pnr,
      currency,
      priceDifference,
    };
    return fetcher.post(
      urlFormatter('createOpenTicketRedemptionRequest'),
      payload
    );
  };

  module.checkRedemptionRequest = function checkRedemptionRequest({
    bookingId,
    pnr,
  }) {
    return fetcher
      .get(urlFormatter(`redemption/booking/${bookingId}/pnr/${pnr}`))
      .then((data) => {
        return !data || Object.keys(data).length === 0
          ? Promise.reject()
          : data;
      });
  };

  return module;
}

export default confirmRepository;
