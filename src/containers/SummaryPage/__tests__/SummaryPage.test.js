import React from 'react';
import { fireEvent, RenderWithOpenTicketContext } from 'test-utils';
import { getAllByTestId } from '@testing-library/dom';
import { ItineraryMocks } from 'serverMocks';
import SummaryPageContainer from '../';
import { ResultsSegmentMocks } from '../../../components/Results/Segment/__mocks__';

const ERROR_MESSAGE = `summaryPage:paymentInfo.priceBreakdown.error`;
const CHECKBOX_SELECTOR = 'input[type="checkbox"]';
const CONTINUE_BUTTON_SELECTOR = `button[type="button"]`;
const PAYMENTINFO_SELECTOR = `paymentInfo`;
const PRICE_ZERO_DECIMAL = `00`;

describe('SummaryPage tests', () => {
  const { ItineraryWith } = ItineraryMocks();
  const bookingInfo = {
    bookingId: '123456789',
    price: {
      amount: 156.63,
      currency: 'EUR',
    },
    travellers: [
      {
        name: 'TEST',
      },
      {
        name: 'TEST2',
      },
    ],
  };

  describe('With itinerary price greater than 0 ', function () {
    const { SegmentSample } = ResultsSegmentMocks();
    const itinerary = ItineraryWith({
      price: {
        price: 1000,
        currency: 'EUR',
      },
      segments: [
        SegmentSample({
          duration: 95,
        }),
      ],
    });

    let renderResult;
    beforeEach(() => {
      renderResult = RenderWithOpenTicketContext(
        <SummaryPageContainer bookingInfo={bookingInfo} itinerary={itinerary} />
      );
    });
    it('Should render page wrapper', async () => {
      const component = renderResult.container.querySelector(
        '.summary-page-wrapper'
      );
      expect(component !== null).toBe(true);
    });

    it('Should show error message when click continue button without checking the checkbox', async () => {
      Element.prototype.scrollIntoView = jest.fn();
      const continueButton = renderResult.container.querySelector(
        CONTINUE_BUTTON_SELECTOR
      );

      expect(continueButton).not.toBeNull();
      fireEvent.click(continueButton);
      expect(renderResult.queryByText(ERROR_MESSAGE)).toBeVisible();
    });

    it('Should not show error message when checkbox is checked', async () => {
      Element.prototype.scrollIntoView = jest.fn();

      const checkBox = renderResult.container.querySelector(CHECKBOX_SELECTOR);
      const continueButton = renderResult.container.querySelector(
        CONTINUE_BUTTON_SELECTOR
      );

      expect(checkBox).not.toBeNull();
      expect(continueButton).not.toBeNull();
      fireEvent.click(checkBox);
      fireEvent.click(continueButton);
      expect(renderResult.queryByText(ERROR_MESSAGE)).toBeNull();
    });

    it("Should show PaymentInfo when there's additional payment to do", async () => {
      expect(
        getAllByTestId(renderResult.container, PAYMENTINFO_SELECTOR)
      ).toHaveLength(1);
    });
  });

  it("Should not show PaymentInfo when there's no additional payment to do", async () => {
    const { SegmentSample } = ResultsSegmentMocks();
    const itinerary = ItineraryWith({
      price: {
        price: 0,
        priceDifference: 0,
        currency: 'EUR',
      },
      segments: [
        SegmentSample({
          duration: 95,
        }),
      ],
    });

    const { queryByText } = RenderWithOpenTicketContext(
      <SummaryPageContainer bookingInfo={bookingInfo} itinerary={itinerary} />
    );
    expect(queryByText(PRICE_ZERO_DECIMAL)).toBeVisible();
  });
});
