import confirmReducer, { getInitialState } from '../confirm_reducer';
import * as actions from '../actions';

let initialState = null;
beforeEach(() => {
  initialState = getInitialState();
});

describe('confirm reducer tests', () => {
  const redemptionRequest = { creationDate: Date.now(), todStatus: 'PENDING' };
  const error = 'fatal';

  describeCaseForAction({
    message: 'START_CONFIRM reducer test',
    mutationObject: { loading: true },
    action: actions.startConfirm,
  });

  describeCaseForAction({
    message: 'CONFIRM_SUCCESS reducer test',
    mutationObject: { loading: false, redemptionRequest },
    action: actions.onConfirm,
    payload: redemptionRequest,
  });

  describeCaseForAction({
    message: 'CONFIRM_ERROR reducer test',
    mutationObject: { loading: false, error },
    action: actions.onError,
    payload: error,
  });

  describeCaseForAction({
    message: 'CONFIRM_RESET reducer test',
    mutationObject: { loading: false, error: '' },
    action: actions.onConfirmReset,
  });
});

function describeCaseForAction({ message, mutationObject, action, payload }) {
  describe(message, () => {
    it('should return a new reference to state', () => {
      const newState = confirmReducer(initialState, action(payload));
      expect(initialState).not.toBe(newState);
    });

    it('should set PROPERTY on state', () => {
      const newState = confirmReducer(initialState, action(payload));
      expect(newState).toEqual(getExpectedState(mutationObject));
    });
  });
}

function getExpectedState({
  loading = false,
  error = '',
  redemptionRequest = {
    creationDate: null,
    todStatus: '',
  },
  requestedFlights = {
    flights: [],
    price: {},
  },
}) {
  return {
    loading,
    error,
    redemptionRequest,
    requestedFlights,
  };
}
