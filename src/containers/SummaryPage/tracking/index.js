const LABEL_PREFIX = 'pb_flexible_';
const LABEL_SUFFIX = '_pag:flsum';

const ACTIONS = {
  main_button_flexible_ticket: 'main_button_flexible_ticket',
  trip_details_flexible_ticket: 'trip_details_flexible_ticket',
};

const labelDecorator = (label) => `${LABEL_PREFIX}${label}${LABEL_SUFFIX}`;

const tracking = {
  PAX_INFO: {
    category: 'pb_flexible_summary_page',
    label: labelDecorator('open_pax'),
    action: ACTIONS.trip_details_flexible_ticket,
  },
  PRICE_BREAKDOWN: {
    category: 'pb_flexible_summary_page',
    label: labelDecorator('open_price'),
    action: ACTIONS.trip_details_flexible_ticket,
  },
  CHECKBOX_CHECKED: {
    category: 'pb_flexible_summary_page',
    label: labelDecorator('accept_terms'),
    action: ACTIONS.trip_details_flexible_ticket,
  },
  BACK_BUTTON: {
    category: 'pb_flexible_summary_page',
    label: labelDecorator('go_back'),
    action: ACTIONS.main_button_flexible_ticket,
  },
  FINISH_REQUEST: {
    category: 'pb_flexible_summary_page',
    label: labelDecorator('go_next'),
    action: ACTIONS.main_button_flexible_ticket,
  },
};

export const getTracking = (key) => tracking[key];
