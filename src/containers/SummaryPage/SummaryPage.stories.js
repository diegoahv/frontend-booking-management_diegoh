import React from 'react';
import SummaryPageContainer from './index';
import { boolean, number, select, withKnobs } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import {
  ItineraryMocks,
  getBookingInfo,
  mockEndpoint,
  getPNRInfo,
} from 'serverMocks';
import { tripTypes } from '@/src/common/utils/enums';
import generateSegments from '@/src/components/Trip/Itinerary/__mocks__';
import StoryRouter from 'storybook-react-router';
import {
  DebugAssistant,
  Mocks,
} from 'frontend-react-component-builder/dist/debug';
import { OpenTicketContextDecorator } from 'decorators/OpenTicketContextDecorator';

const confirmResponse = { ok: true };

const getMock = () =>
  Mocks.CustomApplicationContextMock({
    data: {
      bookingInfo: getBookingInfo(),
      pnrInfo: getPNRInfo(),
      services: {
        bookingChangeRequest: (input) => '/' + input,
      },
    },
  });

storiesOf('OpenTicket/Pages/Summary Page', module)
  .addDecorator(StoryRouter())
  .addDecorator(withKnobs)
  .addDecorator(OpenTicketContextDecorator)
  .add('Playground', () => {
    const hasResponse = boolean('Has response', true);
    mockEndpoint({
      method: 'post',
      url: '/createOpenTicketRedemptionRequest',
      data: hasResponse ? confirmResponse : null,
    });

    mockEndpoint({
      method: 'get',
      url: `/redemption/booking/7082689696/pnr/WHBEW`,
      data: { creationDate: Date.now(), todStatus: 'PENDING' },
    });

    const bookingInfo = getBookingInfo();
    const selectPrice = (label = '') => ({
      price: number(`${label} price amount:`, 210),
      currency: select('currency', ['USD', 'EUR'], 'EUR'),
    });

    const tripType = select(
      'Trip Type',
      Object.keys(tripTypes),
      tripTypes.ONE_WAY.type
    );
    const segments = generateSegments(tripTypes[tripType].length);
    const { ItineraryWith } = ItineraryMocks();
    const itinerary = ItineraryWith({
      price: selectPrice(),
      segments: segments,
    });

    return (
      <DebugAssistant.AssistantApplicationContextMocker
        applicationContext={getMock()}
      >
        <SummaryPageContainer
          bookingInfo={bookingInfo}
          itinerary={itinerary}
          onBack={(history) => history.goBack()}
          onContinue={(history) => history.push('/confirmation')}
        />
      </DebugAssistant.AssistantApplicationContextMocker>
    );
  });
