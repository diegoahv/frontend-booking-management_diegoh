import * as actionTypes from './actions/confirm_action_types';

export const getInitialState = () => ({
  loading: false,
  error: '',
  redemptionRequest: {
    creationDate: null,
    todStatus: '',
  },
  requestedFlights: { price: {}, flights: [] },
});

function confirmReducer(state, action) {
  switch (action.type) {
    case actionTypes.START_CONFIRM: {
      state.loading = true;
      return { ...state };
    }
    case actionTypes.CONFIRM_SUCCESS: {
      state.loading = false;
      state.redemptionRequest = getRedemptionRequest(action.payload);
      return { ...state };
    }
    case actionTypes.CONFIRM_ERROR: {
      state.loading = false;
      state.error = action.payload;
      return { ...state };
    }
    case actionTypes.CONFIRM_RESET: {
      state.loading = false;
      state.error = '';
      return { ...state };
    }
    case actionTypes.SET_REQUESTED_FLIGHTS: {
      state.requestedFlights = action.payload;
      return { ...state };
    }
  }

  return state;
}

function getRedemptionRequest({ creationDate = null, todStatus = '' } = {}) {
  return { creationDate, todStatus };
}
export default confirmReducer;
