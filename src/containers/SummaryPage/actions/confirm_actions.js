import {
  START_CONFIRM,
  CONFIRM_SUCCESS,
  CONFIRM_ERROR,
  CONFIRM_RESET,
  SET_REQUESTED_FLIGHTS,
} from './confirm_action_types';
import { formatFlights } from '@/src/export/OpenTicket/flightFactory';

export function startConfirm() {
  return createAction(START_CONFIRM);
}

export function onConfirm(response) {
  return createAction(CONFIRM_SUCCESS, response);
}

export function onError(error) {
  return createAction(CONFIRM_ERROR, error);
}

export function onConfirmReset() {
  return createAction(CONFIRM_RESET);
}

export function setRequestedFlights(payload) {
  return createAction(SET_REQUESTED_FLIGHTS, formatFlights(payload));
}

function createAction(type, payload) {
  return { type, payload };
}
