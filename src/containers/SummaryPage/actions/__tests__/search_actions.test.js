import * as actionTypes from '../confirm_action_types';
import * as confirmActions from '../confirm_actions';

describe('Summary Page actions tests', () => {
  describe('startConfirm action tests', () => {
    runCommonExpectations(
      confirmActions.startConfirm(),
      actionTypes.START_CONFIRM
    );
  });

  describe('onConfirm action tests', () => {
    const payload = [1, 2, 3];
    const action = confirmActions.onConfirm([1, 2, 3]);
    runCommonExpectations(action, actionTypes.CONFIRM_SUCCESS, payload);
  });

  describe('onError action tests', () => {
    const payload = 'fatal';
    const action = confirmActions.onError('fatal');
    runCommonExpectations(action, actionTypes.CONFIRM_ERROR, payload);
  });

  describe('onError action tests', () => {
    const action = confirmActions.onConfirmReset();
    runCommonExpectations(action, actionTypes.CONFIRM_RESET);
  });
});

function runCommonExpectations(action, expectedType, expectedPayload) {
  it('should return an action with properties type and payload', () => {
    const expectedProperties = ['type', 'payload'];

    Object.keys(action).forEach((key) => {
      expect(expectedProperties).toContain(key);
    });
  });

  it('should return an action with the expected payload and type', () => {
    expect(action).toEqual({
      type: expectedType,
      payload: expectedPayload,
    });
  });
}
