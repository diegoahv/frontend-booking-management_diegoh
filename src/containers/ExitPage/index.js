import React, { useEffect } from 'react';
import LoadingPage from '@/src/export/OpenTicket/LoadingPage';
import { ApplicationContext } from 'frontend-react-component-builder/dist/conchita';
import { GO_TO_TRIPDETAILS } from 'utils/of_events';

function ExitPage() {
  const dispatch = ApplicationContext.useDispatch();

  useEffect(() => {
    dispatch({ message: GO_TO_TRIPDETAILS });
  }, [dispatch]);

  return <LoadingPage />;
}

export default ExitPage;
