import React from 'react';
import OpenTicketContext from '@/src/export/OpenTicket/OpenTicketContext';
import { MemoryRouter } from 'react-router-dom';
import { Mocks } from 'frontend-react-component-builder/dist/debug';
import { render } from 'test-utils';
import { getBookingInfo } from 'serverMocks/bookingInfo';
import { initState } from '@/src/export/OpenTicket/reducer';
import { getPNRInfo } from 'serverMocks/pnrInfo';
import { platforms } from '@/src/common/utils/enums';

function RenderWithOpenTicketContext(
  RenderWithContext,
  platform = platforms.MOBILE
) {
  return render(
    <MemoryRouter>
      <OpenTicketContext.Provider value={[initState(), () => {}]}>
        {RenderWithContext}
      </OpenTicketContext.Provider>
    </MemoryRouter>,
    {
      applicationContext: Mocks.CustomApplicationContextMock({
        data: {
          pnrInfo: getPNRInfo(),
          services: { bookingChangeRequest: (input) => '/' + input },
          bookingInfo: getBookingInfo(),
          platform: platforms[platform],
        },
      }),
    }
  );
}

export default RenderWithOpenTicketContext;
