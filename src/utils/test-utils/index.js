import { render } from '@testing-library/react';
import { TestUtils } from '@frontend-react-component-builder/debug';
import RenderWithOpenTicketContext from 'utils/test-utils/RenderWithOpenTicketContext';

// re-export everything
export * from '@testing-library/react';

// override render method
const customRender = TestUtils.customRender(render);
export { customRender as render, RenderWithOpenTicketContext };
