import { ApplicationContext } from '@frontend-react-component-builder/conchita';

export const useBookingInfo = () => {
  const [{ bookingInfo } = {}] = ApplicationContext.useApplicationData();
  return [bookingInfo];
};
