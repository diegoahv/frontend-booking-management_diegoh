import * as textUtils from './text_utils';

describe('Text Utils tests', () => {
  describe('capitalize tests', () => {
    it('should return a string with first letter in capital', () => {
      const expected = 'Dilemma';
      const result = textUtils.capitalize('dilemma');
      expect(result).toEqual(expected);
    });
  });

  describe('toCamelCase tests', () => {
    it('should return a string with first letter of each word in capital', () => {
      const expected = 'weHaveADilemma';
      const result = textUtils.toCamelCase('We_have_a_dilemma', '_');
      expect(result).toEqual(expected);
    });
  });
});
