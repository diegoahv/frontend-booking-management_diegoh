const capitalize = (s) => {
  return s.charAt(0).toUpperCase() + s.slice(1);
};

const toCamelCase = (text, separator = ' ') => {
  let arrayString = text
    .toLowerCase()
    .split(separator)
    .map((s, i) => (i === 0 ? s : capitalize(s)));
  return arrayString.join('');
};

export { capitalize, toCamelCase };
