export {
  isSameDate,
  isWeekend,
  isBeforeDate,
  isAfterDate,
  isInRange,
  minutesToHours,
  minutesInHour,
  isPastDate,
  minutesToHoursAndMinutes,
};

const MINUTES_IN_AN_HOUR = 60;

function isPastDate(date) {
  return date < new Date(new Date().toDateString());
}

function isSameDate(date, target) {
  return (
    date instanceof Date &&
    target instanceof Date &&
    date.toDateString() === target.toDateString()
  );
}

function isWeekend(date) {
  return date instanceof Date && (date.getDay() === 0 || date.getDay() === 6);
}

function isBeforeDate(date, target) {
  if (!(date instanceof Date && target instanceof Date)) {
    return false;
  }
  return new Date(date.toDateString()) <= new Date(target.toDateString());
}

function isAfterDate(date, target) {
  if (!(date instanceof Date && target instanceof Date)) {
    return false;
  }
  return new Date(date.toDateString()) >= new Date(target.toDateString());
}

function isInRange(date, range) {
  return (
    date instanceof Date &&
    isBeforeDate(date, range[1]) &&
    isAfterDate(date, range[0])
  );
}

function minutesToHours(durationInMinutes) {
  return Math.floor(durationInMinutes / MINUTES_IN_AN_HOUR);
}

function minutesInHour(durationInMinutes) {
  return durationInMinutes % MINUTES_IN_AN_HOUR;
}

function minutesToHoursAndMinutes(durationInMinutes) {
  const hours = minutesToHours(durationInMinutes);
  const minutes = String(minutesInHour(durationInMinutes)).padStart(2, '0');

  return { hours, minutes };
}
