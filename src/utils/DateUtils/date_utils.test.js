import * as dateUtils from './date_utils';

describe('Date Utils tests', () => {
  describe('isSameDay tests', () => {
    it('return false if some argument is not a date object', () => {
      const invalidArg = 'test';
      const date = new Date();
      expect(dateUtils.isSameDate(invalidArg, date)).toBe(false);
    });

    it('return true if both dates has the same day, month and year', () => {
      const date1 = new Date(2010, 0, 1);
      const date2 = new Date(2010, 0, 1);

      expect(dateUtils.isSameDate(date1, date2)).toBe(true);
    });
    it('return false otherwise', () => {
      const date1 = new Date(2010, 0, 1);
      const differentYear = new Date(
        date1.getFullYear() + 1,
        date1.getMonth(),
        date1.getDate()
      );
      const differentMonth = new Date(
        date1.getFullYear(),
        date1.getMonth() + 1,
        date1.getDate()
      );
      const differentDay = new Date(
        date1.getFullYear(),
        date1.getMonth(),
        date1.getDate() + 1
      );

      expect(dateUtils.isSameDate(date1, differentYear)).toBe(false);
      expect(dateUtils.isSameDate(date1, differentMonth)).toBe(false);
      expect(dateUtils.isSameDate(date1, differentDay)).toBe(false);
    });
  });

  describe('isWeekend tests', () => {
    it('returns false if argument date is not a date object', () => {
      expect(dateUtils.isWeekend(58)).toBe(false);
    });

    it('should return true if date is Saturday or Sunday', () => {
      const saturday = new Date(2020, 5, 6);
      const sunday = new Date(2020, 5, 7);

      expect(dateUtils.isWeekend(saturday)).toBe(true);
      expect(dateUtils.isWeekend(sunday)).toBe(true);
    });
    it('should return false if date is not Saturday or Sunday', () => {
      const monday = new Date(2020, 5, 1);
      const friday = new Date(2020, 5, 5);

      expect(dateUtils.isWeekend(monday)).toBe(false);
      expect(dateUtils.isWeekend(friday)).toBe(false);
    });
  });

  describe('isBeforeDate tests', () => {
    it('returns false if any argument is not a date object', () => {
      expect(dateUtils.isBeforeDate(false, new Date())).toBe(false);
      expect(dateUtils.isBeforeDate(new Date(), '2010-10-10')).toBe(false);
    });

    it('should return true if date is before target', () => {
      const date = new Date(2000, 0, 1);
      const target = new Date(2000, 1, 1);

      expect(dateUtils.isBeforeDate(date, target)).toBe(true);
    });

    it('should return true if date is equal than target without comparing time', () => {
      const date = new Date(2000, 0, 1);
      date.setHours(8);
      const target = new Date(2000, 0, 1);
      target.setHours(5);

      expect(dateUtils.isBeforeDate(date, target)).toBe(true);
    });

    it('should return false if date is after', () => {
      const date = new Date(2000, 0, 2);
      date.setHours(8);
      const target = new Date(2000, 0, 1);
      target.setHours(5);

      expect(dateUtils.isBeforeDate(date, target)).toBe(false);
    });
  });

  describe('isAfterDate tests', () => {
    it('returns false if any argument is not a date object', () => {
      expect(dateUtils.isAfterDate(false, new Date())).toBe(false);
      expect(dateUtils.isAfterDate(new Date(), '2010-10-10')).toBe(false);
    });

    it('should return true if date is after target', () => {
      const date = new Date(2000, 1, 1);
      const target = new Date(2000, 0, 1);

      expect(dateUtils.isAfterDate(date, target)).toBe(true);
    });

    it('should return true if date is equal than target without comparing time', () => {
      const date = new Date(2000, 0, 1);
      date.setHours(8);
      const target = new Date(2000, 0, 1);
      target.setHours(5);

      expect(dateUtils.isAfterDate(date, target)).toBe(true);
    });

    it('should return false if date is before', () => {
      const date = new Date(2000, 0, 1);
      date.setHours(8);
      const target = new Date(2000, 0, 2);
      target.setHours(5);

      expect(dateUtils.isAfterDate(date, target)).toBe(false);
    });
  });
  describe('isInRange tests', () => {
    const beginningDate = new Date(2010, 0, 1);
    const endDate = new Date(2010, 6, 30);
    const range = [beginningDate, endDate];

    it('returns false if any argument is not a date object', () => {
      expect(dateUtils.isInRange(null, [false, new Date()])).toBe(false);
      expect(dateUtils.isInRange({}, [new Date(), new Date()])).toBe(false);
    });

    it('should return true if date is between range', () => {
      const date = new Date(2010, 3, 1);

      expect(dateUtils.isInRange(date, range)).toBe(true);
    });
    it('should return true if date is equal to the first date in the range', () => {
      const date = new Date(2010, 0, 1);

      expect(dateUtils.isInRange(date, range)).toBe(true);
    });
    it('should return true if date is equal to the last date in the range', () => {
      const date = new Date(2010, 6, 30);

      expect(dateUtils.isInRange(date, range)).toBe(true);
    });
    it('should return false if date is not between range', () => {
      const dateBefore = new Date(2009, 11, 30);
      const dateAfter = new Date(2010, 7, 1);

      expect(dateUtils.isInRange(dateBefore, range)).toBe(false);
      expect(dateUtils.isInRange(dateAfter, range)).toBe(false);
    });
  });
  describe('minutesToHours tests', () => {
    it('should return 1h when 75 minutes are passed as parameters', () => {
      expect(dateUtils.minutesToHours(75)).toBe(1);
    });
    it('should return 2h when 125 minutes are passed as parameters', () => {
      expect(dateUtils.minutesToHours(125)).toBe(2);
    });
  });
  describe('minutesToHoursAndMinutes tests', () => {
    it('should return an object containing hours and minutes', () => {
      const expected = { hours: 1, minutes: '00' };
      const result = dateUtils.minutesToHoursAndMinutes(60);
      expect(result).toMatchObject(expected);
    });
  });

  describe('isPastDate tests', () => {
    it('should return true for dates in the past', () => {
      const pastDate = new Date(1999, 5, 3);
      expect(dateUtils.isPastDate(pastDate)).toBe(true);
    });

    it("should't compare time", () => {
      const now = new Date();
      expect(dateUtils.isPastDate(now)).toBe(false);
    });

    it('should return false for future dates', () => {
      const date = new Date();
      date.setMonth(date.getMonth() + 1);
      expect(dateUtils.isPastDate(date)).toBe(false);
    });
  });
});
