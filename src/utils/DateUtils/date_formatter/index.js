import format from 'date-fns/format';

export default function formatDate(date, formatStr = 'dd/MM/yyyy HH:mm') {
  return format(date, formatStr);
}
