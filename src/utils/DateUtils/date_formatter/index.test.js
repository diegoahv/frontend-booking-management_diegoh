import formatDate from './index';

describe('formatDate tests', () => {
  it('should return input date formatted', () => {
    const date = new Date('2020/06/17');
    expect(formatDate(date, 'EEE dd/MM')).toBe('Wed 17/06');
  });

  it('should return input timestampp formatted', () => {
    const timestampp = new Date('2020/06/17').getTime();
    expect(formatDate(timestampp, 'EEE dd/MM')).toBe('Wed 17/06');
  });

  it('default format is dd/mm/yyyy hh:MM', () => {
    const date = new Date('2020/06/17');
    expect(formatDate(date)).toBe('17/06/2020 00:00');
  });
});
