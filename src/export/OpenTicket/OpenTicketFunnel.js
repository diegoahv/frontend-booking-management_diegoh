import React, { Suspense, lazy } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import SearchPage from '@/src/containers/SearchPage';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import { tripTypes } from '@/src/common/utils/enums';
import { useOpenTicketContext } from './OpenTicketContext';
import useAppRouter, { routes } from './useAppRouter';
import OpenTicketLoadingPage from '@/src/export/OpenTicket/LoadingPage';

const ItineraryPage = lazy(() => import('@/src/containers/ItineraryPage'));
const SummaryPage = lazy(() => import('@/src/containers/SummaryPage'));
const ConfirmationPage = lazy(() =>
  import('@/src/containers/ConfirmationPage')
);
const ExitPage = lazy(() => import('@/src/containers/ExitPage'));

function OpenTicketFunnel() {
  const [appData] = ApplicationContext.useApplicationData();
  const bookingInfo = appData.bookingInfo;
  const [state] = useOpenTicketContext();
  const router = useAppRouter();

  return (
    <Suspense fallback={<OpenTicketLoadingPage backAction={router.previous} />}>
      <Switch>
        <Route path={routes.search} render={() => <SearchPage />} />
        <Route
          path={routes.itinerary}
          render={() => (
            <ItineraryPage
              itinerary={state.results.selectedItinerary}
              bookingId={bookingInfo.bookingId}
              tripType={
                state.results.selectedItinerary.legs.length === 1
                  ? tripTypes.ONE_WAY.type
                  : tripTypes.ROUND_TRIP.type
              }
            />
          )}
        />
        <Route
          path={routes.summary}
          render={() => (
            <SummaryPage
              bookingInfo={bookingInfo}
              itinerary={state.results.selectedItinerary}
            />
          )}
        />
        <Route
          path={routes.confirmation}
          render={() => (
            <ConfirmationPage itinerary={state.results.selectedItinerary} />
          )}
        />
        <Route path={routes.exit} render={() => <ExitPage />} />
        <Redirect
          to={
            state.redemption.redemptionRequest.creationDate
              ? routes.confirmation
              : routes.search
          }
        />
      </Switch>
    </Suspense>
  );
}

export default OpenTicketFunnel;
