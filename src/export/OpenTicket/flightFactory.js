const getFlightsFromItinerary = (itinerary) => {
  const flights = [];
  itinerary.legs.map((leg) => {
    const segment = leg.segments[0];
    segment.sections.map((section) => {
      const flight = {
        carrier: section.carrier.id,
        departureDateTimestamp: new Date(section.departureDate).getTime(),
        route: {
          origin: section.departure.iata,
          destination: section.destination.iata,
        },
        number: section.id,
        bookingClass: section.bookingClass,
        fareBasisId: section.fareBasisId,
      };
      flights.push(flight);
    });
  });
  return flights;
};
const formatFlights = ({
  flights = [],
  priceDifference = 0,
  currency = {},
} = {}) => {
  return {
    price: {
      priceDifference: priceDifference,
      currency: currency.code,
    },
    flights: flights.map((flight) => {
      return {
        departureDate: new Date(flight.departureDateTimestamp).toISOString(),
        carriers: [
          {
            id: flight.carrier,
            name: flight.carrier,
            code: flight.carrier,
          },
        ],
        departure: { iata: flight.route.origin },
        destination: { iata: flight.route.destination },
      };
    }),
  };
};

export { getFlightsFromItinerary, formatFlights };
