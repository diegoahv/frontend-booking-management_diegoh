import { tripTypes } from '@/src/common/utils/enums';

const ARRIVAL = 'ARRIVAL';
const DEPARTURE = 'departure';
const RETURN = 'return';
const FLIGHT = 'flight';

function rebuildTrip(sections) {
  sections.sort(orderByDepartureDateAsc);
  const firstSectionName = sections[0].sectionName;

  switch (firstSectionName) {
    case DEPARTURE:
    case RETURN:
      return createRoundTrip(sections);
    case FLIGHT:
      return createOneWay(sections);
    default:
      throw new Error('unexpected case');
  }
}

function createRoundTrip(sections) {
  const outboundArrivalSectionIndex = sections.findIndex(isArrivalSection);

  const origin = sections[0].section.from;
  const destination = sections[outboundArrivalSectionIndex].section.to;
  const outboundDate = getDate(sections[0].section.departureDate);
  const inboundDate = getDate(
    sections[outboundArrivalSectionIndex + 1].section.departureDate
  );

  return {
    tripType: tripTypes.ROUND_TRIP.type,
    origin,
    destination,
    outboundDate,
    inboundDate,
  };
}

function createOneWay(sections) {
  const arrivalSection = sections.find(isArrivalSection);

  const origin = sections[0].section.from;
  const destination = arrivalSection.section.to;
  const outboundDate = getDate(sections[0].section.departureDate);
  const inboundDate = getDate(arrivalSection.section.departureDate);

  return {
    tripType: tripTypes.ONE_WAY.type,
    origin,
    destination,
    outboundDate,
    inboundDate,
  };
}

export default function (pnrInfo) {
  try {
    const trip = rebuildTrip(pnrInfo.sections);

    return {
      tripType: trip.tripType,
      outboundLocation: {
        iata: trip.origin.iataCode,
        name: trip.origin.name,
      },
      outboundDate: trip.outboundDate,
      inboundLocation: {
        iata: trip.destination.iataCode,
        name: trip.destination.name,
      },
      inboundDate: trip.inboundDate,
    };
  } catch (e) {
    return {};
  }
}

function orderByDepartureDateAsc(section1, section2) {
  return section1.section.departureDate - section2.section.departureDate;
}

function isArrivalSection(section) {
  return section.sectionType === ARRIVAL;
}

function getDate(date) {
  return Date.now() < date ? new Date(date) : undefined;
}
