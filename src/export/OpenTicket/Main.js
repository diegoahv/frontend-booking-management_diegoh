import React, { useEffect, useReducer, useState, useMemo } from 'react';
import * as actions from '@/src/containers/SummaryPage/actions';
import OpenTicketFunnel from '@/src/export/OpenTicket/OpenTicketFunnel';
import confirmRepository from '@/src/containers/SummaryPage/confirm_repository';
import reducer, { initState } from '@/src/export/OpenTicket/reducer';
import buildFormInitialState from '@/src/export/OpenTicket/formInitialStateBuilder';
import OpenTicketContext from '@/src/export/OpenTicket/OpenTicketContext';
import { ApplicationContext } from 'frontend-react-component-builder/dist/conchita';
import { HashRouter } from 'react-router-dom';
import { GO_TO_TRIPDETAILS } from 'utils/of_events';
import OpenTicketLoadingPage from '@/src/export/OpenTicket/LoadingPage';

function Main() {
  const [loaded, setLoaded] = useState(false);
  const [appContext = {}] = ApplicationContext.useApplication();
  const dispatchToConchita = ApplicationContext.useDispatch();
  const fetcher = ApplicationContext.useFetcher();
  const [appData] = ApplicationContext.useApplicationData();
  const repository = useMemo(
    () =>
      confirmRepository({
        fetcher,
        urlFormatter: appData.services.bookingChangeRequest,
      }),
    [fetcher, appData.services.bookingChangeRequest]
  );
  const [state, dispatch] = useReducer(
    reducer,
    buildFormInitialState(appData.pnrInfo),
    initState
  );
  const bookingInfo = appData.bookingInfo;

  useEffect(() => {
    const { bookingId } = bookingInfo;
    const { pnr } = appData.pnrInfo;
    repository
      .checkRedemptionRequest({ bookingId, pnr })
      .then((data) => {
        dispatch(actions.onConfirm(data));
        dispatch(actions.setRequestedFlights(data));
      })
      .catch(() => {})
      .finally(() => {
        setLoaded(true);
      });
  }, [appData, bookingInfo, repository]);

  return loaded ? (
    <HashRouter hashType="noslash" basename={appContext.basePath}>
      <OpenTicketContext.Provider value={[state, dispatch]}>
        <OpenTicketFunnel />
      </OpenTicketContext.Provider>
    </HashRouter>
  ) : (
    <OpenTicketLoadingPage
      backAction={() => {
        dispatchToConchita({ message: GO_TO_TRIPDETAILS });
      }}
    />
  );
}

export default Main;
