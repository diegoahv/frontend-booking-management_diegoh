import { useHistory, useLocation } from 'react-router-dom';

export const routes = {
  search: '/searchAlternatives',
  itinerary: '/itinerary',
  summary: '/summary',
  confirmation: '/confirmation',
  exit: '/exit',
};

const order = [
  routes.search,
  routes.itinerary,
  routes.summary,
  routes.confirmation,
  routes.exit,
];

export default function useAppRouter() {
  const history = useHistory();
  const location = useLocation();

  return {
    previous() {
      history.goBack();
    },
    next() {
      const index =
        (order.findIndex(locationIncludesPageName) + 1) % order.length;
      if (index === 0) {
        return;
      }

      history.push(order[index]);
    },
  };

  function locationIncludesPageName(pageName) {
    pageName = pageName.slice(1);
    return location.pathname.includes(pageName);
  }
}
