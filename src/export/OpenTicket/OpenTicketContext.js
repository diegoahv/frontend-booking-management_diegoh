import React from 'react';

const context = React.createContext();

export function useOpenTicketContext() {
  return React.useContext(context);
}

export default context;
