import resultsReducer, {
  getInitialState as initResults,
} from '@/src/containers/SearchPage/Search/search_reducer';
import formReducer, {
  getInitialState as initForm,
} from '@/src/containers/SearchPage/Search/SearchForm/search_form_reducer';
import confirmReducer, {
  getInitialState as initConfirm,
} from '@/src/containers/SummaryPage/confirm_reducer';

export function initState(initialArg) {
  return {
    results: initResults(initialArg),
    form: initForm(initialArg),
    redemption: initConfirm(initialArg),
  };
}

export default function OpenTicketReducer(state, action) {
  return {
    results: resultsReducer(state.results, action),
    form: formReducer(state.form, action),
    redemption: confirmReducer(state.redemption, action),
  };
}
