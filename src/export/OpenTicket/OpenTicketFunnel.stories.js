import React from 'react';
import { storiesOf } from '@storybook/react';
import OpenTicketFunnel from './OpenTicketFunnel';
import {
  mockEndpoint,
  autocompleteResponse,
  searchResults,
  getBookingInfo,
  getPNRInfo,
  getRedemptionRequest,
} from 'serverMocks';
import { boolean } from '@storybook/addon-knobs';
import {
  DebugAssistant,
  Mocks,
} from 'frontend-react-component-builder/dist/debug';

storiesOf('OpenTicket/Pages/Open Ticket Funnel', module).add(
  'Playground',
  () => {
    const getMock = () =>
      Mocks.CustomApplicationContextMock({
        data: {
          bookingInfo: getBookingInfo(),
          pnrInfo: getPNRInfo(),
          services: {
            bookingChangeRequest: (input) => '/' + input,
          },
        },
      });

    mockEndpoint({
      method: 'get',
      url:
        '/service/geo/autocomplete;searchWord=osa;departureOrArrival=DEPARTURE;addSearchByCountry=true;addSearchByRegion=true;nearestLocations=true;product=FLIGHT',
      data: autocompleteResponse,
    });
    const hasResults = boolean('Has results', true);
    mockEndpoint({
      method: 'post',
      url: '/searchAlternatives',
      data: hasResults ? searchResults : null,
    });

    const confirmSuccess = boolean('Confirm success', true);
    const withRedirection = boolean('With redirection', false);
    const confirmResponse = getRedemptionRequest;

    mockEndpoint({
      method: 'post',
      url: '/createOpenTicketRedemptionRequest',
      data: () => {
        return confirmSuccess ? Promise.resolve(null) : Promise.reject();
      },
    });

    mockEndpoint({
      method: 'get',
      url: '/redemption/booking/7082689696/pnr/WHBEW',
      /*url: /\/redemption\/booking\/\d+\/pnr\/[A-Z]+/,*/
      /*data: withRedirection ? confirmResponse : confirmResponse,*/
      data: () => {
        return withRedirection
          ? Promise.resolve(confirmResponse)
          : Promise.reject();
      },
    });

    return (
      <DebugAssistant.AssistantApplicationContextMocker
        applicationContext={getMock()}
      >
        <OpenTicketFunnel />
      </DebugAssistant.AssistantApplicationContextMocker>
    );
  }
);
