import React from 'react';
import LoadingPage from '@/src/components/LoadingPage';
import { useTranslation } from 'react-i18next';

function OpenTicketLoadingPage({ backAction = () => {} }) {
  const { t } = useTranslation();
  return (
    <LoadingPage
      headerBackAction={() => {
        backAction();
      }}
      headerTitle={t('searchPage:header')}
      loadingText={t('loadingPage:text')}
    />
  );
}

export default OpenTicketLoadingPage;
