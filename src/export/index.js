import 'core-js/modules/es.promise';
import 'core-js/modules/es.array.iterator';
import { withConchita } from '@frontend-react-component-builder/conchita';
import MainMenu from '../containers/MainMenu';
import { OpenTicket } from './OpenTicket';

export const standaloneMainMenu = withConchita(MainMenu);

export const openTicketFunnel = withConchita(OpenTicket);

export { unmountComponentAtNode } from 'react-dom';
