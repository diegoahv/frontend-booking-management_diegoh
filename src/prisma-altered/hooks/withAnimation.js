import React, { Component } from 'react';

const CLOSED = 'closed';
const OPENING = 'opening';
const OPENED_STATE = { state: 'opened' };
const CLOSED_STATE = { state: CLOSED };
const OPENING_STATE = { state: OPENING };
const CLOSING_STATE = { state: 'closing' };
const isClosed = ({ state }) => state === CLOSED;

const withAnimation = (WrappedComponent, transitionDuration = 200) => {
  return class extends Component {
    constructor(props) {
      super(props);
      const { open } = props;
      this.setOpenedWithDelay = this.setOpenedWithDelay.bind(this);
      this.state = open ? OPENED_STATE : CLOSED_STATE;
    }

    setOpenedWithDelay() {
      this.openedTimeoutId = setTimeout(() => {
        this.setState(OPENED_STATE);
      }, 0);
    }

    componentWillUnmount() {
      clearTimeout(this.closedTimeoutId);
      clearTimeout(this.openedTimeoutId);
    }

    componentDidUpdate(prevProps) {
      const { open } = this.props;
      const { state } = this.state;
      if (open !== prevProps.open) {
        this.setState(open ? OPENING_STATE : CLOSING_STATE);
        if (!open) {
          this.closedTimeoutId = setTimeout(() => {
            this.setState(CLOSED_STATE);
          }, transitionDuration);
        }
      } else if (state === 'opening') {
        this.setOpenedWithDelay();
      }
    }

    render() {
      return (
        !isClosed(this.state) && (
          <WrappedComponent
            transitionDuration={`${transitionDuration}ms`}
            state={this.state.state}
            {...this.props}
          />
        )
      );
    }
  };
};

export default withAnimation;
