import styled from '@emotion/styled';
import { Box } from 'prisma-design-system';
import css from '@styled-system/css';

export const StyledCloseIcon = styled(Box)(({ color, hoverColor }) =>
  css({
    lineHeight: 0,
    cursor: 'pointer',
    color: color,
    ':hover': {
      color: hoverColor,
      cursor: 'pointer',
    },
  })
);
