import React from 'react';
import { StyledCloseIcon } from './StyledCloseIcon';
import { CrossLightIcon } from 'prisma-design-system';
import PropTypes from 'prop-types';

const CloseIcon = ({ onClose, closeLabel, color, hoverColor, size }) => {
  return (
    <StyledCloseIcon
      alignSelf="center"
      p={1}
      onClick={onClose}
      role="button"
      aria-pressed="false"
      aria-label={closeLabel}
      color={color}
      hoverColor={hoverColor}
    >
      <CrossLightIcon size={size} />
    </StyledCloseIcon>
  );
};
CloseIcon.defaultProps = {
  size: 'medium',
};

CloseIcon.propTypes = {
  size: PropTypes.string.isRequired,
};

export default CloseIcon;
