import css from '@styled-system/css';

const computedStyles = {
  selected: {
    bg: 'brandPrimary.0',
    color: 'brandPrimary.2',
    borderColor: 'brandPrimary.3',
  },
};

/**
 * It returns an object with the computed styles based on the values of type and selected component props.-
 * When isSelected is undefined means that the component is not using the selected behaviour and it'll return an empty object.-
 * @param {string} type - it's used to identify the type of the component: default, secondary, text.-
 * @param {boolean} isSelected - it's used to know if the component is selected or not.
 *
 * @returns {Object}
 */
const getComputedStyles = (type, isSelected) => {
  if (typeof isSelected !== 'boolean') {
    return {};
  }
  const selected = isSelected ? 'selected' : 'unselected';

  return computedStyles[selected] ? computedStyles[selected] : {};
};

const selectedVariant = ({ variant, selected }) =>
  css({
    ...getComputedStyles(variant, selected),
  });

export default selectedVariant;
