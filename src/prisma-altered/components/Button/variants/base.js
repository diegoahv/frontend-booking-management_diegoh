import variant from '@styled-system/variant';
import { getTheme } from 'prisma-design-system';

const base = {
  border: 1,
  borderRadius: `${getTheme().radii[2]}px`,
  outline: 0,
  fontFamily: 'body',
  cursor: 'pointer',
  color: 'neutrals.1',
  bg: 'white',
  borderColor: 'neutrals.1',
  padding: `${getTheme().space[3]}px ${getTheme().space[2]}px`,
  fontSize: 'body.1',
  fontWeight: 'normal',
  minWidth: 'auto',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
};

export default variant({
  variants: {
    base,
    primary: {
      ...base,
      borderRadius: '22px',
      backgroundColor: 'brand.primary.base',
      borderColor: 'unset',
      fontSize: 'body.2',
      color: 'white',
      padding: 3,
      '&:hover,&:active': {
        backgroundColor: 'brand.primary.light',
      },
    },
  },
});
