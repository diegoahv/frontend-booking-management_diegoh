import { variant } from 'styled-system';

const fullWidthVariant = variant({
  prop: 'fullWidth',
  variants: {
    true: {
      flex: 1,
    },
    percentage: {
      width: '100%',
    },
  },
});

export default fullWidthVariant;
