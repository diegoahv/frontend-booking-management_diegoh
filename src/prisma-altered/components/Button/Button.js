import React from 'react';
import * as PropTypes from 'prop-types';
import styled from '@emotion/styled';
import shouldForwardProp from '@styled-system/should-forward-prop';

import base from './variants/base';
import selectedVariant from './variants/selectedVariant';
import fullWidthVariant from './variants/fullWidthVariant';

const StyledButton = styled('button', {
  shouldForwardProp,
})(base, selectedVariant, fullWidthVariant);

function Button({ nativeType: type, variant, children, ...rest }) {
  return (
    <StyledButton type={type} variant={variant} {...rest}>
      {children}
    </StyledButton>
  );
}

Button.propTypes = {
  /**
   * action
   */
  onClick: PropTypes.func.isRequired,
  /**
   * true when element is selected.-
   * false when element is not selected.-
   * Undefined means the component is not using the selected behaviour.-
   */
  selected: PropTypes.bool,
  /**
   * set the native type button prop. useful in forms
   * https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button#attr-type
   */
  nativeType: PropTypes.oneOf(['submit', 'reset', 'button']),
  variant: PropTypes.oneOf(['base', 'primary']),
};

Button.defaultProps = {
  nativeType: 'button',
  variant: 'base',
};

export { StyledButton };
export default Button;
