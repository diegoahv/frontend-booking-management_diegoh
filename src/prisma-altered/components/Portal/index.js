import { Component } from 'react';
import { createPortal } from 'react-dom';

export default class Portal extends Component {
  constructor(props) {
    super(props);

    if (typeof window !== 'undefined') {
      const propsElement = document.getElementById(this.props.rootElement);
      this.node =
        this.props.rootElement && propsElement ? propsElement : document.body;
      this.el = document.createElement('div');
    }
  }

  componentDidMount() {
    if (this.node && this.el) {
      this.node.appendChild(this.el);
    }
  }

  componentWillUnmount() {
    if (this.node && this.el) {
      this.node.removeChild(this.el);
    }
  }

  render() {
    const { children } = this.props;

    if (typeof window !== 'undefined' && this.el) {
      return createPortal(children, this.el);
    }

    return null;
  }
}
