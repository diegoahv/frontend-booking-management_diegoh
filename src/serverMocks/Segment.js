import idGenerator from './idGenerator';
import { dateGenerator, dateWithTime } from './Date';
import { CharlesDeGaulle, Tegel } from './Location';
import { Iberia } from './Carrier';
import { sectionGenerator, SectionMocks } from './Section';

const isFilledArray = (array) => Array.isArray(array) && array.length > 0;

export const SegmentMocks = () => {
  const { Section } = SectionMocks();
  const segmentIdIterator = idGenerator();
  const Segment = (props = {}) => ({
    id: props.id || segmentIdIterator.next().value,
    ...props,
  });

  const addSectionsWhenEmpty = (segment) => {
    if (!isFilledArray(segment.sections)) {
      segment.sections = [Section()];
    }
  };

  const addStops = (segment, stops) => {
    if (Number.isInteger(stops) && !isFilledArray(segment.sections)) {
      segment.sections = Array.from({ length: Math.max(stops, 0) + 1 }, () =>
        Section()
      );
    }
  };

  const addCarrier = (segment, carrier) => {
    if (carrier) {
      addSectionsWhenEmpty(segment);
      segment.sections = segment.sections.map((section) => ({
        carrier,
        ...section,
      }));
      segment.carrier = carrier;
    }
  };

  const addLocations = (segment, departure, destination) => {
    if (departure || destination) {
      addSectionsWhenEmpty(segment);
    }
    if (departure) {
      segment.sections[0].departure = departure;
    }
    if (destination) {
      segment.sections[segment.sections.length - 1].destination = destination;
    }
  };

  const addSectionIterator = (segment, iterator) => {
    addSectionsWhenEmpty(segment);
    segment.sections = segment.sections.map((section) =>
      Section({
        iterator,
        ...section,
      })
    );
  };

  const SegmentWith = ({
    sectionIterators,
    stops,
    carrier,
    departure,
    destination,
    departureDate,
    ...props
  }) => {
    const segment = Segment(props);

    addStops(segment, stops);
    addCarrier(segment, carrier);
    addLocations(segment, departure, destination);
    const sectionIterator = sectionGenerator({
      ...sectionIterators,
      dateIterator: dateGenerator(departureDate || '2020-04-27T22:25:40'),
    });
    addSectionIterator(segment, sectionIterator);

    return segment;
  };

  const createFromToSegmentWith = (departure, destination) => (props) => {
    const segment = SegmentWith(props);
    let { sections } = segment;
    if (!isFilledArray(sections)) {
      sections = [Section()];
    }
    sections[0].departure = departure;
    sections[sections.length - 1].destination = destination;
    segment.sections = sections;
    return segment;
  };

  const SegmentSample = ({
    stops = 0,
    carrier = Iberia(),
    departure = CharlesDeGaulle(),
    destination = Tegel(),
    departureDate = dateWithTime('12:30'),
    arrivalDate = dateWithTime('15:00'),
    ...props
  } = {}) => {
    const segment = SegmentWith({
      stops,
      carrier,
      departure,
      destination,
      departureDate,
      arrivalDate,
    });
    return {
      ...segment,
      ...props,
    };
  };

  return {
    Section,
    Segment,
    SegmentWith,
    SegmentSample,
    createFromToSegmentWith,
  };
};
