import { fromArray } from './entityGenerator';

export const ElPrat = () => ({
  id: 157,
  iata: 'BCN',
  name: 'El Prat',
  cityName: 'Barcelona',
  countryName: 'Spain',
});

export const Reus = () => ({
  id: 1581,
  iata: 'REU',
  name: 'Reus',
  cityName: 'Barcelona',
  countryName: 'Spain',
});

export const Gatwick = () => ({
  id: 1068,
  iata: 'LGW',
  name: 'Gatwick',
  cityName: 'London',
  countryName: 'United Kingdom',
});

export const Heathrow = () => ({
  id: 1070,
  iata: 'LHR',
  name: 'Heathrow',
  cityName: 'London',
  countryName: 'United Kingdom',
});

export const LutonInternational = () => ({
  id: 1121,
  iata: 'LTN',
  name: 'Luton International',
  cityName: 'London',
  countryName: 'United Kingdom',
});

export const Southend = () => ({
  id: 1072447,
  iata: 'LCY',
  name: 'Southend',
  cityName: 'London',
  countryName: 'United Kingdom',
});

export const Stansted = () => ({
  id: 1775,
  iata: 'STN',
  name: 'Stansted',
  cityName: 'London',
  countryName: 'United Kingdom',
});

export const Barajas = () => ({
  id: 1147,
  iata: 'MAD',
  name: 'Adolfo Suárez Madrid - Barajas',
  cityName: 'Madrid',
  countryName: 'Spain',
});

export const CharlesDeGaulle = () => ({
  id: 330,
  iata: 'CDG',
  name: 'Charles De Gaulle',
  cityName: 'Paris',
  countryName: 'France',
});

export const Schoenefeld = () => ({
  id: 1804,
  iata: 'SXF',
  name: 'Schoenefeld',
  cityName: 'Berlin',
  countryName: 'Germany',
});

export const Tegel = () => ({
  id: 1937,
  iata: 'TXL',
  name: 'Tegel',
  cityName: 'Berlin',
  countryName: 'Germany',
});

export const PalmaDeMallorca = () => ({
  id: 1483,
  iata: 'PMI',
  name: 'Palma de Mallorca',
  cityName: 'Palma de Mallorca',
  countryName: 'Spain',
});

export const BaselEuroairport = () => ({
  id: 276,
  iata: 'BSL',
  name: 'Basel Euroairport',
  cityName: 'Zurich',
  countryName: 'Switzerland',
});

export const Zurich = () => ({
  id: 2231,
  iata: 'ZRH',
  name: 'Zurich',
  cityName: 'Zurich',
  countryName: 'Switzerland',
});

export const SingaporeChangiAirport = () => ({
  id: 9823,
  iata: 'SIN',
  name: 'Singapore Changi Airport',
  cityName: 'Singapore',
  countryName: 'Singapore',
});

export const SoekarnoHatta = () => ({
  id: 9723,
  iata: 'JKT',
  name: 'Soekarno-Hatta International Airport',
  cityName: 'Jakarta',
  countryName: 'Indonesia',
});

export const BarcelonaLocations = () => [ElPrat(), Reus()];

export const MadridLocations = () => [Barajas()];

export const LondonLocations = () => [
  Gatwick(),
  Heathrow(),
  LutonInternational(),
  Southend(),
  Stansted(),
];

export const BerlinLocations = () => [Schoenefeld(), Tegel()];

export const ParisLocations = () => [CharlesDeGaulle()];

export const Locations = () => [
  ElPrat(),
  Reus(),
  Gatwick(),
  Heathrow(),
  LutonInternational(),
  Southend(),
  Stansted(),
  Barajas(),
  CharlesDeGaulle(),
  Schoenefeld(),
  Tegel(),
  PalmaDeMallorca(),
  BaselEuroairport(),
  Zurich(),
  SingaporeChangiAirport(),
  SoekarnoHatta(),
];

export const locationGenerator = (locationOptions) => {
  return fromArray(locationOptions || Locations());
};
