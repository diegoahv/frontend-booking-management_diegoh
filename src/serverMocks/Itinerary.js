import idGenerator from './idGenerator';
import { SegmentMocks } from './Segment';

export const ItineraryMocks = () => {
  const {
    Segment,
    SegmentWith,
    createFromToSegmentWith,
    Section,
  } = SegmentMocks();
  const itineraryIdIterator = idGenerator();

  const Itinerary = (props) => {
    return {
      id: props.id || itineraryIdIterator.next().value,
      ...props,
    };
  };
  return {
    Section,
    Segment,
    SegmentWith,
    createFromToSegmentWith,
    Itinerary,
    ItineraryWith: ({ segments, ...props }) =>
      Itinerary({
        legs: (segments || []).map((segment) => ({
          segments: [Segment(segment)],
        })),
        ...props,
      }),
  };
};
