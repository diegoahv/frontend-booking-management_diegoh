const Money = (currency) => (amount) => ({
  amount,
  amount2: Number(amount),
  currency,
});

export const Euros = Money('EUR');

export const Dollars = Money('USD');
