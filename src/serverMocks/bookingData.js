export const DefaultBookingData = () => ({
  bookingId: '5073807923',
  flags: {
    contracted: true,
    flown: false,
    lowcostBooking: false,
    prime: false,
    eticket: true,
    canxFlags: {
      contractedInCovid: true,
      schChanges: false,
      involuntaryCanx: false,
      refunds: true,
    },
    bookingFlags: {
      hotel: false,
      insurances: true,
      refunds: true,
      checkin: true,
      bags: true,
      seats: true,
    },
  },
  addUrls: {
    addBagUrl:
      'https://www.edreams.ma/travel/secure/#/post_booking/trackingId=bGV2ZWx1cC5wb2QucWFAZ21haWwuY29t;bookingId=5001055749;product=bags',
    addSeatUrl:
      'https://www.edreams.ma/travel/secure/#/post_booking/trackingId=bGV2ZWx1cC5wb2QucWFAZ21haWwuY29t;bookingId=5001055749;product=seats',
  },
});
