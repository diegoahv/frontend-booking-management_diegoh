import parseISO from 'date-fns/parseISO';
import formatISO from 'date-fns/formatISO';
import addMinutes from 'date-fns/addMinutes';

export const dayWithHour = (hour) => `2020-04-29T${hour}:35:00+01:00`;

export const isoDateRegexpWithoutTimezone = /^(\d+-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}).*$/;

const removeTimeZone = (isoDateString) => {
  const [, dateWithoutTimezoneString] =
    isoDateRegexpWithoutTimezone.exec(isoDateString) || [];
  return dateWithoutTimezoneString;
};

const defaultSteps = [75, 110, 40, 230, 195, 40];

export function* dateGenerator(startDate, steps = defaultSteps) {
  let index = 0;
  let step;
  let date = parseISO(removeTimeZone(startDate));
  while (true) {
    yield formatISO(date);
    step = steps[index];
    date = addMinutes(date, step);
    index = (index + 1) % steps.length;
  }
}

export const dateWithTime = (time) => `2020-04-27T${time}:20`;
