function* idGenerator() {
  let index = 1;
  while (true) {
    yield index++;
  }
}

export default idGenerator;
