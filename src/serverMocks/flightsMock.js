export default {
  price: {
    priceDifference: 10,
    currency: 'EUR',
  },
  flights: [
    {
      departureDate: new Date().toISOString(),
      carriers: [
        {
          id: 'UX',
          name: 'UX',
          code: 'UX',
        },
      ],
      departure: { iata: 'OSA' },
      destination: { iata: 'BCN' },
    },
    {
      departureDate: new Date().toISOString(),
      carriers: [
        {
          id: 'UX',
          name: 'UX',
          code: 'UX',
        },
      ],
      departure: { iata: 'BCN' },
      destination: { iata: 'OSA' },
    },
  ],
};
