import idGenerator from './idGenerator';
import { Barajas, ElPrat } from './Location';
import { Iberia } from './Carrier';

const addPropsByIterator = (props) => (section, iterator) => {
  if (!iterator) {
    return;
  }
  props.forEach((prop) => {
    if (!section[prop]) {
      section[prop] = iterator.next().value;
    }
  });
};

const addLocations = addPropsByIterator(['departure', 'destination']);
const addFlightCode = addPropsByIterator(['flightCode']);
const addVehicleModel = addPropsByIterator(['vehicleModel']);
const addInsuranceOffer = addPropsByIterator(['insuranceOffer']);

const addTechnicalStops = (section, technicalStopsIterator) => {
  if (!Array.isArray(section.technicalStops)) {
    section.technicalStops = technicalStopsIterator
      ? technicalStopsIterator.next().value
      : [];
  }
};

const addDates = (section, dateIterator) => {
  if (!dateIterator) {
    return;
  }
  if (!section.departureDate) {
    section.departureDate = dateIterator.next().value;
  }
  if (Array.isArray(section.technicalStops)) {
    section.technicalStops = section.technicalStops.map((technicalStop) => ({
      arrivalDate: dateIterator.next().value,
      departureDate: dateIterator.next().value,
      ...technicalStop,
    }));
  }
  if (!section.arrivalDate) {
    section.arrivalDate = dateIterator.next().value;
  }
};

export function* sectionGenerator({
  dateIterator,
  insuranceOfferIterator,
  technicalStopsIterator,
  stopLocationIterator,
  flightCodeIterator,
  vehicleModelIterator,
} = {}) {
  while (true) {
    const section = {};

    addInsuranceOffer(section, insuranceOfferIterator);
    addTechnicalStops(section, technicalStopsIterator);
    addLocations(section, stopLocationIterator);
    addFlightCode(section, flightCodeIterator);
    addVehicleModel(section, vehicleModelIterator);
    addDates(section, dateIterator);

    yield section;
  }
}

export const SectionMocks = () => {
  const sectionIdIterator = idGenerator();

  const Section = ({ iterator, ...props } = {}) => {
    return {
      id: props.id || sectionIdIterator.next().value,
      ...(iterator && iterator.next().value),
      ...props,
    };
  };

  const SectionSample = (props) =>
    Section({
      departureDate: '2020-04-29T10:30:00+01:00',
      arrivalDate: '2020-04-29T11:45:33+01:00',
      departure: Barajas(),
      destination: ElPrat(),
      carrier: Iberia(),
      technicalStops: [],
      ...props,
    });

  return {
    Section,
    SectionSample,
  };
};
