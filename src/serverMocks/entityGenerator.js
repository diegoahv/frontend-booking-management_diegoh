export function* fromArray(entities) {
  let index = 0;
  while (true) {
    yield entities[index];
    index = (index + 1) % entities.length;
  }
}
