import { Mocks } from '@frontend-react-component-builder/debug';

export function mockEndpoint({ method = 'get', data, url } = {}) {
  const httpMethod = capitalizeMethod(method);
  Mocks.Fetcher[`mock${httpMethod}`](url, data);
}

function capitalizeMethod(method) {
  let meth = method.toLowerCase();
  return `${meth.slice(0, 1).toUpperCase()}${meth.slice(1).toLowerCase()}`;
}
