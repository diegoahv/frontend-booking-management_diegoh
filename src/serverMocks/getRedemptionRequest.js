export default {
  flights: [
    {
      number: '123',
      carrier: 'UX',
      departureDateTimestamp: 1594306291000,
      route: {
        origin: 'MAD',
        destination: 'BCN',
      },
      bookingClass: 'E',
      fareBasisId: 'ABCDEFGH',
    },
  ],
  currency: 'EUR',
  priceDifference: 10,
  creationDate: 1601848800000,
  todStatus: 'PENDING',
};
