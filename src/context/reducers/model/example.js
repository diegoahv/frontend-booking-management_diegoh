import CONSTANTS from '../../actions/model/constants';

export const exampleReducer = (example = false, action = {}) => {
  const { type } = action;

  return type === CONSTANTS.DO_SOMETHING ? !example : example;
};
