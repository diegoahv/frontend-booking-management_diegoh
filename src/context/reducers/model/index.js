import { exampleReducer } from './example';

export default ({ example } = {}, action) => ({
  example: exampleReducer(example, action),
});
