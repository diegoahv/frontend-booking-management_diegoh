import CONSTANTS from './constants';

export default {
  doSomething: ({ foo = 'bar' }) => ({ type: CONSTANTS.DO_SOMETHING, foo }),
};
