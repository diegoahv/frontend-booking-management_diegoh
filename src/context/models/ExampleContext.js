import { ContextFactory } from '@frontend-react-component-builder/conchita';

import reducer from '../reducers/model/index';
import actions from '../actions/model/actions';

export default ContextFactory({
  reducer,
  actions,
});
