import * as urlUtils from './url_utils';

const TEST_MAIL = 'TEST_MAIL';
const TEST_BOOKINGID = 'TEST_BOOKINGID';
const TEST_BRAND = 'TEST_BRAND';

describe('Url Utils tests', () => {
  describe('getMyBookingUrl test', () => {
    it('should return the correct myBookingUrl', () => {
      expect(
        urlUtils.getMyBookingUrl({ mail: TEST_MAIL, bookingId: TEST_BOOKINGID })
      ).toBe(
        `/service/bookingchangerequest/mybooking/${TEST_MAIL}/${TEST_BOOKINGID}`
      );
    });
  });

  describe('getTokenUrl test', () => {
    it('should return the correct tokenUrl', () => {
      expect(
        urlUtils.getTokenUrl({ mail: TEST_MAIL, bookingId: TEST_BOOKINGID })
      ).toBe(
        `/service/users/managebookingtoken?bookingid=${TEST_BOOKINGID}&email=${TEST_MAIL}`
      );
    });
  });

  describe('getIOSButtonImage test', () => {
    it('should return the correct IOSButtonImage depending on the passed brand', () => {
      expect(urlUtils.getIOSButtonImage(TEST_BRAND)).toBe(
        `https://in2.odistatic.net/images/onefront/bluestone/${TEST_BRAND}/apple-badge-no-language.png`
      );
    });

    it('should return the eDreams IOSButtonImage if no brand is passed (default value)', () => {
      expect(urlUtils.getIOSButtonImage()).toBe(
        `https://in2.odistatic.net/images/onefront/bluestone/ED/apple-badge-no-language.png`
      );
    });
  });

  describe('getAndroidButtonImage test', () => {
    it('should return the correct IOSButtonImage depending on the passed brand', () => {
      expect(urlUtils.getAndroidButtonImage(TEST_BRAND)).toBe(
        `https://in4.odistatic.net/images/onefront/bluestone/${TEST_BRAND}/android-badge-no-language.png`
      );
    });

    it('should return the eDreams androidButtonImage if no brand is passed (default value)', () => {
      expect(urlUtils.getAndroidButtonImage()).toBe(
        `https://in4.odistatic.net/images/onefront/bluestone/ED/android-badge-no-language.png`
      );
    });
  });

  describe('getDecorativeImg test', () => {
    it('should return the correct IOSButtonImage depending on the passed brand', () => {
      expect(urlUtils.getDecorativeImg(TEST_BRAND)).toBe(
        `https://ak4.odistatic.net/images/onefront/bluestone/${TEST_BRAND}/app-promo.png`
      );
    });

    it('should return the eDreams decorativeImage if no brand is passed (default value)', () => {
      expect(urlUtils.getDecorativeImg()).toBe(
        `https://ak4.odistatic.net/images/onefront/bluestone/ED/app-promo.png`
      );
    });
  });
});
