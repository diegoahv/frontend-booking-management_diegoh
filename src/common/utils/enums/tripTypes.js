export default {
  ONE_WAY: {
    length: 1,
    type: 'ONE_WAY',
  },
  ROUND_TRIP: {
    length: 2,
    type: 'ROUND_TRIP',
  },
};
