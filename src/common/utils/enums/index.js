import tripTypes from './tripTypes';
import platforms from './platforms';

export { tripTypes, platforms };
