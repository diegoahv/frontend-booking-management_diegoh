export {
  getMyBookingUrl,
  getTokenUrl,
  getIOSButtonImage,
  getAndroidButtonImage,
  getDecorativeImg,
  APP_DOWNLOAD_URL_IOS,
  APP_DOWNLOAD_URL_ANDROID,
  PRIME_LOGO_URL,
};

const IOS_APPS_BASE_URL = 'https://apps.apple.com/app';
const ANDROID_APPS_BASE_URL = 'https://play.google.com/store/apps/details?id=';

const APP_DOWNLOAD_URL_IOS = {
  ED: `${IOS_APPS_BASE_URL}/id551367321`,
  TL: `${IOS_APPS_BASE_URL}/id1202940520`,
  OP: `${IOS_APPS_BASE_URL}/id646692973`,
  GV: `${IOS_APPS_BASE_URL}/id388162153`,
};

const APP_DOWNLOAD_URL_ANDROID = {
  ED: `${ANDROID_APPS_BASE_URL}com.edreams.travel`,
  TL: `${ANDROID_APPS_BASE_URL}com.travellink`,
  OP: `${ANDROID_APPS_BASE_URL}com.opodo.reisen`,
  GV: `${ANDROID_APPS_BASE_URL}go.voyage`,
};

const BLUESTONE = 'https://a1.odistatic.net/images/onefront/bluestone';
const PRIME_LOGO_URL = {
  ED: `${BLUESTONE}/ED/prime_logo/ED-logo-prime-negative-short.svg`,
  TL: `${BLUESTONE}/ED/prime_logo/ED-logo-prime-negative-short.svg`,
  OP: `${BLUESTONE}/OP/prime_logo/OP-logo-prime-negative-short.svg`,
  GV: `${BLUESTONE}/GV/prime_logo/GO-logo-prime-negative-short.svg`,
};

const getMyBookingUrl = ({ mail, bookingId }) =>
  `/service/bookingchangerequest/mybooking/${mail}/${bookingId}`;
const getTokenUrl = ({ mail, bookingId }) =>
  `/service/users/managebookingtoken?bookingid=${bookingId}&email=${mail}`;
const getIOSButtonImage = (brand = 'ED') =>
  `https://in2.odistatic.net/images/onefront/bluestone/${brand}/apple-badge-no-language.png`;
const getAndroidButtonImage = (brand = 'ED') =>
  `https://in4.odistatic.net/images/onefront/bluestone/${brand}/android-badge-no-language.png`;
const getDecorativeImg = (brand = 'ED') =>
  `https://ak4.odistatic.net/images/onefront/bluestone/${brand}/app-promo.png`;
