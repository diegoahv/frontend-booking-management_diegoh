import React from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon.js';

export default function ArrowRightIcon({ color, size }) {
  return (
    <Icon viewBox="0 0 16 16" size={size} color={color}>
      <g>
        <polygon points="4 12.9725 5.5222672 14.5 12 8 5.5222672 1.5 4 3.0275 8.944664 8" />
      </g>
    </Icon>
  );
}
ArrowRightIcon.defaultProps = {
  size: 'medium',
};
ArrowRightIcon.propTypes = {
  size: PropTypes.string,
  color: PropTypes.string.isRequired,
};
