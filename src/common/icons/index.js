import React from 'react';
import PaxIcon from './PaxIcon';
import PlaneGoingIcon from './PlaneGoingIcon';
import BoardingPassIcon from './BoardingPassIcon';
import BaggageLargeIcon from './BaggageLargeIcon';
import SeatIcon from './SeatIcon';
import AdditionalServicesIcon from './AdditionalServicesIcon';
import RefundsIcon from './RefundsIcon';
import BillingInformationIcon from './BillingInformationIcon';
import BookingDocumentationIcon from './BookingDocumentationIcon';
import LeftArrowIcon from './LeftArrowIcon';
import ArrowRightIcon from './ArrowRightIcon';
import CrossIcon from './CrossIcon';
import HotelPlaneErrorIcon from './HotelPlaneErrorIcon';
import CrossCircleLightIcon from './CrossCircleLightIcon';
import {
  TickIcon,
  FreeChangeIcon,
  EyeOpenIcon,
  InformationIcon,
} from 'prisma-design-system';

const defaults = {
  size: 'large',
  color: 'brandPrimary.2',
};

const wrap = (IconComponent) => ({
  size = defaults.size,
  color = defaults.color,
}) => <IconComponent size={size} color={color} />;

const ModifyTrip = wrap(PlaneGoingIcon);
const PassengerInfo = wrap(PaxIcon);
const Booking = wrap(TickIcon);
const CheckIn = wrap(BoardingPassIcon);
const Bags = wrap(BaggageLargeIcon);
const Seats = wrap(SeatIcon);
const AdditionalServices = wrap(AdditionalServicesIcon);
const Refunds = wrap(RefundsIcon);
const Billing = wrap(BillingInformationIcon);
const FreeRebooking = wrap(FreeChangeIcon);
const RightArrow = wrap(ArrowRightIcon);
const LeftArrow = wrap(LeftArrowIcon);
const EyeOpen = wrap(EyeOpenIcon);
const Information = wrap(InformationIcon);
const Tick = wrap(TickIcon);
const Document = wrap(BookingDocumentationIcon);
const Cross = wrap(CrossIcon);
const HotelPlaneError = wrap(HotelPlaneErrorIcon);
const CrossCircleLight = wrap(CrossCircleLightIcon);

export {
  AdditionalServices,
  Bags,
  Billing,
  Booking,
  CheckIn,
  Document,
  EyeOpen,
  FreeRebooking,
  Information,
  LeftArrow,
  ModifyTrip,
  PassengerInfo,
  Refunds,
  RightArrow,
  Seats,
  Tick,
  Cross,
  HotelPlaneError,
  CrossCircleLight,
};
