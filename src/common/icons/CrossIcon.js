import React from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon.js';

export default function CrossIcon({ color, size }) {
  return (
    <Icon viewBox="0 0 24 24" size={size} color={color}>
      <path
        fill={color}
        d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
      />
    </Icon>
  );
}
CrossIcon.defaultProps = {
  size: 'medium',
};
CrossIcon.propTypes = {
  size: PropTypes.string,
  color: PropTypes.string.isRequired,
};
