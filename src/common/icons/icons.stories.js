import React from 'react';
import {
  AdditionalServices,
  Bags,
  Billing,
  Booking,
  CheckIn,
  Document,
  EyeOpen,
  FreeRebooking,
  Information,
  LeftArrow,
  ModifyTrip,
  PassengerInfo,
  Refunds,
  RightArrow,
  Seats,
  Tick,
  HotelPlaneError,
} from './index';

export default {
  title: 'Common/Icons',
};

export const AdditionalServicesIcon = () => <AdditionalServices />;

export const BagsIcon = () => <Bags />;

export const ABillingIcon = () => <Billing />;

export const BookingIcon = () => <Booking />;

export const CheckInIcon = () => <CheckIn />;

export const DocumentIcon = () => <Document />;

export const EyeOpenIcon = () => <EyeOpen />;

export const FreeRebookingIcon = () => <FreeRebooking />;

export const InformationIcon = () => <Information />;

export const LeftArrowIcon = () => <LeftArrow />;

export const ModifyTripIcon = () => <ModifyTrip />;

export const PassengerInfoIcon = () => <PassengerInfo />;

export const RefundsIcon = () => <Refunds />;

export const RightArrowIcon = () => <RightArrow />;

export const SeatsIcon = () => <Seats />;

export const TickIcon = () => <Tick />;

export const HotelPlaneErrorIcon = () => <HotelPlaneError />;
