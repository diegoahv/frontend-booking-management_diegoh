import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import { variant, color } from 'styled-system';
import shouldForwardProp from '@styled-system/should-forward-prop';

const iconSize = {
  SMALL: 'small',
  MEDIUM: 'medium',
  LARGE: 'large',
  XLARGE: 'xlarge',
};

const StyledIcon = styled('svg', { shouldForwardProp })(
  color,
  variant({
    prop: 'size',
    variants: {
      [iconSize.SMALL]: {
        height: 0,
        width: 0,
        minHeight: 0,
        minWidth: 0,
      },
      [iconSize.MEDIUM]: {
        height: 1,
        width: 1,
        minHeight: 1,
        minWidth: 1,
      },
      [iconSize.LARGE]: {
        height: 2,
        width: 2,
        minHeight: 2,
        minWidth: 2,
      },
      [iconSize.XLARGE]: {
        height: 3,
        width: 3,
        minHeight: 3,
        minWidth: 3,
      },
    },
  }),
  {
    fill: 'currentColor',
  }
);

const Icon = ({ children, ...props }) => (
  <StyledIcon {...props}>{children}</StyledIcon>
);

Icon.defaultProps = {
  size: iconSize.MEDIUM,
};

Icon.propTypes = {
  size: PropTypes.string.isRequired,
  color: PropTypes.string,
};

export default Icon;

export { iconSize };
