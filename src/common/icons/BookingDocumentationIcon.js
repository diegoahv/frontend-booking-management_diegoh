import React from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon.js';
export default function BookingDocumentationIcon({ color, size }) {
  return (
    <Icon viewBox="0 0 32 32" size={size} color={color}>
      <g
        fill="none"
        stroke="currentColor"
        strokeWidth="1.4"
        transform="translate(5 2)"
      >
        <rect fill="none" width="21" height="26.6" x=".7" y=".7" rx="2.8" />
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          d="M5.349 14L9.642 18.109 17.795 11.784"
        />
      </g>
    </Icon>
  );
}
BookingDocumentationIcon.defaultProps = {
  size: 'medium',
};
BookingDocumentationIcon.propTypes = {
  size: PropTypes.string,
  color: PropTypes.string.isRequired,
};
