import React from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon.js';

export default function PaxIcon({ color, size }) {
  return (
    <Icon viewBox="0 0 32 32" size={size} color={color}>
      <g fill={color} strokeWidth=".2">
        <path
          d="M12.253 13.978c-3.853 0-6.989-3.135-6.989-6.99C5.264 3.136 8.4 0 12.254 0c3.853 0 6.988 3.135 6.988 6.989s-3.135 6.989-6.989 6.989h0zm0-12.23c-2.89 0-5.241 2.35-5.241 5.241 0 2.89 2.35 5.242 5.241 5.242 2.89 0 5.242-2.352 5.242-5.242s-2.351-5.242-5.242-5.242zM23.61 27.956H.896c-.483 0-.873-.39-.873-.874v-6.014c0-.902.461-1.75 1.203-2.214 6.561-4.093 15.5-4.092 22.053-.001.743.465 1.205 1.313 1.205 2.215v6.014c0 .483-.39.874-.874.874h0zM1.77 26.209h20.967v-5.14c0-.305-.147-.587-.383-.734-5.999-3.745-14.198-3.747-20.202 0-.235.147-.382.429-.382.733v5.14z"
          transform="translate(4 2)"
        />
      </g>
    </Icon>
  );
}
PaxIcon.defaultProps = {
  size: 'medium',
};
PaxIcon.propTypes = {
  size: PropTypes.string,
  color: PropTypes.string.isRequired,
};
