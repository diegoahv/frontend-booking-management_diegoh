import React from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon.js';
export default function PlaneGoingIconNew({ color, size }) {
  return (
    <Icon viewBox="0 0 32 32" size={size} color={color}>
      <g fill={color}>
        <path
          stroke={color}
          strokeWidth=".1"
          d="M17 15c2.76-.003 4.997-2.326 5-5.192V5.192C22 2.325 19.761 0 17 0s-5 2.325-5 5.192v4.616c.003 2.866 2.24 5.189 5 5.192zm0-13.846c1.718.003 3.23 1.176 3.722 2.884h-7.444C13.77 2.33 15.282 1.158 17 1.154zm-3.889 4.038h7.778v4.616c0 2.23-1.741 4.038-3.889 4.038-2.148 0-3.889-1.808-3.889-4.038V5.192z"
          transform="translate(5)"
        />
        <path
          d="M22 29.6H11.2V26h4c1.767 0 3.2-1.433 3.2-3.2 0-1.767-1.433-3.2-3.2-3.2H6.4v-.8h5.2c1.105 0 2-.895 2-2s-.895-2-2-2H6.4v-3.2c0-1.767-1.433-3.2-3.2-3.2C1.433 8.4 0 9.833 0 11.6v13.6c0 .442.358.8.8.8h4v3.6h-4c-.442 0-.8.358-.8.8 0 .442.358.8.8.8h4.234c.363 0 .71-.144.966-.4s.4-.603.4-.966V26h3.2v3.834c0 .363.144.71.4.966s.603.4.966.4H22c.442 0 .8-.358.8-.8 0-.442-.358-.8-.8-.8zM11.6 16.4c.221 0 .4.179.4.4 0 .221-.179.4-.4.4H6.4v-.8h5.2zm-10-4.8c0-.884.716-1.6 1.6-1.6.884 0 1.6.716 1.6 1.6v8.8c0 .442.358.8.8.8h9.6c.884 0 1.6.716 1.6 1.6 0 .884-.716 1.6-1.6 1.6H1.6V11.6z"
          transform="translate(5)"
        />
        <path
          d="M6.4 22H8c.442 0 .8.358.8.8 0 .442-.358.8-.8.8H6.4c-.442 0-.8-.358-.8-.8 0-.442.358-.8.8-.8z"
          transform="translate(5)"
        />
      </g>
    </Icon>
  );
}
PlaneGoingIconNew.defaultProps = {
  size: 'medium',
};
PlaneGoingIconNew.propTypes = {
  size: PropTypes.string,
  color: PropTypes.string.isRequired,
};
