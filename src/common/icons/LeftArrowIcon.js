import React from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon.js';

export default function LeftArrowIcon({ color, size }) {
  return (
    <Icon viewBox="0 0 24 24" size={size} color={color}>
      <g fill={color}>
        <path d="M11.2342463,5 L12.7844419,6.48338039 L8.064,11 L19,11 L19,13 L8.064,13 L12.7844419,17.5166196 L11.2342463,19 L4,12.0775582 L4.081,12 L4,11.9224418 L11.2342463,5 Z"></path>
      </g>
    </Icon>
  );
}
LeftArrowIcon.defaultProps = {
  size: 'medium',
};
LeftArrowIcon.propTypes = {
  size: PropTypes.string,
  color: PropTypes.string.isRequired,
};
