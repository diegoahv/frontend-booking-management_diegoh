import React from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon.js';
export default function PlaneGoingIcon({ color, size }) {
  return (
    <Icon viewBox="0 0 32 32" size={size} color={color}>
      <g fill={color}>
        <path
          d="M1.04 22.609H31.328V23.926000000000002H1.04zM30.87 1.864c-2.2-1.283-4.7-.602-6.409.195l-5.095 2.376L10.604.028 5.386.252l7.235 7.336-4.611 2.19-4.798-1.826L0 9.45l2.903 3.605c-.307.366-.585.881-.332 1.422.33.707 1.303 1.063 2.9 1.063.329 0 .682-.015 1.062-.045 1.754-.139 3.656-.586 4.845-1.14L29.59 5.861c1.573-.734 2.356-1.504 2.395-2.355.02-.441-.157-1.085-1.116-1.643zm-1.837 2.804l-18.212 8.493c-1.013.472-2.676.868-4.237 1.008-1.595.143-2.448-.036-2.732-.19.058-.08.168-.205.37-.383l.472-.416L2.08 9.933l1.184-.552 4.803 1.828 6.793-3.227L8.407 1.44l1.911-.082 9.026 4.54 5.674-2.645c2.071-.966 3.817-1.05 5.188-.252.346.202.467.377.463.446 0 .005-.04.477-1.636 1.221z"
          transform="translate(0 4)"
        />
      </g>
    </Icon>
  );
}
PlaneGoingIcon.defaultProps = {
  size: 'medium',
};
PlaneGoingIcon.propTypes = {
  size: PropTypes.string,
  color: PropTypes.string.isRequired,
};
