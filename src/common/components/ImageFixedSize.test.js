import React from 'react';
import { render } from 'test-utils';
import ImageFixedSize from './ImageFixedSize';

const TEST_WIDTH_PX = 50;
const TEST_HEIGHT_PX = 60;
const TEST_ALT = 'alt_image';
const TEST_SRC =
  'https://ak4.odistatic.net/images/onefront/bluestone/EDREAMS/app-promo.png';

const getAltSelector = () => `[alt="${TEST_ALT}"]`;

const getSrcSelector = () => `[src="${TEST_SRC}"]`;

const renderImageFixedSize = (width, height, alt, src) =>
  render(<ImageFixedSize width={width} height={height} alt={alt} src={src} />);

describe('ImageFixedSize tests', () => {
  describe('Render tests', () => {
    let image;

    beforeEach(() => {
      image = renderImageFixedSize(
        TEST_WIDTH_PX,
        TEST_HEIGHT_PX,
        TEST_ALT,
        TEST_SRC
      ).container;
    });

    it('Should render an image with the correct alt', () => {
      expect(image).toContainElement(image.querySelector(getAltSelector()));
    });

    it('Should render an image with the correct src', () => {
      expect(image).toContainElement(image.querySelector(getSrcSelector()));
    });
  });
});
