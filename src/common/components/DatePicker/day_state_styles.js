import { isInRange, isSameDate, isWeekend } from 'utils/DateUtils';

export const highlightedDayStyles = {
  backgroundColor: 'brandPrimary.0',
  color: 'black',
};

export const disabledDayStyles = {
  opacity: 0.5,
  cursor: 'not-allowed',
};

export const firstDayStyles = {
  borderTopLeftRadius: '2',
  borderBottomLeftRadius: '2',
  backgroundColor: 'brandPrimary.2',
  color: 'white',
};

export const lastDayStyles = {
  borderTopRightRadius: '2',
  borderBottomRightRadius: '2',
  backgroundColor: 'brandPrimary.2',
  color: 'white',
};

export const oneDayStyles = {
  borderRadius: '2',
  backgroundColor: 'brandPrimary.2',
  color: 'white',
};

export const weekendStyles = {
  fontWeight: 'medium',
};

export function mergeStyles(
  date,
  numberOfDates,
  dateRange = [],
  isDisabled = false
) {
  const styles = {};

  if (isWeekend(date)) {
    Object.assign(styles, weekendStyles);
  }

  if (isDisabled) {
    return Object.assign(styles, disabledDayStyles);
  }

  if (numberOfDates === 1 && isSameDate(date, dateRange[0])) {
    return Object.assign(styles, oneDayStyles);
  }

  if (isFirstDayInRange(date, dateRange)) {
    return Object.assign(styles, firstDayStyles);
  }
  if (isLastDayInRange(date, dateRange)) {
    return Object.assign(styles, lastDayStyles);
  }

  if (isSameDaySelected(date, dateRange)) {
    return Object.assign(styles, oneDayStyles);
  }
  if (isInDateRange(dateRange, date)) {
    return Object.assign(styles, highlightedDayStyles);
  }

  return styles;
}

function isFirstDayInRange(date, dateRange) {
  return (
    isSameDate(date, dateRange[0]) && !isSameDate(dateRange[1], dateRange[0])
  );
}

function isLastDayInRange(date, dateRange) {
  return (
    isSameDate(date, dateRange[1]) && !isSameDate(dateRange[1], dateRange[0])
  );
}

function isSameDaySelected(date, dateRange) {
  return (
    isSameDate(dateRange[1], dateRange[0]) && isSameDate(dateRange[0], date)
  );
}

function isInDateRange(dateRange, date) {
  if (dateRange instanceof Date) {
    return false;
  }

  const startDate = dateRange[0];
  const endDate = dateRange[1];

  if (!(endDate instanceof Date && startDate instanceof Date)) {
    return false;
  }

  return isInRange(date, [startDate, endDate]);
}
