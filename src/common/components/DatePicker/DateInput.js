import React from 'react';
import PropTypes from 'prop-types';
import TextInput from '../TextInput';
import { CalendarIcon } from 'prisma-design-system';
import { formatDate } from 'utils/DateUtils';

const ExportedDateInput = React.forwardRef(DateInput);
ExportedDateInput.displayName = 'DateInput';

export default ExportedDateInput;

const defaultDateFormat = 'EEE dd/MM';

function DateInput(
  {
    label,
    selectedDates = [],
    dateFormat = defaultDateFormat,
    placeholder = '',
    error = '',
    name = '',
    onFocus,
    numberOfDates,
  },
  ref
) {
  return (
    <TextInput
      type="text"
      label={label}
      placeholder={placeholder}
      value={formatRange(selectedDates, dateFormat)}
      name={name}
      onChange={() => {}}
      onFocus={onFocus}
      Icon={CalendarIcon}
      error={error}
      validated={selectedDates.length === numberOfDates}
      ref={ref}
    />
  );
}

function formatRange(selectedDates, dateFormat) {
  if (selectedDates.length > 0) {
    return selectedDates
      .map((date) => {
        return date ? formatDate(date, dateFormat) : undefined;
      })
      .join(' - ');
  }

  return '';
}

DateInput.propTypes = {
  label: PropTypes.string.isRequired,
  selectedDates: PropTypes.arrayOf(Date),
  dateFormat: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  error: PropTypes.string,
};
