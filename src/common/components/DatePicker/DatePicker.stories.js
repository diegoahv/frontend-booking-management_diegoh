import React, { useState } from 'react';
import DatePicker from './index';

export default {
  title: 'Common/Inputs/DatePicker',
  component: DatePicker,
};

export const OneWay = () => {
  const [date, setDate] = useState([]);
  const from = new Date();
  const to = new Date(from.getFullYear() + 1, from.getMonth(), from.getDate());
  return (
    <DatePicker
      label="departure"
      name="departure"
      placeholder="Select a date"
      onSelectDate={setDate}
      selectedDates={date}
      from={new Date()}
      to={to}
    />
  );
};

export const RoundTrip = () => {
  const [dateRange, setDateRange] = useState([]);

  const from = new Date();
  const to = new Date(from.getFullYear() + 1, from.getMonth(), from.getDate());

  return (
    <>
      <div>
        <DatePicker
          label="departure"
          name="departure"
          placeholder="Select a date"
          numberOfDates={2}
          onSelectDate={setDateRange}
          selectedDates={dateRange}
          from={new Date()}
          to={to}
        />
      </div>
    </>
  );
};

export const Preselected = () => {
  const from = new Date();
  const to = new Date(from.getFullYear(), from.getMonth(), from.getDate() + 3);
  const [dateRange, setDateRange] = useState([from, to]);

  return (
    <>
      <div>
        <DatePicker
          label="departure"
          name="departure"
          placeholder="Select a date"
          numberOfDates={2}
          onSelectDate={setDateRange}
          selectedDates={dateRange}
          from={new Date()}
          to={to}
        />
      </div>
    </>
  );
};
