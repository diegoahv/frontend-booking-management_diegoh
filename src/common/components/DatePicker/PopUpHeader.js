import React from 'react';
import PropTypes from 'prop-types';
import css from '@styled-system/css';
import {
  Flex,
  ArrowLeftIcon,
  ArrowRightIcon,
  Text,
} from 'prisma-design-system';
import { useTranslation } from 'react-i18next';

export default PopUpHeader;

function PopUpHeader({ month, onNextMonth, onPreviousMonth }) {
  const { t } = useTranslation();
  const monthsStr = t('datePicker:months').split(',');

  return (
    <Flex
      flexDirection="row"
      justifyContent="space-between"
      alignItems="center"
    >
      <div
        className="prev_month_btn"
        onClick={onPreviousMonth}
        css={css({
          paddingLeft: 4,
        })}
      >
        <ArrowLeftIcon size="medium" color="brandPrimary.2" />
      </div>
      <Text as="p" priority={2} fontWeight="medium">
        {monthsStr[month.getMonth()]} {month.getFullYear()}
      </Text>
      <div
        className="next_month_btn"
        onClick={onNextMonth}
        css={css({
          paddingRight: 4,
        })}
      >
        <ArrowRightIcon size="medium" color="brandPrimary.2" />
      </div>
    </Flex>
  );
}

PopUpHeader.propTypes = {
  month: PropTypes.instanceOf(Date).isRequired,
  onPreviousMonth: PropTypes.func.isRequired,
  onNextMonth: PropTypes.func.isRequired,
};
