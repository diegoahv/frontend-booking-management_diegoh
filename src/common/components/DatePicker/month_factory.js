import React from 'react';
import css from '@styled-system/css';
import Day from './Day';
import { mergeStyles } from './day_state_styles';
import {
  isInRange,
  isBeforeDate,
  isAfterDate,
} from 'utils/DateUtils/date_utils';

export { createMonth };

function createMonth(
  { selectedDates, onSelect, validate, from, to, numberOfDates },
  currentMonth
) {
  const month = createEmptyMonth();
  const monthIt = monthIterator(currentMonth);
  let it = monthIt.next();
  const initialIndex = (i) => (i === 0 ? getNormalDay(it.value.getDay()) : 0);

  for (let week = 0; week < month.length && !it.done; week++) {
    for (
      let day = initialIndex(week);
      day < month[week].length && !it.done;
      day++
    ) {
      const disabled = isDisabled(it.value, {
        selectedDates,
        validate,
        from,
        to,
        numberOfDates,
      });

      const dayStates = mergeStyles(
        it.value,
        numberOfDates,
        selectedDates,
        disabled
      );

      month[week][day] = (
        <Day
          css={css(dayStates)}
          date={it.value}
          onMouseDown={getMouseDownHandler(disabled, onSelect, it.value)}
        >
          {it.value.getDate()}
        </Day>
      );
      it = monthIt.next();
    }
  }

  return month;
}

function* monthIterator(currentMonth) {
  const month = new Date(currentMonth.getTime());
  const initialMonth = month.getMonth();
  month.setDate(1);

  while (month.getMonth() === initialMonth) {
    yield new Date(month.getTime());
    month.setDate(month.getDate() + 1);
  }
}

function isValidDate(date, from, to) {
  if (from && to) {
    return isInRange(date, [from, to]);
  } else if (from) {
    return isAfterDate(date, from);
  } else if (to) {
    return isBeforeDate(date, to);
  } else {
    return true;
  }
}

function createEmptyMonth() {
  return [
    createEmptyWeek(),
    createEmptyWeek(),
    createEmptyWeek(),
    createEmptyWeek(),
    createEmptyWeek(),
    createEmptyWeek(),
  ];
}

function createEmptyWeek() {
  return new Array(7).fill(null);
}

function getNormalDay(day) {
  return {
    0: 6,
    1: 0,
    2: 1,
    3: 2,
    4: 3,
    5: 4,
    6: 5,
  }[day];
}

function getMouseDownHandler(isDisabled, onSelect, date) {
  return (e) => {
    if (isDisabled) {
      e.preventDefault();
    } else {
      onSelect(date);
    }
  };
}

function isDisabled(
  date,
  { numberOfDates, selectedDates, from, to, validate }
) {
  const origin =
    numberOfDates === 2 && selectedDates.length === 1 ? selectedDates[0] : from;
  return !(isValidDate(date, origin, to) && validate(date));
}
