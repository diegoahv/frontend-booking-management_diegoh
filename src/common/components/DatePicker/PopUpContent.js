import React, { useState } from 'react';
import css from '@styled-system/css';
import PopUpHeader from './PopUpHeader';
import Table from '../Table';
import { createMonth } from './month_factory';
import { useTranslation } from 'react-i18next';
import { getTheme } from '@frontend-react-component-builder/conchita';

export default DatePickerPopUp;

const styles = {
  position: 'absolute',
  top: '100%',
  left: 0,
  right: 0,
  zIndex: 'popup',
  backgroundColor: 'white',
  paddingTop: '6',
  paddingBottom: '7',
  userSelect: 'none',
};

function DatePickerPopUp(props) {
  const { t } = useTranslation();
  const [currentMonth, setCurrentMonth] = useState(props.initialDate);

  const tableHeader = getTableHeader(t('datePicker:weekDaysShort'));
  const month = createMonth(props, currentMonth);

  return (
    <div css={css(styles)} className="datepicker_popup">
      <PopUpHeader
        month={currentMonth}
        onNextMonth={() =>
          showNextMonth(props.to, currentMonth, setCurrentMonth)
        }
        onPreviousMonth={() =>
          showPreviousMonth(props.from, currentMonth, setCurrentMonth)
        }
      />
      <div css={(theme) => ({ marginTop: `${getTheme().space[5]}px` })}>
        <Table body={month} head={[tableHeader]} />
      </div>
    </div>
  );
}

export function showPreviousMonth(from, currentMonth, setCurrentMonth) {
  const newMonth = new Date(currentMonth.getTime());
  newMonth.setDate(1);
  newMonth.setMonth(currentMonth.getMonth() - 1);

  if (!from) {
    setCurrentMonth(newMonth);
  } else if (newMonth.getFullYear() > from.getFullYear()) {
    setCurrentMonth(newMonth);
  } else if (
    newMonth.getFullYear() === from.getFullYear() &&
    newMonth.getMonth() >= from.getMonth()
  ) {
    setCurrentMonth(newMonth);
  }
}

export function showNextMonth(to, currentMonth, setCurrentMonth) {
  const newMonth = new Date(currentMonth.getTime());
  newMonth.setDate(1);
  newMonth.setMonth(currentMonth.getMonth() + 1);
  if (!to) {
    setCurrentMonth(newMonth);
  } else if (newMonth.getFullYear() < to.getFullYear()) {
    setCurrentMonth(newMonth);
  } else if (
    newMonth.getFullYear() === to.getFullYear() &&
    newMonth.getMonth() <= to.getMonth()
  ) {
    setCurrentMonth(newMonth);
  }
}

function getTableHeader(daysStr) {
  const dayArr = getWeekStr(daysStr);

  return dayArr.map((day, index) => {
    const styles =
      index >= 5
        ? {
            fontWeight: 'medium',
            fontFamily: 'body',
            fontSize: 'body.1',
          }
        : {
            fontFamily: 'body',
            fontSize: 'body.1',
          };
    return <span css={css(styles)}>{day}</span>;
  });
}

function getWeekStr(textDays) {
  const days = textDays.split(',');
  const firstElement = days.shift();
  days.push(firstElement);

  return days;
}
