import styled from '@emotion/styled';
import css from '@styled-system/css';

const StyledDay = styled.span(
  css({
    display: 'block',
    cursor: 'pointer',
    padding: '2',
    fontFamily: 'body',
    fontSize: 'body.2',
    fontWeight: 'normal',
  })
);

export default StyledDay;
