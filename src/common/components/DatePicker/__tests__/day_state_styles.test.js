import * as dayStates from '../day_state_styles';

const oneWay = 1;
const roundTrip = 2;

describe('mergeStyles tests', () => {
  test('it should return disabledDayStyles', () => {
    const date = new Date(2020, 5, 2);
    const selectedDates = [new Date(), new Date()];
    const isDisabled = true;
    expect(
      dayStates.mergeStyles(date, oneWay, selectedDates, isDisabled)
    ).toEqual(dayStates.disabledDayStyles);
  });
  test('it should return oneDayStyles', () => {
    const date = new Date(2020, 5, 2);
    const selectedDates = [date];
    const isDisabled = false;
    expect(
      dayStates.mergeStyles(date, oneWay, selectedDates, isDisabled)
    ).toEqual(dayStates.oneDayStyles);
  });
  test('it should return firstDayStyles', () => {
    const date = new Date(2020, 5, 2);
    const selectedDates = [date, new Date()];
    const isDisabled = false;
    expect(
      dayStates.mergeStyles(date, roundTrip, selectedDates, isDisabled)
    ).toEqual(dayStates.firstDayStyles);
  });
  test('it should return lastDayStyles', () => {
    const date = new Date(2020, 5, 2);
    const selectedDates = [new Date(), date];
    const isDisabled = false;
    expect(
      dayStates.mergeStyles(date, roundTrip, selectedDates, isDisabled)
    ).toEqual(dayStates.lastDayStyles);
  });
  test('it should return oneDayStyles for multiple date pickers', () => {
    const date = new Date(2020, 5, 2);
    const selectedDates = [date, date];
    const isDisabled = false;
    expect(
      dayStates.mergeStyles(date, roundTrip, selectedDates, isDisabled)
    ).toEqual(dayStates.oneDayStyles);
  });
  test('it should return highlightedDayStyles', () => {
    const date = new Date(2020, 5, 2);
    const selectedDates = [new Date(2020, 4, 28), new Date(2020, 5, 4)];
    const isDisabled = false;
    expect(
      dayStates.mergeStyles(date, roundTrip, selectedDates, isDisabled)
    ).toEqual(dayStates.highlightedDayStyles);
  });
  test('it should return weekendStyles', () => {
    const date = new Date(2020, 5, 7);
    const selectedDates = [new Date(2020, 4, 28), new Date(2020, 5, 4)];
    const isDisabled = false;
    expect(dayStates.mergeStyles(date, selectedDates, isDisabled)).toEqual(
      dayStates.weekendStyles
    );
  });
  test('it should return merge weekendStyles with highlightedDayStyles', () => {
    const date = new Date(2020, 5, 7);
    const selectedDates = [new Date(2020, 4, 28), new Date(2020, 5, 9)];
    const isDisabled = false;
    const finalStyles = Object.assign(
      {},
      dayStates.highlightedDayStyles,
      dayStates.weekendStyles
    );

    expect(
      dayStates.mergeStyles(date, roundTrip, selectedDates, isDisabled)
    ).toEqual(finalStyles);
  });
});
