import React from 'react';
import { render } from 'test-utils';
import Day from '../Day';

describe('Day component test', () => {
  describe('Render tests', () => {
    test('it should render the day of date', () => {
      const date = new Date(2010, 10, 10);
      const { container, getByText } = renderDay(date);
      expect(container).toContainElement(getByText('10'));
    });
    test('it should only render 1 element', () => {
      const date = new Date(2010, 10, 10);
      const { getAllByText } = renderDay(date);
      expect(getAllByText('10')).toHaveLength(1);
    });
  });
});

function renderDay(
  date = new Date(),
  onSelect = () => {},
  selectedDates = [new Date(), new Date()],
  isDisabled = false
) {
  return render(
    <Day
      date={date}
      onSelect={onSelect}
      selectedDates={selectedDates}
      isDisabled={isDisabled}
    >
      {date.getDate()}
    </Day>
  );
}
