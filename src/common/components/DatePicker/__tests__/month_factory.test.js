import React from 'react';
import * as monthFactory from '../month_factory';
import { render, fireEvent } from 'test-utils';
import DatePickerPopUp from '../PopUpContent';

describe('Month factory tests', () => {
  describe('On select callback test', () => {
    test('it should call onSelect when isDisable is false', () => {
      const spy = jest.fn();
      const from = new Date(2020, 2, 15);
      const to = new Date(2020, 2, 31);
      const currentMonth = new Date(2020, 2, 1);
      const { container } = renderDatePickerPopUp({
        initialDate: currentMonth,
        from,
        to,
        onSelect: spy,
        validate: () => true,
      });
      fireEvent.mouseDown(container.querySelectorAll('span')[23]);
      expect(spy).toHaveBeenCalled();
    });
    test("it shouldn't call onSelect when isDisable is true", () => {
      const spy = jest.fn();
      const from = new Date(2020, 2, 15);
      const to = new Date(2020, 2, 31);
      const currentMonth = new Date(2020, 2, 1);
      const { container } = renderDatePickerPopUp({
        initialDate: currentMonth,
        from,
        to,
        onSelect: spy,
        validate: () => true,
      });
      fireEvent.mouseDown(container.querySelectorAll('span')[10]);
      expect(spy).not.toHaveBeenCalled();
    });
  });

  it('returned Matrix should have proper dimensions', () => {
    const month = createMonth();

    expect(month).toHaveLength(6);
    for (const week of month) {
      expect(week).toHaveLength(7);
    }
  });

  it('it should call validate callback for each day in the range', () => {
    const spy = jest.fn();
    const from = new Date(2020, 2, 15);
    const to = new Date(2020, 2, 31);
    const currentMonth = new Date(2020, 2, 1);
    renderDatePickerPopUp({
      initialDate: currentMonth,
      from,
      to,
      validate: spy,
    });

    expect(spy).toHaveBeenCalledTimes(17);
  });

  it('should render the first day of the month in the proper column of the week', () => {
    const initialDate = new Date(2020, 7, 1);
    renderDatePickerPopUp({ initialDate });
    const firstDay = document.querySelector('tbody span').parentElement;
    const parent = firstDay.parentElement;

    // initial date is a saturday so it should be the sixth child in a week with monday as first day
    expect(Array.prototype.indexOf.call(parent.children, firstDay)).toBe(5);
  });
});

function createMonth(
  {
    selectedDates = new Date(),
    onSelect = () => {},
    validate = () => {},
    from = new Date(),
    to = new Date(),
  } = {},
  currentMonth = new Date()
) {
  return monthFactory.createMonth(
    { selectedDates, onSelect, validate, from, to },
    currentMonth
  );
}

function renderDatePickerPopUp({
  initialDate = new Date(),
  from = new Date(),
  to = new Date(),
  selectedDates = [new Date(), new Date()],
  validate = () => {},
  onSelect = () => {},
} = {}) {
  return render(
    <DatePickerPopUp
      initialDate={initialDate}
      from={from}
      to={to}
      selectedDates={selectedDates}
      validate={validate}
      onSelect={onSelect}
    />
  );
}
