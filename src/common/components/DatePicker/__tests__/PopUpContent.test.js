import React from 'react';
import { render } from 'test-utils';
import { getDaysInMonth } from 'date-fns';
import DatePickerPopUp, {
  showNextMonth,
  showPreviousMonth,
} from '../PopUpContent';

describe('DatePicker popup tests', () => {
  describe('initial date tests', () => {
    it('should render current month when initial date is not specified', () => {
      const { container } = renderDatePickerPopUp();
      const days = document.querySelectorAll('tbody span');
      expect(days).toHaveLength(getDaysInMonth(new Date()));
      for (const day of days) {
        expect(container).toContainElement(day);
      }
    });

    it('should render target month when initial date is specified', () => {
      const initialDate = new Date(2020, 1, 1);
      const { container } = renderDatePickerPopUp({ initialDate });
      const days = document.querySelectorAll('tbody span');
      expect(days).toHaveLength(getDaysInMonth(initialDate));
      for (const day of days) {
        expect(container).toContainElement(day);
      }
    });
  });

  describe('onPreviousMonth tests', () => {
    test('There is no validation if prop from is not passed', () => {
      const spy = jest.fn();
      const today = new Date();
      const from = false;
      showPreviousMonth(from, new Date(), spy);
      expect(spy).toHaveBeenCalled();
      expect(spy.mock.calls[0][0].getMonth()).toBe(today.getMonth() - 1);
    });

    test('it will change the month if its year is greater than the current year', () => {
      const spy = jest.fn();
      const today = new Date();
      const from = new Date(2000, 0, 1);

      showPreviousMonth(from, today, spy);
      expect(spy).toHaveBeenCalled();
      expect(spy.mock.calls[0][0].getMonth()).toBe(today.getMonth() - 1);
    });

    test('it will change the month if its the same year and currentMonth has a greater month than from', () => {
      const spy = jest.fn();
      const currentDate = new Date(2000, 1, 1);
      const from = new Date(2000, 0, 1);

      showPreviousMonth(from, currentDate, spy);
      expect(spy).toHaveBeenCalled();
      expect(spy.mock.calls[0][0].getMonth()).toBe(currentDate.getMonth() - 1);
    });

    test('it should not change the month if is same year and same month than from', () => {
      const spy = jest.fn();
      const currentDate = new Date(2000, 1, 1);
      const from = new Date(2000, 1, 1);

      showPreviousMonth(from, currentDate, spy);
      expect(spy).not.toHaveBeenCalled();
    });
  });

  describe('onNextMonth tests', () => {
    test('There is no validation if prop to is not passed', () => {
      const spy = jest.fn();
      const today = new Date();
      const to = false;
      showNextMonth(to, new Date(), spy);
      expect(spy).toHaveBeenCalled();
      expect(spy.mock.calls[0][0].getMonth()).toBe(today.getMonth() + 1);
    });

    test('it will change the month if its year is lesser than the current year', () => {
      const spy = jest.fn();
      const currentMonth = new Date(1999, 5, 1);
      const to = new Date(2000, 0, 1);

      showNextMonth(to, currentMonth, spy);
      expect(spy).toHaveBeenCalled();
      expect(spy.mock.calls[0][0].getMonth()).toBe(currentMonth.getMonth() + 1);
    });

    test('it will change the month if its the same year and currentMonth has a lesser month than to', () => {
      const spy = jest.fn();
      const currentDate = new Date(2000, 1, 1);
      const to = new Date(2000, 2, 1);

      showNextMonth(to, currentDate, spy);
      expect(spy).toHaveBeenCalled();
      expect(spy.mock.calls[0][0].getMonth()).toBe(currentDate.getMonth() + 1);
    });

    test('it should not change the month if is same year and same month than to', () => {
      const spy = jest.fn();
      const currentDate = new Date(2000, 1, 1);
      const from = new Date(2000, 1, 1);

      showNextMonth(from, currentDate, spy);
      expect(spy).not.toHaveBeenCalled();
    });
  });
});

function renderDatePickerPopUp({
  initialDate = new Date(),
  from = new Date(),
  to = new Date(),
  selectedDates = [new Date(), new Date()],
  validate = () => {},
  onSelect = () => {},
} = {}) {
  return render(
    <DatePickerPopUp
      initialDate={initialDate}
      from={from}
      to={to}
      selectedDates={selectedDates}
      validate={validate}
      onSelect={onSelect}
    />
  );
}
