import React from 'react';
import { render, fireEvent } from 'test-utils';
import PopUpHeader from '../PopUpHeader';

describe('Datepicker PopUpHeader tests', () => {
  describe('render tests', () => {
    it('should render the year of month', () => {
      const today = new Date(2020, 1, 1);
      const { container, getByText } = renderPopUpHeader({ month: today });

      expect(container).toContainElement(getByText(/2020/g));
    });
  });

  describe('callbacks tests', () => {
    it('should call onPreviousMonth when left arrow is clicked', () => {
      const spy = jest.fn();
      const { container } = renderPopUpHeader({ onPreviousMonth: spy });
      fireEvent.click(container.querySelector('.prev_month_btn'));

      expect(spy).toHaveBeenCalled();
    });

    it('should call onNextMonth when right arrow is clicked', () => {
      const spy = jest.fn();
      const { container } = renderPopUpHeader({ onNextMonth: spy });
      fireEvent.click(container.querySelector('.next_month_btn'));

      expect(spy).toHaveBeenCalled();
    });
  });
});

function renderPopUpHeader({
  onNextMonth = () => {},
  onPreviousMonth = () => {},
  month = new Date(),
}) {
  return render(
    <PopUpHeader
      onNextMonth={onNextMonth}
      month={month}
      onPreviousMonth={onPreviousMonth}
    />
  );
}
