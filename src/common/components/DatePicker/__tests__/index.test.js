import React from 'react';
import { render, fireEvent } from 'test-utils';
import DatePicker from '../index';

describe('Datepicker index tests', () => {
  describe('isOpen tests', () => {
    it('should open the popup when the input is focused', () => {
      const { container } = renderDatePicker();
      const input = container.querySelector('input');
      input.focus();

      expect(container).toContainElement(
        container.querySelector('.datepicker_popup')
      );
    });

    it('once popup is open, it should be closed when clicking outside', () => {
      const { container } = renderDatePicker();
      const input = container.querySelector('input');
      input.focus();

      fireEvent.mouseDown(document.getElementById('outside'));
      expect(container).not.toContainElement(
        container.querySelector('.datepicker_popup')
      );
    });

    it('once popup is open, it should be closed when clicking on a day', () => {
      const { container } = renderDatePicker({ selectedDates: [] });
      const input = container.querySelector('input');
      input.focus();

      fireEvent.mouseDown(container.querySelector('tbody span'));
      expect(container).not.toContainElement(
        container.querySelector('.datepicker_popup')
      );
    });
  });

  it('should open popup in the month and year of selected date', () => {
    const { container, getByText } = renderDatePicker({
      selectedDates: [new Date(2010, 5, 15)],
    });

    const input = container.querySelector('input');
    input.focus();

    expect(container).toContainElement(getByText(/June/));
    expect(container).toContainElement(getByText(/2010/));
  });
});

function renderDatePicker({
  selectedDates = [new Date()],
  onSelectDate = () => {},
  label = 'example',
} = {}) {
  return render(
    <div>
      <div id="outside">outside</div>
      <DatePicker
        selectedDates={selectedDates}
        onSelectDate={onSelectDate}
        label={label}
      />
    </div>
  );
}
