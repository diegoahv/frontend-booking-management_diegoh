import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import DateInput from './DateInput';
import DatePickerPopUp from './PopUpContent';

const ExportedDatePicker = React.forwardRef(DatePicker);
ExportedDatePicker.displayName = 'DatePicker';

export default ExportedDatePicker;

const styles = {
  position: 'relative',
  width: '100%',
};

function DatePicker(
  {
    selectedDates = [],
    onSelectDate,
    validate = () => true,
    from = null,
    to = null,
    initialDate = new Date(),
    label,
    placeholder,
    name,
    error,
    numberOfDates = 1,
  },
  ref
) {
  const [isOpen, setOpen] = useState(false);
  const [dateRange, setDateRange] = useState(selectedDates);
  const elRef = useRef(null);

  useEffect(() => {
    document.addEventListener('mousedown', onClickOutside);

    return () => {
      document.removeEventListener('mousedown', onClickOutside);
    };
  }, []);

  useEffect(() => {
    if (datesAreDifferent(dateRange, selectedDates)) {
      setDateRange(selectedDates);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedDates]);

  return (
    <div style={styles} ref={elRef}>
      <DateInput
        name={name}
        selectedDates={selectedDates}
        onFocus={() => setOpen(true)}
        onChange={() => {}}
        label={label}
        placeholder={placeholder}
        error={error}
        ref={ref}
        numberOfDates={numberOfDates}
      />
      {isOpen && (
        <DatePickerPopUp
          onSelect={onSelect}
          selectedDates={dateRange}
          validate={validate}
          from={from}
          to={to}
          initialDate={selectedDates[0] || initialDate}
          numberOfDates={numberOfDates}
        />
      )}
    </div>
  );

  function onSelect(date) {
    let result;
    if (numberOfDates === 1 || dateRange.length >= numberOfDates) {
      result = [date];
    } else {
      result = [...dateRange, date];
    }

    onSelectDate(result, numberOfDates);
    if (result.length === numberOfDates) {
      setOpen(false);
    }

    setDateRange(result);
  }

  function onClickOutside(e) {
    if (elRef.current && !elRef.current.contains(e.target)) {
      setOpen(false);
    }
  }
}

ExportedDatePicker.propTypes = {
  onSelectDate: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  /**
   * Dates already selected by the user
   */
  selectedDates: PropTypes.oneOfType([
    PropTypes.arrayOf(Date),
    PropTypes.instanceOf(Date),
  ]),
  /**
   * Number of dates to select on this datepicker, defaults to 1
   */
  numberOfDates: PropTypes.number,
  /**
   * Predicate to run on each day of the month showed in the datepicker
   */
  validate: PropTypes.func,
  /**
   * the first valid date, previous dates won't be valid
   */
  from: PropTypes.instanceOf(Date),
  /**
   * the last valid date, following dates won't be valid
   */
  to: PropTypes.instanceOf(Date),
  /**
   * Initial date for the datepicker popup
   * */
  initialDate: PropTypes.instanceOf(Date),
  dateFormat: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  error: PropTypes.string,
};

function datesAreDifferent(dates, selectedDates) {
  return (
    dates.length !== selectedDates.length ||
    dates.some((date, index) => date !== selectedDates[index])
  );
}
