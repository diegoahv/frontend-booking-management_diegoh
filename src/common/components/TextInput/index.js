import React, { useState, useRef } from 'react';
import styled from '@emotion/styled';
import css from '@styled-system/css';
import PropTypes from 'prop-types';
import { getTheme } from '@frontend-react-component-builder/conchita';

import {
  Flex,
  ExclamationCircleIcon,
  TickCircleIcon,
  CrossCircleLightIcon,
  Text,
} from 'prisma-design-system';
import StyledLabel from './Label';

const TextInput = React.forwardRef(Input);
TextInput.displayName = 'TextInput';

export default React.forwardRef(Input);

const InputDiv = styled(Flex)(
  css({
    position: 'relative',
    backgroundColor: 'neutrals.7',
    color: 'neutrals.3',
    cursor: 'text',
    height: '56px',
    boxShadow: 'borders.bottom',
    borderRadius: '2px',
    padding: '3',
    '& > svg': {
      alignSelf: 'center',
    },
  }),
  (props) => {
    return css({
      ...(props.focus && {
        backgroundColor: 'brand.primary.lightest',
        color: 'brand.primary.base',
        boxShadow: `0 1px 0 0 ${getTheme().colors.brand.primary.base}`,
      }),
      ...(props.error && {
        backgroundColor: 'critical.lightest',
        color: 'critical.base',
        boxShadow: `0 1px 0 0 ${getTheme().colors.critical.base}`,
      }),
    });
  }
);

const StyledInput = styled('input')(
  css({
    width: '100%',
    fontSize: 'body.2',
    color: 'neutrals.1',
    backgroundColor: 'unset',
    outline: 0,
    border: 0,
    '&::placeholder': {
      color: 'neutrals.1',
    },
  }),
  (props) => {
    return {
      caretColor: props.error
        ? getTheme().colors.critical.base
        : getTheme().colors.brand.primary.base,
    };
  }
);

function Input(
  {
    label,
    onChange,
    onFocus = () => {},
    onBlur = () => {},
    onCleanClick,
    type = 'text',
    placeholder = '',
    name = '',
    value = '',
    error = '',
    Icon = '',
    validated = false,
  },
  parentRef
) {
  const [focus, setFocus] = useState(false);
  const ref = useRef();
  const isOpen = focus || value.length > 0;

  return (
    <div css={css({ fontFamily: 'body' })}>
      <InputDiv
        focus={focus}
        error={Boolean(error)}
        onClick={() => ref.current.focus()}
        alignItems={isOpen ? 'flex-end' : 'center'}
        justifyContent="flex-start"
        flexDirection="row"
      >
        {Icon && <Icon size="medium" />}
        <div css={css({ width: '100%', marginLeft: Icon ? 12 : 0 })}>
          <StyledLabel isOpen={isOpen}>{label}</StyledLabel>
          <StyledInput
            ref={(el) => {
              ref.current = el;
              if (parentRef) {
                parentRef.current = el;
              }
            }}
            type={type}
            placeholder={focus ? placeholder : ''}
            name={name}
            value={value}
            onChange={(e) => {
              onChange(e);
            }}
            onFocus={onFocusWrapper}
            onBlur={onBlurWrapper}
            autoComplete="off"
            error={Boolean(error)}
          />
        </div>
        {error && <ExclamationCircleIcon size="medium" alignSelf="flex-end" />}
        {!error && !focus && validated && (
          <TickCircleIcon
            size="medium"
            color="success.base"
            alignSelf="flex-end"
          />
        )}
        {onCleanClick && !error && focus && value.length > 0 && (
          <div css={css({ cursor: 'pointer' })} onMouseDown={onCleanClick}>
            <CrossCircleLightIcon
              size="medium"
              color="neutrals.3"
              alignSelf="flex-end"
            />
          </div>
        )}
      </InputDiv>
      {error && (
        <Text
          as="span"
          priority={6}
          css={css({ marginLeft: 4, color: 'critical.base' })}
        >
          {error}
        </Text>
      )}
    </div>
  );

  function onFocusWrapper(e) {
    setFocus(true);
    onFocus(e);
  }

  function onBlurWrapper(e) {
    setFocus(false);
    onBlur(e);
  }
}

Input.propTypes = {
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onCleanClick: PropTypes.func,
  onFocus: PropTypes.func,
  type: PropTypes.oneOf(['text', 'email', 'password', 'number']),
  placeholder: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  error: PropTypes.string,
  Icon: PropTypes.elementType,
  /**
   * Specify that the value has been validated. Adds a tick icon to the input
   * */
  validated: PropTypes.bool,
};
