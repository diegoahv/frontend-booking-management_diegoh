import React, { useState } from 'react';
import { withKnobs, text, boolean, select } from '@storybook/addon-knobs';
import { CalendarIcon } from 'prisma-design-system';
import TextInput from './index';

const inputTypes = ['text', 'number', 'email', 'password'];

export default {
  title: 'Common/Inputs/Text',
  component: TextInput,
  decorators: [withKnobs],
};

export const Default = () => {
  const [value, setValue] = useState('');
  const [error, setError] = useState('');
  const withIcon = boolean('With Icon', true);

  return (
    <div>
      <p>Type any number to mark the input as invalid</p>
      <TextInput
        name={text('Name', 'Example')}
        placeholder={text('Placeholder', 'Helper test')}
        type={select('Type', inputTypes, 'text')}
        error={error}
        Icon={withIcon ? CalendarIcon : null}
        onChange={onChange}
        label={text('Label', 'Example')}
        value={value}
        validated={boolean('Validated', false)}
      />
    </div>
  );

  function onChange(e) {
    setValue(e.target.value);

    if (/\d/.test(e.target.value)) {
      setError('numbers not allowed');
    } else {
      setError('');
    }
  }
};
