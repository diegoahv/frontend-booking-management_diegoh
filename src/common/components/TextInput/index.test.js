import React from 'react';
import { CalendarIcon } from 'prisma-design-system';
import { render, fireEvent } from 'test-utils';
import Input from './index';
//
describe('Input component tests', () => {
  it('should render properly', () => {
    const label = 'test';
    const { container, getByText } = renderInput({ label, Icon: CalendarIcon });
    const input = container.getElementsByTagName('input');
    const icon = container.getElementsByTagName('svg');

    expect(input).toHaveLength(1);
    expect(icon).toHaveLength(1);
    expect(container).toContainElement(icon[0]);
    expect(container).toContainElement(input[0]);
    expect(container).toContainElement(getByText(label));
  });

  it('should have a callback attached to onChange that receives the event as argument', () => {
    const event = { target: { value: 'test' } };
    const onChange = jest.fn();
    const { container } = renderInput({ onChange });
    const input = container.getElementsByTagName('input')[0];

    fireEvent.change(input, event);
    expect(onChange).toHaveBeenCalled();
  });

  it('should have a callback attached to onCleanClick', () => {
    const value = 'value';
    const onCleanClick = jest.fn();
    const { container } = renderInput({ onCleanClick, value });
    const input = container.getElementsByTagName('input')[0];
    fireEvent.click(input);
    const icon = container.getElementsByTagName('svg')[0];
    fireEvent.mouseDown(icon);
    expect(onCleanClick).toHaveBeenCalled();
  });

  describe('input states tests', () => {
    it('error state tests', () => {
      const error = 'error';
      const label = 'label';
      const value = 'value';
      const { container, getByText, getByDisplayValue } = renderInput({
        error,
        label,
        value,
      });

      const errorIcon = container.getElementsByTagName('svg');
      expect(container).toContainElement(errorIcon[0]);
      expect(container).toContainElement(getByText(error));
      expect(container).toContainElement(getByDisplayValue(value));
    });
    it('focus state tests', () => {
      const placeholder = 'placeholder';
      const label = 'label';

      const { container, getByText, getByPlaceholderText } = renderInput({
        placeholder,
        label,
      });

      const input = container.getElementsByTagName('input')[0];
      input.focus();

      expect(container).toContainElement(getByText(label));
      expect(container).toContainElement(getByPlaceholderText(placeholder));
    });
    it('filled state tests', () => {
      const value = 'value';
      const label = 'label';
      const { container, getByText, getByDisplayValue } = renderInput({
        value,
        label,
      });

      expect(container).toContainElement(getByText(label));
      expect(container).toContainElement(getByDisplayValue(value));
    });

    it('valid state tests', () => {
      const label = 'label';
      const value = 'value';
      const { container, getByText, getByDisplayValue } = renderInput({
        label,
        value,
        validated: true,
      });

      const checkIcon = container.getElementsByTagName('svg');
      expect(container).toContainElement(checkIcon[0]);
      expect(container).toContainElement(getByText(label));
      expect(container).toContainElement(getByDisplayValue(value));
    });

    it('clean text state tests', () => {
      const label = 'label';
      const value = 'value';
      const { container, getByText, getByDisplayValue } = renderInput({
        label,
        value,
        onCleanClick: jest.fn(),
        validated: false,
      });
      const input = container.getElementsByTagName('input')[0];
      fireEvent.click(input);
      const checkIcon = container.getElementsByTagName('svg');
      expect(container).toContainElement(checkIcon[0]);
      expect(container).toContainElement(getByText(label));
      expect(container).toContainElement(getByDisplayValue(value));
    });

    it('clean text not available state tests', () => {
      const label = 'label';
      const value = 'value';
      const { container } = renderInput({
        label,
        value,
        validated: false,
      });
      const input = container.getElementsByTagName('input')[0];
      fireEvent.click(input);
      const checkIcon = container.getElementsByTagName('svg');
      expect(container).not.toContain(checkIcon[0]);
    });
  });
});

function renderInput({
  type = 'text',
  name = '',
  label = '',
  placeholder = '',
  error = '',
  onChange = () => {},
  onCleanClick,
  Icon = '',
  value = '',
  validated = false,
} = {}) {
  return render(
    <Input
      onChange={onChange}
      onCleanClick={onCleanClick}
      error={error}
      Icon={Icon}
      name={name}
      label={label}
      placeholder={placeholder}
      type={type}
      value={value}
      validated={validated}
    />
  );
}
