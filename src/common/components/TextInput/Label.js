import styled from '@emotion/styled';
import css from '@styled-system/css';
import { CSSTransition } from 'react-transition-group';
import React from 'react';

const transitionName = 'focus';
const transition = 'transform 200ms ease-in';
const transformOrigin = 'translateY(0) scale(1)';
const transformTarget = 'translateY(-18px) scale(0.8)';

const StyledLabel = styled.label(
  css({
    position: 'absolute',
    pointerEvents: 'none',
    fontSize: 'body.2',
    [`&.${transitionName}-enter`]: {
      transform: `${transformOrigin}`,
      transformOrigin: 'left',
    },
    [`&.${transitionName}-enter-active`]: {
      transform: `${transformTarget}`,
      transformOrigin: 'left',
      transition: `${transition}`,
    },
    [`&.${transitionName}-enter-done`]: {
      transformOrigin: 'left',
      transform: `${transformTarget}`,
    },
    [`&.${transitionName}-exit`]: {
      transformOrigin: 'left',
      transform: `${transformTarget}`,
    },
    [`&.${transitionName}-exit-active`]: {
      transformOrigin: 'left',
      transform: `${transformOrigin}`,
      transition: `${transition}`,
    },
  })
);

export default ({ children, isOpen }) => {
  return (
    <CSSTransition
      in={isOpen}
      appear={isOpen}
      timeout={300}
      classNames={transitionName}
    >
      <StyledLabel>{children}</StyledLabel>
    </CSSTransition>
  );
};
