import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import { getTheme } from '@frontend-react-component-builder/conchita';

const StyledTable = styled.table(
  {
    borderCollapse: 'separate',
    textAlign: 'center',
    width: '100%',
  },
  (props) => ({
    borderSpacing: `0 ${getTheme().space[1]}px`,
  })
);

const StyledTd = styled.td({
  padding: '0',
});

export default Table;

function Table({ head = [[]], body }) {
  return (
    <StyledTable>
      <thead>
        {head.map((row, index) => (
          <tr key={`head_row_${index}`}>
            {row.map((column, index) => (
              <StyledTd key={`head_row_${index}_column_${index}`}>
                {column}
              </StyledTd>
            ))}
          </tr>
        ))}
      </thead>
      <tbody>
        {body.map((row, index) => (
          <tr key={`body_row_${index}`}>
            {row.map((column, index) => (
              <StyledTd key={`body_row_${index}_column_${index}`}>
                {column}
              </StyledTd>
            ))}
          </tr>
        ))}
      </tbody>
    </StyledTable>
  );
}

Table.propTypes = {
  head: PropTypes.arrayOf(Array),
  body: PropTypes.arrayOf(Array).isRequired,
};
