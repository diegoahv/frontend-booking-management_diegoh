import React from 'react';
import { render, getAllByText } from 'test-utils';
import Table from './index';

const headMatrix = [[1, 2, 3, 4]];
const bodyMatrix = [
  [1, 2, 3, 4],
  [5, 6, 7, 8],
];

describe('Table component tests', () => {
  describe('table head tests', () => {
    it('should render a tr for each row ana a td for each column in the matrx', () => {
      const { container } = render(
        <Table head={headMatrix} body={bodyMatrix} />
      );
      const trsInHead = container.querySelectorAll('thead tr');
      const tdsInHead = container.querySelectorAll('thead td');

      expect(trsInHead).toHaveLength(headMatrix.length);
      expect(tdsInHead).toHaveLength(headMatrix.length * headMatrix[0].length);
    });

    it('should render the contents of the matrix', () => {
      const { container } = render(
        <Table head={headMatrix} body={bodyMatrix} />
      );

      const matches = getAllByText(container.querySelector('thead'), /^\d{1}$/);
      expect(matches).toHaveLength(headMatrix[0].length);
    });
  });

  describe('table body tests', () => {
    it('should render a tr for each row ana a td for each column in the matrx', () => {
      const { container } = render(
        <Table head={headMatrix} body={bodyMatrix} />
      );
      const trsInBody = container.querySelectorAll('tbody tr');
      const tdsInBody = container.querySelectorAll('tbody td');

      expect(trsInBody).toHaveLength(bodyMatrix.length);
      expect(tdsInBody).toHaveLength(bodyMatrix.length * bodyMatrix[0].length);
    });

    it('should render the contents of the matrix', () => {
      const { container } = render(
        <Table head={headMatrix} body={bodyMatrix} />
      );

      const matches = getAllByText(container.querySelector('tbody'), /^\d{1}$/);
      expect(matches).toHaveLength(bodyMatrix.length * bodyMatrix[0].length);
    });
  });
});
