import React from 'react';
import * as PropTypes from 'prop-types';
import { Box, Link, Text } from 'prisma-design-system';
import BottomDrawer from '../BottomDrawer';
import styled from '@emotion/styled';
import css from '@styled-system/css';
import { useTranslation } from 'react-i18next';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import { getTracking } from '@/src/containers/SearchPage/tracking';

const TermsAndConditionsModal = ({
  showModal,
  toggleModal,
  title,
  bodyText,
  carrierCode,
}) => {
  const [sessionData = {}] = ApplicationContext.useSession();
  const { t } = useTranslation();
  const anchorText = t('searchPage:modal.airlineTcsLink');
  const tcsLinkLanguage = sessionData.locale.slice(0, 2).toUpperCase();
  const tracking = ApplicationContext.useTracking();

  return (
    <BottomDrawer open={showModal} onClose={toggleModal} header={title}>
      <Box className="modal_content">
        <StyledText as={'p'} priority={6}>
          {bodyText}
          <StyledLink
            onClick={() => {
              const { category, action, label } = getTracking(
                'TERMS_MORE_INFO'
              );
              tracking.trackEventWithCategory(category, action, label);
              showTermsAndConditionsPdf(carrierCode, tcsLinkLanguage);
            }}
            data-testid="nexStepsLink"
          >
            {` ${anchorText}`}
          </StyledLink>
        </StyledText>
      </Box>
    </BottomDrawer>
  );
};

const StyledText = styled(Text)(
  css({
    textAlign: 'justify',
  })
);

const StyledLink = styled(Link)(
  css({
    fontSize: 'body.0',
  })
);

function showTermsAndConditionsPdf(carrierCode, tcsLinkLanguage) {
  window.open(
    `https://www.edreams.com/images/pdf/open-tickets/conditions/${carrierCode}-2994-${tcsLinkLanguage}.pdf`,
    '_blank'
  );
}

TermsAndConditionsModal.propTypes = {
  showModal: PropTypes.bool.isRequired,
  toggleModal: PropTypes.func.isRequired,
};
export default TermsAndConditionsModal;
