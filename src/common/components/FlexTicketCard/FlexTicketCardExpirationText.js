import React from 'react';
import css from '@styled-system/css';
import { Text } from 'prisma-design-system';
import styled from '@emotion/styled';

const StyledText = styled(Text)(
  css({
    paddingY: 5,
    paddingLeft: 4,
  })
);

const FlexTicketCardExpirationText = ({ expirationText }) => {
  return (
    <StyledText
      as="div"
      fontWeight="normal"
      textAlign="left"
      color="white"
      fontSize={'body.0'}
    >
      {expirationText}
    </StyledText>
  );
};
export default FlexTicketCardExpirationText;
