import React from 'react';
import { storiesOf } from '@storybook/react';
import { text, withKnobs } from '@storybook/addon-knobs';
import { selectCarrier } from 'decorators';

import FlexTicketCard from './FlexTicketCard';

storiesOf('OpenTicket/Search/Flexible Ticket Card', module)
  .addDecorator(withKnobs)
  .add('Playground', () => {
    return (
      <FlexTicketCard
        carrier={[selectCarrier()][0]}
        expirationDate={text('Expiration date', '2021-07-23T01:15:00+01:00')}
      />
    );
  });
