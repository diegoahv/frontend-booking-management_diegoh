import React, { useState } from 'react';
import { Card, CardContent } from 'prisma-design-system';
import css from '@styled-system/css';
import styled from '@emotion/styled';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
import { getTracking } from '@/src/containers/SearchPage/tracking';

import TermsAndConditionsModal from './TermsAndConditionsModal';
import FlexTicketCardHeader from './FlexTicketCardHeader';
import FlexTicketCardExpirationText from './FlexTicketCardExpirationText';
import FlexTicketCardFooter from './FlexTicketCardFooter';

const StyledCard = styled(Card)(
  css({
    backgroundColor: 'brandPrimary.3',
    padding: 0,
    borderRadius: 1,
  })
);

const FlexTicketCard = ({ carrier, content }) => {
  const [modalShown, setModalShown] = useState(false);
  const tracking = ApplicationContext.useTracking();

  const toggleModal = () => {
    const { category, action, label } = getTracking('TERMS_CLOSE');
    if (modalShown) {
      tracking.trackEventWithCategory(category, action, label);
    }
    setModalShown(!modalShown);
  };
  return (
    <StyledCard>
      <CardContent>
        <FlexTicketCardHeader
          carrierCode={carrier.code}
          carrierName={carrier.name}
          headerHeadline={content.title}
          headerDescription={content.subtitle}
        />
        <FlexTicketCardExpirationText expirationText={content.validUntil} />
      </CardContent>
      <FlexTicketCardFooter
        toggleModal={toggleModal}
        footerText={content.footer}
      />
      <TermsAndConditionsModal
        showModal={modalShown}
        toggleModal={toggleModal}
        title={content.modalTitle}
        bodyText={content.modalBody}
        carrierCode={carrier.code}
      />
    </StyledCard>
  );
};

export default FlexTicketCard;
