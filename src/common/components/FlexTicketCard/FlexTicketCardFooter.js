import React from 'react';
import { Flex, Text, ArrowRightIcon } from 'prisma-design-system';
import css from '@styled-system/css';
import styled from '@emotion/styled';
import { getTracking } from '@/src/containers/SearchPage/tracking';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';

const StyledFlex = styled(Flex)(
  css({
    paddingY: 3,
    paddingLeft: 4,
    paddingRight: 3,
    color: 'brandPrimary.2',
  })
);

const FlexTicketCardFooter = ({ toggleModal, footerText }) => {
  const tracking = ApplicationContext.useTracking();
  return (
    <div>
      <StyledFlex
        justifyContent="space-between"
        alignItems="flex-start"
        color="white"
        fontSize="body.0"
        onClick={() => {
          const { category, action, label } = getTracking(
            'TERMS_AND_CONDITIONS'
          );
          tracking.trackEventWithCategory(category, action, label);
          toggleModal();
        }}
      >
        <Text as="div" textAlign="left">
          {footerText}
        </Text>
        <ArrowRightIcon size="small" color="brandPrimary.2" />
      </StyledFlex>
    </div>
  );
};

export default FlexTicketCardFooter;
