import React from 'react';
import css from '@styled-system/css';
import CarrierLogo from '../../../components/Carrier/CarrierLogo';
import { Flex, Text, Box } from 'prisma-design-system';
import styled from '@emotion/styled';

const StyledCarrierLogo = styled(CarrierLogo)(css({ marginTop: 1 }));

const FlexTicketCardHeader = ({
  carrierCode,
  carrierName,
  headerHeadline,
  headerDescription,
}) => {
  const imageSize = 'small';

  return (
    <Flex
      justifyContent="flex-start"
      alignItems="flex-start"
      ml={4}
      mt={4}
      pr={4}
    >
      <StyledCarrierLogo
        key={carrierCode}
        id={carrierCode}
        name={carrierName}
        imageSize={imageSize}
      />
      <Box pl={1}>
        <Text
          as="div"
          fontWeight="bold"
          textAlign="left"
          color="white"
          priority={2}
        >
          {headerHeadline}
        </Text>
        <Text
          as="div"
          fontWeight="normal"
          textAlign="left"
          priority={6}
          color="negative"
        >
          {headerDescription}
        </Text>
      </Box>
    </Flex>
  );
};

export default FlexTicketCardHeader;
