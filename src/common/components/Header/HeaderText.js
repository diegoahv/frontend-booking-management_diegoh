import styled from '@emotion/styled';
import { Text } from 'prisma-design-system';
import css from '@styled-system/css';

const HeaderText = styled(Text)(
  css({
    fontWeight: 'medium',
  }),
  {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  }
);

export default HeaderText;
