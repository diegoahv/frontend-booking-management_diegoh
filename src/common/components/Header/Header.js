import styled from '@emotion/styled';
import css from '@styled-system/css';
import { Box } from 'prisma-design-system';
import { variant } from 'styled-system';

const isInternetExplorer = () =>
  !!navigator.userAgent.match(/Trident/g) ||
  !!navigator.userAgent.match(/MSIE/g);
const Header = styled(Box)(
  css({
    minHeight: 3,
    backgroundColor: 'white',
  }),
  variant({
    prop: 'fixedHeader',
    variants: {
      fixed: {
        position: isInternetExplorer() ? 'absolute' : 'fixed',
        top: 0,
        left: 0,
        width: '100%',
        zIndex: 'popover',
      },
      relative: {
        position: 'relative',
      },
    },
  }),
  {
    padding: '20px',
    boxShadow: '0 1px 8px 2px rgba(0, 13, 41, 0.24)',
  }
);

export default Header;
