import Header from './Header';
import React from 'react';
import { ArrowLeftIcon, Box, Flex } from 'prisma-design-system';
import HeaderText from './HeaderText';
import { text, withKnobs } from '@storybook/addon-knobs';

export default {
  title: 'Common/Header',
  component: Header,
  decorators: [withKnobs],
};

export const Default = () => {
  return (
    <Header>
      <Flex alignItems="center">
        <Box>
          <ArrowLeftIcon color="brandPrimary.2" size="medium" />
        </Box>
        <HeaderText fontWeight="bold" priority={2}>
          {text(
            'Title',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
          )}
        </HeaderText>
      </Flex>
    </Header>
  );
};
