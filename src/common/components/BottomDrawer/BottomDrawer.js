import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Portal from '@/src/prisma-altered/components/Portal';
import DrawerContainer from './DrawerContainer';
import DrawerWrapper from './DrawerWrapper';
import withAnimation from '@/src/prisma-altered/hooks/withAnimation';
import DrawerHeader from './DrawerHeader';
import { Box } from 'prisma-design-system';
import styled from '@emotion/styled';
import css from '@styled-system/css';

const AnimatedDrawerWrapper = withAnimation(DrawerWrapper, 400);

const DrawerContentWrapper = styled(Box)(() =>
  css({
    overflowY: 'auto',
  })
);

const BottomDrawer = ({
  children,
  open = true,
  onClose,
  header,
  withCloseIcon,
  closeLabel,
}) => {
  useEffect(() => {
    open
      ? (document.body.style.overflow = 'hidden')
      : (document.body.style.overflow = 'unset');
  }, [open]);
  return (
    <Portal>
      <AnimatedDrawerWrapper open={open} onClose={onClose}>
        <DrawerContainer>
          <DrawerHeader
            withCloseIcon={withCloseIcon}
            onClose={onClose}
            closeLabel={closeLabel}
          >
            {header}
          </DrawerHeader>
          <DrawerContentWrapper p={2}>{children}</DrawerContentWrapper>
        </DrawerContainer>
      </AnimatedDrawerWrapper>
    </Portal>
  );
};

BottomDrawer.propTypes = {
  /** Whether the drawer is open. Is passed to the DrawerWrapper. */
  open: PropTypes.bool,
  onClose: PropTypes.func,
  header: PropTypes.node,
  withCloseIcon: PropTypes.bool,
  closeLabel: PropTypes.string,
};

export default BottomDrawer;
