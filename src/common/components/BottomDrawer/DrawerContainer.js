import React from 'react';
import styled from '@emotion/styled';
import css from '@styled-system/css';
import PropTypes from 'prop-types';
import { Flex } from 'prisma-design-system';
import { getTheme } from '@frontend-react-component-builder/conchita';

const StyledDrawerContainer = styled(Flex)((props) =>
  css({
    position: 'fixed',
    bottom: 0,
    right: 0,
    width: '100%',
    maxHeight: '90%',
    flexDirection: 'column',
    fontFamily: 'body',
    bg: 'white',
    borderRadius: `${getTheme().radii[2]}px ${getTheme().radii[2]}px 0 0`,
  })
);

const DrawerContainer = ({ children }) => {
  return (
    <StyledDrawerContainer
      p={4}
      onClick={(e) => {
        e.stopPropagation();
      }}
    >
      {children}
    </StyledDrawerContainer>
  );
};

DrawerContainer.propTypes = {
  children: PropTypes.node,
};

export default DrawerContainer;
