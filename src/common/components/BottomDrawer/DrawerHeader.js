import React from 'react';
import { Flex } from 'prisma-design-system';
import CloseIcon from '@/src/prisma-altered/components/CloseIcon';
import PropTypes from 'prop-types';

const DrawerHeader = ({
  withCloseIcon,
  onClose = () => {},
  closeLabel,
  children,
}) => {
  return (
    <Flex width="100%" justifyContent="space-between" alignItems="flex-start">
      {withCloseIcon && (
        <Flex pr={2}>
          <CloseIcon
            onClose={onClose}
            closeLabel={closeLabel}
            color={'brand.primary.base'}
            size={'small'}
          />
        </Flex>
      )}
      <Flex
        flex="1 1 0"
        justifyContent="center"
        alignSelf="center"
        fontWeight="medium"
        fontSize="body.2"
        textAlign="center"
        pr={6}
      >
        {children}
      </Flex>
    </Flex>
  );
};

DrawerHeader.propTypes = {
  closeLabel: PropTypes.string,
  onClose: PropTypes.func,
  withCloseIcon: PropTypes.bool,
};
DrawerHeader.defaultProps = {
  withCloseIcon: true,
  closeLabel: 'close',
};

export default DrawerHeader;
