import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import BottomDrawer from '../.';

const CONTENT = {
  TITLE: 'drawer title',
  BODY: 'drawer content',
  CLOSE_LABEL: 'click to close',
};

beforeEach(() => jest.useFakeTimers());

describe('BottomDrawer', () => {
  describe('Display', () => {
    test('Header and content appear when drawer is open', () => {
      const { getByText } = render(
        <BottomDrawer open={true} header={CONTENT.TITLE}>
          {CONTENT.BODY}
        </BottomDrawer>
      );

      expect(getByText(CONTENT.TITLE)).toBeVisible();
      expect(getByText(CONTENT.BODY)).toBeVisible();
    });

    test('Content does not appear when drawer is closed', () => {
      const { queryByText } = render(
        <BottomDrawer open={false} header={CONTENT.TITLE}>
          {CONTENT.BODY}
        </BottomDrawer>
      );

      expect(queryByText(CONTENT.TITLE)).toBe(null);
      expect(queryByText(CONTENT.BODY)).toBe(null);
    });
  });

  describe('Actions', () => {
    test('onClose callback is called when drawer is closed', () => {
      const onClose = jest.fn();
      const { getByLabelText } = render(
        <BottomDrawer
          open={true}
          header={CONTENT.TITLE}
          onClose={onClose}
          closeLabel={CONTENT.CLOSE_LABEL}
        >
          {CONTENT.BODY}
        </BottomDrawer>
      );
      fireEvent.click(getByLabelText(CONTENT.CLOSE_LABEL));
      expect(onClose).toHaveBeenCalled();
    });
  });
});
