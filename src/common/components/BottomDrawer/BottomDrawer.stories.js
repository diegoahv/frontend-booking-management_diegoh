import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { Button } from 'prisma-design-system';
import { action } from '@storybook/addon-actions';
import NoResults from '@/src/components/Results/NoResults';
import { withKnobs, text } from '@storybook/addon-knobs';
import HotelPlaneErrorIcon from '@/src/common/icons/HotelPlaneErrorIcon';

import BottomDrawer from './BottomDrawer';

const noResultsContent = {
  title: "Sorry, we can't find any flights for that search",
  text: 'Why not try flying to a nearby airport or adjusting your dates?',
  cta: 'New Search',
};

storiesOf('Common/BottomDrawer', module)
  .addDecorator(withKnobs)
  .add('Default', () => {
    const [isOpen, setStatus] = useState(false);
    const handleClose = () => {
      action('close');
      setStatus(false);
    };
    const callToAction = () => {
      handleClose();
      alert('new search launched!');
    };
    const title = text('Title', '');
    return (
      <>
        <Button onClick={() => setStatus(true)}>Open drawer</Button>
        <BottomDrawer header={title} open={isOpen} onClose={handleClose}>
          <NoResults
            content={noResultsContent}
            icon={<HotelPlaneErrorIcon />}
            onCallToAction={callToAction}
          />
        </BottomDrawer>
      </>
    );
  });
