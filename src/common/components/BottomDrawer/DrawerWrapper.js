import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import css from '@styled-system/css';
import { variant, system } from 'styled-system';
import { Flex } from 'prisma-design-system';

const StyledDrawerShadow = styled(Flex)(
  () =>
    css({
      zIndex: 'modal',
      width: '100%',
      overflowY: 'auto',
      minHeight: '100%',
      maxHeight: '100%',
      position: 'fixed',
      left: 0,
      top: 0,
      backgroundColor: 'rgba(39, 39, 39, 0.6)',
    }),
  system({
    transitionDuration: true,
  }),
  variant({
    prop: 'state',
    variants: {
      opening: {
        opacity: 0,
        transitionTimingFunction: 'ease-in',
      },
      opened: {
        opacity: 1,
      },
      closing: {
        opacity: 1,
        transitionTimingFunction: 'ease-in',
      },
      closed: {
        opacity: 0,
      },
    },
  })
);

const StyledDrawerWrapper = styled(Flex)(
  () =>
    css({
      width: '100%',
      overflowY: 'auto',
      minHeight: '100%',
      maxHeight: '100%',
      position: 'fixed',
      left: 0,
      top: 0,
    }),
  system({
    transitionDuration: true,
  }),
  variant({
    prop: 'state',
    variants: {
      opening: {
        transform: 'translateY(100%)',
        transitionTimingFunction: 'ease-in',
      },
      closing: {
        transform: 'translateY(100%)',
        transitionTimingFunction: 'ease-in',
      },
      closed: {
        opacity: 0,
      },
    },
  })
);

const DrawerWrapper = ({ children, state, transitionDuration, onClose }) => {
  return (
    <StyledDrawerShadow state={state}>
      <StyledDrawerWrapper
        state={state}
        transitionDuration={transitionDuration}
        onClick={onClose}
      >
        {children}
      </StyledDrawerWrapper>
    </StyledDrawerShadow>
  );
};

DrawerWrapper.propTypes = {
  children: PropTypes.any.isRequired,
  state: PropTypes.string.isRequired,
  transitionDuration: PropTypes.string.isRequired,
};

export default DrawerWrapper;
