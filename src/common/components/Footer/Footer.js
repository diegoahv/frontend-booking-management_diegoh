import styled from '@emotion/styled';
import css from '@styled-system/css';
import { Box } from 'prisma-design-system';
import { variant } from 'styled-system';

const Footer = styled(Box)(
  css({
    backgroundColor: 'white',
  }),
  variant({
    prop: 'footerType',
    variants: {
      fixed: {
        position: 'fixed',
        bottom: 0,
        left: 0,
        width: '100%',
        zIndex: 'popover',
      },
      relative: {
        position: 'relative',
      },
    },
  })
);

export default Footer;
