import React from 'react';
import PropTypes from 'prop-types';

const ImageFixedSize = ({ width, height, alt = '', ...rest }) => (
  <img
    css={{
      width: width ? `${width}px` : 'auto',
      height: height ? `${height}px` : 'auto',
    }}
    alt={alt}
    {...rest}
  />
);

ImageFixedSize.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  alt: PropTypes.string,
  src: PropTypes.string.isRequired,
};

ImageFixedSize.defaultProps = {
  alt: '',
};

export default ImageFixedSize;
