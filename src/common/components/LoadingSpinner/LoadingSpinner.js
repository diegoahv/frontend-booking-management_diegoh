import React from 'react';
import styled from '@emotion/styled';
import { keyframes } from '@emotion/core';
import { Box } from 'prisma-design-system';
import css from '@styled-system/css';
import { getTheme } from '@frontend-react-component-builder/conchita';

const StyledLoadingWrapper = styled(Box)(
  css({
    color: 'transparent',
    position: 'relative',
    p: 4,
  })
);

const spinner = keyframes`
  0% { opacity: 1; }
  100% { opacity: 0; }
`;

const LoadingPart = styled(Box)`
  transform-origin: 8px 9px;
  animation: ${spinner} 1.2s linear infinite;
  &:after {
    content: '';
    display: block;
    position: absolute;
    top: -5px;
    left: 7px;
    width: 3px;
    height: 7px;
    border-radius: 30%;
    background: ${({ color }) => color || getTheme().colors.brand.primary.base};
  }
`;

const StyledLoadingSpinner = styled(Box)`
  display: block;
  transition: all 0.4s ease;
  position: absolute;
  left: 50%;
  top: 50%;
  vertical-align: text-bottom;
  margin: -8px 0 0 -8px;

  ${LoadingPart}:nth-of-type(1) {
    transform: rotate(0deg);
    animation-delay: -1.1s;
  }
  ${LoadingPart}:nth-of-type(2) {
    transform: rotate(30deg);
    animation-delay: -1s;
  }
  ${LoadingPart}:nth-of-type(3) {
    transform: rotate(60deg);
    animation-delay: -0.9s;
  }
  ${LoadingPart}:nth-of-type(4) {
    transform: rotate(90deg);
    animation-delay: -0.8s;
  }
  ${LoadingPart}:nth-of-type(5) {
    transform: rotate(120deg);
    animation-delay: -0.7s;
  }
  ${LoadingPart}:nth-of-type(6) {
    transform: rotate(150deg);
    animation-delay: -0.6s;
  }
  ${LoadingPart}:nth-of-type(7) {
    transform: rotate(180deg);
    animation-delay: -0.5s;
  }
  ${LoadingPart}:nth-of-type(8) {
    transform: rotate(210deg);
    animation-delay: -0.4s;
  }
  ${LoadingPart}:nth-of-type(9) {
    transform: rotate(240deg);
    animation-delay: -0.3s;
  }
  ${LoadingPart}:nth-of-type(10) {
    transform: rotate(270deg);
    animation-delay: -0.2s;
  }
  ${LoadingPart}:nth-of-type(11) {
    transform: rotate(300deg);
    animation-delay: -0.1s;
  }
  ${LoadingPart}:nth-of-type(12) {
    transform: rotate(330deg);
    animation-delay: 0s;
  }
`;

const LoadingSpinner = ({ color }) => {
  return (
    <StyledLoadingWrapper>
      <StyledLoadingSpinner>
        <LoadingPart color={color} />
        <LoadingPart color={color} />
        <LoadingPart color={color} />
        <LoadingPart color={color} />
        <LoadingPart color={color} />
        <LoadingPart color={color} />
        <LoadingPart color={color} />
        <LoadingPart color={color} />
        <LoadingPart color={color} />
        <LoadingPart color={color} />
        <LoadingPart color={color} />
        <LoadingPart color={color} />
      </StyledLoadingSpinner>
    </StyledLoadingWrapper>
  );
};

export default LoadingSpinner;
