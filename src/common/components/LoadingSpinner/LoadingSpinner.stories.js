import React from 'react';
import LoadingSpinner from './LoadingSpinner';

export default {
  title: 'Common/Loading Spinner',
  component: LoadingSpinner,
};

export const Default = () => <LoadingSpinner />;
