import React from 'react';
import { render } from 'test-utils';
import MenuCard from '../MenuCard';
import { Text } from 'prisma-design-system';

const renderMenuCard = (type = 'default') =>
  render(
    <MenuCard type={type}>
      <Text className="textElementClass">Test</Text>
    </MenuCard>
  );

describe('MenuCard tests', () => {
  describe('Render tests', () => {
    it('MenuCard renders children elements', () => {
      const { container } = renderMenuCard();
      const textElement = container.querySelector('.textElementClass');
      expect(container).toContainElement(textElement);
    });
  });

  describe('MenuCard Type values', () => {
    it('Default value', () => {
      const { container } = renderMenuCard();
      expect(container).toContainElement(
        container.querySelector('[type="default"]')
      );
    });

    it('Prime value', () => {
      const { container } = renderMenuCard('prime');
      expect(container).toContainElement(
        container.querySelector('[type="prime"]')
      );
    });
  });
});
