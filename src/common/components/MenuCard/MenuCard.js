import React from 'react';
import styled from '@emotion/styled';
import { variant } from 'styled-system';
import PropTypes from 'prop-types';
import { Box } from 'prisma-design-system';
import { ApplicationContext } from '@frontend-react-component-builder/conchita';
const StyledMenuCard = styled(Box)((props) =>
  variant({
    prop: 'type',
    variants: {
      default: {
        pl: props.isMobile ? '4' : '5',
        pr: '4',
        py: props.isMobile ? '4' : '5',
        border: 1,
        borderRadius: 2,
        borderColor: 'neutrals.5',
        backgroundColor: 'white',
      },
      prime: {
        px: props.isMobile ? 4 : 5,
        py: 5,
        borderRadius: 2,
        backgroundColor: 'brandPrimary.4',
        color: 'white',
        fontFamily: '"Open Sans", sans-serif',
      },
    },
  })
);

const MenuCard = ({ children, ...rest }) => {
  const [{ isMobile = false }] = ApplicationContext.useFlow();
  return (
    <StyledMenuCard isMobile={isMobile} {...rest}>
      {children}
    </StyledMenuCard>
  );
};

MenuCard.propTypes = {
  type: PropTypes.oneOf(['default', 'prime']),
  children: PropTypes.node.isRequired,
};

MenuCard.defaultProps = {
  type: 'default',
};

export default MenuCard;
