import MenuCard from './MenuCard';
import React from 'react';

export default {
  title: 'MMB/Menu Card',
  component: MenuCard,
};

export const Default = () => {
  return (
    <MenuCard>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean feugiat
      sed mi id aliquam. In id leo non dolor pulvinar porta tempus nec arcu.
      Cras eleifend velit sed sapien suscipit, non dictum libero porttitor.
      Maecenas pellentesque tincidunt ante. Curabitur non maximus diam. Fusce eu
      nunc quis nisi pellentesque ornare. Vivamus iaculis lectus malesuada lacus
      hendrerit, eget condimentum nisl pulvinar. Sed sodales auctor enim at
      pharetra. Pellentesque habitant morbi tristique senectus et netus et
      malesuada fames ac turpis egestas. Aliquam erat volutpat. Pellentesque
      porttitor turpis ut nisi faucibus tincidunt.
    </MenuCard>
  );
};

export const Prime = () => {
  return (
    <MenuCard type="prime">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean feugiat
      sed mi id aliquam. In id leo non dolor pulvinar porta tempus nec arcu.
      Cras eleifend velit sed sapien suscipit, non dictum libero porttitor.
      Maecenas pellentesque tincidunt ante. Curabitur non maximus diam. Fusce eu
      nunc quis nisi pellentesque ornare. Vivamus iaculis lectus malesuada lacus
      hendrerit, eget condimentum nisl pulvinar. Sed sodales auctor enim at
      pharetra. Pellentesque habitant morbi tristique senectus et netus et
      malesuada fames ac turpis egestas. Aliquam erat volutpat. Pellentesque
      porttitor turpis ut nisi faucibus tincidunt.
    </MenuCard>
  );
};
