import { Mocks } from '@frontend-react-component-builder/debug';

import { openTicketFunnel } from '@/src/export';
import { DefaultBookingData } from 'serverMocks/bookingData';
import {
  autocompleteResponse,
  getBookingInfo,
  getPNRInfo,
  mockEndpoint,
  searchResults,
  getRedemptionRequest,
} from 'serverMocks';

const translations = require('@/assets/translations/ED/en.json');
let called = false;

const mockContext = {
  ...Mocks.CustomApplicationContextMock(),
  application: {
    session: {
      ...Mocks.Session.DefaultSession(),
      brandNewNomenclature: 'ED',
      flow: 'mobile',
      locale: 'es_ES',
      website: 'ES',
    },
    ab: { mmbDocumentation: true, mmbAdditionalServices: true },
    data: {
      bookingInfo: getBookingInfo(),
      services: {
        bookingChangeRequest: (input) => '/' + input,
      },
      pnrInfo: getPNRInfo(),
    },
  },
};

Mocks.mockTranslations(translations);

Mocks.Fetcher.mockGet(
  '/service/bookingchangerequest/mybooking/conchita.the.best@odigeo.com/7082689696',
  DefaultBookingData()
);

Mocks.Fetcher.mockGet(
  '/service/users/managebookingtoken?bookingid=7082689696&email=conchita.the.best@odigeo.com',
  {
    messageError: null,
    responseOK: true,
    token: 'token',
  }
);

mockEndpoint({
  method: 'get',
  url:
    '/service/geo/autocomplete;searchWord=osa;departureOrArrival=DEPARTURE;addSearchByCountry=true;addSearchByRegion=true;nearestLocations=true;product=FLIGHT',
  data: autocompleteResponse,
});

mockEndpoint({
  method: 'post',
  url: '/searchAlternatives',
  data: searchResults,
});

mockEndpoint({
  method: 'post',
  url: '/createOpenTicketRedemptionRequest',
  data: () => {
    return Promise.resolve();
  },
});

mockEndpoint({
  method: 'get',
  url: `/redemption/booking/7082689696/pnr/DDA2L9`,
  data: () => {
    if (!called) {
      called = true;
      return Promise.resolve(null);
    } else {
      return Promise.resolve(getRedemptionRequest);
    }
  },
});

if (typeof window !== 'undefined') {
  openTicketFunnel({
    translationPath: '/translations',
    applicationContext: mockContext,
    container: document.querySelector('#one-front-root'),
  }).then((ref) => console.log('ref', ref));
}
