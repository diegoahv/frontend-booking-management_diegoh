const path = require('path');
const webpackMerge = require('webpack-merge');

const loadPresets = require('./build/loadPresets');
const commonConfig = require('./build/webpack.common');
const modeConfig = (env) => require(`./build/webpack.${env.mode}`)(env);

const dist = path.resolve(__dirname, 'dist');
const assets = path.resolve(__dirname, 'assets');
const root = path.resolve(__dirname);

module.exports = (env = { mode: 'development', presets: [] }) => {
  env.paths = {
    dist,
    assets,
    root,
  };

  return webpackMerge(commonConfig(env), modeConfig(env), loadPresets(env));
};
