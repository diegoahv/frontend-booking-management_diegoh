# Manage My Booking
##### WeReactTogether

## Prerequisites

- We need node 12 version, at least, for this project to work.
    - [node.js](https://nodejs.org/en/)

## Build

1 - Install dependencies:

```
npm install
```

2 - Download translations:

```
npm run translate
```

3 - Adds Prisma preview to storybook:

Clone Prisma inside `config/prisma` folder

```
cd config/prisma
git clone https://bitbucket.org/odigeoteam/prisma-design-system
```

4 - Explore storybook

```
npm run storybook
```

5 - Explore the application

```
npm start
```

6 - Launch Unit Tests

Please, ensure you have the latest translations running `npm run translate` before launching UTs.

```
npm run ut
```

## A note on prisma-design-system imports

There is a re-export of prisma in src/prisma_rexport/index.js to allow tree shaking in our side. If webpack throws you an error regarding a missing prisma component, you'll need to add that component to the re-export file
   
## Technical overview

This application uses the following technology:

- Testing:
    - Unit test:
        - [Jest](https://jestjs.io/)
        - [Testing Library](https://testing-library.com/)
    - Functional tests
        - [Cypress](https://www.cypress.io/)

- CSS-in-JS
    - [Emotion](https://emotion.sh/)

- Component development
    - [Storybook](https://storybook.js.org/)

- Format code
    - [Prettier](https://prettier.io/)

## Commit changes

There is a pre-commit hook set that format and lint the files modified.
This is done by using [husky](https://github.com/typicode/husky) and [lintstaged](https://github.com/okonet/lint-staged), you can find the setup in the following files: 

`.huskyrc` and `.linstagedrc`

## Generating a release

- After merging your PR wait for the Jenkins CI job to finish [Jenkins](http://bcn-jenkins-01.odigeo.org/jenkins/job/frontend-booking-management/)
- Log into http://bcn-jenkins-01.odigeo.org/jenkins/job/npm-release-pipeline/
- Click on "Build with Parameters"
- In Project we enter "frontend-booking-management"
- Select between: patch, minor or major
- Launch the process

## Linking the new release in OF

- Once you have created the new release, create a new ticket ticket for the OF development.
- In `package.json` change the version number of `frontend-booking-management` to match the new one.
- Implement any new dispatch method to communicate from MMB to OF or vice versa.


**Note**:

It would be handy if you configure [Prettier](https://prettier.io/) to work in your IDE or Text Editor, to autoformat the files at the same time you code or just format them whenever you want.
   
- IntelliJ setup: https://prettier.io/docs/en/webstorm.html#running-prettier-on-save-using-file-watcher
    
## Additional info

You can find more info in our [confluence page](https://jira.odigeo.com/wiki/display/CSS/Customer+Self+Service+Frontend+Module)
 
