import '@testing-library/jest-dom';

jest.mock('react-i18next', () => {
  const KEYS = {
    datePicker: {
      weekDaysShort: 'Sun,Mon,Tue,Wed,Thu,Fri,Sat',
      months:
        'January,February,March,April,May,June,July,August,September,October,November,December',
    },
    results: {
      multiple: {
        airlines: 'Multiple airlines',
      },
      segment: {
        duration: "{{hours}}h {{minutes}}'",
      },
      cabinClass: {
        tourist: 'Economy',
        premiumEconomy: 'Premium economy',
        first: 'First',
        economicDiscounted: 'Economy',
        business: 'Business',
      },
    },
    resultsPage: {
      noResults: {
        modal: {
          title: "Sorry, we can't find any flights for that search",
          text:
            'Why not try flying to a nearby airport or adjusting your dates?',
          cta: 'New Search',
        },
      },
    },
    trip: {
      seats: 'seats',
      baggage: 'baggage',
      bookingNumber: 'Booking number',
      stop: {
        airportChange: 'Changing airports is your responsibility',
        terminalChange: 'Change of terminal',
        airlineChange: 'Change of airline',
        missedConnection: 'Connection covered by Vueling',
        technical: 'Technical stopover',
        duration: 'Stop duration',
      },
      segment: {
        name: {
          nomultiple1: 'Return',
          nomultiple0: 'Departure',
        },
      },
    },
    summaryPage: {
      header: 'header',
      paymentInfo: {
        cta: 'cta',
        priceBreakdown: {
          numberPax: 'Test Num Pax String',
        },
      },
    },
    confirmationPage: {
      summary: {
        header: 'Booking change request confirmed!',
      },
    },
  };
  const getContentKey = (translations, key) =>
    key
      .split(/:|\./)
      .reduce((result = {}, fragment) => result[fragment], translations) || key;

  const replaceArgs = (key = '', args = {}) =>
    Object.entries(args).reduce(
      (result, [key, value]) => result.replace(`{{${key}}}`, value),
      key
    );

  const t = (key, args) => replaceArgs(getContentKey(KEYS, key), args);

  return {
    default: { t },
    useTranslation: () => ({ t }),
    t,
    initReactI18next: () => {},
  };
});
