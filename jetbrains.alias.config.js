const path = require('path');

module.exports = {
  resolve: {
    alias: {
      '@': path.resolve(__dirname),
      utils: path.resolve(__dirname, './src/utils'),
      decorators: path.resolve(__dirname, './.storybook/decorators'),
      serverMocks: path.resolve(__dirname, './src/serverMocks'),
      themes: path.resolve(__dirname, './src/themes'),
      'prisma-design-system': path.resolve(__dirname, './src/prisma_rexport'),
      '@prisma-design-system': path.resolve(
        __dirname,
        './node_modules/prisma-design-system'
      ),
      react: path.resolve('./node_modules/react'),
      i18next: path.resolve('./node_modules/i18next'),
      'react-i18next': path.resolve('./node_modules/react-i18next'),
      'emotion-theming': path.resolve('./node_modules/emotion-theming'),
      '@emotion/styled': path.resolve('./node_modules/@emotion/styled'),
      '@emotion/styled-base': path.resolve(
        './node_modules/@emotion/styled-base'
      ),
      '@styled-system/css': path.resolve('./node_modules/@styled-system/css'),
      'date-fns/locale': path.resolve('./node_modules/date-fns/locale'),
      '@frontend-react-component-builder/conchita': path.resolve(
        './node_modules/frontend-react-component-builder/dist/conchita'
      ),
      '@frontend-react-component-builder/debug': path.resolve(
        './node_modules/frontend-react-component-builder/dist/debug'
      ),
    },
  },
};
