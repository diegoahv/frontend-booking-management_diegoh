const path = require('path');

module.exports = (env) => {
  const { paths } = env;

  return {
    devtool: 'nosources-source-map',
    entry: path.resolve(paths.root, 'src/export/index.js'),
    optimization: {
      usedExports: true,
    },
    output: {
      publicPath: '/travel/secure/',
    },
  };
};
