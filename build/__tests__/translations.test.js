import fs from 'fs';

const sitesConfig = require('@frontend-shell/react-hooks/dist/sites.json');

const getTranslationsPath = ({ brand, locale }) =>
  `./assets/translations/${brand}/${locale}.json`;

describe('assets/translations', () => {
  describe('check translation files exist', () => {
    sitesConfig.forEach(({ locales = [], brandCode } = {}) => {
      locales.forEach((locale) => {
        it(`/${brandCode}/${locale}.json`, () => {
          expect(
            fs.existsSync(getTranslationsPath({ brand: brandCode, locale }))
          ).toBe(true);
          const data = fs.readFileSync(
            getTranslationsPath({ brand: brandCode, locale }),
            'utf8'
          );
          expect(data.trim().length).not.toBe(0);
        });
      });
    });
  });
});
