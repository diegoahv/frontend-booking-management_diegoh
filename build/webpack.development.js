const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');

module.exports = ({ app = 'menu', ...env }) => {
  const { paths } = env;
  return {
    devtool: 'eval-source-map',
    entry: [
      'core-js/modules/es.promise',
      'core-js/modules/es.array.iterator',
      path.resolve(paths.root, `src/start.${app}.js`),
    ],
    module: {
      rules: [
        {
          test: /\.html$/,
          use: [
            {
              loader: 'html-loader',
            },
          ],
        },
      ],
    },
    plugins: [
      new HtmlWebPackPlugin({
        template: path.resolve(__dirname, '../src/index.html'),
      }),
    ],
  };
};
