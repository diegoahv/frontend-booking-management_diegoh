const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const minifyJson = require('node-json-minify');

const isProdBuild = (mode) => mode === 'production';

module.exports = (env = { mode: 'development' }) => {
  const { paths } = env;
  return {
    plugins: [
      new CopyPlugin({
        patterns: [
          {
            from: path.resolve(paths.assets, 'translations'),
            to: path.resolve(paths.dist, 'translations'),
            ...(isProdBuild(env.mode) && {
              transform: function (content) {
                return minifyJson(content.toString());
              },
            }),
          },
        ],
      }),
    ],
  };
};
