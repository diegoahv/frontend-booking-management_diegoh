const WebpackBundleAnalyzer = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin;

module.exports = (env) => ({
  optimization: {
    concatenateModules: false,
  },
  plugins: [new WebpackBundleAnalyzer()],
});
