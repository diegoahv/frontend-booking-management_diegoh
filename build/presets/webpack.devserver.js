module.exports = (env) => ({
  devServer: {
    contentBase: [env.paths.assets, env.paths.dist],
    compress: true,
    port: 9000,
    open: true,
    historyApiFallback: true,
    proxy: {
      '/images': {
        target: 'http://www.edreams.com',
        changeOrigin: true,
      },
    },
  },
});
