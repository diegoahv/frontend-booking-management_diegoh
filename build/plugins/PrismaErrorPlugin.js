module.exports = class PrismaErrorPlugin {
  apply(compiler) {
    const appPrismaImports = getAllPrismaImports(compiler);
    const prismaExports = getPrismaRexports(compiler);

    comparePrismaImportsAndExports(compiler, appPrismaImports, prismaExports);
  }
};

function getAllPrismaImports(compiler) {
  const imports = new Set();
  compiler.hooks.normalModuleFactory.tap('PrismaErrorPlugin', (factory) => {
    factory.hooks.parser
      .for('javascript/auto')
      .tap('PrismaErrorPlugin', (parser, options) => {
        parser.hooks.importSpecifier.tap(
          'PrismaErrorPlugin',
          (_, source, exportName) => {
            if (source === 'prisma-design-system' && exportName !== 'default') {
              imports.add(exportName);
            }
          }
        );
      });
  });

  return imports;
}

function getPrismaRexports(compiler) {
  const exports = {};

  compiler.hooks.afterCompile.tap('PrismaErrorPlugin', (compilation) => {
    compilation.modules.forEach((m) => {
      if (m.request && m.request.includes('prisma_rexport')) {
        m.buildMeta.providedExports.forEach((namedExport) => {
          exports[namedExport] = true;
        });
      }
    });
  });

  return exports;
}

function comparePrismaImportsAndExports(
  compiler,
  prismaImports,
  prismaExports
) {
  compiler.hooks.afterCompile.tap('PrismaErrorPlugin', (compilation) => {
    const missingExports = [];
    for (let fileImport of prismaImports) {
      if (!prismaExports[fileImport]) {
        missingExports.push(fileImport);
      }
    }

    if (missingExports.length > 0) {
      logErrorAndThrow(
        `-------ADD MISSING COMPONENTS TO PRISMA REXPORT ON SRC/PRISMA_REXPORT, MISSING COMPONENTS: ${missingExports}-------`
      );
    }
  });
}

function logErrorAndThrow(message) {
  console.error('\x1b[31m', message);

  throw new Error('See explanation above');
}
