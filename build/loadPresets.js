const webpackMerge = require('webpack-merge');

const flat = (a) => [].concat.apply([], a);

module.exports = loadPresets;

function loadPresets(env = { presets: [] }) {
  const presets = flat([env.presets || []]);
  const configs = presets.map((presetName) =>
    require(`./presets/webpack.${presetName}`)(env)
  );

  return webpackMerge({}, ...configs);
}
