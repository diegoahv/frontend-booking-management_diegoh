import { create } from '@storybook/theming/create';
import logo from './edo.png';

export default create({
  base: 'light',

  brandTitle: 'eDreams | MMB',
  brandUrl: 'https://www.edreams.es',
  brandImage: logo,
});
