import { addons } from '@storybook/addons';
import theme from './theme/mmb.theme';

addons.setConfig({
  theme,
});
