const proxy = require('http-proxy-middleware');

module.exports = function expressMiddleware(router) {
  router.use(
    '/images',
    proxy({
      target: 'https://edreams.com',
      changeOrigin: true,
    })
  );
};
