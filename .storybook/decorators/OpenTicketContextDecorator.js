import { useReducer } from 'react';

import OpenTicketContext from '@/src/export/OpenTicket/OpenTicketContext';
import reducer, { initState } from '@/src/export/OpenTicket/reducer';

export function OpenTicketContextDecorator(story) {
  const [state, dispatch] = useReducer(reducer, {}, initState);

  return (
    <OpenTicketContext.Provider value={[state, dispatch]}>
      {story()}
    </OpenTicketContext.Provider>
  );
}
