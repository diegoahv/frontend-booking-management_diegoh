import { select } from '@storybook/addon-knobs';
import * as carriers from 'serverMocks/Carrier';

const options = Object.values(carriers).reduce((options, Carrier) => {
  const carrier = Carrier();
  options[carrier.name] = carrier;
  return options;
}, {});

const selectCarrier = (
  label = 'Carrier',
  defaultValue = carriers.AirEuropa().name
) => select(label, options, options[defaultValue]);

export default selectCarrier;
