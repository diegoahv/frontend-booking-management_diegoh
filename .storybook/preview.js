/*
 * File for the preview application
 * you can add global decorators and parameters here
 * */

import { addParameters, configure } from '@storybook/react';
import { addDecorator } from '@storybook/react';

import { Mocks, DebugAssistant } from '@frontend-react-component-builder/debug';
import { initTranslations } from '@frontend-react-component-builder/conchita';
const translations = require('../assets/translations/ED/es-ES.json');

const position = (item, array = []) => {
  const index = array.indexOf(item);
  // Show unsorted items at the bottom.
  return index === -1 ? 10000 : index;
};

addParameters({
  options: {
    showRoots: true,
    storySort: (a, b) => {
      // Add group and story names to the sort order to explicitly order them.
      // Items that are not included in the list are shown below the sorted items.
      const sortOrder = {
        Common: ['Page', 'Header', 'BottomDrawer', 'Loading Spinner', 'Icons'],
        MMB: [],
        OpenTicket: [
          'Pages',
          'Search',
          'Results',
          'Trip',
          'Summary',
          'Confirmation',
        ],
        Playground: ['Basics'],
      };
      const groups = Object.keys(sortOrder);
      const aKind = a[1].kind;
      const bKind = b[1].kind;
      const [aGroup, aComponent] = aKind.split('/');
      const [bGroup, bComponent] = bKind.split('/');

      // Preserve story sort order.
      if (aKind === bKind) {
        return 0;
      }

      // Sort stories in a group.
      if (aGroup === bGroup) {
        const group = sortOrder[aGroup];

        return position(aComponent, group) - position(bComponent, group);
      }

      // Sort groups.
      return position(aGroup, groups) - position(bGroup, groups);
    },
  },
});

Mocks.mockTranslations(translations);

initTranslations().then(() => {
  configure(
    [
      require.context('../src/', true, /\.stories\.js$/),
      require.context(
        '../config/prisma/',
        true,
        /src\/([^\/]+\/)*([^\.]+)\.stories\.js$/
      ),
    ],
    module
  );

  addDecorator(DebugAssistant.assistantDecorator);
});
