const merge = require('webpack-merge');
const webpackConfig = require('../webpack.config');

module.exports = {
  addons: [
    '@storybook/addon-actions',
    '@storybook/addon-links',
    '@storybook/addon-knobs/register',
    '@storybook/addon-storysource',
  ],
  webpackFinal: async (config, { configType }) => {
    // do mutation to the config
    const ownConfig = webpackConfig({ mode: configType.toLowerCase() });

    return merge(
      { module: ownConfig.module, resolve: ownConfig.resolve },
      config
    );
  },
};
