import { initState } from '@/src/export/OpenTicket/reducer';

export function mockOpenTicketContext(initValue) {
  const state = initState();
  return mergeDeep(state, initValue);
}

function mergeDeep(target, ...sources) {
  if (!sources.length) return target;
  const source = sources.shift();
  if (isObject(target) && isObject(source)) {
    for (const key in source) {
      if (source.hasOwnProperty(key)) {
        if (isObject(source[key])) {
          if (!target[key]) Object.assign(target, { [key]: {} });
          mergeDeep(target[key], source[key]);
        } else {
          Object.assign(target, { [key]: source[key] });
        }
      }
    }
  }
  return mergeDeep(target, ...sources);
}

function isObject(obj) {
  return obj !== null && !Array.isArray(obj) && typeof obj === 'object';
}
